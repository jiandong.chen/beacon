#include "../../include/beacon/datatype/visual_feature_data.h"

namespace beacon {

/* ##################### VisualFeatureData ################################## */
/* ########################################################################## */
VisualFeatureData::Ptr VisualFeatureData::create() {
    return std::make_shared<VisualFeatureData>();
}

/* ########################################################################## */
VisualFeatureData::Ptr VisualFeatureData::create(
    const std::vector<cv::KeyPoint>& cvkp) {
    Ptr p = VisualFeatureData::create();

    p->points.resize(cvkp.size());
    std::transform(cvkp.cbegin(),
                   cvkp.cend(),
                   p->points.begin(),
                   [](const cv::KeyPoint& kp) {
                       VisualFeaturePoint pt;
                       pt.uv = {static_cast<float>(kp.pt.x),
                                static_cast<float>(kp.pt.y)};
                       return pt;
                   });

    return p;
}

}  // namespace beacon
