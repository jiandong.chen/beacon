#include "../../include/beacon/datatype/visual_match_data.h"

namespace beacon {

/* ##################### VisualFeatureMatchData ############################# */
/* ########################################################################## */
VisualFeatureMatchData::Ptr VisualFeatureMatchData::create() {
    return std::make_shared<VisualFeatureMatchData>();
}

/* ########################################################################## */
void VisualFeatureMatchData::fillPoints() {
    if (auto p0 = pdata0.lock(), p1 = pdata1.lock();
        p0 && p0->feature && p1 && p1->feature) {
        // if we have data and data.feature
        // we can create point0 and point1
        point0.clear();
        point0.reserve(matches.size());
        point1.clear();
        point1.reserve(matches.size());
        for (std::size_t i = 0; i < matches.size(); ++i) {
            const auto& [idx0, idx1, score, label] = matches[i];
            point0.emplace_back(p0->feature->points.at(idx0));
            point1.emplace_back(p1->feature->points.at(idx1));
        }
    } else {
        // we dont have the data we need
        return;
    }
}

/* ########################################################################## */
void VisualFeatureMatchData::fillPoints(const ImageData& data0,
                                        const ImageData& data1) {
    if (!data0.feature || !data1.feature) {
        // we dont have the data we need
        throw std::runtime_error(
            "VisualFeatureMatchData::fillPoints Get data.feature nullptr!");

    } else {
        point0.clear();
        point0.reserve(matches.size());
        point1.clear();
        point1.reserve(matches.size());
        for (std::size_t i = 0; i < matches.size(); ++i) {
            const auto& [idx0, idx1, score, label] = matches[i];
            point0.emplace_back(data0.feature->points.at(idx0));
            point1.emplace_back(data1.feature->points.at(idx1));
        }
    }
}

}  // namespace beacon
