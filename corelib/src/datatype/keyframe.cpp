#include "../../include/beacon/datatype/keyframe.h"

#include "../../include/beacon/datatype/image_data.h"

namespace beacon {

/* ########################## keyframe ###################################### */
/* ########################################################################## */
Keyframe::Keyframe(const ImageData& pdata)
    : KeyframeBase{pdata.T_wc},
      m_feature{pdata.feature},
      m_camera{pdata.camera} {
}

}  // namespace beacon
