#include "../../include/beacon/datatype/image_data.h"

namespace beacon {

/* ##################### ImageData ########################################## */
/* ########################################################################## */
ImageData::ImageData() = default;

/* ########################################################################## */
ImageData::ImageData(const cv::Mat& img,
                     Timestamp time,
                     const CameraPtr& cam,
                     Transform T)
    : timestamp{time}, image{img}, camera{cam}, T_wc{T} {
}

/* ########################################################################## */
BEACON_NODISCARD cv::Mat ImageData::getGrayImage(const ImageData& data,
                                                 const bool use_buffer) {
    if (use_buffer && !data.image_gray_buffer.empty()) {
        return data.image_gray_buffer;
    }
    cv::Mat res;
    cv::cvtColor(data.image, res, cv::COLOR_BGR2GRAY);
    if (use_buffer) {
        data.image_gray_buffer = res;
    }
    return res;
}

/* ########################################################################## */
BEACON_NODISCARD cv::Mat ImageData::getGrayImage(const bool use_buffer) const {
    return ImageData::getGrayImage(*this, use_buffer);
}

/* ########################################################################## */
ImageData::Ptr ImageData::create() {
    return std::make_shared<ImageData>();
}

/* ########################################################################## */
ImageData::Ptr ImageData::create(cv::Mat img,
                                 Timestamp timestamp,
                                 const ImageData::CameraPtr& cam,
                                 Transform T) {
    return std::make_shared<ImageData>(img, timestamp, cam, T);
}

}  // namespace beacon
