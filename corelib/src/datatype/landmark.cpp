#include "../../include/beacon/datatype/landmark.h"

#include "../../include/beacon/datatype/image_data.h"

namespace beacon {

/* ########################## Landmark ###################################### */
/* ########################################################################## */
LandmarkInverseDepth& LandmarkInverseDepth::setRefFrame(
    const ImagePtr& p_frame,
    const std::size_t feature_idx) {
    if (!p_frame->feature || feature_idx >= p_frame->feature->points.size()) {
        throw std::runtime_error(
            "LandmarkInverseDepth setRefFrame: Ref Frame is without valid "
            "feature!");
    }

    m_ref_frame_and_feature_idx = std::make_pair(p_frame, feature_idx);

    return *this;
}

}  // namespace beacon
