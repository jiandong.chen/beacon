#include "../../include/beacon/sensor/stereo_pinhole.h"

#include <opencv4/opencv2/opencv.hpp>

#include "../../include/beacon/datatype/image_data.h"
#include "../../include/beacon/util/cv_helper.h"

namespace beacon {

/* ##################### StereoPinhole ###################################### */
/* ########################################################################## */
void StereoPinhole::undistortRectifyImagePair(ImageData& img_left,
                                              ImageData& img_right) const {
    cv::Mat res;
    if (!img_left.image.empty()) {
        cv::remap(img_left.image,
                  res,
                  m_left_remap1,
                  m_left_remap2,
                  cv::INTER_LINEAR);
        img_left.image = res;
    }
    if (!img_right.image.empty()) {
        cv::remap(img_right.image,
                  res,
                  m_right_remap1,
                  m_right_remap2,
                  cv::INTER_LINEAR);
        img_right.image = res;
    }
}

/* ########################################################################## */
bool StereoPinhole::setCamPair(const CameraPtr& left,
                               const CameraPtr& right,
                               const int target_img_width,
                               const int target_img_height) {
    m_left = left;
    m_right = right;

    // validate the cameras
    if (!isValidPair()) {
        throw std::runtime_error(
            "StereoPinhole: Get nullptr when setting cameras!");
    }
    if (m_left->getImageWidth() != m_right->getImageWidth() ||
        m_left->getImageHeight() != m_right->getImageHeight()) {
        throw std::runtime_error(
            "StereoPinhole: Get cameras with different image size!");
    }

    // update transform
    const auto T_lr =
        m_left->getTransformBC().getInverse() * m_right->getTransformBC();
    const auto R_lr = buildCVRotationFromEigen(T_lr.getRotationMatrix());
    const auto t_lr = buildCVTranslationFromEigen(T_lr.getTranslation());

    // do stereo rectify
    const auto left_K = left->getCVK();
    const auto left_dist = left->getCVDistortion();
    const auto right_K = right->getCVK();
    const auto right_dist = right->getCVDistortion();
    const auto inputsize =
        cv::Size{left->getImageWidth(), left->getImageHeight()};
    const auto target_size =
        (target_img_width == 0 || target_img_height == 0)
            ? inputsize
            : cv::Size{target_img_width, target_img_height};

    std::cout << inputsize << std::endl;
    std::cout << target_size << std::endl;

    cv::stereoRectify(left_K,
                      left_dist,
                      right_K,
                      right_dist,
                      inputsize,
                      R_lr,
                      t_lr,
                      m_left_R_ur,
                      m_right_R_ur,
                      m_left_P_ur,
                      m_right_P_ur,
                      m_Q,
                      cv::CALIB_ZERO_DISPARITY,
                      0,
                      target_size,
                      &m_left_roi,
                      &m_right_roi);

    std::cout << m_left_P_ur << std::endl;

    cv::initUndistortRectifyMap(left_K,
                                left_dist,
                                m_left_R_ur,
                                m_left_P_ur,
                                target_size,
                                CV_32FC1,
                                m_left_remap1,
                                m_left_remap2);

    cv::initUndistortRectifyMap(right_K,
                                right_dist,
                                m_right_R_ur,
                                m_right_P_ur,
                                target_size,
                                CV_32FC1,
                                m_right_remap1,
                                m_right_remap2);

    return true;
}

/* ########################################################################## */

}  // namespace beacon
