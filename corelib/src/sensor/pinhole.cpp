#include "../../include/beacon/sensor/pinhole.h"

namespace beacon {

/* ##################### Pinhole ############################################ */
/* ########################################################################## */
void Pinhole::undistortKeypoint(VisualFeatureData& data) const {
    const auto fx = m_fx;
    const auto fy = m_fy;

    const auto cx = m_cx;
    const auto cy = m_cy;

    const auto s = m_s;

    const auto p1 = m_p1;
    const auto p2 = m_p2;

    const auto k1 = m_k1;
    const auto k2 = m_k2;
    const auto k3 = m_k3;

    auto distortKeypoint = [&](const Eigen::Vector2d& pt) {
        // distort pt on normalize point to pixel pt

        const auto x = pt.x();
        const auto y = pt.y();

        const auto xy = x * y;
        const auto xx = x * x;
        const auto yy = y * y;

        const auto r2 = xx + yy;
        const auto r4 = r2 * r2;
        const auto r6 = r2 * r4;
        const auto g = (1 + k1 * r2 + k2 * r4 + k3 * r6);

        const auto dx = 2 * p1 * xy + p2 * (r2 + 2 * xx);
        const auto dy = 2 * p2 * xy + p1 * (r2 + 2 * yy);

        const auto res_x = g * x + dx;
        const auto res_y = g * y + dy;

        return Eigen::Vector2d{fx * res_x + s * res_y + cx, fy * res_y + cy};
    };

    auto undistortAlgo = [&](VisualFeaturePoint& pt) {
        const auto distort_y = (1.0 / fy) * (pt.uv.y() - cy);
        const auto distort_x = (1.0 / fx) * (pt.uv.x() - cx - s * distort_y);
        const auto distort_pt = Eigen::Vector2d{distort_x, distort_y};

        constexpr const int max_iter = 10;
        constexpr const double eps = 1e-9;
        auto res = distort_pt;
        double last_err = 0.0;
        for (int it = 0; it < max_iter; ++it) {
            last_err = (distortKeypoint(res) - pt.uv).norm();
            if (last_err < eps) {
                break;
            }

            const auto x = res.x();
            const auto y = res.y();

            const auto xy = x * y;
            const auto xx = x * x;
            const auto yy = y * y;

            const auto r2 = xx + yy;
            const auto r4 = r2 * r2;
            const auto r6 = r2 * r4;
            const auto g = (1 + k1 * r2 + k2 * r4 + k3 * r6);

            const auto dx = 2 * p1 * xy + p2 * (r2 + 2 * xx);
            const auto dy = 2 * p2 * xy + p1 * (r2 + 2 * yy);

            res = (distort_pt - Eigen::Vector2d{dx, dy}) / g;
        }
        if (last_err > eps) {
            throw std::runtime_error("Pinhole Undistort Get High Error: " +
                                     std::to_string(last_err));
        }
        pt.xy = res;
    };

    std::for_each(data.points.begin(), data.points.end(), undistortAlgo);
}

/* ########################################################################## */
void Pinhole::undistortImage(ImageData&) const {
    // do nothing
    return;
}

/* ########################################################################## */
Pinhole::Ptr Pinhole::create() {
    return std::make_shared<Pinhole>();
}

/* ########################################################################## */
Pinhole::Ptr Pinhole::create(double fx,
                             double fy,
                             double cx,
                             double cy,
                             double s) {
    return std::make_shared<Pinhole>(fx, fy, cx, cy, s);
}

}  // namespace beacon
