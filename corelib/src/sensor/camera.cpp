#include "../../include/beacon/sensor/camera.h"

#include <opencv4/opencv2/core/eigen.hpp>

namespace beacon {

/* ##################### Camera ############################################# */
/* ########################################################################## */
cv::Mat Camera::getCVIdentityK() {
    return cv::Mat::eye(3, 3, CV_64F);
}

/* ########################################################################## */
BEACON_NODISCARD cv::Mat Camera::getCVK() const {
    // return (cv::Mat1d(3, 3) << m_fx, m_s, m_cx, 0, m_fy, m_cy, 0, 0, 1);
    return (cv::Mat1d(3, 3) << m_fx, 0, m_cx, 0, m_fy, m_cy, 0, 0, 1);
}

/* ########################################################################## */
BEACON_NODISCARD cv::Mat Camera::getCVDistortion() const {
    constexpr int total_coeff_num = 5;
    cv::Mat res = cv::Mat::zeros(1, total_coeff_num, CV_64F);

    res.at<double>(0) = m_k1;
    res.at<double>(1) = m_k2;
    res.at<double>(2) = m_p1;
    res.at<double>(3) = m_p2;

    res.at<double>(4) = m_k3;

    return res;
}

/* ########################################################################## */
Eigen::Matrix3d Camera::getIntrinsicEigen() const {
    Eigen::Matrix3d res = Eigen::Matrix3d ::Identity();

    res(0, 0) = m_fx;
    // res(0, 1) = m_s;
    res(0, 2) = m_cx;

    // res(1, 0) = 0;
    res(1, 1) = m_fy;
    res(1, 2) = m_cy;

    // res(2, 0) = 0;
    // res(2, 1) = 0;
    // res(2, 2) = 1;

    return res;
}

/* ########################################################################## */
Eigen::Matrix<double, 3, 4> Camera::getProjMatEigen(
    const Transform& T_bw) const {
    Eigen::Matrix3d intrinsic = Eigen::Matrix3d ::Identity();

    auto T_cw = m_T_bc.getInverse() * T_bw;

    // same as getIntrinsicEigen()
    intrinsic(0, 0) = m_fx;
    // intrinsic(0, 1) = m_s;
    intrinsic(0, 2) = m_cx;

    intrinsic(1, 1) = m_fy;
    intrinsic(1, 2) = m_cy;

    return intrinsic * T_cw.getTransformMat34();
}

/* ########################################################################## */
Camera& Camera::setDistortion(double k1,
                              double k2,
                              double p1,
                              double p2,
                              double k3) {
    std::vector tmp{k1, k2, p1, p2, k3};
    return setDistortion(tmp.begin(), tmp.end());
}

/* ########################################################################## */
void Camera::print(std::ostream& os) const {
    os << "ImageHeight: " << m_image_height << " ImageWidth: " << m_image_width
       << " ";

    os << "fx: " << m_fx << " fy: " << m_fy << " cx: " << m_cx
       << " cy: " << m_cy << " s: " << m_s;

    os << " k1: " << m_k1;
    os << " k2: " << m_k2;
    os << " p1: " << m_p1;
    os << " p2: " << m_p2;

    os << " k3: " << m_k3;

    os << " ";
}

/* ########################################################################## */
std::ostream& operator<<(std::ostream& os, const Camera& cam) {
    cam.print(os);
    return os;
}

// PinholeCamera

}  // namespace beacon
