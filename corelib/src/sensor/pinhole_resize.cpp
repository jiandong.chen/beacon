#include "../../include/beacon/sensor/pinhole_resize.h"

#include <opencv4/opencv2/opencv.hpp>

#include "../../include/beacon/datatype/image_data.h"

namespace beacon {

/* ##################### PinholeResize ###################################### */
/* ########################################################################## */
void PinholeResize::undistortKeypoint(VisualFeatureData& data) const {
    auto uv = data.uv2CV<float>();

    decltype(uv) xy;
    cv::undistortPoints(uv, xy, getCVK(), getCVDistortion());

    for (std::size_t i = 0; i < xy.size(); ++i) {
        data.points[i].xy.x() = xy[i].x;
        data.points[i].xy.y() = xy[i].y;
    }
}

/* ########################################################################## */
void PinholeResize::undistortImage(ImageData& data) const {
    cv::Mat res;
    cv::undistort(data.image,
                  res,
                  Camera::getCVK(),
                  Camera::getCVDistortion(),
                  this->getCVK());
    data.image = cv::Mat(res, cv::Rect{0, 0, m_image_width, m_image_height});
}

/* ########################################################################## */
void PinholeResize::calcNewIntrinsic() {
    auto K = Camera::getCVK();
    auto dist = Camera::getCVDistortion();
    auto img_sz = cv::Size{m_ori_img_width, m_ori_img_height};
    auto new_img_sz = cv::Size{m_image_width, m_image_height};

    cv::Rect roi;

    auto new_K = cv::getOptimalNewCameraMatrix(K,
                                               dist,
                                               img_sz,
                                               1,
                                               new_img_sz,
                                               &roi);  // double

    m_new_fx = new_K.at<double>(0, 0);
    m_new_fy = new_K.at<double>(1, 1);

    m_new_cx = new_K.at<double>(0, 2);
    m_new_cy = new_K.at<double>(1, 2);

    m_roi = std::move(roi);
}

/* ########################################################################## */
cv::Mat PinholeResize::getCVK() const {
    return (cv::Mat1d(3, 3) << m_new_fx,
            0,
            m_new_cx,
            0,
            m_new_fy,
            m_new_cy,
            0,
            0,
            1);
}

/* ########################################################################## */
BEACON_NODISCARD cv::Mat PinholeResize::getCVDistortion() const {
    // return cv::Mat::zeros(1, 5, CV_64F);
    return {};
}

/* ########################################################################## */
Eigen::Matrix3d PinholeResize::getIntrinsicEigen() const {
    Eigen::Matrix3d res = Eigen::Matrix3d ::Identity();

    res(0, 0) = m_new_fx;
    // res(0, 1) = m_s;
    res(0, 2) = m_new_cx;

    // res(1, 0) = 0;
    res(1, 1) = m_new_fy;
    res(1, 2) = m_new_cy;

    // res(2, 0) = 0;
    // res(2, 1) = 0;
    // res(2, 2) = 1;

    return res;
}

/* ########################################################################## */
Eigen::Matrix<double, 3, 4> PinholeResize::getProjMatEigen(
    const Transform& T_bw) const {
    Eigen::Matrix3d intrinsic = Eigen::Matrix3d ::Identity();

    auto T_cw = m_T_bc.getInverse() * T_bw;

    // same as getIntrinsicEigen()
    intrinsic(0, 0) = m_new_fx;
    // intrinsic(0, 1) = m_s;
    intrinsic(0, 2) = m_new_cx;

    intrinsic(1, 1) = m_new_fy;
    intrinsic(1, 2) = m_new_cy;

    return intrinsic * T_cw.getTransformMat34();
}

/* ########################################################################## */
PinholeResize::PinholeResize(int old_img_width,
                             int old_img_height,
                             int new_img_width,
                             int new_img_height,
                             double fx,
                             double fy,
                             double cx,
                             double cy,
                             double s)
    : m_ori_img_height{old_img_height}, m_ori_img_width{old_img_width} {
    setImageSize(new_img_width, new_img_height);

    m_fx = fx;
    m_fy = fy;
    m_cx = cx;
    m_cy = cy;
    m_s = s;

    m_new_fx = fx;
    m_new_fy = fy;

    m_new_cx = cx;
    m_new_cy = cy;

    calcNewIntrinsic();
}

/* ########################################################################## */
PinholeResize::PinholeResize(int old_img_width,
                             int old_img_height,
                             int new_img_width,
                             int new_img_height)
    : PinholeResize{old_img_width,
                    old_img_height,
                    new_img_width,
                    new_img_height,
                    1.0,
                    1.0,
                    0.0,
                    0.0,
                    0.0} {
    calcNewIntrinsic();
}

/* ########################################################################## */
PinholeResize::PinholeResize(const Camera& cam,
                             int new_img_width,
                             int new_img_height)
    : PinholeResize(cam.getImageWidth(),
                    cam.getImageHeight(),
                    new_img_width,
                    new_img_height,
                    cam.getFx(),
                    cam.getFy(),
                    cam.getCx(),
                    cam.getCy(),
                    cam.getS()) {
}

/* ########################################################################## */
BEACON_NODISCARD PinholeResize::Ptr PinholeResize::create(int old_img_width,
                                                          int old_img_height,
                                                          int new_img_width,
                                                          int new_img_height) {
    return std::make_shared<PinholeResize>(old_img_width,
                                           old_img_height,
                                           new_img_width,
                                           new_img_height);
}

/* ########################################################################## */
BEACON_NODISCARD PinholeResize::Ptr PinholeResize::create(int old_img_width,
                                                          int old_img_height,
                                                          int new_img_width,
                                                          int new_img_height,
                                                          double fx,
                                                          double fy,
                                                          double cx,
                                                          double cy,
                                                          double s) {
    return std::make_shared<PinholeResize>(old_img_width,
                                           old_img_height,
                                           new_img_width,
                                           new_img_height,
                                           fx,
                                           fy,
                                           cx,
                                           cy,
                                           s);
}

/* ########################################################################## */
void PinholeResize::print(std::ostream& os) const {
    Camera::print(os);

    os << "Resize From Image Size [h, w]: " << m_ori_img_height << " "
       << m_ori_img_width << " To Size [h, w]: " << m_image_height << " "
       << m_image_width << " ";
    os << "new_fx: " << m_new_fx << " new_fy: " << m_new_fy
       << " new_cx: " << m_new_cx << " new_cy: " << m_new_cy;

    os << " ";
}

}  // namespace beacon
