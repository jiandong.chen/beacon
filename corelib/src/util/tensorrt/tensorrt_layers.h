#pragma once
#include "NvInfer.h"
#include "tensor_manager.h"
#include "weight_manager.h"

namespace beacon::tensorrt {

nvinfer1::IShuffleLayer* createBatchNorm1D(
    nvinfer1::INetworkDefinition& network,
    nvinfer1::ITensor& input,
    const std::string& layer_prefix,
    const float eps,
    WeightManager& weights);

nvinfer1::IShuffleLayer* createConv1D(nvinfer1::INetworkDefinition& network,
                                      nvinfer1::ITensor& input,
                                      const int output_channel,
                                      const int kernel_size,
                                      const int stride,
                                      const int padding,
                                      const int dilation,
                                      const std::string& layer_prefix,
                                      WeightManager& weights);

}  // namespace beacon::tensorrt
