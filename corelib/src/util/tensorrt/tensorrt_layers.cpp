#include "tensorrt_layers.h"

#include <cmath>

#include "tensorrt_util.h"

namespace beacon::tensorrt {

nvinfer1::IShuffleLayer* createBatchNorm1D(
    nvinfer1::INetworkDefinition& network,
    nvinfer1::ITensor& input,
    const std::string& layer_prefix,
    const float eps,
    WeightManager& weights) {
    // ref:
    // https://github.com/wang-xinyu/tensorrtx/blob/47baebce6089b5a6ae1d6bacb7589bee8f619cd7/ufld/common.hpp#L68
    // ref:
    // https://github.com/NVIDIA-AI-IOT/torch2trt/blob/master/torch2trt/converters/BatchNorm1d.py

    // build BatchNorm1D Layer using nvinfer1::IScaleLayer

    static std::size_t count = 0;
    ++count;

    // get data from weights
    const float* gamma = reinterpret_cast<const float*>(
        weights[layer_prefix + ".weight"].values);
    const float* beta =
        reinterpret_cast<const float*>(weights[layer_prefix + ".bias"].values);
    const float* mean = reinterpret_cast<const float*>(
        weights[layer_prefix + ".running_mean"].values);
    const float* var = reinterpret_cast<const float*>(
        weights[layer_prefix + ".running_var"].values);
    const auto len = weights[layer_prefix + ".running_var"].count;

    // compute scale value
    auto& scval_w =
        weights.createWeight(layer_prefix + ".scale" + std::to_string(count),
                             len,
                             4,
                             nvinfer1::DataType::kFLOAT);
    auto scval = (float*)(scval_w.values);
    for (int i = 0; i < len; i++) {
        scval[i] = gamma[i] / std::sqrt(var[i] + eps);
    }

    // compute shift value
    auto& shval_w =
        weights.createWeight(layer_prefix + ".shift" + std::to_string(count),
                             len,
                             4,
                             nvinfer1::DataType::kFLOAT);
    auto shval = (float*)(shval_w.values);
    for (int i = 0; i < len; i++) {
        shval[i] = beta[i] - mean[i] * scval[i];
    }

    // compute power value
    auto& pval_w =
        weights.createWeight(layer_prefix + ".power" + std::to_string(count),
                             len,
                             4,
                             nvinfer1::DataType::kFLOAT);
    auto pval = (float*)(pval_w.values);
    for (int i = 0; i < len; i++) {
        pval[i] = 1.0;
    }

    // we need to reshape [batch, channel, xx]
    nvinfer1::IShuffleLayer* reshape_layer = network.addShuffle(input);
    reshape_layer->setReshapeDimensions(
        {4, {0, 0, 0, 1}});  // get [batch, channel, xx, 1]
    reshape_layer->setName(
        (layer_prefix + ".BatchNorm1DReshape" + std::to_string(count)).c_str());

    // perform BatchNorm
    nvinfer1::IScaleLayer* scale_1 =
        network.addScale(*reshape_layer->getOutput(0),
                         nvinfer1::ScaleMode::kCHANNEL,
                         shval_w,
                         scval_w,
                         pval_w);
    scale_1->setName(
        (layer_prefix + ".BatchNorm1D" + std::to_string(count)).c_str());

    // reshape back
    auto reshape_back_layer = network.addShuffle(*scale_1->getOutput(0));
    reshape_back_layer->setReshapeDimensions(
        {3, {0, 0, 0}});  // get [batch, channel, xx]
    reshape_back_layer->setName(
        (layer_prefix + ".BatchNorm1DReshapeBack" + std::to_string(count))
            .c_str());

    return reshape_back_layer;
}

nvinfer1::IShuffleLayer* createConv1D(nvinfer1::INetworkDefinition& network,
                                      nvinfer1::ITensor& input,
                                      const int output_channel,
                                      const int kernel_size,
                                      const int stride,
                                      const int padding,
                                      const int dilation,
                                      const std::string& layer_prefix,
                                      WeightManager& weights) {
    // ref:
    // https://github.com/NVIDIA-AI-IOT/torch2trt/blob/master/torch2trt/converters/Conv1d.py

    static std::size_t count = 0;
    ++count;
    auto new_prefix = layer_prefix + ".Conv1D" + std::to_string(count);

    auto reshape_input = unsqueezeLast(network, input, new_prefix, weights);

    // load weights and bias
    const auto& weight = weights[layer_prefix + ".weight"];
    const auto& bias = weights[layer_prefix + ".bias"];

    nvinfer1::Dims new_kernel_size{2, {kernel_size, 1}};
    nvinfer1::Dims new_stride{2, {stride, 1}};
    nvinfer1::Dims new_padding{2, {padding, 0}};
    nvinfer1::Dims new_dilation{2, {dilation, 1}};

    auto conv2d = network.addConvolutionNd(*reshape_input->getOutput(0),
                                           output_channel,
                                           new_kernel_size,
                                           weight,
                                           bias);

    conv2d->setName((new_prefix).c_str());
    conv2d->setStrideNd(new_stride);
    conv2d->setPaddingNd(new_padding);
    conv2d->setDilationNd(new_dilation);

    auto res_layer =
        removeSucceedingDim(network, *conv2d->getOutput(0), 1, new_prefix);

    return res_layer;
}

}  // namespace beacon::tensorrt
