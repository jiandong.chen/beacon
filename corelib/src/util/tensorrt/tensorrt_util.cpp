#include "tensorrt_util.h"

#include <iostream>

#include "tensor_manager.h"

namespace beacon::tensorrt {

nvinfer1::IShuffleLayer* removeDim(nvinfer1::INetworkDefinition& network,
                                   nvinfer1::ITensor& tensor,
                                   const int start,
                                   const int num_removed,
                                   const std::string& layer_prefix) {
    static std::size_t count = 0;
    ++count;

    auto shape_layer = network.addShape(tensor);
    shape_layer->setName(
        (layer_prefix + ".removeDimShape" + std::to_string(count)).c_str());

    auto total_dim = shape_layer->getOutput(0)->getDimensions().d[0];
    auto new_shape_layer = network.addSlice(*shape_layer->getOutput(0),
                                            {1, {start}},
                                            {1, {total_dim - num_removed}},
                                            {1, {1}});
    new_shape_layer->setName(
        (layer_prefix + ".removeDimSlice" + std::to_string(count)).c_str());

    auto res_layer = network.addShuffle(tensor);
    res_layer->setInput(1, *new_shape_layer->getOutput(0));
    res_layer->setName(
        (layer_prefix + ".removeDimShuffle" + std::to_string(count)).c_str());

    return res_layer;
}

nvinfer1::IShuffleLayer* removePrecedingDim(
    nvinfer1::INetworkDefinition& network,
    nvinfer1::ITensor& tensor,
    const int num_removed,
    const std::string& layer_prefix) {
    // usually used to remvoe preceding '1'

    return removeDim(network, tensor, num_removed, num_removed, layer_prefix);
}

nvinfer1::IShuffleLayer* removeSucceedingDim(
    nvinfer1::INetworkDefinition& network,
    nvinfer1::ITensor& tensor,
    const int num_removed,
    const std::string& layer_prefix) {
    return removeDim(network, tensor, 0, num_removed, layer_prefix);
}

nvinfer1::IShuffleLayer* unsqueezeLast(nvinfer1::INetworkDefinition& network,
                                       nvinfer1::ITensor& input,
                                       const std::string& layer_prefix,
                                       WeightManager& weights) {
    static std::size_t count = 0;
    ++count;

    // the shape we add
    auto& w = weights.getOnes(1);
    auto layer_1 = network.addConstant({1, {1}}, w);
    layer_1->setName(
        (layer_prefix + ".UnsqueezeLastConstant" + std::to_string(count))
            .c_str());

    // ori shape
    auto shape_layer = network.addShape(input);
    shape_layer->setName(
        (layer_prefix + ".UnsqueezeLastShape" + std::to_string(count)).c_str());

    // add them by Concatenation
    static TensorManager tmp_data;
    auto& arr =
        tmp_data.get(layer_prefix + ".UnsqueezeLast" + std::to_string(count));
    arr.push_back(shape_layer->getOutput(0));
    arr.push_back(layer_1->getOutput(0));
    auto concat_layer = network.addConcatenation(arr.data(), arr.size());
    concat_layer->setName(
        (layer_prefix + ".UnsqueezeLastConcatenate" + std::to_string(count))
            .c_str());

    // reshape
    auto res = network.addShuffle(input);
    res->setInput(1, *concat_layer->getOutput(0));
    res->setName(
        (layer_prefix + ".UnsqueezeLastShuffle" + std::to_string(count))
            .c_str());

    return res;
}

}  // namespace beacon::tensorrt
