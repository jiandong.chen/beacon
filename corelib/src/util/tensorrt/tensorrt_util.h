#pragma once

#include "NvInfer.h"
#include "weight_manager.h"

namespace beacon::tensorrt {

nvinfer1::IShuffleLayer* removeDim(nvinfer1::INetworkDefinition& network,
                                   nvinfer1::ITensor& tensor,
                                   const int start,
                                   const int num_removed,
                                   const std::string& layer_prefix);

nvinfer1::IShuffleLayer* removePrecedingDim(
    nvinfer1::INetworkDefinition& network,
    nvinfer1::ITensor& tensor,
    const int num_removed,
    const std::string& layer_prefix);

nvinfer1::IShuffleLayer* removeSucceedingDim(
    nvinfer1::INetworkDefinition& network,
    nvinfer1::ITensor& tensor,
    const int num_removed,
    const std::string& layer_prefix);

nvinfer1::IShuffleLayer* unsqueezeLast(nvinfer1::INetworkDefinition& network,
                                       nvinfer1::ITensor& input,
                                       const std::string& layer_prefix,
                                       WeightManager& weights);

}  // namespace beacon::tensorrt
