#pragma once
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "NvInfer.h"

namespace beacon::tensorrt {

class TensorManager {
  public:
    using TensorVec = std::vector<nvinfer1::ITensor*>;

    TensorVec& get(const std::string& name) {
        if (m_data.count(name) != 0) {
            std::cout << "TensorManager Return existed data with name: " << name
                      << "\n";
        }
        return m_data[name];
    }

  protected:
    std::unordered_map<std::string, std::vector<nvinfer1::ITensor*>> m_data;

};  // class TensorManager

}  // namespace beacon::tensorrt
