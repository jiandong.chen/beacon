#pragma once
#include <filesystem>
#include <fstream>
#include <unordered_map>
#include <vector>

#include "NvInfer.h"

namespace beacon::tensorrt {

class WeightManager {
  public:
    ~WeightManager() {
        for (auto& [name, weight] : weight_data) {
            if (weight.values != nullptr) {
                free((void*)weight.values);
            }
            weight.count = 0;
            weight.values = nullptr;
        }
    }

    void readFromFile(const std::string& path) {
        if (!std::filesystem::exists(path)) {
            throw std::runtime_error("Weight File: " + path +
                                     " Do NOT exist!!!");
        }

        std::ifstream file{path};
        if (!file.is_open()) {
            throw std::runtime_error("Cannot Open Weights File: " + path +
                                     " !!");
        }

        std::size_t count = 0;
        file >> count;

        while (count--) {
            // load weight name and len
            std::string name;
            std::int32_t len;
            file >> name >> std::dec >> len;

            // load weights value
            // uint32_t == 4 bytes
            auto& w = createWeight(name, len, 4, nvinfer1::DataType::kFLOAT);
            auto p_data = (std::uint32_t*)(w.values);

            for (std::int32_t i = 0; i < len; ++i) {
                // the weights are saved as hex in script
                file >> std::hex >> p_data[i];
            }
        }
    }

    nvinfer1::Weights& createWeight(const std::string& name,
                                    std::size_t num_elem,
                                    std::int64_t byte_per_elem,
                                    nvinfer1::DataType data_type) {
        if (weight_data.count(name) != 0) {
            throw std::runtime_error(
                "WeightManager Create Weight using exist name: " + name);
        }

        nvinfer1::Weights w{data_type, nullptr, 0};
        auto p = malloc(num_elem * byte_per_elem);
        if (p) {
            w.values = p;
            w.count = num_elem;
        } else {
            throw std::runtime_error("WeightManager malloc Failed!");
        }
        weight_data[name] = w;

        return weight_data[name];
    }

    nvinfer1::Weights& getWeight(const std::string& name) {
        if (auto it = weight_data.find(name); it != weight_data.end()) {
            return it->second;
        } else {
            throw std::runtime_error("WeightReader Weight: " + name +
                                     " Not Found!");
        }
    }

    nvinfer1::Weights& operator[](const std::string& name) {
        return getWeight(name);
    }

    nvinfer1::Weights& getOnes(
        int num,
        nvinfer1::DataType type = nvinfer1::DataType::kINT32) {
        static std::string ones_prefix = "InternalOnes";
        std::string name = ones_prefix + std::to_string(num);
        if (auto it = weight_data.find(name); it != weight_data.end()) {
            return it->second;
        }
        auto& w = createWeight(name, num, 4, type);
        auto pw = (int*)w.values;
        for (int i = 0; i < num; ++i) {
            pw[i] = 1;
        }
        return w;
    }

  protected:
    std::unordered_map<std::string, nvinfer1::Weights> weight_data;

};  // class WeightManager

}  // namespace beacon::tensorrt
