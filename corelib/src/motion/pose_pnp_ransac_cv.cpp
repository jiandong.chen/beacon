#include "../../include/beacon/motion/pose_pnp_ransac_cv.h"

#include <opencv4/opencv2/opencv.hpp>

#include "../../include/beacon/util/cv_helper.h"

namespace beacon {

PoseFromMatchPnPRansacCV::ResultType PoseFromMatchPnPRansacCV::process(
    const VisualFeatureMatchDataPtr& match) {
    m_res = std::nullopt;
    m_status.clear();
    m_objpt.clear();
    m_imgpt.clear();

    // check data
    if (!match) {
        return m_res;
    }

    if (auto img0 = match->pdata0.lock(); img0 && !match->point1.empty()) {
        if (m_imgpt.empty() || m_objpt.empty()) {
            // extract all pair with obj and ima points if not exist yet
            for (std::size_t i = 0; i < match->matches.size(); ++i) {
                const auto& m = match->matches[i];
                const auto& pt0 = img0->feature->points[m.idx0];
                const auto& pt1 = match->point1[i];
                if (auto objpt0 = pt0.landmark.lock()) {
                    m_objpt.emplace_back(objpt0->get3DXYZ().x(),
                                         objpt0->get3DXYZ().y(),
                                         objpt0->get3DXYZ().z());
                    m_imgpt.emplace_back(pt1.xy2CV());
                }
            }
        }

        return calculate();
    }

    return m_res;
}

PoseFromMatchPnPRansacCV::ResultType PoseFromMatchPnPRansacCV::calculate() {
    cv::Mat rvec, tvec;
    cv::solvePnPRansac(m_objpt,
                       m_imgpt,
                       m_K,
                       cv::noArray(),
                       rvec,
                       tvec,
                       false,
                       100,
                       1.0,
                       0.99,
                       m_status);

    cv::Mat R;
    cv::Rodrigues(rvec, R);

    m_res = Transform{buildTranslationFromCVMat(tvec),
                      buildRotationQuatFromCVMat(R)};
    return m_res;
}

}  // namespace beacon
