#include "../../include/beacon/motion/pose_essential_mat_cv.h"

#include "../../include/beacon/util/cv_helper.h"

namespace beacon {

/* ##################### PoseFromMatchEssentialMatCV ######################## */
/* ########################################################################## */
void PoseFromMatchEssentialMatCV::findEssentialMat() {
    m_essential_mat = cv::Mat{};
    m_status.clear();

    // check data
    if (!m_input_data) {
        return;
    }
    if (m_input_data->point0.empty() || m_input_data->point1.empty() ||
        m_input_data->point0.size() != m_input_data->point1.size()) {
        return;
    }

    // convert to OpenCV data
    m_kpxy0 = PointUVXY::uv2CV<float>(m_input_data->point0.cbegin(),
                                      m_input_data->point0.cend());
    m_kpxy1 = PointUVXY::uv2CV<float>(m_input_data->point1.cbegin(),
                                      m_input_data->point1.cend());

    // use OpoenCV Utility
    m_essential_mat = cv::findEssentialMat(m_kpxy0,
                                           m_kpxy1,
                                           m_K,
                                           cv::RANSAC,
                                           0.999,
                                           1.0,
                                           1000,
                                           m_status);
}

/* ########################################################################## */
void PoseFromMatchEssentialMatCV::recoverPose() {
    if (m_essential_mat.empty() || m_status.empty()) {
        return;
    }

    // T_01
    cv::Mat R, t;  // Datatype: CV_64FC1
    cv::recoverPose(m_essential_mat, m_kpxy0, m_kpxy1, m_K, R, t, m_status);

    m_res =
        Transform{buildTranslationFromCVMat(t), buildRotationQuatFromCVMat(R)};
}

}  // namespace beacon
