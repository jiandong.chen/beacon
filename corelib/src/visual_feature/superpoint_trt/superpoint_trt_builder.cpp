#include "superpoint_trt_builder.h"

#include <filesystem>

#include "beacon/cuda/tensorrt_logger.h"

namespace beacon {

using namespace beacon::tensorrt;

SuperPointBuilder::SuperPointBuilder(const SuperPointTRTBuilderParameter& param)
    : m_param{param} {
}

SuperPointBuilder::~SuperPointBuilder() = default;

void SuperPointBuilder::buildEngine() {
    builder->setMaxBatchSize(batch_size);

    config->setMemoryPoolLimit(nvinfer1::MemoryPoolType::kWORKSPACE,
                               (1ull << 30));
    config->setFlag(nvinfer1::BuilderFlag::kGPU_FALLBACK);
    config->setProfilingVerbosity(nvinfer1::ProfilingVerbosity::kDETAILED);
    config->setBuilderOptimizationLevel(5);  // default: 3 max_level: 5
    if (m_param.m_enable_fp16) {
        config->setFlag(nvinfer1::BuilderFlag::kFP16);
    }
    if (m_param.m_enable_int8) {
        config->setFlag(nvinfer1::BuilderFlag::kINT8);
    }

    // dynamic shape
    auto opt_profile = builder->createOptimizationProfile();
    opt_profile->setDimensions(input_name,
                               nvinfer1::OptProfileSelector::kMIN,
                               nvinfer1::Dims4(batch_size,
                                               1,
                                               m_param.m_min_image_height,
                                               m_param.m_min_image_width));
    opt_profile->setDimensions(input_name,
                               nvinfer1::OptProfileSelector::kOPT,
                               nvinfer1::Dims4(batch_size,
                                               1,
                                               m_param.m_opt_image_height,
                                               m_param.m_opt_image_width));
    opt_profile->setDimensions(input_name,
                               nvinfer1::OptProfileSelector::kMAX,
                               nvinfer1::Dims4(batch_size,
                                               1,
                                               m_param.m_max_image_height,
                                               m_param.m_max_image_width));
    config->addOptimizationProfile(opt_profile);

    // generate model
    serilize_engine = std::unique_ptr<nvinfer1::IHostMemory>(
        builder->buildSerializedNetwork(*network, *config));
}

std::unique_ptr<nvinfer1::IHostMemory> SuperPointBuilder::buildFromFile() {
    const auto path = m_param.m_weight_path;
    if (!std::filesystem::exists(path)) {
        throw std::runtime_error(
            "The Weight File: " + path +
            " do NOT exist!!!!! SuperPoint TRT Engine cannot be built!!!");
    }

    // init
    logger = std::make_unique<BeaconTRTLogger>();
    builder = std::unique_ptr<nvinfer1::IBuilder>(
        nvinfer1::createInferBuilder(*logger));
    config = std::unique_ptr<nvinfer1::IBuilderConfig>(
        builder->createBuilderConfig());
    network =
        std::unique_ptr<nvinfer1::INetworkDefinition>(builder->createNetworkV2(
            1U << static_cast<uint32_t>(
                nvinfer1::NetworkDefinitionCreationFlag::kEXPLICIT_BATCH)));

    // load from weight file
    loadWeightFromFile();

    // build the engine
    buildEngineNetwork();
    buildEngine();

    return std::move(serilize_engine);
}

void SuperPointBuilder::buildEngineNetwork() {
#if 0  
    auto image = network->addInput(
        input_name, nvinfer1::DataType::kFLOAT,
        nvinfer1::Dims4{batch_size, 1, m_params.m_image_height,
                        m_params.m_image_width});
#endif
    auto image = network->addInput(input_name,
                                   nvinfer1::DataType::kFLOAT,
                                   nvinfer1::Dims4{batch_size, 1, -1, -1});
    image->setDimensionName(2, "image_height");
    image->setDimensionName(3, "image_width");

    static const auto dim11 = nvinfer1::DimsHW{1, 1};
    static const auto dim22 = nvinfer1::DimsHW{2, 2};
    static const auto dim33 = nvinfer1::DimsHW{3, 3};

    auto conv1a = network->addConvolutionNd(*image,
                                            64,
                                            dim33,
                                            weights["conv1a.weight"],
                                            weights["conv1a.bias"]);
    conv1a->setStrideNd(dim11);
    conv1a->setPaddingNd(dim11);
    conv1a->setName("conv1a");
    auto relu1 = network->addActivation(*conv1a->getOutput(0),
                                        nvinfer1::ActivationType::kRELU);
    relu1->setName("relu1");

    auto conv1b = network->addConvolutionNd(*relu1->getOutput(0),
                                            64,
                                            dim33,
                                            weights["conv1b.weight"],
                                            weights["conv1b.bias"]);
    conv1b->setStrideNd(dim11);
    conv1b->setPaddingNd(dim11);
    conv1b->setName("conv1b");
    auto relu2 = network->addActivation(*conv1b->getOutput(0),
                                        nvinfer1::ActivationType::kRELU);
    relu2->setName("relu2");

    auto pool1 = network->addPoolingNd(*relu2->getOutput(0),
                                       nvinfer1::PoolingType::kMAX,
                                       dim22);
    pool1->setStrideNd(dim22);
    pool1->setName("pool1");

    auto conv2a = network->addConvolutionNd(*pool1->getOutput(0),
                                            64,
                                            dim33,
                                            weights["conv2a.weight"],
                                            weights["conv2a.bias"]);
    conv2a->setStrideNd(dim11);
    conv2a->setPaddingNd(dim11);
    conv2a->setName("conv2a");
    auto relu3 = network->addActivation(*conv2a->getOutput(0),
                                        nvinfer1::ActivationType::kRELU);
    relu3->setName("relu3");

    auto conv2b = network->addConvolutionNd(*relu3->getOutput(0),
                                            64,
                                            dim33,
                                            weights["conv2b.weight"],
                                            weights["conv2b.bias"]);
    conv2b->setStrideNd(dim11);
    conv2b->setPaddingNd(dim11);
    conv2b->setName("conv2b");
    auto relu4 = network->addActivation(*conv2b->getOutput(0),
                                        nvinfer1::ActivationType::kRELU);
    relu4->setName("relu4");

    auto pool2 = network->addPoolingNd(*relu4->getOutput(0),
                                       nvinfer1::PoolingType::kMAX,
                                       dim22);
    pool2->setStrideNd(dim22);
    pool2->setName("pool2");

    auto conv3a = network->addConvolutionNd(*pool2->getOutput(0),
                                            128,
                                            dim33,
                                            weights["conv3a.weight"],
                                            weights["conv3a.bias"]);
    conv3a->setStrideNd(dim11);
    conv3a->setPaddingNd(dim11);
    conv3a->setName("conv3a");
    auto relu5 = network->addActivation(*conv3a->getOutput(0),
                                        nvinfer1::ActivationType::kRELU);
    relu5->setName("relu5");

    auto conv3b = network->addConvolutionNd(*relu5->getOutput(0),
                                            128,
                                            dim33,
                                            weights["conv3b.weight"],
                                            weights["conv3b.bias"]);
    conv3b->setStrideNd(dim11);
    conv3b->setPaddingNd(dim11);
    conv3b->setName("conv3b");
    auto relu6 = network->addActivation(*conv3b->getOutput(0),
                                        nvinfer1::ActivationType::kRELU);
    relu6->setName("relu6");

    auto pool3 = network->addPoolingNd(*relu6->getOutput(0),
                                       nvinfer1::PoolingType::kMAX,
                                       dim22);
    pool3->setStrideNd(dim22);
    pool3->setName("pool3");

    auto conv4a = network->addConvolutionNd(*pool3->getOutput(0),
                                            128,
                                            dim33,
                                            weights["conv4a.weight"],
                                            weights["conv4a.bias"]);
    conv4a->setStrideNd(dim11);
    conv4a->setPaddingNd(dim11);
    conv4a->setName("conv4a");
    auto relu7 = network->addActivation(*conv4a->getOutput(0),
                                        nvinfer1::ActivationType::kRELU);
    relu7->setName("relu7");

    auto conv4b = network->addConvolutionNd(*relu7->getOutput(0),
                                            128,
                                            dim33,
                                            weights["conv4b.weight"],
                                            weights["conv4b.bias"]);
    conv4b->setStrideNd(dim11);
    conv4b->setPaddingNd(dim11);
    conv4b->setName("conv4b");
    auto relu8 = network->addActivation(*conv4b->getOutput(0),
                                        nvinfer1::ActivationType::kRELU);
    relu8->setName("relu8");

    // #################### Keypoint
    // #############################################
    auto convPa = network->addConvolutionNd(*relu8->getOutput(0),
                                            256,
                                            dim33,
                                            weights["convPa.weight"],
                                            weights["convPa.bias"]);
    convPa->setStrideNd(dim11);
    convPa->setPaddingNd(dim11);
    convPa->setName("convPa");
    auto relu9 = network->addActivation(*convPa->getOutput(0),
                                        nvinfer1::ActivationType::kRELU);
    relu9->setName("relu9");

    auto convPb = network->addConvolutionNd(*relu9->getOutput(0),
                                            65,
                                            dim11,
                                            weights["convPb.weight"],
                                            weights["convPb.bias"]);
    convPb->setStrideNd(dim11);
    convPb->setName("convPb");

    auto softmax_convPb = network->addSoftMax(*convPb->getOutput(0));
    // softmax_convPb->setAxes(1<<1); // alone channel C (BCHW/NCHW)
    softmax_convPb->setName("soft_max_conPb");

    // #### new slice cut the dustbin
    auto kp_shape = network->addShape(*softmax_convPb->getOutput(0));
    kp_shape->setName("KeypointShape");

    nvinfer1::Dims dim2;
    dim2.nbDims = 1;
    dim2.d[0] = 2;
    nvinfer1::Dims dim1;
    dim1.nbDims = 1;
    dim1.d[0] = 1;
    auto kp_hw = network->addSlice(*kp_shape->getOutput(0),
                                   {1, {2}},
                                   {1, {2}},
                                   {1, {1}});
    kp_hw->setName("KeypointHeightWidth");

    nvinfer1::Weights w_64;
    w_64.count = tmp_w_64.size();
    w_64.type = nvinfer1::DataType::kINT32;
    w_64.values = tmp_w_64.data();
    auto const_64 = network->addConstant(dim2, w_64);  // [1,64]
    const_64->setName("Constant[1,64]");

    tmp_kp_64hw = {const_64->getOutput(0), kp_hw->getOutput(0)};
    auto kp_64hw =
        network->addConcatenation(tmp_kp_64hw.data(),
                                  tmp_kp_64hw.size());  // [1,64,kp_h,kp_w]
    kp_64hw->setName("Concatenation[1,64,kp_h,kp_w]");
    auto kp_slice = network->addSlice(*softmax_convPb->getOutput(0),
                                      {4, {0, 0, 0, 0}},
                                      {4, {}},
                                      {4, {1, 1, 1, 1}});
    kp_slice->setInput(2, *kp_64hw->getOutput(0));
    kp_slice->setName("Kp Slice");

    // #### new slice end

    // #### new shuffle 1

    nvinfer1::Weights w_88;
    w_88.count = tmp_w_88.size();
    w_88.type = nvinfer1::DataType::kINT32;
    w_88.values = tmp_w_88.data();
    auto const_88 = network->addConstant(dim2, w_88);  // [8, 8]
    const_88->setName("Constant[8, 8]");

    nvinfer1::Weights w_1;
    w_1.count = tmp_w_1.size();
    w_1.type = nvinfer1::DataType::kINT32;
    w_1.values = tmp_w_1.data();
    auto const_1 = network->addConstant(dim1, w_1);  // [1]
    const_1->setName("Constant[1]");

    tmp_kp_hw88 = {const_1->getOutput(0),
                   kp_hw->getOutput(0),
                   const_88->getOutput(0)};
    auto kp_hw88 =
        network->addConcatenation(tmp_kp_hw88.data(),
                                  tmp_kp_hw88.size());  // [1,kp_h,kp_w,8,8]
    kp_hw88->setName("Concatenation[1,kp_h,kp_w, 8, 8]");

    auto kp_shuffle_1 = network->addShuffle(*kp_slice->getOutput(0));
    kp_shuffle_1->setFirstTranspose({0, 2, 3, 1});
    kp_shuffle_1->setInput(1, *kp_hw88->getOutput(0));
    kp_shuffle_1->setName("KeypointShuffle_1");

    // #### new shuffl 1 end

    // #### new shuffl 2

    auto scale_kp_hw =
        network->addElementWise(*kp_hw->getOutput(0),
                                *const_88->getOutput(0),
                                nvinfer1::ElementWiseOperation::kPROD);
    scale_kp_hw->setName("ElementWise[8, 8]");

    tmp_scale_kp_shape = {const_1->getOutput(0), scale_kp_hw->getOutput(0)};
    auto scale_kp_shape = network->addConcatenation(
        tmp_scale_kp_shape.data(),
        tmp_scale_kp_shape.size());  // [1,kp_h*8,kp_w*8]
    scale_kp_shape->setName("Concatenation[1,kp_h*8,kp_w*8]");

    auto kp_shuffle_2 = network->addShuffle(*kp_shuffle_1->getOutput(0));
    kp_shuffle_2->setFirstTranspose({0, 1, 3, 2, 4});
    kp_shuffle_2->setInput(1, *scale_kp_shape->getOutput(0));
    kp_shuffle_2->setName("KeypointShuffle_2");

    // #### new shuffl 2 end

    // #################### Descriptor
    // #############################################
    auto convDa = network->addConvolutionNd(*relu8->getOutput(0),
                                            256,
                                            dim33,
                                            weights["convDa.weight"],
                                            weights["convDa.bias"]);
    convDa->setStrideNd(dim11);
    convDa->setPaddingNd(dim11);
    convDa->setName("convDa");
    auto relu10 = network->addActivation(*convDa->getOutput(0),
                                         nvinfer1::ActivationType::kRELU);
    relu10->setName("relu10");

    auto convDb = network->addConvolutionNd(*relu10->getOutput(0),
                                            256,
                                            dim11,
                                            weights["convDb.weight"],
                                            weights["convDb.bias"]);
    convDb->setStrideNd(dim11);
    convDb->setName("convDb");

    auto permute_convDb = network->addShuffle(*convDb->getOutput(0));
    permute_convDb->setFirstTranspose({0, 2, 3, 1});
    permute_convDb->setName("permute_convDb");

    // #################### Output #############################################
    kp_shuffle_2->getOutput(0)->setName(keypoint_map_name);
    network->markOutput(*kp_shuffle_2->getOutput(0));

    permute_convDb->getOutput(0)->setName(descriptor_name);
    network->markOutput(*permute_convDb->getOutput(0));
}

void SuperPointBuilder::loadWeightFromFile() {
    // Weight File are generated from scripts
    weights.readFromFile(m_param.m_weight_path);
}

}  // namespace beacon
