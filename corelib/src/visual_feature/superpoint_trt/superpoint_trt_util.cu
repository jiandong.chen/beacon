#include <beacon/cuda/cuda_buffer.h>
#include <beacon/cuda/cuda_util.h>
#include <thrust/device_vector.h>
#include <thrust/execution_policy.h>
#include <thrust/scan.h>
#include <thrust/sort.h>

#include "beacon/cuda/cuda_check.h"

namespace beacon::sp_trt_util {

[[maybe_unused]] static constexpr auto KEYPOINT_DIM = 3;  // x y score = float3
[[maybe_unused]] static constexpr auto WARP_SZ = 32;
[[maybe_unused]] static constexpr auto MAX_THREAD_PER_BLK = 256;
[[maybe_unused]] static constexpr auto FULL_MASK_WARP = 0xffffffff;

/* ##################### Keypoint compaction ################################ */
/* ########################################################################## */
static __device__ int pow2i(int b) {
    return 1 << b;
}

/* ########################################################################## */
template <typename DataType>
__global__ __launch_bounds__(MAX_THREAD_PER_BLK) void computeBlockCount(
    const DataType* image,
    const int height,
    const int width,
    const DataType threshold,
    const int window_radius,
    const int remove_border,
    char* mask,
    int* blk_count) {
    // coordinates on image
    const int global_x = blockDim.x * blockIdx.x + threadIdx.x;
    const int global_y = blockDim.y * blockIdx.y + threadIdx.y;
    const int global_idx = global_x + global_y * width;  // row major

    const auto withinImage = [height, width, remove_border] __device__(int x,
                                                                       int y) {
        int border = max(remove_border, 0);
        return x >= border && y >= border && x < width - border &&
               y < height - border;
    };

    bool is_keypoint = false;
    if (withinImage(global_x, global_y)) {
        const auto score = image[global_idx];
        is_keypoint = score > threshold ? true : false;

        // if score is higher than threshold
        // we test the neighbors
        for (int x = global_x - window_radius;
             x <= global_x + window_radius && is_keypoint;
             ++x) {
            for (int y = global_y - window_radius;
                 y <= global_y + window_radius && is_keypoint;
                 ++y) {
                if (!withinImage(x, y)) {
                    continue;
                }

                auto global_idx_tmp = x + y * width;
                if (image[global_idx_tmp] > score) {
                    // we only need to check if the score is maximum
                    // no need to know the max value
                    // so we dont need to update score
                    is_keypoint = false;
                }
            }
        }
        if (is_keypoint) {
            mask[global_idx] = 1;

        } else {
            mask[global_idx] = 0;
        }
    }

    // count the number of keypoints in the block
    auto count = __syncthreads_count(is_keypoint);
    if (threadIdx.x == 0) {
        blk_count[blockIdx.x + blockIdx.y * gridDim.x] = count;
    }
}

/* ########################################################################## */
template <typename DataType>
__global__ __launch_bounds__(MAX_THREAD_PER_BLK) void computeBlockCountShared(
    const DataType* image,
    const int height,
    const int width,
    const DataType threshold,
    const int window_radius,
    const int remove_border,
    char* mask,
    int* blk_count) {
    // shared memory version of computeBlockCount()
    extern __shared__ DataType shmem[];

    const auto withinImage = [height, width, remove_border] __device__(int x,
                                                                       int y) {
        int border = max(remove_border, 0);
        return x >= border && y >= border && x < width - border &&
               y < height - border;
    };

    // shape of shmem
    const int shmem_w = blockDim.x + 2 * window_radius;
    const int shmem_h = blockDim.y + 2 * window_radius;

    // compute offset by blk
    const int blk_offset_x = blockDim.x * blockIdx.x;
    const int blk_offset_y = blockDim.y * blockIdx.y;

    // fill the shmem
    for (int j = threadIdx.x; j < shmem_w; j += blockDim.x) {
        const int global_x = blk_offset_x + j - window_radius;

        for (int i = threadIdx.y; i < shmem_h; i += blockDim.y) {
            const int global_y = blk_offset_y + i - window_radius;

            if (withinImage(global_x, global_y)) {
                shmem[i * shmem_w + j] = image[global_y * width + global_x];
            } else {
                shmem[i * shmem_w + j] = 0;
            }
        }
    }

    __syncthreads();

    const int global_x = blk_offset_x + threadIdx.x;
    const int global_y = blk_offset_y + threadIdx.y;

    bool is_keypoint = false;
    if (withinImage(global_x, global_y)) {
        const auto score = shmem[(threadIdx.y + window_radius) * shmem_w +
                                 (threadIdx.x + window_radius)];
        is_keypoint = score > threshold ? true : false;

        // if score is higher than threshold
        // we test the neighbors
        for (int j = -window_radius; j <= window_radius && is_keypoint; ++j) {
            for (int i = -window_radius; i <= window_radius && is_keypoint;
                 ++i) {
                const auto val =
                    shmem[(i + threadIdx.y + window_radius) * shmem_w +
                          (j + threadIdx.x + window_radius)];

                if (val > score) {
                    // we only need to check if the score is maximum
                    // no need to know the max value
                    // so we dont need to update score
                    is_keypoint = false;
                }
            }
        }
        if (is_keypoint) {
            mask[global_x + global_y * width] = 1;

        } else {
            mask[global_x + global_y * width] = 0;
        }
    }

    // count the number of keypoints in the block
    auto count = __syncthreads_count(is_keypoint);
    if (threadIdx.x == 0) {
        blk_count[blockIdx.x + blockIdx.y * gridDim.x] = count;
    }
}

/* ########################################################################## */
template <typename DataType>
__global__ __launch_bounds__(MAX_THREAD_PER_BLK) void compactKeypoint(
    const DataType* image,
    const int height,
    const int width,
    const char* mask,
    const int* blk_offset,
    float3* res) {
    __shared__ int sh_warp_kp_count[MAX_THREAD_PER_BLK / WARP_SZ];

    const int global_x = blockDim.x * blockIdx.x + threadIdx.x;
    const int global_y = blockDim.y * blockIdx.y + threadIdx.y;
    const int global_idx = global_x + global_y * width;  // row major

    const auto withinImage = [height, width] __device__(int x, int y) {
        return x >= 0 && y >= 0 && x < width && y < height;
    };

    const int thread_idx_in_blk = threadIdx.x + threadIdx.y * blockDim.x;
    // warp idx in the blk
    const int warp_idx = thread_idx_in_blk / WARP_SZ;
    // thread idx in the warp
    const int thread_idx_in_warp = thread_idx_in_blk % WARP_SZ;

    const auto thread_mask = FULL_MASK_WARP >> (warpSize - thread_idx_in_warp);
    const bool is_keypoint =
        withinImage(global_x, global_y) ? mask[global_idx] : false;

    // exclusive sum for thread in warp
    // bits of return of __ballot_sync() is set to one if corresponding thread
    // is keypoint result is masked by thread_mask
    const int b = __ballot_sync(FULL_MASK_WARP, is_keypoint) & thread_mask;

    // num of kp before current idx in warp
    const int num_keypoint_warp = __popc(b);
    if (thread_idx_in_warp == warpSize - 1) {
        sh_warp_kp_count[warp_idx] = num_keypoint_warp + is_keypoint;
    }

    __syncthreads();

    // compute offset for each warp in blk
    // use first warp to do this
    const int num_warp = (blockDim.x * blockDim.y) / WARP_SZ;
    if (warp_idx == 0 && thread_idx_in_warp < num_warp) {
        // compute what thread in first warp we need to use
        const auto mask_first_warp = FULL_MASK_WARP >> (warpSize - num_warp);

        int offset = 0;
        // compute exclusive sum
        // 2^5 = 32 == warpSize
        for (int i = 0; i <= 5; ++i) {
            int tmp_b =
                __ballot_sync(mask_first_warp,
                              sh_warp_kp_count[thread_idx_in_warp] & pow2i(i));
            offset += (__popc(tmp_b & thread_mask) << i);
        }
        sh_warp_kp_count[thread_idx_in_warp] = offset;
    }

    __syncthreads();

    if (is_keypoint) {
        const auto idx = num_keypoint_warp + sh_warp_kp_count[warp_idx] +
                         blk_offset[blockIdx.x + blockIdx.y * gridDim.x];
        res[idx].x = global_x;
        res[idx].y = global_y;
        res[idx].z = image[global_idx];
    }
}

/* ########################################################################## */
void getKeypointFromScore(const float* d_score,
                          const int score_height,
                          const int score_width,
                          const float score_threshold,
                          const int max_kp_num,
                          const int window_radius,
                          const int remove_border,
                          cuda::DeviceBuffer<float3>& res,
                          cudaStream_t stream) {
    constexpr const int block_h = 16;
    constexpr const int block_w = 16;
    dim3 block_sz{block_w, block_h};

    dim3 grid_sz;
    grid_sz.x = cuda::computeGridSz1D(score_width, block_w);
    grid_sz.y = cuda::computeGridSz1D(score_height, block_h);

    const auto num_count_blk = grid_sz.x * grid_sz.y * grid_sz.z;
    const auto num_val = score_height * score_width;

    // step 0 prepare intermediate data
    static cuda::UnifiedBuffer<int> block_count;
    block_count.resize(num_count_blk);

    static cuda::DeviceBuffer<char> mask;
    mask.resize(num_val);
    BEACON_CUDA_CHECK(
        cudaMemsetAsync(mask.getData(), 0, num_val * sizeof(char), stream));

    static cuda::UnifiedBuffer<int> block_offset;
    block_offset.resize(block_count.getSize());

    static cuda::DeviceBuffer<float3> tmp_res;
    tmp_res.resize(num_val);

    // step 1 count the valid pt in each blk
    // compute the shared mem size
    const auto num_shmem_elem =
        (block_h + 2 * window_radius) * (block_w + 2 * window_radius);
    computeBlockCountShared<<<grid_sz,
                              block_sz,
                              sizeof(float) * num_shmem_elem,
                              stream>>>(d_score,
                                        score_height,
                                        score_width,
                                        score_threshold,
                                        window_radius,
                                        remove_border,
                                        mask.getData(),
                                        block_count.getData());

    // step 2 using exclusive prefix sum to compute the offset for each blk

    thrust::device_ptr<int> p_blk_count(block_count.getData());
    thrust::device_ptr<int> p_blk_offset(block_offset.getData());
    thrust::exclusive_scan(thrust::cuda::par_nosync.on(stream),
                           p_blk_count,
                           p_blk_count + block_count.getSize(),
                           p_blk_offset);

    // step 3 find all valid keypoint location
    compactKeypoint<<<grid_sz, block_sz, 0, stream>>>(d_score,
                                                      score_height,
                                                      score_width,
                                                      mask.getData(),
                                                      block_offset.getData(),
                                                      tmp_res.getData());

    BEACON_CUDA_CHECK(cudaStreamSynchronize(stream));

    int num_valid_data = block_offset.getData()[num_count_blk - 1] +
                         block_count.getData()[num_count_blk - 1];

    // sort the kps by score
    auto scoreDescend = [] __device__ __host__(float3 a, float3 b) {
        return a.z > b.z;
    };
    thrust::device_ptr<float3> p_res(tmp_res.getData());
    thrust::sort(thrust::cuda::par_nosync.on(stream),
                 p_res,
                 p_res + num_valid_data,
                 scoreDescend);

    // extract the result
    const auto num_kp = std::min(num_valid_data, max_kp_num);
    res.resize(num_kp);
    BEACON_CUDA_CHECK(cudaMemcpyAsync(res.getData(),
                                      tmp_res.getData(),
                                      num_kp * sizeof(float3),
                                      cudaMemcpyDeviceToDevice,
                                      stream));

    BEACON_CUDA_CHECK(cudaStreamSynchronize(stream));
}

/* ##################### Sample Descriptor ################################## */
/* ########################################################################## */
static __global__ __launch_bounds__(MAX_THREAD_PER_BLK) void sampleDescriptor(
    const float* d_desc,
    const int desc_h,
    const int desc_w,
    const int desc_c,
    const float3* kpsc,
    const int num_kp,
    float* desc) {
    // bilinear interpolation

    // defined by SuperPoint
    constexpr const int s = 8;

    const int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_kp) {
        const float x = (kpsc[idx].x - s / 2.0f + 0.5f) /
                        (desc_w * s - s / 2.0f - 0.5f) * (desc_w - 1.0f);
        const float y = (kpsc[idx].y - s / 2.0f + 0.5f) /
                        (desc_h * s - s / 2.0f - 0.5f) * (desc_h - 1.0f);

        const int lu_y = floorf(y);
        const int lu_x = floorf(x);

        const int lb_y = lu_y + 1;
        const int lb_x = lu_x;

        const int ru_y = lu_y;
        const int ru_x = lu_x + 1;

        const int rb_y = lu_y + 1;
        const int rb_x = lu_x + 1;

        const float lu_area = (y - lu_y) * (x - lu_x);
        const float lb_area = (lb_y - y) * (x - lb_x);
        const float ru_area = (y - ru_y) * (ru_x - x);
        const float rb_area = (rb_y - y) * (rb_x - x);

        const int y_offset = desc_c * desc_w;
        const int x_offset = desc_c;

        const auto withinDescriptor = [desc_h, desc_w] __device__(const int x,
                                                                  const int y) {
            return y >= 0 && x >= 0 && y < desc_h && x < desc_w;
        };

        // extract the desc
        const auto desc_out_start = desc + idx * desc_c;
        const auto extractDesc =
            [x_offset, y_offset, desc_c, d_desc, desc_out_start] __device__(
                const int x,
                const int y,
                const float area) {
                for (int c = 0; c < desc_c; ++c) {
                    desc_out_start[c] +=
                        d_desc[y * y_offset + x * x_offset + c] * area;
                }
            };

        if (withinDescriptor(lu_x, lu_y)) {
            extractDesc(lu_x, lu_y, lu_area);
        }
        if (withinDescriptor(lb_x, lb_y)) {
            extractDesc(lb_x, lb_y, lb_area);
        }
        if (withinDescriptor(ru_x, ru_y)) {
            extractDesc(ru_x, ru_y, ru_area);
        }
        if (withinDescriptor(rb_x, rb_y)) {
            extractDesc(rb_x, rb_y, rb_area);
        }

        // normalize the desc
        float square_sum = 0.0f;
        for (int c = 0; c < desc_c; ++c) {
            square_sum += desc_out_start[c] * desc_out_start[c];
        }
        square_sum = sqrtf(square_sum);
        for (int c = 0; c < desc_c; ++c) {
            desc_out_start[c] /= square_sum;
        }
    }
}

/* ########################################################################## */
void getDescriptorFromKeypoint(const float* d_desc_in,
                               const int desc_h,
                               const int desc_w,
                               const int desc_c,
                               const cuda::DeviceBuffer<float3>& kpsc,
                               cuda::DeviceBuffer<float>& desc_out,
                               cudaStream_t stream) {
    desc_out.resize(desc_c * kpsc.getSize());

    BEACON_CUDA_CHECK(cudaMemsetAsync(desc_out.getData(),
                                      0,
                                      kpsc.getSize() * desc_c * sizeof(float),
                                      stream));

    constexpr std::size_t blk_sz = 256;
    const int grid_sz = cuda::computeGridSz1D(kpsc.getSize(), blk_sz);
    sampleDescriptor<<<grid_sz, blk_sz, 0, stream>>>(d_desc_in,
                                                     desc_h,
                                                     desc_w,
                                                     desc_c,
                                                     kpsc.getData(),
                                                     kpsc.getSize(),
                                                     desc_out.getData());
    BEACON_CUDA_CHECK(cudaStreamSynchronize(stream));
}

}  // namespace beacon::sp_trt_util
