#include "superpoint_trt_util.h"

namespace beacon::sp_trt_util {

BEACON_NODISCARD static int rowMajorIdx(int row, int col, int width) {
    return row * width + col;
}

BEACON_NODISCARD std::vector<cv::Point3f> getKeypointByThreashold(
    const float* score,
    const int score_height,
    const int score_width,
    const float score_threshold,
    const int border_width) {
    std::vector<cv::Point3f> res;
    auto kps_begin = std::chrono::steady_clock::now();  // TODO timer
    for (int y = border_width; y < score_height - border_width; ++y) {
        for (int x = border_width; x < score_width - border_width; ++x) {
            int idx = rowMajorIdx(y, x, score_width);
            if (score[idx] > score_threshold) {
                res.emplace_back(x, y, score[idx]);
            }
        }
    }

    std::cout << "kps time: "
              << std::chrono::duration_cast<std::chrono::microseconds>(
                     std::chrono::steady_clock::now() - kps_begin)
                     .count()
              << " microseconds\n";  // todo timer
    // higher score preferred
    std::sort(res.begin(),
              res.end(),
              [](const cv::Point3f& pt1, const cv::Point3f& pt2) {
                  return pt1.z > pt2.z;
              });
    std::cout << "kps time: "
              << std::chrono::duration_cast<std::chrono::microseconds>(
                     std::chrono::steady_clock::now() - kps_begin)
                     .count()
              << " microseconds\n";  // todo timer

    return res;
}

void suppress(cv::Mat& grid,
              const int row_idx,
              const int col_idx,
              const int score_height,
              const int score_width,
              int suppress_width) {
    // val of grid
    //  0   --> unprocessed
    //  1   --> processed and max
    // -1   --> processed and non-max

    const int leftmost = std::max(0, col_idx - suppress_width);
    const int rightmost = std::min(score_width - 1, col_idx + suppress_width);
    const int topmost = std::max(0, row_idx - suppress_width);
    const int bottommost = std::min(score_height - 1, row_idx + suppress_width);
    for (int y = topmost; y <= bottommost; ++y) {
        for (int x = leftmost; x <= rightmost; ++x) {
            grid.at<int8_t>(y, x) = -1;
        }
    }
    grid.at<int8_t>(row_idx, col_idx) = 1;
}

BEACON_NODISCARD std::vector<cv::Point3f> fastNMS(
    const std::vector<cv::Point3f>& sorted_features,
    const int score_height,
    const int score_width,
    const int suppress_width,
    const std::size_t max_num_kps) {
    cv::Mat grid = cv::Mat::zeros(score_height, score_width, CV_8S);

    std::vector<cv::Point3f> suppress_kps;
    suppress_kps.reserve(max_num_kps);

    for (auto& kp : sorted_features) {
        if (grid.at<int8_t>(kp.y, kp.x) == 0) {
            suppress(grid,
                     kp.y,
                     kp.x,
                     score_height,
                     score_width,
                     suppress_width);
            suppress_kps.push_back(kp);
        }
        if (suppress_kps.size() >= max_num_kps) {
            break;
        }
    }
    return suppress_kps;
}

BEACON_NODISCARD cv::Mat sampleDescriptor(const std::vector<cv::Point3f>& kps,
                                          const float* desc,
                                          const int desc_h,
                                          const int desc_w,
                                          const int desc_c) {
    // https://github.com/magicleap/SuperGluePretrainedNetwork/blob/ddcf11f42e7e0732a0c4607648f9448ea8d73590/models/superpoint.py#L80

    if (kps.size() == 0) {
        return {};
    }

    constexpr const int s = 8;
    constexpr const int desc_len = 256;
    const int kps_sz = kps.size();
    const int y_offset = desc_c * desc_w;
    const int x_offset = desc_c;

    auto res = cv::Mat(kps_sz,
                       desc_len,
                       CV_32F);  // TODO shape and datatype double check

    const auto withinDescriptor = [&](const int y, const int x) {
        return y >= 0 && x >= 0 && y < desc_h && x < desc_w;
    };

    // grid sample bilinear
    for (int i = 0; i < kps_sz; ++i) {
        // normalize to (0, 1) then to (0, desc_height) and (0, desc_width)
        const float y = ((float)kps[i].y - s / 2.0f + 0.5f) /
                        ((float)desc_h * s - s / 2.0 - 0.5f) *
                        ((float)desc_h - 1.0f);
        const float x = ((float)kps[i].x - s / 2.0f + 0.5f) /
                        ((float)desc_w * s - s / 2.0f - 0.5f) *
                        ((float)desc_w - 1.0f);

        // l == left
        // r == right
        // u == up
        // b == bottom
        const int lu_y = std::floor(y);
        const int lu_x = std::floor(x);

        const int lb_y = lu_y + 1;
        const int lb_x = lu_x;

        const int ru_y = lu_y;
        const int ru_x = lu_x + 1;

        const int rb_y = lu_y + 1;
        const int rb_x = lu_x + 1;

        // calculate percentages of area of lu lb ru rb square
        const float lu_area = (y - lu_y) * (x - lu_x);
        const float lb_area = (lb_y - y) * (x - lb_x);
        const float ru_area = (y - ru_y) * (ru_x - x);
        const float rb_area = (rb_y - y) * (rb_x - x);

        // apply bilinear interpolation
        float tmp_square_sum = 0.0f;
        for (int c = 0; c < desc_c; ++c) {
            float tmp = 0.0f;
            if (withinDescriptor(lu_y, lu_x)) {
                tmp += desc[lu_y * y_offset + lu_x * x_offset + c] * rb_area;
            }
            if (withinDescriptor(lb_y, lb_x)) {
                tmp += desc[lb_y * y_offset + lb_x * x_offset + c] * ru_area;
            }
            if (withinDescriptor(ru_y, ru_x)) {
                tmp += desc[ru_y * y_offset + ru_x * x_offset + c] * lb_area;
            }
            if (withinDescriptor(rb_y, rb_x)) {
                tmp += desc[rb_y * y_offset + rb_x * x_offset + c] * lu_area;
            }
            res.at<float>(i, c) = tmp;
            tmp_square_sum += tmp * tmp;
        }

        // apply normalization
        res.row(i) /= std::sqrt(tmp_square_sum);
    }
    return res;
}

}  // namespace beacon::sp_trt_util
