#pragma once
#include <array>
#include <memory>

#include "../../util/tensorrt/weight_manager.h"
#include "NvInfer.h"
#include "beacon/visual_feature/superpoint_trt.h"

namespace beacon {

using namespace beacon::tensorrt;

struct SuperPointTRTBuilderParameter {
    ADD_VISUAL_FEATURE_PARAM(enable_int8, bool, false, EnableINT8)
    ADD_VISUAL_FEATURE_PARAM(enable_fp16, bool, true, EnableFP16)
    ADD_VISUAL_FEATURE_PARAM(image_height, int, 480, ImageHeight)
    ADD_VISUAL_FEATURE_PARAM(image_width, int, 640, ImageWidth)

    ADD_VISUAL_FEATURE_PARAM(min_image_height, int, 120, MinImageHeight)
    ADD_VISUAL_FEATURE_PARAM(min_image_width, int, 160, MinImageWidth)
    ADD_VISUAL_FEATURE_PARAM(opt_image_height, int, 480, OptImageHeight)
    ADD_VISUAL_FEATURE_PARAM(opt_image_width, int, 640, OptImageWidth)
    ADD_VISUAL_FEATURE_PARAM(max_image_height, int, 1080, MaxImageHeight)
    ADD_VISUAL_FEATURE_PARAM(max_image_width, int, 1920, MaxImageWidth)

    std::string m_weight_path{};

    auto setWeightPath(const std::string& path) {
        m_weight_path = path;
    }

};  // struct SuperPointTRTBuilderParameter

class SuperPointBuilder {
  public:
    SuperPointBuilder(const SuperPointTRTBuilderParameter& param);
    ~SuperPointBuilder();

    std::unique_ptr<nvinfer1::IHostMemory> buildFromFile();

  public:
    static constexpr auto batch_size{1};  // TODO set from config ?
    static constexpr auto input_name{"Image"};
    static constexpr auto keypoint_map_name{"Score"};
    static constexpr auto descriptor_name{"Desc"};

  protected:
    void buildEngine();
    void buildEngineNetwork();
    void loadWeightFromFile();

  protected:
    std::unique_ptr<nvinfer1::ILogger> logger{};
    std::unique_ptr<nvinfer1::IBuilder> builder{};
    std::unique_ptr<nvinfer1::IBuilderConfig> config{};
    std::unique_ptr<nvinfer1::INetworkDefinition> network{};
    std::unique_ptr<nvinfer1::IHostMemory> serilize_engine{};

  protected:
    // tmp data used by builder
    WeightManager weights{};
    std::array<int, 1> tmp_w_1{1};
    std::array<int, 2> tmp_w_64{1, 64};
    std::array<int, 2> tmp_w_88{8, 8};
    std::array<nvinfer1::ITensor*, 2> tmp_kp_64hw;
    std::array<nvinfer1::ITensor*, 3> tmp_kp_hw88;
    std::array<nvinfer1::ITensor*, 2> tmp_scale_kp_shape;

    SuperPointTRTBuilderParameter m_param{};

};  // struct SuperPointBuilder

}  // namespace beacon
