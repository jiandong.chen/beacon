#pragma once
#include "../../../include/beacon/visual_feature/superpoint_trt.h"
#include "superpoint_trt_impl.h"

namespace beacon::sp_trt_util {

/* ##################### SuperPoint Util CPU ################################ */
/* ########################################################################## */
BEACON_NODISCARD std::vector<cv::Point3f> getKeypointByThreashold(
    const float* score,
    const int score_height,
    const int score_width,
    const float score_threshold,
    const int border_width);

/* ########################################################################## */
void suppress(cv::Mat& grid,
              const int row_idx,
              const int col_idx,
              const int score_height,
              const int score_width,
              int suppress_width);

/* ########################################################################## */
BEACON_NODISCARD std::vector<cv::Point3f> fastNMS(
    const std::vector<cv::Point3f>& sorted_features,
    const int score_height,
    const int score_width,
    const int suppress_width,
    const std::size_t max_num_kps);

/* ########################################################################## */
BEACON_NODISCARD cv::Mat sampleDescriptor(const std::vector<cv::Point3f>& kps,
                                          const float* desc,
                                          const int desc_h,
                                          const int desc_w,
                                          const int desc_c);

/* ##################### Sample Descriptor GPU ############################## */
/* ########################################################################## */
void getDescriptorFromKeypoint(const float* d_desc_in,
                               const int desc_h,
                               const int desc_w,
                               const int desc_c,
                               const cuda::DeviceBuffer<float3>& kpsc,
                               cuda::DeviceBuffer<float>& desc_out,
                               cudaStream_t stream = 0);

/* ##################### Keypoint Compaction GPU ############################ */
/* ########################################################################## */
void getKeypointFromScore(const float* d_score,
                          const int score_height,
                          const int score_width,
                          const float score_threshold,
                          const int max_kp_num,
                          const int window_sz,
                          const int remove_border,
                          cuda::DeviceBuffer<float3>& res_buf,
                          cudaStream_t stream = 0);

}  // namespace beacon::sp_trt_util
