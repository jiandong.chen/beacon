#include "superpoint_trt_impl.h"

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <memory>
#include <numeric>

#include "beacon/cuda/cuda_check.h"
#include "beacon/cuda/tensorrt_logger.h"
#include "beacon/datatype/visual_feature_data.h"
#include "superpoint_trt_util.h"

using namespace beacon::sp_trt_util;
using namespace beacon::tensorrt;

namespace beacon {

/* ##################### SuperPointTRT Impl ################################# */
/* ########################################################################## */
SuperPointTRT::Impl::Impl(const SuperPointTRTParameter& params) {
    m_param = params;

    BEACON_CUDA_CHECK(cudaStreamCreate(&m_stream));

    reload();

    m_context->setOptimizationProfileAsync(0, m_stream);
}

/* ########################################################################## */
SuperPointTRT::Impl::~Impl() {
    BEACON_CUDA_CHECK(cudaStreamDestroy(m_stream));
}

/* ########################################################################## */
void SuperPointTRT::Impl::process(ImageData& data) {
    const auto grey_img = data.getGrayImage();
    grey_img.convertTo(m_input_buffer, CV_32FC1, 1.0f / 255.0f);

    checkAndReshapeIO();

    // copy data to device
    m_image_buffer.copy(m_input_buffer, m_stream);

    // compute!
    BEACON_TENSORRT_CHECK(m_context->enqueueV3(m_stream));

    // compute the keypoint score and location
    const auto score_height = m_context->getTensorShape(score_name).d[1];
    const auto score_width = m_context->getTensorShape(score_name).d[2];
    auto sp_trt_res = std::make_shared<SuperPointTRTResult>();
    getKeypointFromScore(m_keypoint_map_buffer.getDeviceData(),
                         score_height,
                         score_width,
                         m_param.m_keypoint_threshold,
                         m_param.m_max_keypoint,
                         m_param.m_nms_radius,
                         m_param.m_remove_borders,
                         sp_trt_res->kpsc,
                         m_stream);

    // compute the descriptor
    const auto desc_height = m_context->getTensorShape(descriptor_name).d[1];
    const auto desc_width = m_context->getTensorShape(descriptor_name).d[2];
    const auto desc_channel = m_context->getTensorShape(descriptor_name).d[3];
    getDescriptorFromKeypoint(m_descriptor_buffer.getDeviceData(),
                              desc_height,
                              desc_width,
                              desc_channel,
                              sp_trt_res->kpsc,
                              sp_trt_res->desc,
                              m_stream);

    // move the kp location to host
    // fill the host feature data
    const auto num_kps = sp_trt_res->kpsc.getSize();
    auto kp_host_data = std::make_unique<float3[]>(num_kps);
    BEACON_CUDA_CHECK(cudaMemcpyAsync(kp_host_data.get(),
                                      sp_trt_res->kpsc.getData(),
                                      num_kps * sizeof(float3),
                                      cudaMemcpyDeviceToHost,
                                      m_stream));
    BEACON_CUDA_CHECK(cudaStreamSynchronize(m_stream));

    data.feature = VisualFeatureData::create();
    data.feature->points.resize(num_kps);
    std::transform(kp_host_data.get(),
                   kp_host_data.get() + num_kps,
                   data.feature->points.begin(),
                   [](const float3& pt) {
                       VisualFeaturePoint res;
                       res.uv = {pt.x, pt.y};
                       return res;
                   });

    // fill the rest of the SuperPointTRTResult
    sp_trt_res->image_height = m_input_buffer.rows;
    sp_trt_res->image_width = m_input_buffer.cols;
    sp_trt_res->num_kps = num_kps;
    sp_trt_res->state = SuperPointTRTResult::State::SuperPoint;

    data.feature->device_data = sp_trt_res;
}

/* ########################################################################## */
void SuperPointTRT::Impl::reload() {
    if (!std::filesystem::exists(m_param.m_engine_path)) {
        //  if no engine file, we build the engine and save it
        throw std::runtime_error("The SuperPoint Engine File: " +
                                 m_param.m_engine_path + " do NOT exist!!!!! ");
    }

    // init tensorrt components
    m_logger = std::make_unique<BeaconTRTLogger>();
    m_rumtime = std::unique_ptr<nvinfer1::IRuntime>(
        nvinfer1::createInferRuntime(*m_logger));

    // load engine from file
    std::ifstream file(m_param.m_engine_path, std::ios::binary);
    auto file_size = std::filesystem::file_size(m_param.m_engine_path);
    std::vector<char> data(file_size);
    file.read(data.data(), file_size);
    m_engine = std::unique_ptr<nvinfer1::ICudaEngine>(
        m_rumtime->deserializeCudaEngine(data.data(), file_size));
    m_context = std::unique_ptr<nvinfer1::IExecutionContext>(
        m_engine->createExecutionContext());
}

/* ########################################################################## */
void SuperPointTRT::Impl::checkAndReshapeIO() {
    const auto image_h = m_context->getTensorShape(input_image_name).d[2];
    const auto image_w = m_context->getTensorShape(input_image_name).d[3];

    if (image_h == m_input_buffer.rows && image_w == m_input_buffer.cols) {
        // if the dynamic shape set
        return;
    }

    // init image buffer
    BEACON_TENSORRT_CHECK(m_context->setInputShape(
        input_image_name,
        nvinfer1::Dims{
            4,
            {m_batch_size, 1, m_input_buffer.rows, m_input_buffer.cols}}));
    m_image_buffer.resize(m_input_buffer.rows * m_input_buffer.cols);
    BEACON_TENSORRT_CHECK(
        m_context->setTensorAddress(input_image_name,
                                    m_image_buffer.getDeviceData()));

    // init keypoint map buffer
    auto kp_dim = m_context->getTensorShape(score_name);
    auto kp_vol = std::accumulate(kp_dim.d,
                                  kp_dim.d + kp_dim.nbDims,
                                  1,
                                  std::multiplies<int>());
    m_keypoint_map_buffer.resize(kp_vol);
    BEACON_TENSORRT_CHECK(
        m_context->setTensorAddress(score_name,
                                    m_keypoint_map_buffer.getDeviceData()));

    // init descriptor buffer
    auto desc_d = m_context->getTensorShape(descriptor_name);
    auto desc_vol = std::accumulate(desc_d.d,
                                    desc_d.d + desc_d.nbDims,
                                    1,
                                    std::multiplies<int>());
    m_descriptor_buffer.resize(desc_vol);
    BEACON_TENSORRT_CHECK(
        m_context->setTensorAddress(descriptor_name,
                                    m_descriptor_buffer.getDeviceData()));
}

/* ########################################################################## */
void SuperPointTRT::Impl::setParameter(const SuperPointTRTParameter& params) {
    bool need_reload{false};
    if (m_param.m_engine_path != params.m_engine_path) {
        need_reload = true;
    }
    m_param = params;
    if (need_reload) {
        reload();
    }
}

}  // namespace beacon
