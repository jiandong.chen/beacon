#include "../../../include/beacon/visual_feature/superpoint_trt.h"

#include "superpoint_trt_impl.h"

namespace beacon {

SuperPointTRT::SuperPointTRT(const SuperPointTRTParameter& params) {
    m_impl = std::make_unique<Impl>(params);
}

void SuperPointTRT::processImpl(ImageData& data) {
    m_impl->process(data);
}

void SuperPointTRT::setParameter(const SuperPointTRTParameter& params) {
    // set and reload
    m_impl->setParameter(params);
}

void SuperPointTRT::setParameter(const VisualFeatureParameter& params) {
    if (params.type() != FeatureType::SuperPointTRT) {
        return;
    }
    setParameter(static_cast<const SuperPointTRTParameter&>(params));
}

}  // namespace beacon
