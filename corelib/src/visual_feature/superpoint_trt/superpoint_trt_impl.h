#pragma once

#include "../../../include/beacon/visual_feature/superpoint_trt.h"
#include "NvInfer.h"
#include "beacon/cuda/cuda_buffer_cv.h"

namespace beacon {

struct SuperPointTRTResult {
    int image_height{};
    int image_width{};
    std::size_t num_kps{};
    cuda::DeviceBuffer<float3> kpsc{};
    cuda::DeviceBuffer<float> desc{};
    enum class State {
        Unknown,
        SuperPoint,
        SuperGlue,  // SuperGlue normalize kps
    } state{State::Unknown};

};  // struct SuperPointTRTResult

struct SuperPointTRT::Impl {
    Impl(const SuperPointTRTParameter& params);
    ~Impl();

    // API
    void process(ImageData& data);
    void setParameter(const SuperPointTRTParameter& params);

    // temporary vars
    cudaStream_t m_stream{};

    cv::Mat m_input_buffer{};
    cuda::BufferCV<float> m_image_buffer{};
    cuda::PinnedBuffer<float> m_keypoint_map_buffer{};
    cuda::PinnedBuffer<float> m_descriptor_buffer{};

    // TensorRT components
    std::unique_ptr<nvinfer1::ILogger> m_logger{};
    std::unique_ptr<nvinfer1::IRuntime> m_rumtime{};
    std::unique_ptr<nvinfer1::ICudaEngine> m_engine{};
    std::unique_ptr<nvinfer1::IExecutionContext> m_context{};

    // all params
    SuperPointTRTParameter m_param{};
    int m_batch_size{1};

    // network input output names
    static constexpr auto input_image_name{"Image"};
    static constexpr auto score_name{"Score"};
    static constexpr auto descriptor_name{"Desc"};

  private:
    void reload();
    void checkAndReshapeIO();

};  // SuperPointTRT::Impl

}  // namespace beacon
