#include "../../include/beacon/visual_feature/superpoint.h"

#include <torch/torch.h>

namespace beacon {

static constexpr int c1 = 64;
static constexpr int c2 = 64;
static constexpr int c3 = 128;
static constexpr int c4 = 128;
static constexpr int c5 = 256;
static constexpr int descriptor_dim = 256;

struct SuperPointResult {
    torch::Device dev{torch::kCPU};

    torch::Tensor keypoints_d;    // [num_kp, 2]
    torch::Tensor scores_d;       // [num_kp]
    torch::Tensor descriptors_d;  // [dim_desc, num_kp]

    torch::Tensor keypoints_h;
    torch::Tensor scores_h;
    torch::Tensor descriptors_h;

    SuperPointResult& moveToDevice();
    SuperPointResult& moveToHost();
};  // struct SuperPointResult

inline SuperPointResult& SuperPointResult::moveToDevice() {
    if (keypoints_h.defined()) {
        keypoints_d = keypoints_h.to(dev);
    }
    if (scores_h.defined()) {
        scores_d = scores_h.to(dev);
    }
    if (descriptors_h.defined()) {
        descriptors_d = descriptors_h.to(dev);
    }
    return *this;
}

inline SuperPointResult& SuperPointResult::moveToHost() {
    if (keypoints_d.defined()) {
        keypoints_h = keypoints_d.to(torch::kCPU);
    }
    if (scores_d.defined()) {
        scores_h = scores_d.to(torch::kCPU);
    }
    if (descriptors_d.defined()) {
        descriptors_h = descriptors_d.to(torch::kCPU);
    }
    return *this;
}

struct SuperPoint::SuperPointNet : torch::nn::Module {
    SuperPointNet();
    SuperPointResult forward(torch::Tensor);

    torch::nn::Conv2d conv1a, conv1b;
    torch::nn::Conv2d conv2a, conv2b;
    torch::nn::Conv2d conv3a, conv3b;
    torch::nn::Conv2d conv4a, conv4b;

    torch::nn::Conv2d convPa, convPb;

    torch::nn::Conv2d convDa, convDb;

};  // struct SuperPoint::SuperPointNet

inline SuperPoint::SuperPointNet::SuperPointNet()
    : conv1a(torch::nn::Conv2dOptions(1, c1, 3).stride(1).padding(1)),
      conv1b(torch::nn::Conv2dOptions(c1, c1, 3).stride(1).padding(1)),
      conv2a(torch::nn::Conv2dOptions(c1, c2, 3).stride(1).padding(1)),
      conv2b(torch::nn::Conv2dOptions(c2, c2, 3).stride(1).padding(1)),
      conv3a(torch::nn::Conv2dOptions(c2, c3, 3).stride(1).padding(1)),
      conv3b(torch::nn::Conv2dOptions(c3, c3, 3).stride(1).padding(1)),
      conv4a(torch::nn::Conv2dOptions(c3, c4, 3).stride(1).padding(1)),
      conv4b(torch::nn::Conv2dOptions(c4, c4, 3).stride(1).padding(1)),
      convPa(torch::nn::Conv2dOptions(c4, c5, 3).stride(1).padding(1)),
      convPb(torch::nn::Conv2dOptions(c5, 65, 1).stride(1).padding(0)),
      convDa(torch::nn::Conv2dOptions(c4, c5, 3).stride(1).padding(1)),
      convDb(torch::nn::Conv2dOptions(c5, descriptor_dim, 1)
                 .stride(1)
                 .padding(0)) {
    register_module("conv1a", conv1a);
    register_module("conv1b", conv1b);
    register_module("conv2a", conv2a);
    register_module("conv2b", conv2b);
    register_module("conv3a", conv3a);
    register_module("conv3b", conv3b);
    register_module("conv4a", conv4a);
    register_module("conv4b", conv4b);

    register_module("convPa", convPa);
    register_module("convPb", convPb);

    register_module("convDa", convDa);
    register_module("convDb", convDb);
}

inline SuperPointResult SuperPoint::SuperPointNet::forward(torch::Tensor x) {
    x = torch::relu(conv1a->forward(x));
    x = torch::relu(conv1b->forward(x));
    x = torch::max_pool2d(x, 2, 2);

    x = torch::relu(conv2a->forward(x));
    x = torch::relu(conv2b->forward(x));
    x = torch::max_pool2d(x, 2, 2);

    x = torch::relu(conv3a->forward(x));
    x = torch::relu(conv3b->forward(x));
    x = torch::max_pool2d(x, 2, 2);

    x = torch::relu(conv4a->forward(x));
    x = torch::relu(conv4b->forward(x));

    // Compute the dense keypoint scores
    torch::Tensor scores = torch::relu(convPa->forward(x));
    scores = convPb->forward(scores);
    scores = torch::softmax(scores, 1);
    // discard last bin
    scores = scores.slice(1, 0, 64);
    // why contiguous?
    // https://discuss.pytorch.org/t/contigious-vs-non-contigious-tensor/30107/2
    long long b = scores.size(0);
    long long h = scores.size(2);
    long long w = scores.size(3);
    scores = scores.permute({0, 2, 3, 1}).contiguous();
    scores = scores.view({b, h, w, 8, 8}).contiguous();
    scores = scores.permute({0, 1, 3, 2, 4}).contiguous();
    scores = scores.view({b, h * 8, w * 8}).contiguous();

    // compute dense descriptors
    torch::Tensor descriptors = torch::relu(convDa->forward(x));
    descriptors = convDb->forward(descriptors);
    namespace F = torch::nn::functional;
    descriptors =
        F::normalize(descriptors, F::NormalizeFuncOptions().p(2).dim(1));

    SuperPointResult res;
    res.dev = scores.device();
    res.scores_d = scores;
    res.descriptors_d = descriptors;

    return res;
}

static torch::Tensor simple_nms(const torch::Tensor& scores,
                                const int nms_radius) {
    if (nms_radius < 0) {
        return {};
    }

    auto pool = [=](const torch::Tensor& tensor) {
        return torch::max_pool2d(tensor, nms_radius * 2 + 1, 1, nms_radius);
    };

    torch::Tensor zeros = torch::zeros_like(scores);
    torch::Tensor max_mask = (scores == pool(scores));
    for (int i = 0; i < 2; ++i) {
        // find curr maximal effective region
        torch::Tensor supp_mask = (pool(max_mask.to(torch::kFloat)) > 0);
        // find scores outside the region
        torch::Tensor supp_scores = torch::where(supp_mask, zeros, scores);
        torch::Tensor new_max_mask = (supp_scores == pool(supp_scores));
        // update mask
        max_mask = max_mask | (new_max_mask & (~supp_mask));
    }
    return torch::where(max_mask, scores, zeros);
}

static void removeBorderKeypoint(torch::Tensor& kp,
                                 torch::Tensor& sc,
                                 const int height,
                                 const int width,
                                 const int border) {
    // TODO possible empty after removal
    // TODO mask dont change most of the time
    auto mask_h = (kp.index({torch::indexing::Slice(), 0}) >= border) &
                  (kp.index({torch::indexing::Slice(), 0}) < (height - border));
    auto mask_w = (kp.index({torch::indexing::Slice(), 1}) >= border) &
                  (kp.index({torch::indexing::Slice(), 1}) < (width - border));
    auto mask = mask_w & mask_h;
    kp = kp.index({mask});
    sc = sc.index({mask});
}

static torch::Tensor sampleDescriptor(const torch::Tensor& kp,
                                      const torch::Tensor& desc,
                                      const int s = 8) {
    auto dev = kp.device();
    auto option = torch::TensorOptions().device(dev);
    torch::Tensor keypoints = kp - s / 2 + 0.5;
    keypoints /= torch::tensor({desc.size(3) * s - s / 2.0 - 0.5,
                                desc.size(2) * s - s / 2.0 - 0.5},
                               option)
                     .unsqueeze(0);
    keypoints = keypoints * 2 - 1;
    keypoints = keypoints.view({desc.size(0), 1, -1, 2});
    namespace F = torch::nn::functional;
    torch::Tensor descriptors = F::grid_sample(
        desc,
        keypoints,
        F::GridSampleFuncOptions().mode(torch::kBilinear).align_corners(true));
    descriptors = F::normalize(
        descriptors.reshape({desc.size(0), desc.size(1), -1}),  // assume b == 1
        F::NormalizeFuncOptions().dim(1).p(2));
    return descriptors;
}

void SuperPoint::processImpl(ImageData& data) {
    if (!m_net) {
        throw std::runtime_error(
            "SuperPoint cannot process images without a valid model!!!!");
    }

    // init
    torch::InferenceMode guard(true);

    auto gray_img = data.getGrayImage();
    torch::Tensor img_tensor =
        torch::from_blob(gray_img.data,
                         {1, 1, gray_img.rows, gray_img.cols},
                         torch::kByte);
    img_tensor = img_tensor.to(torch::kFloat) / 255;
    img_tensor = img_tensor.to(m_dev);

    auto res = m_net->forward(img_tensor);

    // extract keypoints
    torch::Tensor nms_res = simple_nms(res.scores_d, m_params.m_nms_radius);

    // shape [b,1,h,w] --> b == 1 --> [num_keypoints, 2]
    // shape [b,1,h,w] --> b != 1 --> [num_keypoints, 3]
    torch::Tensor keypoints =
        (nms_res > m_params.m_keypoint_threshold).squeeze().nonzero();

    // extract keypoint scores
    // currently assume b == 1
    auto vec_keypoints_xy = torch::split(keypoints, 1, 1);

    // currently assume b == 1
    torch::Tensor keypoint_scores =
        nms_res.squeeze().index({vec_keypoints_xy[0], vec_keypoints_xy[1]});

    // border points removal
    removeBorderKeypoint(keypoints,
                         keypoint_scores,
                         img_tensor.size(2),
                         img_tensor.size(3),
                         m_params.m_remove_borders);

    // MAX NUM KEYPOINTS
    torch::Tensor idx;
    if (keypoint_scores.size(0) > m_params.m_max_keypoint) {
        std::tie(keypoint_scores, idx) =
            torch::topk(keypoint_scores, m_params.m_max_keypoint, 0);
        keypoints = keypoints.index({idx.squeeze()});
    }

    // [H, W] --> [x,y]
    keypoints = torch::flip(keypoints, {1}).to(torch::kFloat);

    // sample descriptors
    torch::Tensor descriptors = sampleDescriptor(keypoints, res.descriptors_d);

    // update final result
    res.keypoints_d = keypoints.unsqueeze(0);        // [1, num_kp, 2] // float
    res.descriptors_d = descriptors;                 // float
    res.scores_d = keypoint_scores.permute({1, 0});  // [1, num_kp] // float

    // move to data to ImageData
    res.moveToHost();
    const auto num_kp = res.keypoints_h.sizes()[1];
    data.feature = VisualFeatureData::create();

    data.feature->points.reserve(num_kp);
    for (int i = 0; i < num_kp; ++i) {
        VisualFeaturePoint pt;
        pt.uv = {res.keypoints_h[0][i][0].item<float>(),
                 res.keypoints_h[0][i][1].item<float>()};
        data.feature->points.push_back(pt);
    }

    // TOOD process descriptor
}

SuperPoint::SuperPoint(const SuperPointParameter& params)
    : m_net{std::make_shared<SuperPointNet>()} {
    setParameter(params);
}

void SuperPoint::setParameter(const SuperPointParameter& params) {
    // set and reload
    m_params = params;
    if (m_params.m_use_cuda && !torch::cuda::is_available()) {
        throw std::runtime_error(
            "SuperPoint Set UseCUDA, but CUDA is NOT available!!!");
    }
    m_dev = m_params.m_use_cuda ? torch::kCUDA : torch::kCPU;
    torch::load(m_net, m_params.m_model_path);
    m_net->to(m_dev);
    m_net->eval();
}

void SuperPoint::setParameter(const VisualFeatureParameter& params) {
    if (params.type() != FeatureType::SuperPointTorch) {
        return;
    }
    setParameter(static_cast<const SuperPointParameter&>(params));
}

}  // namespace beacon
