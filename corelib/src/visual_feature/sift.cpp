#include "../../include/beacon/visual_feature/sift.h"

#include <stdexcept>

namespace beacon {

SIFT::SIFT(const SIFTParameter& param)
    : m_sift{BEACON_SIFT::create(param.m_max_keypoint,
                                 param.m_num_octave_layer,
                                 param.m_contrast_threshold,
                                 param.m_edge_threshold,
                                 param.m_sigma)} {
}

SIFT::SIFT(const VisualFeatureParameter& param) : m_sift{} {
    setParameter(param);
}

void SIFT::processImpl(ImageData& data) {
    std::vector<cv::KeyPoint> res;
    cv::Mat desc;
    m_sift->detectAndCompute(data.getGrayImage(), data.mask, res, desc);

    data.feature = VisualFeatureData::create(res);
    data.feature->descriptors = desc;
}

void SIFT::setParameter(const SIFTParameter& param) {
    m_sift = BEACON_SIFT::create(param.m_max_keypoint,
                                 param.m_num_octave_layer,
                                 param.m_contrast_threshold,
                                 param.m_edge_threshold,
                                 param.m_sigma);
}

void SIFT::setParameter(const VisualFeatureParameter& ori_param) {
    if (ori_param.type() != FeatureType::SIFT) {
        throw std::runtime_error("SIFT Parameter Setting Unmatched!!!");
    }
    auto param = static_cast<const SIFTParameter&>(ori_param);
    setParameter(param);
}

}  // namespace beacon
