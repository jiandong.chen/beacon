#include "../../include/beacon/visual_feature/visual_feature_base.h"

#include "../../include/beacon/visual_feature/gftt.h"
#include "../../include/beacon/visual_feature/sift.h"

namespace beacon {

/* ##################### VisualFeature ###################################### */
/* ########################################################################## */
void VisualFeature::process(ImageData& data) {
    // put pre-process here

    // call actual process
    processImpl(data);

    // put post-process here
    if (data.camera && data.feature) {
        data.camera->undistortKeypoint(*data.feature);
    }
}

/* ########################################################################## */
VisualFeature::Ptr VisualFeature::create(const std::string& name,
                                         const VisualFeatureParameter& param) {
    return getFactory(name)(param);
}

}  // namespace beacon
