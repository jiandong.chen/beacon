#include "../../include/beacon/visual_feature/gftt.h"

#include "../../include//beacon/datatype/visual_feature_data.h"

namespace beacon {

GFTT::GFTT(const GFTTParameter& params)
    : m_use_harris(params.m_use_harris),
      m_max_keypoint(params.m_max_keypoint),
      m_block_size(params.m_block_size),
      m_quality_level(params.m_quality_level),
      m_min_keypoint_distance(params.m_min_keypoint_distance),
      m_harris_k(params.m_harris_k) {
    m_gftt = cv::GFTTDetector::create(m_max_keypoint,
                                      m_quality_level,
                                      m_min_keypoint_distance,
                                      m_block_size,
                                      m_use_harris,
                                      m_harris_k);
}

void GFTT::processImpl(ImageData& data) {
    std::vector<cv::KeyPoint> res;
    m_gftt->detect(ImageData::getGrayImage(data), res, data.mask);

    data.feature = VisualFeatureData::create(res);
}

void GFTT::setParameter(const GFTTParameter& params) {
    m_use_harris = params.m_use_harris;
    m_max_keypoint = params.m_max_keypoint;
    m_block_size = params.m_block_size;
    m_harris_k = params.m_harris_k;
    m_quality_level = params.m_quality_level;
    m_min_keypoint_distance = params.m_min_keypoint_distance;
    m_gftt->setMaxFeatures(m_max_keypoint);
    m_gftt->setQualityLevel(m_quality_level);
    m_gftt->setMinDistance(m_min_keypoint_distance);
    m_gftt->setBlockSize(m_block_size);
    m_gftt->setHarrisDetector(m_use_harris);
    m_gftt->setK(m_harris_k);
}

void GFTT::setParameter(const VisualFeatureParameter& params) {
    if (params.type() != FeatureType::GFTT) {
        return;
    }
    setParameter(static_cast<const GFTTParameter&>(params));
}

}  // namespace beacon
