#include "superglue_trt_builder.h"

#include <filesystem>

#include "../../util/tensorrt/tensorrt_layers.h"
#include "beacon/cuda/tensorrt_logger.h"

namespace beacon {

SuperGlueBuilder::SuperGlueBuilder(const SuperGlueTRTBuilderParameter& param)
    : m_param{param} {
}

SuperGlueBuilder::~SuperGlueBuilder() = default;

void SuperGlueBuilder::buildEngine() {
    builder->setMaxBatchSize(batch_size);
    config->setMemoryPoolLimit(nvinfer1::MemoryPoolType::kWORKSPACE,
                               (1ull << 32));
    config->setFlag(nvinfer1::BuilderFlag::kGPU_FALLBACK);
    config->setProfilingVerbosity(nvinfer1::ProfilingVerbosity::kDETAILED);
    config->setBuilderOptimizationLevel(5);  // default: 3 max_level: 5
    if (m_param.m_enable_fp16) {
        config->setFlag(nvinfer1::BuilderFlag::kFP16);
    }
    if (m_param.m_enable_int8) {
        config->setFlag(nvinfer1::BuilderFlag::kINT8);
    }

    // dynamic shape
    auto opt_profile = builder->createOptimizationProfile();
    // dynamic shape kps 0
    opt_profile->setDimensions(
        kps_name_0,
        nvinfer1::OptProfileSelector::kMIN,
        nvinfer1::Dims3(batch_size, m_param.m_min_keypoint_number, kps_dim));
    opt_profile->setDimensions(
        kps_name_0,
        nvinfer1::OptProfileSelector::kOPT,
        nvinfer1::Dims3(batch_size, m_param.m_opt_keypoint_number, kps_dim));
    opt_profile->setDimensions(
        kps_name_0,
        nvinfer1::OptProfileSelector::kMAX,
        nvinfer1::Dims3(batch_size, m_param.m_max_keypoint_number, kps_dim));

    // dynamic shape kps 1
    opt_profile->setDimensions(
        kps_name_1,
        nvinfer1::OptProfileSelector::kMIN,
        nvinfer1::Dims3(batch_size, m_param.m_min_keypoint_number, kps_dim));
    opt_profile->setDimensions(
        kps_name_1,
        nvinfer1::OptProfileSelector::kOPT,
        nvinfer1::Dims3(batch_size, m_param.m_opt_keypoint_number, kps_dim));
    opt_profile->setDimensions(
        kps_name_1,
        nvinfer1::OptProfileSelector::kMAX,
        nvinfer1::Dims3(batch_size, m_param.m_max_keypoint_number, kps_dim));

    // dynamic shape desc 0
    opt_profile->setDimensions(
        desc_name_0,
        nvinfer1::OptProfileSelector::kMIN,
        nvinfer1::Dims3(batch_size, m_param.m_min_keypoint_number, desc_dim));
    opt_profile->setDimensions(
        desc_name_0,
        nvinfer1::OptProfileSelector::kOPT,
        nvinfer1::Dims3(batch_size, m_param.m_opt_keypoint_number, desc_dim));
    opt_profile->setDimensions(
        desc_name_0,
        nvinfer1::OptProfileSelector::kMAX,
        nvinfer1::Dims3(batch_size, m_param.m_max_keypoint_number, desc_dim));

    // dynamic shape desc 1
    opt_profile->setDimensions(
        desc_name_1,
        nvinfer1::OptProfileSelector::kMIN,
        nvinfer1::Dims3(batch_size, m_param.m_min_keypoint_number, desc_dim));
    opt_profile->setDimensions(
        desc_name_1,
        nvinfer1::OptProfileSelector::kOPT,
        nvinfer1::Dims3(batch_size, m_param.m_opt_keypoint_number, desc_dim));
    opt_profile->setDimensions(
        desc_name_1,
        nvinfer1::OptProfileSelector::kMAX,
        nvinfer1::Dims3(batch_size, m_param.m_max_keypoint_number, desc_dim));

    config->addOptimizationProfile(opt_profile);

    // generate model
    serilize_engine = std::unique_ptr<nvinfer1::IHostMemory>(
        builder->buildSerializedNetwork(*network, *config));
}

std::unique_ptr<nvinfer1::IHostMemory> SuperGlueBuilder::buildFromFile() {
    const auto path = m_param.m_weight_path;
    if (!std::filesystem::exists(path)) {
        throw std::runtime_error(
            "The Weight File: " + path +
            " do NOT exist!!!!! SuperPoint TRT Engine cannot be built!!!");
    }

    // init
    logger = std::make_unique<BeaconTRTLogger>();
    builder = std::unique_ptr<nvinfer1::IBuilder>(
        nvinfer1::createInferBuilder(*logger));
    config = std::unique_ptr<nvinfer1::IBuilderConfig>(
        builder->createBuilderConfig());
    network =
        std::unique_ptr<nvinfer1::INetworkDefinition>(builder->createNetworkV2(
            1U << static_cast<uint32_t>(
                nvinfer1::NetworkDefinitionCreationFlag::kEXPLICIT_BATCH)));

    // load from weight file
    weights.readFromFile(m_param.m_weight_path);

    // build the network
    buildNetwork();

    // build engine
    buildEngine();

    return std::move(serilize_engine);
}

void SuperGlueBuilder::buildNetwork() {
    auto kps0 = network->addInput(kps_name_0,
                                  nvinfer1::DataType::kFLOAT,
                                  nvinfer1::Dims3{batch_size, -1, kps_dim});
    auto kps1 = network->addInput(kps_name_1,
                                  nvinfer1::DataType::kFLOAT,
                                  nvinfer1::Dims3{batch_size, -1, kps_dim});

    auto desc0 = network->addInput(desc_name_0,
                                   nvinfer1::DataType::kFLOAT,
                                   nvinfer1::Dims3{batch_size, -1, desc_dim});
    auto desc1 = network->addInput(desc_name_1,
                                   nvinfer1::DataType::kFLOAT,
                                   nvinfer1::Dims3{batch_size, -1, desc_dim});

    kps0->setDimensionName(1, "NumKps0");
    desc0->setDimensionName(1, "NumKps0");
    kps1->setDimensionName(1, "NumKps1");
    desc1->setDimensionName(1, "NumKps1");

    // permute the input
    auto tp_kps0 = network->addShuffle(*kps0);
    tp_kps0->setFirstTranspose({0, 2, 1});
    kps0 = tp_kps0->getOutput(0);
    auto tp_kps1 = network->addShuffle(*kps1);
    tp_kps1->setFirstTranspose({0, 2, 1});
    kps1 = tp_kps1->getOutput(0);

    auto tp_desc0 = network->addShuffle(*desc0);
    tp_desc0->setFirstTranspose({0, 2, 1});
    desc0 = tp_desc0->getOutput(0);
    auto tp_desc1 = network->addShuffle(*desc1);
    tp_desc1->setFirstTranspose({0, 2, 1});
    desc1 = tp_desc1->getOutput(0);

    // keypoint encoder
    auto tmp_desc_0 = buildKeypointEncoder(kps0);
    auto tmp_desc_1 = buildKeypointEncoder(kps1);
    auto add_desc_0 =
        network->addElementWise(*desc0,
                                *tmp_desc_0->getOutput(0),
                                nvinfer1::ElementWiseOperation::kSUM);
    auto add_desc_1 =
        network->addElementWise(*desc1,
                                *tmp_desc_1->getOutput(0),
                                nvinfer1::ElementWiseOperation::kSUM);

    // attention GNN
    auto [gnn_desc_0, gnn_desc_1] = buildAttentionGNN(add_desc_0->getOutput(0),
                                                      add_desc_1->getOutput(0),
                                                      "gnn.layers");

    // final proj
    auto mdesc0 = createConv1D(*network,
                               *gnn_desc_0,
                               256,
                               1,
                               1,
                               0,
                               1,
                               "final_proj",
                               weights);
    auto mdesc1 = createConv1D(*network,
                               *gnn_desc_1,
                               256,
                               1,
                               1,
                               0,
                               1,
                               "final_proj",
                               weights);

    // score map
    auto& score_map_tensor = m_tensors.get("FinalScoreMap");
    score_map_tensor.push_back(mdesc0->getOutput(0));
    score_map_tensor.push_back(mdesc1->getOutput(0));
    auto score_map =
        network->addEinsum(score_map_tensor.data(), 2, "bdn,bdm->bnm");

    // score_map / 256**0.5
    constexpr const auto div = 1.0 / std::sqrt(256);
    auto& div_w =
        weights.createWeight("score_map_div", 1, 4, nvinfer1::DataType::kFLOAT);
    auto dim_div_w_val = (float*)(div_w.values);
    dim_div_w_val[0] = div;
    auto& dummy_shift = weights.createWeight("score_map_dummy_shift",
                                             1,
                                             4,
                                             nvinfer1::DataType::kFLOAT);
    auto dummy_shift_val = (float*)(dummy_shift.values);
    dummy_shift_val[0] = 0.0;
    auto& dummy_power = weights.createWeight("score_map_dummy_power",
                                             1,
                                             4,
                                             nvinfer1::DataType::kFLOAT);
    auto dummy_power_val = (float*)(dummy_power.values);
    dummy_power_val[0] = 1.0;
    auto score_div = network->addScale(*score_map->getOutput(0),
                                       nvinfer1::ScaleMode::kUNIFORM,
                                       dummy_shift,
                                       div_w,
                                       dummy_power);
    score_div->setName("score_map_div");

    score_div->getOutput(0)->setName(output_name);
    network->markOutput(*score_div->getOutput(0));
}

std::pair<nvinfer1::ITensor*, nvinfer1::ITensor*>
SuperGlueBuilder::buildAttentionGNN(nvinfer1::ITensor* desc0,
                                    nvinfer1::ITensor* desc1,
                                    const std::string& layer_prefix) {
    for (int i = 0; i < 9; ++i) {
        auto suffix = "." + std::to_string(i * 2);

        nvinfer1::IShuffleLayer* self_desc_0 =
            buildAttentionalPropagation(desc0, desc0, layer_prefix + suffix);
        nvinfer1::IShuffleLayer* self_desc_1 =
            buildAttentionalPropagation(desc1, desc1, layer_prefix + suffix);
        desc0 = network
                    ->addElementWise(*desc0,
                                     *self_desc_0->getOutput(0),
                                     nvinfer1::ElementWiseOperation::kSUM)
                    ->getOutput(0);
        desc1 = network
                    ->addElementWise(*desc1,
                                     *self_desc_1->getOutput(0),
                                     nvinfer1::ElementWiseOperation::kSUM)
                    ->getOutput(0);

        suffix = "." + std::to_string(i * 2 + 1);

        nvinfer1::IShuffleLayer* cross_desc_0 =
            buildAttentionalPropagation(desc0, desc1, layer_prefix + suffix);
        nvinfer1::IShuffleLayer* cross_desc_1 =
            buildAttentionalPropagation(desc1, desc0, layer_prefix + suffix);
        desc0 = network
                    ->addElementWise(*desc0,
                                     *cross_desc_0->getOutput(0),
                                     nvinfer1::ElementWiseOperation::kSUM)
                    ->getOutput(0);
        desc1 = network
                    ->addElementWise(*desc1,
                                     *cross_desc_1->getOutput(0),
                                     nvinfer1::ElementWiseOperation::kSUM)
                    ->getOutput(0);
    }

    return {desc0, desc1};
}

nvinfer1::IShuffleLayer* SuperGlueBuilder::buildAttentionalPropagation(
    nvinfer1::ITensor* x,
    nvinfer1::ITensor* src,
    const std::string& layer_prefix) {
    static std::size_t count = 0;
    ++count;

    const auto name_prefix = layer_prefix + std::to_string(count);

    auto msg_layer = buildMultiHeadedAttention(x, src, src, layer_prefix);

    auto& concat_tensor = m_tensors.get(name_prefix + "msg_concat");
    concat_tensor.push_back(x);
    concat_tensor.push_back(msg_layer->getOutput(0));
    auto msg_concat = network->addConcatenation(concat_tensor.data(),
                                                2);  // default dim = C (BCHW)
    msg_concat->setAxis(1);

    auto msg_conv = createConv1D(*network,
                                 *msg_concat->getOutput(0),
                                 512,
                                 1,
                                 1,
                                 0,
                                 1,
                                 layer_prefix + ".mlp.0",
                                 weights);

    auto msg_bn = createBatchNorm1D(*network,
                                    *msg_conv->getOutput(0),
                                    layer_prefix + ".mlp.1",
                                    eps,
                                    weights);

    auto msg_relu = network->addActivation(*msg_bn->getOutput(0),
                                           nvinfer1::ActivationType::kRELU);

    auto msg_conv_2 = createConv1D(*network,
                                   *msg_relu->getOutput(0),
                                   256,
                                   1,
                                   1,
                                   0,
                                   1,
                                   layer_prefix + ".mlp.3",
                                   weights);

    return msg_conv_2;
}

nvinfer1::IShuffleLayer* SuperGlueBuilder::buildMultiHeadedAttention(
    nvinfer1::ITensor* query,
    nvinfer1::ITensor* key,
    nvinfer1::ITensor* value,
    const std::string& layer_prefix) {
    static std::size_t count = 0;
    ++count;

    const auto new_prefix = layer_prefix + ".attn.proj";
    const auto name_prefix = new_prefix + std::to_string(count);

    constexpr const int desc_dim = 256;
    constexpr const int head_num = 4;

    // query
    auto query_proj = createConv1D(*network,
                                   *query,
                                   256,
                                   1,
                                   1,
                                   0,
                                   1,
                                   new_prefix + ".0",
                                   weights);
    query_proj->setName((name_prefix + "query_proj").c_str());

    auto query_view = network->addShuffle(*query_proj->getOutput(0));
    query_view->setReshapeDimensions(
        {4, {0, desc_dim / head_num, head_num, -1}});
    query_view->setName((name_prefix + "query_view").c_str());

    // key
    auto key_proj = createConv1D(*network,
                                 *key,
                                 256,
                                 1,
                                 1,
                                 0,
                                 1,
                                 new_prefix + ".1",
                                 weights);
    key_proj->setName((name_prefix + "key_proj").c_str());

    auto key_view = network->addShuffle(*key_proj->getOutput(0));
    key_view->setReshapeDimensions({4, {0, desc_dim / head_num, head_num, -1}});
    key_view->setName((name_prefix + "key_view").c_str());

    // value
    auto value_proj = createConv1D(*network,
                                   *value,
                                   256,
                                   1,
                                   1,
                                   0,
                                   1,
                                   new_prefix + ".2",
                                   weights);
    value_proj->setName((name_prefix + "value_proj").c_str());

    auto value_view = network->addShuffle(*value_proj->getOutput(0));
    value_view->setReshapeDimensions(
        {4, {0, desc_dim / head_num, head_num, -1}});
    value_view->setName((name_prefix + "value_view").c_str());

    // attention score
    auto& attn_score_arr = m_tensors.get(name_prefix + ".attn_score_arr");
    attn_score_arr.push_back(query_view->getOutput(0));
    attn_score_arr.push_back(key_view->getOutput(0));
    auto score =
        network->addEinsum(attn_score_arr.data(), 2, "bdhn,bdhm->bhnm");
    score->setName((name_prefix + "score").c_str());

    // score / dim**0.5
    constexpr const auto dim_div = 1.0 / std::sqrt(desc_dim / head_num);
    auto& dim_div_w = weights.createWeight(name_prefix + ".attention_dim",
                                           1,
                                           4,
                                           nvinfer1::DataType::kFLOAT);
    auto dim_div_w_val = (float*)(dim_div_w.values);
    dim_div_w_val[0] = dim_div;
    auto& dummy_shift = weights.createWeight(name_prefix + ".dummy_shift",
                                             1,
                                             4,
                                             nvinfer1::DataType::kFLOAT);
    auto dummy_shift_val = (float*)(dummy_shift.values);
    dummy_shift_val[0] = 0.0;
    auto& dummy_power = weights.createWeight(name_prefix + ".dummy_power",
                                             1,
                                             4,
                                             nvinfer1::DataType::kFLOAT);
    auto dummy_power_val = (float*)(dummy_power.values);
    dummy_power_val[0] = 1.0;
    auto score_div = network->addScale(*score->getOutput(0),
                                       nvinfer1::ScaleMode::kUNIFORM,
                                       dummy_shift,
                                       dim_div_w,
                                       dummy_power);
    score_div->setName((name_prefix + "score_div").c_str());

    // prob
    auto prob = network->addSoftMax(*score_div->getOutput(0));
    prob->setAxes(1 << 3);
    prob->setName((name_prefix + "prob").c_str());

    // res
    auto& res_arr = m_tensors.get(name_prefix + ".res_arr");
    res_arr.push_back(prob->getOutput(0));
    res_arr.push_back(value_view->getOutput(0));
    auto res = network->addEinsum(res_arr.data(), 2, "bhnm,bdhm->bdhn");
    res->setName((name_prefix + "res").c_str());

    // merge
    auto reshape_res = network->addShuffle(*res->getOutput(0));
    reshape_res->setName((name_prefix + "reshape_res").c_str());

    reshape_res->setReshapeDimensions({3, {0, 256, -1}});
    auto merge = createConv1D(*network,
                              *reshape_res->getOutput(0),
                              256,
                              1,
                              1,
                              0,
                              1,
                              layer_prefix + ".attn.merge",
                              weights);
    merge->setName((name_prefix + "merge").c_str());

    return merge;
}

nvinfer1::IShuffleLayer* SuperGlueBuilder::buildKeypointEncoder(
    nvinfer1::ITensor* input) {
    auto encoder_1 = createConv1D(*network,
                                  *input,
                                  32,
                                  1,
                                  1,
                                  0,
                                  1,
                                  "kenc.encoder.0",
                                  weights);
    auto encoder_bn_1 = createBatchNorm1D(*network,
                                          *encoder_1->getOutput(0),
                                          "kenc.encoder.1",
                                          eps,
                                          weights);
    auto relu_1 = network->addActivation(*encoder_bn_1->getOutput(0),
                                         nvinfer1::ActivationType::kRELU);

    auto encoder_2 = createConv1D(*network,
                                  *relu_1->getOutput(0),
                                  64,
                                  1,
                                  1,
                                  0,
                                  1,
                                  "kenc.encoder.3",
                                  weights);
    auto encoder_bn_2 = createBatchNorm1D(*network,
                                          *encoder_2->getOutput(0),
                                          "kenc.encoder.4",
                                          eps,
                                          weights);
    auto relu_2 = network->addActivation(*encoder_bn_2->getOutput(0),
                                         nvinfer1::ActivationType::kRELU);

    auto encoder_3 = createConv1D(*network,
                                  *relu_2->getOutput(0),
                                  128,
                                  1,
                                  1,
                                  0,
                                  1,
                                  "kenc.encoder.6",
                                  weights);
    auto encoder_bn_3 = createBatchNorm1D(*network,
                                          *encoder_3->getOutput(0),
                                          "kenc.encoder.7",
                                          eps,
                                          weights);
    auto relu_3 = network->addActivation(*encoder_bn_3->getOutput(0),
                                         nvinfer1::ActivationType::kRELU);

    auto encoder_4 = createConv1D(*network,
                                  *relu_3->getOutput(0),
                                  256,
                                  1,
                                  1,
                                  0,
                                  1,
                                  "kenc.encoder.9",
                                  weights);
    auto encoder_bn_4 = createBatchNorm1D(*network,
                                          *encoder_4->getOutput(0),
                                          "kenc.encoder.10",
                                          eps,
                                          weights);
    auto relu_4 = network->addActivation(*encoder_bn_4->getOutput(0),
                                         nvinfer1::ActivationType::kRELU);

    auto encoder_5 = createConv1D(*network,
                                  *relu_4->getOutput(0),
                                  256,
                                  1,
                                  1,
                                  0,
                                  1,
                                  "kenc.encoder.12",
                                  weights);

    return encoder_5;
}

}  // namespace beacon
