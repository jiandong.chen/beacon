#include "beacon/cuda/cuda_check.h"

namespace beacon::sg_trt_util {

static constexpr const int TILE = 32;
static constexpr const double bin_score = 4.4124;

__global__ void logOptimalTransportLite(float* score,
                                        const int num_kp0,
                                        const int num_kp1,
                                        const int num_iter) {
    // one blk cover all kp, max_kp_num = 1000
    __shared__ float u[1024];
    __shared__ float v[1024];

    // init
    const int idx = threadIdx.x;
    if (idx < num_kp0 + 1) {
        u[idx] = 0.0f;
    }
    if (idx < num_kp1 + 1) {
        v[idx] = 0.0f;
    }
    __syncthreads();

    const int m = num_kp0;
    const int n = num_kp1;

    const float norm = -logf(m + n);
    const float log_m = logf(m);
    const float log_n = logf(n);

    for (int it = 0; it < num_iter; ++it) {
        if (idx < m + 1) {
            float sum_exp = 0.0f;
            for (int j = 0; j < n + 1; ++j) {
                float coupling_val =
                    (idx == m || j == n) ? bin_score : score[idx * n + j];
                sum_exp += expf(coupling_val + v[j]);
            }
            float log_mu = idx == m ? norm + log_n : norm;
            u[idx] = log_mu - logf(sum_exp);
        }
        __syncthreads();

        if (idx < n + 1) {
            float sum_exp = 0.0f;
            for (int j = 0; j < m + 1; ++j) {
                float coupling_val =
                    (idx == n || j == m) ? bin_score : score[j * n + idx];
                sum_exp += expf(coupling_val + u[j]);
            }
            float log_nu = idx == n ? norm + log_m : norm;
            v[idx] = log_nu - logf(sum_exp);
        }
        __syncthreads();
    }

    // assume m and n are basically same
    for (int r = 0; r < m; ++r) {
        if (idx < n) {
            score[r * n + idx] += u[r] + v[idx] - norm;
        }
    }
}

__global__ void logOptimalTransportUpdateU(const float* score,
                                           float* u,
                                           const float* v,
                                           const int num_kp0,
                                           const int num_kp1) {
    __shared__ float sv[TILE];

    const int m = num_kp0;
    const int n = num_kp1;

    const float norm = -logf(m + n);
    const float log_n = logf(n);

    const int global_idx = blockIdx.x * blockDim.x + threadIdx.x;
    const int idx = threadIdx.x;

    float sum_exp = 0.0f;
    for (int phase = 0; phase < ceil((n + 1) / (float)TILE); ++phase) {
        const int v_idx = idx + phase * TILE;
        if (v_idx < n + 1) {
            sv[idx] = v[v_idx];
        } else {
            sv[idx] = 0.0f;
        }
        __syncthreads();

        if (global_idx < m + 1) {
            for (int j = 0; j < TILE; ++j) {
                if (j + phase * TILE > n)
                    break;
                float coupling_val =
                    (global_idx == m || phase * TILE + j == n)
                    ? bin_score
                    : score[global_idx * n + j + phase * TILE];
                sum_exp += expf(coupling_val + sv[j]);
            }
        }
        __syncthreads();
    }
    if (global_idx < m + 1) {
        float log_mu = global_idx == m ? norm + log_n : norm;
        u[global_idx] = log_mu - logf(sum_exp);
    }
}

__global__ void logOptimalTransportUpdateV(const float* score,
                                           const float* u,
                                           float* v,
                                           const int num_kp0,
                                           const int num_kp1) {
    __shared__ float su[TILE];

    const int m = num_kp0;
    const int n = num_kp1;

    const float norm = -logf(m + n);
    const float log_m = logf(m);

    const int global_idx = blockIdx.x * blockDim.x + threadIdx.x;
    const int idx = threadIdx.x;

    float sum_exp = 0.0f;
    for (int phase = 0; phase < ceil((m + 1) / (float)TILE); ++phase) {
        const int u_idx = idx + phase * TILE;
        if (u_idx < m + 1) {
            su[idx] = u[u_idx];
        } else {
            su[idx] = 0.0f;
        }
        __syncthreads();

        if (global_idx < n + 1) {
            for (int j = 0; j < TILE; ++j) {
                if (j + phase * TILE > m)
                    break;
                float coupling_val =
                    (global_idx == n || phase * TILE + j == m)
                    ? bin_score
                    : score[global_idx + (j + phase * TILE) * n];
                sum_exp += expf(coupling_val + su[j]);
            }
        }
        __syncthreads();
    }
    if (global_idx < n + 1) {
        float log_nu = global_idx == n ? norm + log_m : norm;
        v[global_idx] = log_nu - logf(sum_exp);
    }
}

__global__ void logOptimalTransportUpdateScore(float* score,
                                               const float* u,
                                               const float* v,
                                               const int num_kp0,
                                               const int num_kp1) {
    const int m = num_kp0;
    const int n = num_kp1;
    const float norm = -logf(m + n);

    const int by = blockIdx.y;
    const int bx = blockIdx.x;
    const int ty = threadIdx.y;
    const int tx = threadIdx.x;

    const int row = by * blockDim.y + ty;
    const int col = bx * blockDim.x + tx;

    if (row < m && col < n) {
        score[row * n + col] += u[row] + v[col] - norm;
    }
}

void logOptimalTransportGPU(float* score_d,
                            const int num_kp0,
                            const int num_kp1,
                            const float* tmp_u,
                            const float* tmp_v,
                            const int num_iter,
                            cudaStream_t& stream) {
    if (num_kp0 > 1000 || num_kp1 > 1000) {
        void* u = nullptr;
        void* v = nullptr;
        // CUDACHECK(cudaMalloc(&u, (num_kp0 + 1) * sizeof(float)));
        // CUDACHECK(cudaMalloc(&v, (num_kp1 + 1) * sizeof(float)));
        BEACON_CUDA_CHECK(cudaMemset(u, 0.0f, (num_kp0 + 1) * sizeof(float)));
        BEACON_CUDA_CHECK(cudaMemset(v, 0.0f, (num_kp1 + 1) * sizeof(float)));

        size_t max_num_kp = std::max(num_kp0, num_kp1);
        auto blk = dim3(TILE);
        auto grid = dim3(ceil(max_num_kp / static_cast<float>(TILE)));
        for (int it = 0; it < num_iter; ++it) {
            logOptimalTransportUpdateU<<<grid, blk, 0, stream>>>(score_d,
                                                                 (float*)u,
                                                                 (float*)v,
                                                                 num_kp0,
                                                                 num_kp1);
            logOptimalTransportUpdateV<<<grid, blk, 0, stream>>>(score_d,
                                                                 (float*)u,
                                                                 (float*)v,
                                                                 num_kp0,
                                                                 num_kp1);
        }
        blk = dim3(TILE, TILE);
        grid = dim3(ceil(max_num_kp / static_cast<float>(TILE)),
                    ceil(max_num_kp / static_cast<float>(TILE)));
        logOptimalTransportUpdateScore<<<grid, blk, 0, stream>>>(score_d,
                                                                 (float*)u,
                                                                 (float*)v,
                                                                 num_kp0,
                                                                 num_kp1);
    } else {
        logOptimalTransportLite<<<1, 1024, 0, stream>>>(score_d,
                                                        num_kp0,
                                                        num_kp1,
                                                        num_iter);
    }
}

};  // namespace beacon::sg_trt_util
