#include "superglue_trt_impl.h"

#include <filesystem>
#include <fstream>
#include <numeric>

#include "../../visual_feature/superpoint_trt/superpoint_trt_impl.h"
#include "beacon/cuda/cuda_check.h"
#include "beacon/cuda/tensorrt_logger.h"
#include "superglue_trt_util.h"

namespace beacon {

using namespace beacon::tensorrt;

/* ##################### SuperGlueTRT Impl ################################## */
/* ########################################################################## */
SuperGlueTRT::Impl::Impl(const SuperGlueTRTParameter& params)
    : m_param(params) {
    BEACON_CUDA_CHECK(cudaStreamCreate(&m_stream));

    reload();
}

/* ########################################################################## */
SuperGlueTRT::Impl::~Impl() {
    BEACON_CUDA_CHECK(cudaStreamDestroy(m_stream));
}

/* ########################################################################## */
BEACON_NODISCARD SuperGlueTRT::ResultType SuperGlueTRT::Impl::process(
    VisualFeatureData& data_0,
    VisualFeatureData& data_1) {
    // check if we have superpoint data
    auto extractValidData = [this](const VisualFeatureData& data) {
        if (data.device_data.type() !=
            typeid(std::shared_ptr<SuperPointTRTResult>)) {
            throw std::runtime_error(
                "SuperGlue: Input do NOT have "
                "std::shared_ptr<SuperPointTRTResult>!");
        }
        auto sp = std::any_cast<std::shared_ptr<SuperPointTRTResult>>(
            data.device_data);
        if (sp->state == SuperPointTRTResult::State::SuperGlue) {
            // if already normalized, skip
        } else if (sp->state == SuperPointTRTResult::State::SuperPoint) {
            // raw superpoint results need normalize
            sg_trt_util::normalizeKeypoint(sp->kpsc.getData(),
                                           sp->num_kps,
                                           sp->image_height,
                                           sp->image_width,
                                           m_stream);
            sp->state = SuperPointTRTResult::State::SuperGlue;
        } else {
            // Invalid Data
            throw std::runtime_error(
                "SuperGlue: Addtional Data of Input is NOT Valid!");
        }

        return sp;
    };

    // extract superpoint result
    auto sp_0 = extractValidData(data_0);
    auto sp_1 = extractValidData(data_1);

    // check if we need to resize our buffer
    checkAndReshapeIO(*sp_0, *sp_1);

    BEACON_TENSORRT_CHECK(m_context->enqueueV3(m_stream));

    sg_trt_util::logOptimalTransportGPU(m_output_buffer.getDeviceData(),
                                        sp_0->num_kps,
                                        sp_1->num_kps,
                                        m_u_buffer.getData(),
                                        m_v_buffer.getData(),
                                        m_param.m_sinkhorn_iterations,
                                        m_stream);

    m_output_buffer.moveToHost(m_stream);

    BEACON_CUDA_CHECK(cudaStreamSynchronize(m_stream));

    return sg_trt_util::findMutualMatch(m_output_buffer.getHostData(),
                                        sp_0->num_kps,
                                        sp_1->num_kps,
                                        m_param.m_match_threshold);
}

/* ########################################################################## */
void SuperGlueTRT::Impl::setParameter(const SuperGlueTRTParameter& params) {
    bool need_reload{false};
    if (m_param.m_engine_path != params.m_engine_path) {
        need_reload = true;
    }
    m_param = params;
    if (need_reload) {
        reload();
    }
}

/* ########################################################################## */
void SuperGlueTRT::Impl::checkAndReshapeIO(const SuperPointTRTResult& sp_0,
                                           const SuperPointTRTResult& sp_1) {
    auto checkInput = [this](const SuperPointTRTResult& sp_res,
                             const auto& kpsc_name,
                             const auto& desc_name,
                             auto& buffer) -> bool {
        const auto curr_kps_dim = m_context->getTensorShape(kpsc_name);
        const int num_kp = sp_res.num_kps;
        const auto need_resize = num_kp != curr_kps_dim.d[1];
        if (need_resize) {
            // tell tensorrt change of size
            BEACON_TENSORRT_CHECK(m_context->setInputShape(
                kpsc_name,
                nvinfer1::Dims{3, {m_batch_size, num_kp, keypoint_score_dim}}))
            BEACON_TENSORRT_CHECK(m_context->setInputShape(
                desc_name,
                nvinfer1::Dims{3, {m_batch_size, num_kp, descriptor_dim}}))

            // update  buffer for optimal transport
            buffer.resize(num_kp + 1);
        }
        // update address to new inputs
        BEACON_TENSORRT_CHECK(
            m_context->setTensorAddress(kpsc_name, sp_res.kpsc.getData()));
        BEACON_TENSORRT_CHECK(
            m_context->setTensorAddress(desc_name, sp_res.desc.getData()));

        return need_resize;
    };

    const auto need_resize_0 =
        checkInput(sp_0, keypoint_score_name0, descriptor_name0, m_u_buffer);
    const auto need_resize_1 =
        checkInput(sp_1, keypoint_score_name1, descriptor_name1, m_v_buffer);

    // check output
    if (need_resize_0 || need_resize_1) {
        const auto output_dim = m_context->getTensorShape(output_name);
        const auto output_vol =
            std::accumulate(output_dim.d,
                            output_dim.d + output_dim.nbDims,
                            1,
                            std::multiplies<int>());
        m_output_buffer.resize(output_vol);
        BEACON_TENSORRT_CHECK(
            m_context->setTensorAddress(output_name,
                                        m_output_buffer.getDeviceData()));
    }
}

/* ########################################################################## */
void SuperGlueTRT::Impl::reload() {
    if (!std::filesystem::exists(m_param.m_engine_path)) {
        //  if no engine file, we dont build the engine now
        throw std::runtime_error("The SuperGlue Engine File: " +
                                 m_param.m_engine_path + " do NOT exist!!!!! ");
    }

    // init tensorrt components
    m_logger = std::make_unique<BeaconTRTLogger>();
    m_rumtime = std::unique_ptr<nvinfer1::IRuntime>(
        nvinfer1::createInferRuntime(*m_logger));

    // load engine from file
    std::ifstream file(m_param.m_engine_path, std::ios::binary);
    auto file_size = std::filesystem::file_size(m_param.m_engine_path);
    std::vector<char> data(file_size);
    file.read(data.data(), file_size);
    m_engine = std::unique_ptr<nvinfer1::ICudaEngine>(
        m_rumtime->deserializeCudaEngine(data.data(), file_size));
    m_context = std::unique_ptr<nvinfer1::IExecutionContext>(
        m_engine->createExecutionContext());
}

}  // namespace beacon
