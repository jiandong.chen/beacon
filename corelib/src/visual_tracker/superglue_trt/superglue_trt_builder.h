#pragma once

#include <iostream>
#include <memory>

#include "../../util/tensorrt/tensor_manager.h"
#include "../../util/tensorrt/weight_manager.h"
#include "NvInfer.h"
#include "beacon/visual_tracker/superglue_trt.h"

namespace beacon {

using namespace beacon::tensorrt;

struct SuperGlueTRTBuilderParameter {
    ADD_VISUAL_FEATURE_PARAM(enable_int8, bool, false, EnableINT8)
    ADD_VISUAL_FEATURE_PARAM(enable_fp16, bool, true, EnableFP16)
    ADD_VISUAL_FEATURE_PARAM(min_keypoint_number, int, 100, MinKeypointNumber)
    ADD_VISUAL_FEATURE_PARAM(opt_keypoint_number, int, 1000, OptKeypointNumber)
    ADD_VISUAL_FEATURE_PARAM(max_keypoint_number, int, 4000, MaxKeypointNumber)

    std::string m_weight_path{};

    auto setWeightPath(const std::string& path) {
        m_weight_path = path;
    }

};  // struct SuperGlueTRTBuilderParameter

class SuperGlueBuilder {
  public:
    SuperGlueBuilder(const SuperGlueTRTBuilderParameter& param);
    ~SuperGlueBuilder();

    std::unique_ptr<nvinfer1::IHostMemory> buildFromFile();

  public:
    inline static double eps{1e-5};
    inline static int batch_size{1};
    inline static auto kps_name_0{"kpsc0"};
    inline static auto kps_name_1{"kpsc1"};
    inline static auto desc_name_0{"desc0"};
    inline static auto desc_name_1{"desc1"};

    inline static auto output_name{"Score"};

    inline static int kps_dim{3};
    inline static int desc_dim{256};

  protected:
    void buildNetwork();
    void buildEngine();
    nvinfer1::IShuffleLayer* buildKeypointEncoder(nvinfer1::ITensor* input);
    nvinfer1::IShuffleLayer* buildMultiHeadedAttention(
        nvinfer1::ITensor* query,
        nvinfer1::ITensor* key,
        nvinfer1::ITensor* value,
        const std::string& layer_prefix);
    nvinfer1::IShuffleLayer* buildAttentionalPropagation(
        nvinfer1::ITensor* x,
        nvinfer1::ITensor* src,
        const std::string& layer_prefix);
    std::pair<nvinfer1::ITensor*, nvinfer1::ITensor*> buildAttentionGNN(
        nvinfer1::ITensor* desc1,
        nvinfer1::ITensor* desc2,
        const std::string& layer_prefix);

  protected:
    std::unique_ptr<nvinfer1::ILogger> logger{};
    std::unique_ptr<nvinfer1::IBuilder> builder{};
    std::unique_ptr<nvinfer1::IBuilderConfig> config{};
    std::unique_ptr<nvinfer1::INetworkDefinition> network{};
    std::unique_ptr<nvinfer1::IHostMemory> serilize_engine{};

  protected:
    SuperGlueTRTBuilderParameter m_param{};

    // tmp data used by builder
    WeightManager weights{};
    TensorManager m_tensors{};

};  // class SuperGlueBuilder

}  // namespace beacon
