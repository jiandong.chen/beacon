#pragma once

#include "../../../include/beacon/visual_tracker/superglue_trt.h"
#include "../../visual_feature/superpoint_trt/superpoint_trt_impl.h"
#include "NvInfer.h"
#include "beacon/cuda/cuda_buffer.h"

namespace beacon {

struct SuperGlueTRT::Impl {
    Impl(const SuperGlueTRTParameter& params);
    ~Impl();

  public:
    using SuperPointResPtr = std::shared_ptr<SuperPointTRTResult>;

  public:
    BEACON_NODISCARD ResultType process(VisualFeatureData& data_1,
                                        VisualFeatureData& data_2);
    void setParameter(const SuperGlueTRTParameter& params);

  protected:
    // temporary vars
    cudaStream_t m_stream{};

    cuda::PinnedBuffer<float> m_output_buffer{};
    cuda::DeviceBuffer<float> m_u_buffer;
    cuda::DeviceBuffer<float> m_v_buffer;

    // TensorRT components
    std::unique_ptr<nvinfer1::ILogger> m_logger{};
    std::unique_ptr<nvinfer1::IRuntime> m_rumtime{};
    std::unique_ptr<nvinfer1::ICudaEngine> m_engine{};
    std::unique_ptr<nvinfer1::IExecutionContext> m_context{};

    // all params
    SuperGlueTRTParameter m_param{};
    int m_batch_size{1};

    // network input output names
    static constexpr auto keypoint_score_dim{3};
    static constexpr auto descriptor_dim{256};
    static constexpr auto keypoint_score_name0{"kpsc0"};
    static constexpr auto keypoint_score_name1{"kpsc1"};
    static constexpr auto descriptor_name0{"desc0"};
    static constexpr auto descriptor_name1{"desc1"};
    static constexpr auto output_name{"Score"};

  private:
    void reload();
    void checkAndReshapeIO(const SuperPointTRTResult& sp_0,
                           const SuperPointTRTResult& sp_1);

};  // struct SuperGlueTRT::Impl

}  // namespace beacon
