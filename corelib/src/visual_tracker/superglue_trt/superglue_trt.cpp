#include "../../../include/beacon/visual_tracker/superglue_trt.h"

#include "superglue_trt_impl.h"

namespace beacon {

SuperGlueTRT::SuperGlueTRT(const SuperGlueTRTParameter& params) {
    m_impl = std::make_unique<Impl>(params);
}

BEACON_NODISCARD SuperGlueTRT::ResultType SuperGlueTRT::processImpl(
    ImageData& data0,
    ImageData& data1) {
    return m_impl->process(*data0.feature, *data1.feature);
}

void SuperGlueTRT::setParameter(const VisualTrackerParameter& params) {
    if (params.type() != TrackerType::SuperGlueTRT) {
        return;
    }
    setParameter(static_cast<const SuperGlueTRTParameter&>(params));
}

void SuperGlueTRT::setParameter(const SuperGlueTRTParameter& params) {
    m_impl->setParameter(params);
}

}  // namespace beacon
