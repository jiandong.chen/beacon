#pragma once
#include <cuda_runtime.h>

#include <vector>

#include "../../../include/beacon/datatype/visual_match_data.h"

namespace beacon::sg_trt_util {

void normalizeKeypoint(float3* data,
                       const int num_kps,
                       const int img_height,
                       const int img_width,
                       cudaStream_t stream);

void logOptimalTransportGPU(float* score_d,
                            const int num_kp0,
                            const int num_kp1,
                            const float* tmp_u,
                            const float* tmp_v,
                            const int num_iter,
                            cudaStream_t& stream);

inline std::shared_ptr<VisualFeatureMatchData> findMutualMatch(
    const float* score,
    const int kp0_num,
    const int kp1_num,
    const double match_threshold) {
    std::vector<float> kp0_max(kp0_num, -std::numeric_limits<float>::max());
    std::vector<float> kp1_max(kp1_num, -std::numeric_limits<float>::max());
    std::vector<int> kp0_max_idx(kp0_num, -1);
    std::vector<int> kp1_max_idx(kp1_num, -1);

    // find max of each row and col
    for (int r = 0; r < kp0_num; ++r) {
        for (int c = 0; c < kp1_num; ++c) {
            float curr_score = score[r * kp1_num + c];
            if (curr_score > kp0_max[r]) {
                kp0_max[r] = curr_score;
                kp0_max_idx[r] = c;
            }
            if (curr_score > kp1_max[c]) {
                kp1_max[c] = curr_score;
                kp1_max_idx[c] = r;
            }
        }
    }

    auto res = std::make_shared<VisualFeatureMatchData>();
    for (int i = 0; i < kp0_num; ++i) {
        if (kp1_max_idx[kp0_max_idx[i]] == i) {
            float score = std::exp(kp0_max[i]);
            if (score > match_threshold) {
                VisualFeatureMatch pair;
                pair.idx0 = i;
                pair.idx1 = kp0_max_idx[i];
                pair.score = score;
                res->matches.push_back(pair);
            }
        }
    }

    return res;
}

}  // namespace beacon::sg_trt_util
