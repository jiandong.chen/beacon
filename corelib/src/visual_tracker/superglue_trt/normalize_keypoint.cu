namespace beacon::sg_trt_util {

#define MAX_THREAD_PER_BLK 256

__global__ __launch_bounds__(MAX_THREAD_PER_BLK) void normalizeKeypoint(
    float3* data,
    const int num_kps,
    const int img_height,
    const int img_width) {
    const int x_offset = img_height / 2;
    const int y_offset = img_width / 2;
    const int scaling = max(img_height, img_width);

    auto idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx >= num_kps) {
        return;
    }

    data[idx].x = (data[idx].x - x_offset) / scaling;
    data[idx].y = (data[idx].y - y_offset) / scaling;
}

void normalizeKeypoint(float3* data,
                       const int num_kps,
                       const int img_height,
                       const int img_width,
                       cudaStream_t stream) {
    const auto thread_per_blk = MAX_THREAD_PER_BLK;
    const auto blk_num = (num_kps + thread_per_blk - 1) / thread_per_blk;

    normalizeKeypoint<<<blk_num, thread_per_blk, 0, stream>>>(data,
                                                              num_kps,
                                                              img_height,
                                                              img_width);
}

};  // namespace beacon::sg_trt_util
