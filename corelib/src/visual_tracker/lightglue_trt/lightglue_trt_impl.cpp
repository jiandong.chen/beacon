#include "lightglue_trt_impl.h"

#include <filesystem>
#include <fstream>
#include <numeric>

#include "beacon/cuda/tensorrt_logger.h"

namespace beacon {

/* ##################### LightGlueTRT Impl ################################## */
/* ########################################################################## */
LightGlueTRT::Impl::Impl(const LightGlueTRTParameter& params)
    : m_param(params) {
    BEACON_CUDA_CHECK(cudaStreamCreate(&m_stream));

    reload();
}

/* ########################################################################## */
BEACON_NODISCARD LightGlueTRT::ResultType LightGlueTRT::Impl::process(
    VisualFeatureData& data_0,
    VisualFeatureData& data_1) {
    // check if we have superpoint data

    auto extractValidData = [this](VisualFeatureData& data) {
        // if input is already lightglue input, skip
        if (data.device_data.type() ==
            typeid(std::shared_ptr<lg_trt_util::LightGlueTRTInput>)) {
            return std::any_cast<
                std::shared_ptr<lg_trt_util::LightGlueTRTInput>>(
                data.device_data);
        }

        // if input is superoint we need to normalize the kp
        if (data.device_data.type() ==
            typeid(std::shared_ptr<SuperPointTRTResult>)) {
            auto input = std::any_cast<std::shared_ptr<SuperPointTRTResult>>(
                data.device_data);

            if (input->state != SuperPointTRTResult::State::SuperPoint) {
                // Invalid Data
                throw std::runtime_error(
                    "LightGlue: Got SuperPointTRTResult But Input is NOT "
                    "Valid!");
            }

            auto lg_input = std::make_shared<lg_trt_util::LightGlueTRTInput>(
                input->num_kps);
            lg_trt_util::normalizeKeypoint(input->kpsc.getData(),
                                           input->num_kps,
                                           input->image_height,
                                           input->image_width,
                                           lg_input->kpts.getData(),
                                           m_stream);
            BEACON_CUDA_CHECK(
                cudaMemcpyAsync(lg_input->desc.getData(),
                                input->desc.getData(),
                                input->num_kps * descriptor_dim * sizeof(float),
                                cudaMemcpyDeviceToDevice,
                                m_stream))

            data.device_data = lg_input;

            return lg_input;
        }
        throw std::runtime_error("LightGlue: Got invalid Data Input!");
    };

    auto input0 = extractValidData(data_0);
    auto input1 = extractValidData(data_1);

    if (input0->num_kps == 0 || input1->num_kps == 0) {
        return {};
    }

    checkAndReshapeIO(*input0, *input1);

    BEACON_TENSORRT_CHECK(m_context->enqueueV3(m_stream));

    m_indice0.moveToHost(m_stream);
    m_score0.moveToHost(m_stream);
    m_indice1.moveToHost(m_stream);
    m_score1.moveToHost(m_stream);

    BEACON_CUDA_CHECK(cudaStreamSynchronize(m_stream));

    return lg_trt_util::findMutualMatch(m_indice0.getHostData(),
                                        m_score0.getHostData(),
                                        input0->num_kps,
                                        m_indice1.getHostData(),
                                        input1->num_kps,
                                        m_param.m_match_threshold);
}

/* ########################################################################## */
void LightGlueTRT::Impl::checkAndReshapeIO(
    const lg_trt_util::LightGlueTRTInput& input_0,
    const lg_trt_util::LightGlueTRTInput& input_1) {
    auto setBufferSizeWithName = [this](const auto& name, auto& buffer) {
        const auto dim = m_context->getTensorShape(name);
        const auto vol = std::accumulate(dim.d,
                                         dim.d + dim.nbDims,
                                         1,
                                         std::multiplies<int>());

        buffer.resize(vol);
        BEACON_TENSORRT_CHECK(
            m_context->setTensorAddress(name, buffer.getDeviceData()));
    };

    auto checkInput = [this](const lg_trt_util::LightGlueTRTInput& input,
                             const auto& kpts_name,
                             const auto& desc_name) -> bool {
        const auto curr_kps_dim = m_context->getTensorShape(kpts_name);
        const int num_kp = input.num_kps;
        const auto need_resize = num_kp != curr_kps_dim.d[1];

        if (need_resize) {
            // tell tensorrt change of size
            // check input

            BEACON_TENSORRT_CHECK(m_context->setInputShape(
                kpts_name,
                nvinfer1::Dims{3, {m_batch_size, num_kp, keypoint_score_dim}}))
            BEACON_TENSORRT_CHECK(m_context->setInputShape(
                desc_name,
                nvinfer1::Dims{3, {m_batch_size, num_kp, descriptor_dim}}))
        }

        // update address to new inputs
        BEACON_TENSORRT_CHECK(
            m_context->setTensorAddress(kpts_name, input.kpts.getData()));
        BEACON_TENSORRT_CHECK(
            m_context->setTensorAddress(desc_name, input.desc.getData()));

        return need_resize;
    };

    const auto resize0 = checkInput(input_0, keypoint_name0, descriptor_name0);
    const auto resize1 = checkInput(input_1, keypoint_name1, descriptor_name1);

    if (resize0 || resize1) {
        setBufferSizeWithName(indice_name0, m_indice0);
        setBufferSizeWithName(score_name0, m_score0);
        setBufferSizeWithName(indice_name1, m_indice1);
        setBufferSizeWithName(score_name1, m_score1);
    }

#ifndef NDEBUG
    const auto res_indice0 = m_context->getTensorShape(indice_name0);
    const auto res_score0 = m_context->getTensorShape(score_name0);
    const auto res_indice1 = m_context->getTensorShape(indice_name1);
    const auto res_score1 = m_context->getTensorShape(score_name1);
    assert(static_cast<std::size_t>(res_indice0.d[1]) == input_0.num_kps);
    assert(static_cast<std::size_t>(res_score0.d[1]) == input_0.num_kps);
    assert(static_cast<std::size_t>(res_indice1.d[1]) == input_1.num_kps);
    assert(static_cast<std::size_t>(res_score1.d[1]) == input_1.num_kps);
#endif
}

/* ########################################################################## */
void LightGlueTRT::Impl::reload() {
    if (!std::filesystem::exists(m_param.m_engine_path)) {
        //  if no engine file, we dont build the engine now
        throw std::runtime_error("The LightGlue Engine File: " +
                                 m_param.m_engine_path + " do NOT exist!!!!! ");
    }

    // init tensorrt components
    m_logger = std::make_unique<tensorrt::BeaconTRTLogger>();
    m_rumtime = std::unique_ptr<nvinfer1::IRuntime>(
        nvinfer1::createInferRuntime(*m_logger));

    // load engine from file
    std::ifstream file(m_param.m_engine_path, std::ios::binary);
    auto file_size = std::filesystem::file_size(m_param.m_engine_path);
    std::vector<char> data(file_size);
    file.read(data.data(), file_size);
    m_engine = std::unique_ptr<nvinfer1::ICudaEngine>(
        m_rumtime->deserializeCudaEngine(data.data(), file_size));
    m_context = std::unique_ptr<nvinfer1::IExecutionContext>(
        m_engine->createExecutionContext());
}

/* ########################################################################## */
void LightGlueTRT::Impl::setParameter(const LightGlueTRTParameter& params) {
    bool need_reload{false};
    if (m_param.m_engine_path != params.m_engine_path) {
        need_reload = true;
    }
    m_param = params;
    if (need_reload) {
        reload();
    }
}

/* ########################################################################## */
LightGlueTRT::Impl::~Impl() {
    BEACON_CUDA_CHECK(cudaStreamDestroy(m_stream));
}

}  // namespace beacon
