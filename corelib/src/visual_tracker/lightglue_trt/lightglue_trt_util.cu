namespace beacon::lg_trt_util {

#define MAX_THREAD_PER_BLK 256

__global__ __launch_bounds__(MAX_THREAD_PER_BLK) void normalizeKeypoint(
    float3* input,
    const int num_kps,
    const int img_height,
    const int img_width,
    float2* output) {
    const int x_offset = img_height / 2;
    const int y_offset = img_width / 2;
    const int scaling = max(img_height, img_width);

    auto idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx >= num_kps) {
        return;
    }

    output[idx].x = (input[idx].x - x_offset) / scaling;
    output[idx].y = (input[idx].y - y_offset) / scaling;
}

void normalizeKeypoint(float3* input,
                       const int num_kps,
                       const int img_height,
                       const int img_width,
                       float2* output,
                       cudaStream_t stream) {
    const auto thread_per_blk = MAX_THREAD_PER_BLK;
    const auto blk_num = (num_kps + thread_per_blk - 1) / thread_per_blk;

    normalizeKeypoint<<<blk_num, thread_per_blk, 0, stream>>>(input,
                                                              num_kps,
                                                              img_height,
                                                              img_width,
                                                              output);
}

}  // namespace beacon::lg_trt_util
