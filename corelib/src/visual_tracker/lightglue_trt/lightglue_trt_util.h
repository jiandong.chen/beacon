#pragma once
#include <cuda_runtime.h>

#include "../../../include/beacon/datatype/visual_match_data.h"
#include "beacon/cuda/cuda_buffer.h"

namespace beacon::lg_trt_util {

/* ##################### LightGlueTRTInput ################################## */
/* ########################################################################## */
struct LightGlueTRTInput {
    std::size_t num_kps{};

    cuda::DeviceBuffer<float2> kpts{};
    cuda::DeviceBuffer<float> desc{};

    LightGlueTRTInput(const size_t kp_num) : num_kps{kp_num} {
        kpts.resize(kp_num);
        desc.resize(lightglue_desc_dim * kp_num);
    }

    inline static int lightglue_kpts_dim = 2;
    inline static int lightglue_desc_dim = 256;

};  // struct LightGlueTRTInput

/* ###################### LightGlueUtil ##################################### */
/* ########################################################################## */
void normalizeKeypoint(float3* input,
                       const int num_kps,
                       const int img_height,
                       const int img_width,
                       float2* output,
                       cudaStream_t stream = 0);

/* ########################################################################## */
inline std::shared_ptr<VisualFeatureMatchData> findMutualMatch(
    const int64_t* const indice0,
    const float* const score0,
    const int numkp0,
    const int64_t* const indice1,
    const int numkp1,
    const double threshold) {
    auto res = std::make_shared<VisualFeatureMatchData>();
    for (int i = 0; i < numkp0; ++i) {
        if (indice0[i] < numkp1 && indice1[indice0[i]] == i) {
            const auto score = std::exp(score0[i]);
            if (score >= threshold) {
                VisualFeatureMatch pair;
                pair.idx0 = i;
                pair.idx1 = indice0[i];

                pair.score = score;
                res->matches.push_back(pair);
            }
        }
    }

    return res;
}

}  // namespace beacon::lg_trt_util
