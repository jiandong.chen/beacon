#include "../../../include/beacon/visual_tracker/lightglue_trt.h"

#include "lightglue_trt_impl.h"

namespace beacon {

/* ##################### LightGlueTRT ####################################### */
/* ########################################################################## */
LightGlueTRT::LightGlueTRT(const LightGlueTRTParameter& params) {
    m_impl = std::make_unique<Impl>(params);
}

/* ########################################################################## */
BEACON_NODISCARD LightGlueTRT::ResultType LightGlueTRT::processImpl(
    ImageData& data0,
    ImageData& data1) {
    return m_impl->process(*data0.feature, *data1.feature);
}

/* ########################################################################## */
void LightGlueTRT::setParameter(const VisualTrackerParameter& params) {
    if (params.type() != TrackerType::LightGlueTRT) {
        return;
    }
    setParameter(static_cast<const LightGlueTRTParameter&>(params));
}

/* ########################################################################## */
void LightGlueTRT::setParameter(const LightGlueTRTParameter& params) {
    m_impl->setParameter(params);
}

}  // namespace beacon
