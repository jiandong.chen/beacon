#pragma once
#include "../../../include/beacon/visual_tracker/lightglue_trt.h"
#include "../../visual_feature/superpoint_trt/superpoint_trt_impl.h"
#include "NvInfer.h"
#include "beacon/cuda/cuda_buffer.h"
#include "lightglue_trt_util.h"

namespace beacon {

struct LightGlueTRT::Impl {
    Impl(const LightGlueTRTParameter& params);
    ~Impl();

  public:
    using SuperPointResPtr = std::shared_ptr<SuperPointTRTResult>;

  public:
    BEACON_NODISCARD ResultType process(VisualFeatureData& data_1,
                                        VisualFeatureData& data_2);
    void setParameter(const LightGlueTRTParameter& params);

  protected:
    // temporary vars
    cudaStream_t m_stream{};

    cuda::PinnedBuffer<int64_t> m_indice0{};
    cuda::PinnedBuffer<float> m_score0{};
    cuda::PinnedBuffer<int64_t> m_indice1{};
    cuda::PinnedBuffer<float> m_score1{};

    // TensorRT components
    std::unique_ptr<nvinfer1::ILogger> m_logger{};
    std::unique_ptr<nvinfer1::IRuntime> m_rumtime{};
    std::unique_ptr<nvinfer1::ICudaEngine> m_engine{};
    std::unique_ptr<nvinfer1::IExecutionContext> m_context{};

    // all params
    LightGlueTRTParameter m_param{};
    int m_batch_size{1};

    // network input output names
    static constexpr auto keypoint_score_dim{2};
    static constexpr auto descriptor_dim{256};
    static constexpr auto keypoint_name0{"kpts0"};
    static constexpr auto keypoint_name1{"kpts1"};
    static constexpr auto descriptor_name0{"desc0"};
    static constexpr auto descriptor_name1{"desc1"};
    static constexpr auto indice_name0{"Indice0"};
    static constexpr auto score_name0{"Score0"};
    static constexpr auto indice_name1{"Indice1"};
    static constexpr auto score_name1{"Score1"};

  private:
    void reload();
    void checkAndReshapeIO(const lg_trt_util::LightGlueTRTInput& input_0,
                           const lg_trt_util::LightGlueTRTInput& input_1);

};  // struct LightGlueTRT::Impl

}  // namespace beacon
