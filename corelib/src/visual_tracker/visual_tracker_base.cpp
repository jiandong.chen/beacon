#include "../../include/beacon/visual_tracker/visual_tracker_base.h"

namespace beacon {

/* ##################### VisualTracker ###################################### */
/* ########################################################################## */
VisualTracker::ResultType VisualTracker::process(ImageData& data0,
                                                 ImageData& data1) {
    // put pre-process here

    // call actual process
    auto res = processImpl(data0, data1);

    // put post-process here
    if (res) {
        res->fillPoints(data0, data1);
    }

    return res;
}

/* ########################################################################## */
VisualTracker::ResultType VisualTracker::process(ImagePtr& data0,
                                                 ImagePtr& data1) {
    auto res = process(*data0, *data1);
    res->pdata0 = data0;
    res->pdata1 = data1;
    return res;
}

/* ########################################################################## */
VisualTracker::Ptr VisualTracker::create(const std::string& name,
                                         const VisualTrackerParameter& param) {
    return getFactory(name)(param);
}

}  // namespace beacon
