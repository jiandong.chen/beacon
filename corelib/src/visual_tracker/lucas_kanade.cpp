#include "../../include/beacon/visual_tracker/lucas_kanade.h"

namespace beacon {

LucasKanadeTracker::LucasKanadeTracker(
    const LucasKanadeTrackerParameter& params) {
    setParameter(params);
}

void LucasKanadeTracker::setParameter(
    const LucasKanadeTrackerParameter& params) {
    m_max_pyramid_level = params.m_max_pyramid_level;
    m_flag = params.m_flag;
    m_min_eigen_threshold = params.m_min_eigen_threshold;
    m_win_size = cv::Size{params.m_win_size, params.m_win_size};
    m_criteria =
        cv::TermCriteria(cv::TermCriteria::COUNT + cv::TermCriteria::EPS,
                         params.m_max_iter,
                         params.m_iter_stop_eps);
}

void LucasKanadeTracker::setParameter(const VisualTrackerParameter& params) {
    if (params.type() != TrackerType::LucasKanade) {
        return;
    }
    setParameter(static_cast<const LucasKanadeTrackerParameter&>(params));
}

VisualTracker::ResultType LucasKanadeTracker::processImpl(ImageData& data0,
                                                          ImageData& data1) {
    // check all needed data availability
    if (data0.image.empty() || data0.feature == nullptr ||
        data0.feature->points.empty()) {
        return nullptr;
    }

    // TODO: currently assume data_2.feature empty
    data1.feature = std::make_shared<VisualFeatureData>();

    // using opencv implementation
    std::vector<cv::Point2f> kpt1 = data0.feature->uv2CV<float>();
    std::vector<cv::Point2f> kpt2;
    std::vector<uchar> status;
    std::vector<float> err;

    cv::calcOpticalFlowPyrLK(ImageData::getGrayImage(data0),
                             ImageData::getGrayImage(data1),
                             kpt1,
                             kpt2,
                             status,
                             err,
                             m_win_size,
                             m_max_pyramid_level,
                             m_criteria,
                             m_flag,
                             m_min_eigen_threshold);

    // get the new kps2 and matches by checking status
    auto& kpt2_refine = data1.feature->points;
    std::vector<VisualFeatureMatch> matches;
    for (size_t i = 0; i < status.size(); ++i) {
        if (status[i] == 1) {
            VisualFeaturePoint pt;
            pt.uv = {kpt2[i].x, kpt2[i].y};
            kpt2_refine.push_back(pt);
            VisualFeatureMatch m;
            m.idx0 = i;
            m.idx1 = kpt2_refine.size() - 1;
            m.score = err[i];
            matches.push_back(std::move(m));
        }
    }

    // return res
    auto res = ResultType::element_type::create();
    res->matches = std::move(matches);
    // res->fillPoints(data0, data1);

    return res;
}

}  // namespace beacon
