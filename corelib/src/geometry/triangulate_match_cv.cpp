#include "../../include/beacon/geometry/triangulate_match_cv.h"

#include "../../include/beacon/datatype/point_uvxy.h"
#include "../../include/beacon/util/cv_helper.h"

namespace beacon {

/* ##################### TriangulateMatchCV ################################### */
/* ########################################################################## */
std::vector<TriangulateMatch::PointType> TriangulateMatchCV::triangulate(
    const VisualFeatureMatchData& data,
    const Transform& T_bw) {
    assert(data.point0.size() == data.point1.size());

    // check if we have image data
    if (auto p0 = data.pdata0.lock(), p1 = data.pdata1.lock(); p0 && p1) {
        // we have image data, check if we have camera data
        if (!p0->camera || !p1->camera) {
            throw std::runtime_error("TriangulateMatch get empty cameras!");
        }

        // use undistorted normailzed pts
        auto pts0 = PointUVXY::xy2CV(data.point0.begin(), data.point0.end());
        auto pts1 = PointUVXY::xy2CV(data.point1.begin(), data.point1.end());

        // build T_cw using K == identity
        const auto proj_mat0 = buildCVMat34FromTransform(
            p0->camera->getTransformBC().getInverse() * T_bw);
        const auto proj_mat1 = buildCVMat34FromTransform(
            p1->camera->getTransformBC().getInverse() * T_bw);

        // call cv utility
        cv::Mat tmp;
        cv::triangulatePoints(proj_mat0, proj_mat1, pts0, pts1, tmp);

        // convert data back
        std::vector<TriangulateMatch::PointType> res(data.point0.size());
        for (int i = 0; i < tmp.cols; ++i) {
            const auto inv_w = 1.0 / tmp.at<float>(3, i);
            res[i].x() = inv_w * tmp.at<float>(0, i);
            res[i].y() = inv_w * tmp.at<float>(1, i);
            res[i].z() = inv_w * tmp.at<float>(2, i);
        }

        return res;

    } else {
        throw std::runtime_error(
            "TriangulateMatch get invalid image data pointers!");
    }
}

}  // namespace beacon
