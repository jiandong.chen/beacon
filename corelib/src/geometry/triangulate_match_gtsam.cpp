#include "../../include/beacon/geometry/triangulate_match_gtsam.h"

#include <gtsam/geometry/triangulation.h>

#include "../../include/beacon/datatype/point_uvxy.h"
#include "../../include/beacon/util/cv_helper.h"

namespace beacon {

/* ##################### TriangulateMatchCV ################################### */
/* ########################################################################## */
std::vector<TriangulateMatch::PointType> TriangulateMatchGTSAMDLT::triangulate(
    const VisualFeatureMatchData& data,
    const Transform& T_bw) {
    assert(data.point0.size() == data.point1.size());

    // check if we have image data
    if (auto p0 = data.pdata0.lock(), p1 = data.pdata1.lock(); p0 && p1) {
        // we have image data, check if we have camera data
        if (!p0->camera || !p1->camera) {
            throw std::runtime_error("TriangulateMatch get empty cameras!");
        }

        // use undistorted normailzed pts
        // build T_cw using K == identity
        const auto proj_mat0 =
            (p0->camera->getTransformBC().getInverse() * T_bw)
                .getTransformMat34();
        const auto proj_mat1 =
            (p1->camera->getTransformBC().getInverse() * T_bw)
                .getTransformMat34();
        using GTSAMCamProjMatVec =
            std::vector<gtsam::Matrix34,
                        Eigen::aligned_allocator<gtsam::Matrix34>>;
        GTSAMCamProjMatVec proj_mat_vec{proj_mat0, proj_mat1};

        // convert data back
        std::vector<TriangulateMatch::PointType> res(data.point0.size());
        for (std::size_t i = 0; i < data.point0.size(); ++i) {
            gtsam::Point2Vector tmp_pt_vec(2);
            tmp_pt_vec[0] = static_cast<gtsam::Point2>(data.point0[i].xy);
            tmp_pt_vec[1] = static_cast<gtsam::Point2>(data.point1[i].xy);
            res[i] = static_cast<TriangulateMatch::PointType>(
                gtsam::triangulateDLT(proj_mat_vec, tmp_pt_vec));
        }

        return res;

    } else {
        throw std::runtime_error(
            "TriangulateMatch get invalid image data pointers!");
    }
}

}  // namespace beacon
