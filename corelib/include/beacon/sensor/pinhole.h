#pragma once

#include "./camera.h"

namespace beacon {

class Pinhole : public CameraClone<Pinhole> {
  public:
    // shorthand for ptr
    using Ptr = std::shared_ptr<Pinhole>;

  public:
    // constrctor
    Pinhole() : Pinhole{1.0, 1.0, 0.0, 0.0, 0.0} {
    }

    Pinhole(double fx, double fy, double cx, double cy, double s = 0) {
        m_fx = fx;
        m_fy = fy;
        m_cx = cx;
        m_cy = cy;
        m_s = s;
    }

    BEACON_NODISCARD static Ptr create();
    BEACON_NODISCARD static Ptr create(double fx,
                                       double fy,
                                       double cx,
                                       double cy,
                                       double s = 0);

  public:
    void undistortKeypoint(VisualFeatureData& data) const override;
    void undistortImage(ImageData& data) const override;

};  // class Pinhole

using PinholePtr = Pinhole::Ptr;

}  // namespace beacon
