#pragma once
#include <opencv4/opencv2/opencv.hpp>

#include "../core/transform.h"
#include "camera.h"

namespace beacon {

class StereoPinhole {
  public:
    StereoPinhole(const CameraPtr& left,
                  const CameraPtr& right,
                  const int target_img_width = 0,
                  const int target_img_height = 0) {
        setCamPair(left, right, target_img_width, target_img_height);
    }

  public:
    void undistortRectifyImagePair(ImageData& img_left,
                                   ImageData& img_right) const;

  protected:
    bool setCamPair(const CameraPtr& left,
                    const CameraPtr& right,
                    const int target_img_width = 0,
                    const int target_img_height = 0);

    bool isValidPair() const {
        return m_left && m_right;
    }

  protected:
    CameraPtr m_left{};
    CameraPtr m_right{};

    Transform m_T_lr{};  // left to right cam transform

    cv::Mat m_left_R_ur{};   // rotation from unrectified to rectified
    cv::Mat m_right_R_ur{};  // rotation from unrectified to rectified
    cv::Mat m_left_P_ur{};   // project mat from unrectified to rectified
    cv::Mat m_right_P_ur{};  // project mat from unrectified to rectified

    cv::Mat m_left_remap1{};
    cv::Mat m_left_remap2{};
    cv::Mat m_right_remap1{};
    cv::Mat m_right_remap2{};

    cv::Mat m_Q{};  // disparity-to-depth mapping matrix
    cv::Rect m_left_roi{};
    cv::Rect m_right_roi{};

};  // class StereoPinhole

}  // namespace beacon
