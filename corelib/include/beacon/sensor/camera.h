#pragma once

#include <gtsam/geometry/Cal3DS2.h>
#include <gtsam/geometry/Cal3_S2.h>

#include <iostream>
#include <memory>
#include <opencv4/opencv2/opencv.hpp>

#include "../core/node.h"
#include "../core/transform.h"
#include "../datatype/visual_feature_data.h"

namespace beacon {

// Base Class for Cameras ptr
class Camera : NodeBase<Camera> {
  public:
    // shorthand for cameras
    using Ptr = std::shared_ptr<Camera>;

  public:
    // setter for data members

    Camera& setImageSize(int width = default_image_width,
                         int height = default_image_height) {
        m_image_width = width;
        m_image_height = height;
        return *this;
    }

    Camera& setTransformBC(Transform t) {
        m_T_bc = std::move(t);
        return *this;
    }

    template <typename Iter>
    Camera& setDistortion(Iter beg, Iter end);

    Camera& setDistortion(double k1,
                          double k2,
                          double p1,
                          double p2,
                          double k3 = 0);

  public:
    // getter for data members

    int getImageWidth() const {
        return m_image_width;
    }

    int getImageHeight() const {
        return m_image_height;
    }

    Transform getTransformBC() const {
        return m_T_bc;
    }

    double getFx() const {
        return m_fx;
    };

    double getFy() const {
        return m_fy;
    };

    double getCx() const {
        return m_cx;
    };

    double getCy() const {
        return m_cy;
    };

    double getS() const {
        return m_k1;
    };

    double getK2() const {
        return m_k2;
    };

    double getP1() const {
        return m_p1;
    };

    double getP2() const {
        return m_p2;
    };

    double getK3() const {
        return m_k3;
    };

  public:
    // Helper Function for GTSAM

    BEACON_NODISCARD auto getCal3S2() const {
        return gtsam::Cal3_S2(m_fx, m_fy, m_s, m_cx, m_cy);
    }

    BEACON_NODISCARD auto getCal3DS2() const {
        return gtsam::Cal3DS2(m_fx,
                              m_fy,
                              m_s,
                              m_cx,
                              m_cy,
                              m_k1,
                              m_k2,
                              m_p1,
                              m_p2);
    }

  public:
    // Helper Function for OpenCV

    BEACON_NODISCARD virtual cv::Mat getCVK() const;
    BEACON_NODISCARD virtual cv::Mat getCVDistortion() const;

    BEACON_NODISCARD static cv::Mat getCVIdentityK();

  public:
    // Helper Function using Eigen

    virtual Eigen::Matrix3d getIntrinsicEigen() const;

    virtual Eigen::Matrix<double, 3, 4> getProjMatEigen(
        const Transform& T_bw = Transform{}) const;

  public:
    // apply undistort operation on VisualFeaturePoint
    // Override the x and y field for each VisualFeaturePoint
    // override by derived camera class
    virtual void undistortKeypoint(VisualFeatureData& data) const = 0;

    // direct undistor the image
    virtual void undistortImage(ImageData& data) const = 0;

  public:
    // support print the camera data
    virtual void print(std::ostream& os) const;
    friend std::ostream& operator<<(std::ostream& os, const Camera& cam);

  protected:
    int m_image_width{default_image_width};    // Width of the Image
    int m_image_height{default_image_height};  // Height of the Image
    Transform m_T_bc{};  // Transform between Robot Base and Camera

    // focal length
    double m_fx{1.0};
    double m_fy{1.0};

    // principal point
    double m_cx{};
    double m_cy{};

    // skew
    double m_s{};

    // distortion
    double m_k1{};
    double m_k2{};

    double m_p1{};
    double m_p2{};

    double m_k3{};

  public:
    // default setting for data member
    inline static int default_image_width{2048};
    inline static int default_image_height{1080};

};  // class Camera

/* ##################### Camera ############################################# */
/* ########################################################################## */
template <typename Iter>
Camera& Camera::setDistortion(Iter beg, Iter end) {
    const auto sz = std::distance(beg, end);
    switch (sz) {
        case 0: {
            break;
        }
        case 4: {
            m_k1 = *beg++;
            m_k2 = *beg++;
            m_p1 = *beg++;
            m_p2 = *beg++;
            break;
        }
        case 5: {
            m_k1 = *beg++;
            m_k2 = *beg++;
            m_p1 = *beg++;
            m_p2 = *beg++;
            m_k3 = *beg++;
            break;
        }
        default: {
            throw std::runtime_error(
                "Pinhole Camera Get Unsupported Size Distortion Params: " +
                std::to_string(sz));
        }
    }
    return *this;
}

// shorthand for camera ptr
using CameraPtr = Camera::Ptr;

// shorthand for enabling clone() for cameras
template <typename Derived>
using CameraClone = NodeClone<Derived, Camera>;

}  // namespace beacon
