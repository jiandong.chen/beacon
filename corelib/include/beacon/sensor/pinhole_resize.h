#pragma once

#include "./pinhole.h"

namespace beacon {

class PinholeResize : public CameraClone<PinholeResize> {
  public:
    // shorthand for ptr
    using Ptr = std::shared_ptr<PinholeResize>;

  public:
    // constrctor
    PinholeResize(int old_img_width,
                  int old_img_height,
                  int new_img_width,
                  int new_img_height);

    PinholeResize(int old_img_width,
                  int old_img_height,
                  int new_img_width,
                  int new_img_height,
                  double fx,
                  double fy,
                  double cx,
                  double cy,
                  double s);

    PinholeResize(const Camera& cam, int new_img_width, int new_img_height);

    BEACON_NODISCARD static Ptr create(int old_img_width,
                                       int old_img_height,
                                       int new_img_width,
                                       int new_img_height);
    BEACON_NODISCARD static Ptr create(int old_img_width,
                                       int old_img_height,
                                       int new_img_width,
                                       int new_img_height,
                                       double fx,
                                       double fy,
                                       double cx,
                                       double cy,
                                       double s=0);

  public:
    // Helper Function using OpenCV

    BEACON_NODISCARD cv::Mat getCVK() const override;
    BEACON_NODISCARD cv::Mat getCVDistortion() const override;

  public:
    // Helper Function using Eigen

    Eigen::Matrix3d getIntrinsicEigen() const override;

    Eigen::Matrix<double, 3, 4> getProjMatEigen(
        const Transform& T_bw = Transform{}) const override;

    void undistortKeypoint(VisualFeatureData& data) const override;
    void undistortImage(ImageData& data) const override;

  protected:
    void calcNewIntrinsic();

  protected:
    void print(std::ostream& os) const override;

  protected:
    // new intrinsic after undistort the image

    int m_ori_img_height{};
    int m_ori_img_width{};

    double m_new_fx{};
    double m_new_fy{};

    double m_new_cx{};
    double m_new_cy{};

    cv::Rect m_roi{};

};  // class PinholeResize

using PinholeResizePtr = PinholeResize::Ptr;

}  // namespace beacon
