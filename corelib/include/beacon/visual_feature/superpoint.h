#pragma once
#ifdef BEACON_ENABLE_TORCH

#include <torch/torch.h>

#include "visual_feature_base.h"

namespace beacon {

struct SuperPointParameter
    : public VisualFeatureParameterClone<SuperPointParameter> {
    BEACON_NODISCARD FeatureType type() const noexcept override {
        return FeatureType::SuperPointTorch;
    };
    ADD_VISUAL_FEATURE_PARAM(use_cuda, bool, true, UseCUDA)
    ADD_VISUAL_FEATURE_PARAM(max_keypoint, int, 1000, MaxKeypoint)
    ADD_VISUAL_FEATURE_PARAM(remove_borders, int, 4, RemoveBorder)
    ADD_VISUAL_FEATURE_PARAM(nms_radius, int, 4, NMSRadius)
    ADD_VISUAL_FEATURE_PARAM(keypoint_threshold,
                             double,
                             0.01,
                             KeypointThreshold)
    std::string m_model_path{};

    auto setModelPath(const std::string& path) {
        m_model_path = path;
    }

};  // struct SuperPointParameter

class SuperPoint : public VisualFeatureClone<SuperPoint> {
  public:
    explicit SuperPoint(const SuperPointParameter& params = {});

  public:
    void processImpl(ImageData& data) override;

    void setParameter(const SuperPointParameter& params);
    void setParameter(const VisualFeatureParameter& params) override;

    VISUAL_FEATURE_DEFAULT_REGISTER("SuperPoint",
                                    SuperPoint,
                                    SuperPointParameter)

  protected:
    struct SuperPointNet;

  protected:
  protected:
    std::shared_ptr<SuperPointNet> m_net{};
    SuperPointParameter m_params{};
    torch::Device m_dev{torch::kCPU};

};  // class SuperPoint

}  // namespace beacon
#endif
