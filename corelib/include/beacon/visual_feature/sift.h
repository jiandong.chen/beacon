#pragma once

#include "visual_feature_base.h"

#if CV_MAJOR_VERSION < 3 ||                             \
    (CV_MAJOR_VERSION == 4 && CV_MINOR_VERSION <= 3) || \
    (CV_MAJOR_VERSION == 3 &&                           \
     (CV_MINOR_VERSION < 4 ||                           \
      (CV_MINOR_VERSION == 4 && CV_SUBMINOR_VERSION < 11)))
#include <opencv4/opencv2/xfeatures2d/nonfree.hpp>
using BEACON_SIFT = cv::xfeatures2d::SIFT;
#else
using BEACON_SIFT = cv::SIFT;
#endif

namespace beacon {

struct SIFTParameter : public VisualFeatureParameterClone<SIFTParameter> {
    BEACON_NODISCARD FeatureType type() const noexcept override {
        return FeatureType::SIFT;
    };

    ADD_VISUAL_FEATURE_PARAM(max_keypoint, int, 0, MaxKeypoint)
    ADD_VISUAL_FEATURE_PARAM(num_octave_layer, int, 3, NumOctaveLayer)

    // divided by m_num_octave_layer and the larger and less features
    ADD_VISUAL_FEATURE_PARAM(contrast_threshold,
                             double,
                             0.04,
                             ConstrastThreshold)
    ADD_VISUAL_FEATURE_PARAM(edge_threshold,
                             double,
                             10,
                             EdgeThreshold)  // the larger the more features
    ADD_VISUAL_FEATURE_PARAM(sigma, double, 1.6, Sigma)

};  // struct SIFTParameter

class SIFT : public VisualFeatureClone<SIFT> {
  public:
    explicit SIFT(const SIFTParameter& param = {});
    explicit SIFT(const VisualFeatureParameter& param);

  public:
    void processImpl(ImageData& data) override;

    void setParameter(const VisualFeatureParameter& params) override;
    void setParameter(const SIFTParameter& params);

  protected:
    cv::Ptr<BEACON_SIFT> m_sift;

    VISUAL_FEATURE_DEFAULT_REGISTER("SIFT", SIFT, SIFTParameter)

};  // clsss SIFT

}  // namespace beacon
