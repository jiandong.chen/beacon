#pragma once
#ifdef BEACON_ENABLE_TENSORRT

#include "visual_feature_base.h"

namespace beacon {

struct SuperPointTRTParameter
    : public VisualFeatureParameterClone<SuperPointTRTParameter> {
    BEACON_NODISCARD FeatureType type() const noexcept override {
        return FeatureType::SuperPointTRT;
    };
    ADD_VISUAL_FEATURE_PARAM(max_keypoint, int, 1000, MaxKeypoint)
    ADD_VISUAL_FEATURE_PARAM(remove_borders, int, 4, RemoveBorder)
    ADD_VISUAL_FEATURE_PARAM(nms_radius, int, 4, NMSRadius)
    ADD_VISUAL_FEATURE_PARAM(keypoint_threshold,
                             double,
                             0.01,
                             KeypointThreshold)
    ADD_VISUAL_FEATURE_PARAM(enable_host_descriptor,
                             bool,
                             false,
                             EnableHostDescriptor)

    std::string m_engine_path{};

    auto setModelPath(const std::string& path) {
        m_engine_path = path;
    }

};  // struct SuperPointTRTParameter

class SuperPointTRT : public VisualFeatureClone<SuperPointTRT> {
  public:
    explicit SuperPointTRT(const SuperPointTRTParameter& params = {});

  public:
    void processImpl(ImageData& data) override;

    void setParameter(const SuperPointTRTParameter& params);
    void setParameter(const VisualFeatureParameter& params) override;

  protected:
    struct Impl;
    std::unique_ptr<Impl> m_impl;

    VISUAL_FEATURE_DEFAULT_REGISTER("SuperPointTRT",
                                    SuperPointTRT,
                                    SuperPointTRTParameter)

};  // class SuperPointTRT

}  // namespace beacon
#endif
