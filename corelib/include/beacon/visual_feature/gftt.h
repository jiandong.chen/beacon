#pragma once

#include "visual_feature_base.h"

namespace beacon {

struct GFTTParameter : public VisualFeatureParameterClone<GFTTParameter> {
    BEACON_NODISCARD FeatureType type() const noexcept override {
        return FeatureType::GFTT;
    };

    ADD_VISUAL_FEATURE_PARAM(use_harris, bool, false, UseHarris)
    ADD_VISUAL_FEATURE_PARAM(max_keypoint, int, 1000, MaxKeypoint)
    ADD_VISUAL_FEATURE_PARAM(block_size, int, 3, BlockSize)
    ADD_VISUAL_FEATURE_PARAM(harris_k, double, 0.04, HarrisK)
    ADD_VISUAL_FEATURE_PARAM(quality_level, double, 0.01, QualityLevel)
    ADD_VISUAL_FEATURE_PARAM(min_keypoint_distance, double, 5.0, MinDistance)

};  // struct GFTTParameter

class GFTT : public VisualFeatureClone<GFTT> {
  public:
    explicit GFTT(const GFTTParameter& params = {});

  public:
    void processImpl(ImageData& data) override;

    void setParameter(const GFTTParameter& params);
    void setParameter(const VisualFeatureParameter& params) override;

  protected:
    bool m_use_harris{false};
    int m_max_keypoint{1000};
    int m_block_size{3};
    double m_quality_level{0.01};
    double m_min_keypoint_distance{5.0};
    double m_harris_k{0.04};

    cv::Ptr<cv::GFTTDetector> m_gftt;

    VISUAL_FEATURE_DEFAULT_REGISTER("GFTT", GFTT, GFTTParameter)

};  // class GoodFeatureDetector

}  // namespace beacon
