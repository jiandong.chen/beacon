#pragma once

#include <memory>
#include <opencv4/opencv2/opencv.hpp>

#include "../core/def.h"
#include "../core/factory.h"
#include "../core/node.h"
#include "../datatype/image_data.h"

#define ADD_VISUAL_FEATURE_PARAM(var_name, type, init_value, set_func_name) \
  public:                                                                   \
    auto set##set_func_name(type val) {                                     \
        m_##var_name = val;                                                 \
        return *this;                                                       \
    }                                                                       \
    type m_##var_name{init_value};

#define ADD_VISUAL_FEATURE_PARAM_REF_INPUT(var_name,      \
                                           type,          \
                                           init_value,    \
                                           set_func_name) \
  public:                                                 \
    auto set##func_name(const type& val) {                \
        m_##var_name = val;                               \
        return *this;                                     \
    }                                                     \
    type m_##var_name{init_value};

#define VISUAL_FEATURE_DEFAULT_FACTORY(TYPE, PARAM_TYPE)    \
    BEACON_DEFAULT_FACTORY_FUNCTION(VisualFeature,          \
                                    VisualFeatureParameter, \
                                    TYPE,                   \
                                    PARAM_TYPE)

#define VISUAL_FEATURE_DEFAULT_REGISTER(NAME, TYPE, PARAM_TYPE) \
    BEACON_DEFAULT_REGISTER_HELPER(                             \
        std::string,                                            \
        NAME,                                                   \
        VISUAL_FEATURE_DEFAULT_FACTORY(TYPE, PARAM_TYPE))

namespace beacon {

// Base Class for Visual Feature Extraction Parameter Class
// The Parameter data are used for create the specific algo
// and (re-)set the parameter
struct VisualFeatureParameter : public NodeBase<VisualFeatureParameter> {
    // add custom Feature Extraction type to def.h
    // then override type()
    BEACON_NODISCARD virtual FeatureType type() const noexcept = 0;
};  // struct VisualFeatureParameter

// shorthand
// for implement clone() for VisualFeatureParameter
template <typename Derived>
using VisualFeatureParameterClone = NodeClone<Derived, VisualFeatureParameter>;

// Base Class for Visual Feature Extraction Class
class VisualFeature
    : public NodeBase<VisualFeature>,
      public FactoryBase<std::string,
                         std::function<std::unique_ptr<VisualFeature>(
                             const VisualFeatureParameter&)>> {
  public:
    // shorthand
    using Ptr = std::unique_ptr<VisualFeature>;

  public:
    // process the ImageData
    // pre-process + processImpl + post-process
    // VisualFeatureData is override
    void process(ImageData& data);

    // set parameter for specific algorithm
    // derived class should create Parameter Type by deriving
    // VisualFeatureParameter override by derived algorithm
    virtual void setParameter(const VisualFeatureParameter& params) = 0;

    // virtual void setDescriptor(FeatureType type) = 0;

  protected:
    // actual feature process
    // override by derived algorithm
    virtual void processImpl(ImageData& data) = 0;

  public:
    // factory
    static Ptr create(const std::string& idx,
                      const VisualFeatureParameter& param);

};  // class VisualFeature

// shorthand
// for implement clone() for VisualFeatureClone
template <typename Derived>
using VisualFeatureClone = NodeClone<Derived, VisualFeature>;

using VisualFeaturePtr = VisualFeature::Ptr;

}  // namespace beacon
