#pragma once

namespace beacon::cuda {

template <typename DataType>
constexpr DataType computeGridSz1D(DataType total_sz, DataType blk_sz) {
    return (total_sz + blk_sz - 1) / blk_sz;
}

}  // namespace beacon::cuda
