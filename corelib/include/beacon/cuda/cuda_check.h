#pragma once
#include <iostream>

#define BEACON_CUDA_CHECK_LAST                                              \
    do {                                                                    \
        auto err = cudaGetLastError();                                      \
        if (err != cudaSuccess) {                                           \
            std::cout << "CUDA Check Last Failed at File: " << __FILE__     \
                      << " Line: " << __LINE__ << std::endl;                \
            std::cout << "ERROR: " << cudaGetErrorString(err) << std::endl; \
            exit(1);                                                        \
        }                                                                   \
    } while (0);

#define BEACON_CUDA_CHECK(x)                                             \
    if (x != cudaSuccess) {                                              \
        std::cout << "CUDA API Failed at File: " << __FILE__             \
                  << " Line: " << __LINE__ << std::endl;                 \
        std::cout << "ERROR: " << cudaGetErrorString(cudaGetLastError()) \
                  << std::endl;                                          \
        exit(1);                                                         \
    }

#define BEACON_TENSORRT_CHECK(x)                                 \
    if (x != true) {                                             \
        std::cout << "TensorRT API Failed at File: " << __FILE__ \
                  << " Line: " << __LINE__ << std::endl;         \
        exit(1);                                                 \
    }
