#include <iostream>

#include "NvInfer.h"

namespace beacon::tensorrt {

class BeaconTRTLogger : public nvinfer1::ILogger {
    void log(Severity severity, const char* msg) noexcept override {
        // suppress info-level messages
        if (severity <= Severity::kWARNING)
            std::cout << msg << std::endl;
    }
};

}  // namespace beacon
