#pragma once
#include <opencv4/opencv2/opencv.hpp>

#include "cuda_buffer.h"

namespace beacon::cuda {

template <typename DataType>
class BufferCV {
  public:
    BufferCV() = default;

    BufferCV(const cv::Mat& input) : m_input(input) {
    }

    BufferCV(std::size_t num_elem) {
        resize(num_elem);
    }

    void copy(const cv::Mat& input,
              cudaStream_t stream = 0,
              bool async = true) {
        if (input.elemSize() != sizeof(DataType)) {
            throw std::runtime_error("BufferCV: The ElemSize Unmatched!!!");
        }
        if (static_cast<std::size_t>(input.rows * input.cols) != getSize()) {
            throw std::runtime_error(
                "BufferCV: The Buffer size and Input Size Unmatched!!!");
        }

        constexpr auto mode = cudaMemcpyHostToDevice;
        const auto num_byte = getSize() * sizeof(DataType);
        const auto src = input.data;
        const auto dst = m_data.getData();

        if (async) {
            BEACON_CUDA_CHECK(
                cudaMemcpyAsync(dst, src, num_byte, mode, stream));
        } else {
            BEACON_CUDA_CHECK(cudaMemcpy(dst, src, num_byte, mode));
        }

        m_input = input;
    }

    void copy(cudaStream_t stream = 0, bool async = true) {
        if (m_input.empty()) {
            throw std::runtime_error("BufferCV: Calling copy without input!");
        }
        copy(m_input, stream, async);
    }

    auto freeData() {
        m_data.freeData();
    }

    auto resize(std::size_t num_elem) {
        m_data.resize(num_elem);
    }

    auto isEmpty() const {
        return getSize() == 0;
    }

    auto getInputMat() const {
        return m_input;
    }

    auto getSize() const {
        return m_data.getSize();
    }

    auto getCapacity() const {
        return m_data.getCapacity();
    }

    auto getDeviceData() const {
        return m_data.getData();
    }

  protected:
    cv::Mat m_input{};
    DeviceBuffer<DataType> m_data;

};  // DeviceOnlyBuffer

}  // namespace beacon::cuda
