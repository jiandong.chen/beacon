#pragma once
#include <cuda_runtime_api.h>

#include <cstddef>
#include <cstdlib>

#include "cuda_check.h"

namespace beacon::cuda {

/* ##################### Allocator & Free ################################### */
/* ########################################################################## */
struct DeviceAllocator {
    void operator()(void** ptr, std::size_t num_byte) {
        BEACON_CUDA_CHECK(cudaMalloc(ptr, num_byte));
    }

};  // struct AllocateDevice

/* ########################################################################## */
struct DeviceFree {
    void operator()(void* ptr) {
        BEACON_CUDA_CHECK(cudaFree(ptr));
    }

};  // struct DeviceFree

/* ########################################################################## */
struct UnifiedAllocator {
    void operator()(void** ptr, std::size_t num_byte) {
        BEACON_CUDA_CHECK(cudaMallocManaged(ptr, num_byte));
    }

};  // struct UnifiedAllocator

/* ########################################################################## */
struct UnifiedFree : public DeviceFree {};  // struct UnifiedFree

/* ########################################################################## */
struct HostAllocator {
    void operator()(void** ptr, std::size_t num_byte) {
        *ptr = std::malloc(num_byte);
        if (*ptr == nullptr) {
            throw std::runtime_error("HostAllocator Allocate Failed!");
        }
    }
};  // struct HostAllocator

/* ########################################################################## */
struct HostFree {
    void operator()(void* ptr) {
        std::free(ptr);
    }

};  // struct HostFree

/* ########################################################################## */
struct HostPinnedAllocator {
    void operator()(void** ptr, std::size_t num_byte) {
        BEACON_CUDA_CHECK(cudaMallocHost(ptr, num_byte));
    }

};  // struct HostPinnedAllocator

/* ########################################################################## */
struct HostPinnedFree {
    void operator()(void* ptr) {
        BEACON_CUDA_CHECK(cudaFreeHost(ptr));
    }

};  // struct DeviceFree

/* ##################### BufferBase ######################################### */
/* ########################################################################## */
template <typename DataType, typename Alloc, typename FreeFunc>
class BufferBase {
  public:
    BufferBase() = default;

    BufferBase(std::size_t num_elem) {
        resize(num_elem);
    }

    BufferBase(const BufferBase&) = delete;
    BufferBase& operator=(const BufferBase&) = delete;

    BufferBase(BufferBase&& rhs) {
        *this = std::move(rhs);
    }

    BufferBase& operator=(BufferBase&& rhs) {
        if (&rhs == this) {
            return *this;
        }

        rhs.m_alloc = std::move(this->m_alloc);
        rhs.m_free = std::move(this->m_free);

        rhs.m_data = this->m_data;
        m_data = nullptr;

        rhs.m_size = this->m_size;
        m_size = 0;
        rhs.m_capacity = this->m_capacity;
        m_capacity = 0;
    }

    ~BufferBase() {
        freeData();
    }

    auto resize(std::size_t num_elem) {
        if (num_elem > getCapacity()) {
            // resize invalidate the data
            freeData();
            int num_byte = num_elem * sizeof(DataType);
            m_alloc((void**)&m_data, num_byte);
            m_size = num_elem;
            m_capacity = num_elem;
        } else {
            m_size = num_elem;
        }
    }

    auto freeData() {
        if (m_data != nullptr) {
            m_free(m_data);
            m_data = nullptr;
        }
        m_size = 0;
        m_capacity = 0;
    }

    auto isEmpty() const {
        return getSize() == 0;
    }

    auto getSize() const {
        return m_size;
    }

    auto getCapacity() const {
        return m_capacity;
    }

    auto getData() const {
        return m_data;
    }

  protected:
    Alloc m_alloc{};
    FreeFunc m_free{};

    std::size_t m_size{0};
    std::size_t m_capacity{0};

    DataType* m_data{nullptr};

};  // class BufferBase

/* ##################### Buffer Alias ####################################### */
/* ########################################################################## */
template <typename DataType>
using DeviceBuffer = BufferBase<DataType, DeviceAllocator, DeviceFree>;

/* ########################################################################## */
template <typename DataType>
using UnifiedBuffer = BufferBase<DataType, UnifiedAllocator, UnifiedFree>;

/* ########################################################################## */
template <typename DataType>
using HostBuffer = BufferBase<DataType, HostAllocator, HostFree>;

/* ########################################################################## */
template <typename DataType>
using HostPinnedBuffer =
    BufferBase<DataType, HostPinnedAllocator, HostPinnedFree>;

/* ##################### Buffer ############################################# */
/* ########################################################################## */
template <typename DataType,
          template <typename>
          typename DevBufferType,
          template <typename>
          typename HostBufferType>
class Buffer {
  public:
    Buffer() = default;

    Buffer(std::size_t num_elem) {
        resize(num_elem);
    }

    auto freeData() {
        m_device.freeData();
        m_host.freeData();
        m_size = 0;
        m_capacity = 0;
    }

    auto resize(std::size_t num_elem) {
        if (num_elem > getCapacity()) {
            // resize invalidate the data
            m_device.resize(num_elem);
            m_host.resize(num_elem);
            m_size = num_elem;
            m_capacity = num_elem;
        } else {
            m_size = num_elem;
        }
    }

    auto isEmpty() const {
        return getSize() == 0;
    }

    auto getSize() const {
        return m_size;
    }

    auto getCapacity() const {
        return m_capacity;
    }

    auto getHostData() const {
        return m_host.getData();
    }

    auto getDeviceData() const {
        return m_device.getData();
    }

    void moveMemory(bool host2dev, cudaStream_t stream = 0, bool async = true) {
        auto src = host2dev ? m_host.getData() : m_device.getData();
        auto dst = host2dev ? m_device.getData() : m_host.getData();
        auto mode = host2dev ? cudaMemcpyHostToDevice : cudaMemcpyDeviceToHost;

        int num_byte = getSize() * sizeof(DataType);
        if (async) {
            BEACON_CUDA_CHECK(
                cudaMemcpyAsync(dst, src, num_byte, mode, stream));
        } else {
            BEACON_CUDA_CHECK(cudaMemcpy(dst, src, num_byte, mode));
        }
    }

    void moveToDevice(const cudaStream_t& stream = 0, bool async = true) {
        moveMemory(true, stream, async);
    };

    void moveToHost(const cudaStream_t& stream = 0, bool async = true) {
        moveMemory(false, stream, async);
    };

  protected:
    DevBufferType<DataType> m_device{};
    HostBufferType<DataType> m_host{};

    std::size_t m_size{0};
    std::size_t m_capacity{0};

};  // class Buffer

/* ##################### Normal Buffer ###################################### */
/* ########################################################################## */
template <typename DataType>
using NormalBuffer = Buffer<DataType, DeviceBuffer, HostBuffer>;

/* ##################### Pinned Buffer ###################################### */
/* ########################################################################## */
template <typename DataType>
using PinnedBuffer = Buffer<DataType, DeviceBuffer, HostPinnedBuffer>;

// Ref: https://developer.nvidia.com/blog/how-optimize-data-transfers-cuda-cc/

/* ##################### Unified Buffer ##################################### */
/* ########################################################################## */
template <typename DataType, template <typename> typename UnUsedHostBuffer>
class Buffer<DataType, UnifiedBuffer, UnUsedHostBuffer> {
  public:
    Buffer() = default;

    Buffer(std::size_t num_elem) {
        resize(num_elem);
    }

    auto freeData() {
        m_unified_data.freeData();
        m_size = 0;
        m_capacity = 0;
    }

    auto resize(std::size_t num_elem) {
        if (num_elem > getCapacity()) {
            // resize invalidate the data
            m_unified_data.resize(num_elem);
            m_size = num_elem;
            m_capacity = num_elem;
        } else {
            m_size = num_elem;
        }
    }

    auto isEmpty() const {
        return getSize() == 0;
    }

    auto getSize() const {
        return m_size;
    }

    auto getCapacity() const {
        return m_capacity;
    }

    auto getHostData() const {
        return m_unified_data.getData();
    }

    auto getDeviceData() const {
        return m_unified_data.getData();
    }

    void moveMemory(bool, cudaStream_t = 0, bool = true) {
        // do nothing
    }

    void moveToDevice(const cudaStream_t& = 0, bool = true) {
        // do nothing
    };
    void moveToHost(const cudaStream_t& = 0, bool = true) {
        // do nothing
    };

  protected:
    UnifiedBuffer<DataType> m_unified_data{};

    std::size_t m_size{0};
    std::size_t m_capacity{0};

};  // class Buffer<DataType, UnifiedBuffer, UnUsedHostBuffer>

}  // namespace beacon::cuda
