#pragma once

#include <cstdint>

#define BEACON_NODISCARD [[nodiscard]]
#define BEACON_ENUM_UNDERLYING std::uint64_t

namespace beacon {

enum class FeatureType {
    GFTT = 0,  // Good Feature To Track
    SIFT,
#ifdef BEACON_ENABLE_TORCH
    SuperPointTorch,
#endif
#ifdef BEACON_ENABLE_TENSORRT
    SuperPointTRT,
#endif
    Unknown,
};  // enum FeatureType

enum class TrackerType {
    LucasKanade = 0,
    BruteForce,
    FLANN,
#ifdef BEACON_ENABLE_TENSORRT
    SuperGlueTRT,
    LightGlueTRT,
#endif
    Unknown,
};  // enum TrackerType

}  // namespace beacon
