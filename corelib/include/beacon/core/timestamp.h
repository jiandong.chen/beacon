#pragma once

#include <cmath>
#include <cstdint>
#include <iostream>
#include <ratio>
#include <stdexcept>
#include <type_traits>
#include <utility>

#include "def.h"

namespace beacon {

class Timestamp {
    friend class Duration;

  public:
    struct Sec_t {
    } inline static constexpr set_sec{};

    struct NanoSec_t {
    } inline static constexpr set_nanosec{};

  public:
    // set default time stamp to 0;
    Timestamp() = default;

    // default set timestamp in second
    template <typename T>
    explicit Timestamp(T timestamp) : Timestamp(timestamp, set_sec) {
    }

    // set timestamp in second
    template <typename T,
              std::enable_if_t<std::is_convertible_v<T, double>, int> = 0>
    Timestamp(T timestamp, Sec_t);

    // set timestamp in nanosecond
    template <typename T,
              std::enable_if_t<std::is_convertible_v<T, uint64_t>, int> = 0>
    Timestamp(T timestamp, NanoSec_t);

    // set timestamp by sec and nanosec component
    template <typename T, std::enable_if_t<std::is_integral_v<T>, int> = 0>
    Timestamp(T sec, T nano_sec);

    // comparison
    BEACON_NODISCARD friend bool operator<(const Timestamp& lhs,
                                           const Timestamp& rhs) noexcept {
        return lhs.m_second_part < rhs.m_second_part ||
               (lhs.m_second_part == rhs.m_second_part &&
                lhs.m_nano_second_part < rhs.m_nano_second_part);
    }

    BEACON_NODISCARD friend bool operator>(const Timestamp& lhs,
                                           const Timestamp& rhs) noexcept {
        return rhs < lhs;
    }

    BEACON_NODISCARD friend bool operator<=(const Timestamp& lhs,
                                            const Timestamp& rhs) noexcept {
        return !(rhs < lhs);
    }

    BEACON_NODISCARD friend bool operator>=(const Timestamp& lhs,
                                            const Timestamp& rhs) noexcept {
        return !(rhs > lhs);
    }

    BEACON_NODISCARD friend bool operator==(const Timestamp& lhs,
                                            const Timestamp& rhs) noexcept {
        return !((lhs < rhs) || (rhs < lhs));
    }

    BEACON_NODISCARD friend bool operator!=(const Timestamp& lhs,
                                            const Timestamp& rhs) noexcept {
        return !(rhs == lhs);
    }

  public:
    // getter
    BEACON_NODISCARD double getSec() const noexcept {
        return static_cast<double>(m_nano_second_part) / 1e9 +
               static_cast<double>(m_second_part);
    }

    BEACON_NODISCARD std::pair<uint64_t, uint64_t> getData() const {
        return {m_second_part, m_nano_second_part};
    }

  public:
    // printer
    std::string to_string() const {
        return std::to_string(getSec()) + " Seconds (" +
               std::to_string(m_second_part) + " Seconds and " +
               std::to_string(m_nano_second_part) + " Nanoseconds)";
    }

    friend std::ostream& operator<<(std::ostream& os, const Timestamp& t) {
        os << t.to_string();
        return os;
    }

  protected:
    void checkValidData() {
        m_second_part += m_nano_second_part / sec_to_nanosec;
        m_nano_second_part = m_nano_second_part % sec_to_nanosec;
    }

  protected:
    static constexpr uint64_t sec_to_nanosec{std::giga::num};
    uint64_t m_second_part{};
    uint64_t m_nano_second_part{};

};  // class Timestamp

/* ##################### Timestamp ########################################## */
/* ########################################################################## */
template <typename T, std::enable_if_t<std::is_convertible_v<T, double>, int>>
Timestamp::Timestamp(T timestamp, Sec_t) {
    if (static_cast<double>(timestamp) < 0.0) {
        throw std::runtime_error("Cannot set negative timestamp!");
    }

    if (static_cast<double>(timestamp) >
        static_cast<double>(std::numeric_limits<uint64_t>::max())) {
        throw std::runtime_error("Cannot store too large value as Timestamp!");
    }

    double integral;
    double decimal = std::modf(static_cast<double>(timestamp), &integral);

    m_second_part = static_cast<uint64_t>(integral);
    m_nano_second_part = static_cast<uint64_t>(decimal * sec_to_nanosec);
    checkValidData();
}

/* ########################################################################## */
template <typename T, std::enable_if_t<std::is_convertible_v<T, uint64_t>, int>>
Timestamp::Timestamp(T timestamp, NanoSec_t) {
    if (timestamp < 0) {
        throw std::runtime_error("Cannot set negative timestamp!");
    }
    m_nano_second_part = static_cast<uint64_t>(timestamp);
    checkValidData();
}

/* ########################################################################## */
template <typename T, std::enable_if_t<std::is_integral_v<T>, int>>
Timestamp::Timestamp(T sec, T nano_sec) {
    if (sec < 0 || nano_sec < 0) {
        throw std::runtime_error("Cannot set negative timestamp!");
    }
    m_second_part = sec;
    m_nano_second_part = nano_sec;
    checkValidData();
}

}  // namespace beacon
