#pragma once

#include "def.h"
#include "timestamp.h"

namespace beacon {

class Duration {
  public:
    Duration(const Timestamp& t1, const Timestamp& t2) {
        if (t1 > t2) {
            setDataHelper(t1, t2);
        } else {
            setDataHelper(t2, t1);
        }
    }

    BEACON_NODISCARD friend bool operator<(const Duration& lhs,
                                           const Duration& rhs) noexcept {
        return lhs.m_second_part < rhs.m_second_part ||
               (lhs.m_second_part == rhs.m_second_part &&
                lhs.m_nano_second_part < rhs.m_nano_second_part);
    }

    BEACON_NODISCARD friend bool operator>(const Duration& lhs,
                                           const Duration& rhs) noexcept {
        return rhs < lhs;
    }

    BEACON_NODISCARD friend bool operator<=(const Duration& lhs,
                                            const Duration& rhs) noexcept {
        return !(rhs < lhs);
    }

    BEACON_NODISCARD friend bool operator>=(const Duration& lhs,
                                            const Duration& rhs) noexcept {
        return !(rhs > lhs);
    }

    BEACON_NODISCARD friend bool operator==(const Duration& lhs,
                                            const Duration& rhs) noexcept {
        return !((lhs < rhs) || (rhs < lhs));
    }

    BEACON_NODISCARD friend bool operator!=(const Duration& lhs,
                                            const Duration& rhs) noexcept {
        return !(rhs == lhs);
    }

    BEACON_NODISCARD double getSec() const noexcept {
        return static_cast<double>(m_nano_second_part) / 1e9 +
               static_cast<double>(m_second_part);
    }

    BEACON_NODISCARD std::pair<uint64_t, uint64_t> getData() const noexcept {
        return {m_second_part, m_nano_second_part};
    }

  protected:
    void setDataHelper(const Timestamp& large_t, const Timestamp& small_t) {
        m_second_part = large_t.m_second_part - small_t.m_second_part;
        if (large_t.m_nano_second_part < small_t.m_nano_second_part) {
            // second part cant be zero
            m_second_part -= 1;
            m_nano_second_part = Timestamp::sec_to_nanosec -
                                 small_t.m_nano_second_part +
                                 large_t.m_nano_second_part;
        } else {
            m_nano_second_part =
                large_t.m_nano_second_part - small_t.m_nano_second_part;
        }
    }

  protected:
    uint64_t m_second_part{};
    uint64_t m_nano_second_part{};

};  // class Duration

BEACON_NODISCARD inline Duration operator-(Timestamp t1, Timestamp t2) {
    return Duration{t1, t2};
}

}  // namespace beacon
