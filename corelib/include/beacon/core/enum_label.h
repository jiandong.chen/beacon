#pragma once

#include <algorithm>
#include <initializer_list>
#include <memory>
#include <type_traits>

#include "def.h"

namespace beacon {

template <
    typename E,
    std::enable_if_t<std::is_same_v<std::underlying_type_t<std::decay_t<E>>,
                                    BEACON_ENUM_UNDERLYING>,
                     int> = 0>
class LabelHandler {
  public:
    using Ptr = std::shared_ptr<LabelHandler>;
    using LabelEnum = std::decay_t<E>;
    using Underlying = std::underlying_type_t<LabelEnum>;

  public:
    // modify

    auto set(LabelEnum label) {
        m_data |= underlying(label);

        return *this;
    }

    auto set(std::initializer_list<LabelEnum> labels) {
        std::for_each(labels.begin(), labels.end(), [this](const LabelEnum s) {
            m_data.set(s);
        });
        return *this;
    }

    auto unset(LabelEnum label) {
        m_data = (~underlying(label)) & underlying(label);

        return *this;
    }

    auto unset(std::initializer_list<LabelEnum> labels) {
        std::for_each(labels.begin(), labels.end(), [this](const LabelEnum s) {
            m_data.unset(s);
        });
        return *this;
    }

    auto reset() {
        m_data = Underlying{};

        return *this;
    }

  public:
    // check

    bool is(LabelEnum label) const {
        return static_cast<bool>(m_data & underlying(label));
    }

    bool all(std::initializer_list<LabelEnum> labels) const {
        return std::all_of(labels.begin(), labels.end(), [this](LabelEnum s) {
            return this->is(s);
        });
    }

    bool any(std::initializer_list<LabelEnum> labels) const {
        return std::any_of(labels.begin(), labels.end(), [this](LabelEnum s) {
            return this->is(s);
        });
    }

    bool none(std::initializer_list<LabelEnum> labels) const {
        return std::none_of(labels.begin(), labels.end(), [this](LabelEnum s) {
            return this->is(s);
        });
    }

    auto count(std::initializer_list<LabelEnum> labels) const {
        return std::count_if(labels.begin(), labels.end(), [this](LabelEnum s) {
            return this->is(s);
        });
    }

  public:
    // converter
    static auto underlying(LabelEnum label) {
        return static_cast<std::underlying_type_t<LabelEnum>>(label);
    }

    auto underlying() const {
        return m_data;
    }

  public:
    // constructor
    LabelHandler() = default;

    explicit LabelHandler(std::underlying_type_t<LabelEnum> val) : m_data{val} {
    }

  protected:
    Underlying m_data;

};  // class LabelHandler<E>

template <typename LabelEnumType,
          typename DataType,
          typename GetterType,
          typename std::enable_if_t<std::is_reference_v<std::invoke_result_t<
                                        GetterType,
                                        std::remove_reference_t<DataType>&>>,
                                    int> = 0>
class LabelAdaptor {
  public:
    using Data = std::remove_reference_t<DataType>;
    using Getter = GetterType;
    using LabelEnum = LabelEnumType;
    using Label = LabelHandler<LabelEnum>;
    using UnderlyingType = std::underlying_type_t<LabelEnum>;

  public:
    // constructor
    LabelAdaptor(Data& data, GetterType getter = {})
        : m_data{data}, m_getter{getter} {
    }

  public:
    // modify

    auto set(LabelEnum label) {
        m_getter(m_data) = toUnderlying(toLabel(m_getter(m_data)).set(label));

        return *this;
    }

    auto set(std::initializer_list<LabelEnum> labels) {
        std::for_each(labels.begin(), labels.end(), [this](const LabelEnum s) {
            this->set(s);
        });
        return *this;
    }

    auto unset(LabelEnum label) {
        m_getter(m_data) = toUnderlying(toLabel(m_getter(m_data)).unset(label));

        return *this;
    }

    auto unset(std::initializer_list<LabelEnum> labels) {
        std::for_each(labels.begin(), labels.end(), [this](const LabelEnum s) {
            this->unset(s);
        });
        return *this;
    }

    auto reset() {
        m_getter(m_data) = toUnderlying(toLabel(m_getter(m_data)).reset());

        return *this;
    }

  public:
    // check

    bool is(LabelEnum label) const {
        return toLabel(m_getter(m_data)).is(label);
    }

    bool all(std::initializer_list<LabelEnum> labels) const {
        return toLabel(m_getter(m_data)).all(std::move(labels));
    }

    bool any(std::initializer_list<LabelEnum> labels) const {
        return toLabel(m_getter(m_data)).any(std::move(labels));
    }

    bool none(std::initializer_list<LabelEnum> labels) const {
        return toLabel(m_getter(m_data)).none(std::move(labels));
    }

    auto count(std::initializer_list<LabelEnum> labels) const {
        return toLabel(m_getter(m_data)).count(std::move(labels));
    }

  protected:
    static Label toLabel(UnderlyingType data) {
        return Label{data};
    }

    static UnderlyingType toUnderlying(Label data) {
        return data.underlying();
    }

  protected:
    Data& m_data;
    GetterType m_getter;

};  // class LabelAdaptor

}  // namespace beacon
