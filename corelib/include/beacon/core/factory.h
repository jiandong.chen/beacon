#pragma once
#include <stdexcept>
#include <string>
#include <unordered_map>

#define BEACON_DEFAULT_FACTORY_FUNCTION(BASE_TYPE,             \
                                        BASE_PARAM_TYPE,       \
                                        TYPE,                  \
                                        PARAM_TYPE)            \
    [](const BASE_PARAM_TYPE& param) {                         \
        return std::unique_ptr<BASE_TYPE>(                     \
            new TYPE(dynamic_cast<const PARAM_TYPE&>(param))); \
    }

#define BEACON_DEFAULT_REGISTER_HELPER(IDX_TYPE, IDX_VALUE, FUNC)          \
  public:                                                                  \
    inline static IDX_TYPE register_idx{IDX_VALUE};                        \
                                                                           \
  private:                                                                 \
    struct RegisterHelper {                                                \
        RegisterHelper() {                                                 \
            auto res = getRegistry().try_emplace(IDX_VALUE, FUNC);         \
            if (!res.second) {                                             \
                throw std::runtime_error(                                  \
                    "Get Same Register Index!! Please Check All Register " \
                    "Name");                                               \
            }                                                              \
        }                                                                  \
    } inline static reg_helper{};

namespace beacon {

template <typename IndexType, typename FactoryType>
class FactoryBase {
  public:
    using FactoryIndex = IndexType;
    using Factory = FactoryType;
    using FactoryRegistry = std::unordered_map<FactoryIndex, Factory>;

  public:
    static bool isRegister(const FactoryIndex& idx) {
        if (getRegistry().find(idx) == getRegistry().end()) {
            return false;
        }
        return true;
    }

    static void registerFactory(const FactoryIndex& idx,
                                const Factory& factory) {
        getRegistry()[idx] = factory;
    }

    static Factory getFactory(const FactoryIndex& idx) {
        if (auto it = getRegistry().find(idx); it == getRegistry().end()) {
            throw std::runtime_error("Request non-existent Factory!");
        } else {
            return it->second;
        }
    }

  protected:
    static FactoryRegistry& getRegistry() {
        static FactoryRegistry reg;
        return reg;
    }
};  // class FactoryBase

}  // namespace beacon
