#pragma once

#include <gtsam/geometry/Point3.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/geometry/Rot3.h>

#include <Eigen/Eigen>
#include <opencv4/opencv2/opencv.hpp>

#include "def.h"

namespace beacon {

class Transform {
  public:
    // constructor
    Transform()
        : m_translation{Eigen::Vector3d::Zero()},
          m_rotation{Eigen::Quaterniond::Identity()} {
    }

    Transform(Eigen::Vector3d translation, Eigen::Quaterniond rotation)
        : m_translation{std::move(translation)},
          m_rotation{std::move(rotation)} {
        normalizeRotation();
    }

    Transform(Eigen::Vector3d translation)
        : Transform{std::move(translation), Eigen::Quaterniond::Identity()} {
    }

    Transform(Eigen::Quaterniond rotation)
        : Transform{Eigen::Vector3d::Zero(), std::move(rotation)} {
    }

    Transform(double x, double y, double z)
        : Transform(Eigen::Vector3d{x, y, z}) {
    }

    Transform(double qw, double qx, double qy, double qz)
        : Transform(Eigen::Quaterniond{qw, qx, qy, qz}) {
    }

    Transform(double x,
              double y,
              double z,
              double qw,
              double qx,
              double qy,
              double qz)
        : Transform(Eigen::Vector3d{x, y, z},
                    Eigen::Quaterniond{qw, qx, qy, qz}) {
    }

    // getter
    BEACON_NODISCARD Eigen::Vector3d getTranslation() const {
        return m_translation;
    }

    BEACON_NODISCARD Eigen::Quaterniond getRotation() const {
        return m_rotation;
    }

    BEACON_NODISCARD Eigen::Matrix3d getRotationMatrix() const {
        return getRotation().toRotationMatrix();
    }

    BEACON_NODISCARD Transform getInverse() const {
        return Transform{-(m_rotation.inverse() * m_translation),
                         m_rotation.inverse()};
    }

    // setter
    Transform& setTranslation(const Eigen::Vector3d& translation) {
        m_translation = translation;
        return *this;
    }

    Transform& setRotation(const Eigen::Quaterniond& rotation) {
        m_rotation = rotation.normalized();
        return *this;
    }

    // operator
    BEACON_NODISCARD friend Transform operator*(const Transform& lhs,
                                                const Transform& rhs) {
        Eigen::Vector3d new_t =
            lhs.m_rotation * rhs.m_translation + lhs.m_translation;
        Eigen::Quaterniond new_rot = lhs.m_rotation * rhs.m_rotation;

        return Transform{new_t, new_rot};
    }

    template <typename T>
    BEACON_NODISCARD friend Eigen::Matrix<T, 3, 1> operator*(
        const Transform& lhs,
        const Eigen::Matrix<T, 3, 1>& rhs) {
        return lhs.m_rotation * rhs + lhs.m_translation;
    }

  public:
    // Helper Function for OpenCV

    cv::Mat getMat44CV() const {
        auto rot_mat = getRotationMatrix();
        auto translate = getTranslation();
        cv::Mat res = cv::Mat::eye(4, 4, CV_64F);

        for (int i = 0; i < 3; ++i) {
            res.at<double>(i, 0) = rot_mat.coeff(i, 0);
            res.at<double>(i, 1) = rot_mat.coeff(i, 1);
            res.at<double>(i, 2) = rot_mat.coeff(i, 2);
            res.at<double>(i, 3) = translate.coeff(i);
        }

        return res;
    }

  public:
    // Helper Function using Eigen

    using TransformMat = Eigen::Matrix4d;

    TransformMat getTransformMat() const {
        auto rot = getRotationMatrix();

        TransformMat res = TransformMat::Identity();
        res.block<3, 3>(0, 0) = rot;
        res.block<3, 1>(3, 0) = m_translation;
        return res;
    }

    using TransformMat34 = Eigen::Matrix<double, 3, 4>;

    TransformMat34 getTransformMat34() const {
        auto rot = getRotationMatrix();

        TransformMat34 res = TransformMat34::Zero();
        res.block<3, 3>(0, 0) = rot;
        res.block<3, 1>(0, 3) = m_translation;
        return res;
    }

  public:
    // Helper Function using gtsam
    gtsam::Pose3 getPose3gtsam() const {
        return gtsam::Pose3{getRot3gtsam(), getTranslation3gtsam()};
    }

    gtsam::Rot3 getRot3gtsam() const {
        return gtsam::Rot3{m_rotation.toRotationMatrix()};
    }

    gtsam::Point3 getTranslation3gtsam() const {
        if constexpr (std::is_same_v<gtsam::Point3, Eigen::Vector3d>) {
            return m_translation;
        } else {
            return gtsam::Point3{m_translation.x(),
                                 m_translation.y(),
                                 m_translation.z()};
        }
    }

  protected:
    void normalizeRotation() {
        m_rotation.normalize();
    }

  protected:
    Eigen::Vector3d m_translation;
    Eigen::Quaterniond m_rotation;

};  // class Transform

}  // namespace beacon
