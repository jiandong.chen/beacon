#pragma once
#include <memory>
#include <type_traits>

namespace beacon {

template <typename Base>
struct NodeBase {
    NodeBase() = default;
    NodeBase(const NodeBase&) = default;
    NodeBase(NodeBase&&) = default;
    NodeBase& operator=(const NodeBase&) = default;
    NodeBase& operator=(NodeBase&&) = default;
    virtual ~NodeBase() = default;

    virtual std::unique_ptr<Base> clone() const {
        return nullptr;
    };

    virtual std::unique_ptr<Base> move_clone() {
        return nullptr;
    };
#if 0
    virtual bool assign(const Base*) { return false; };
    virtual bool move_assign(Base*) { return false; };
#endif

};  // struct NodeBase

template <typename Derived, typename Base>
struct NodeClone : public Base {
    std::unique_ptr<Base> clone() const override {
        if constexpr (std::is_abstract_v<Derived> ||
                      !std::is_copy_constructible_v<Derived>) {
            return nullptr;
        } else {
            return std::make_unique<Derived>(
                static_cast<const Derived&>(*this));
        }
    };

    std::unique_ptr<Base> move_clone() override {
        if constexpr (std::is_abstract_v<Derived> ||
                      !std::is_move_constructible_v<Derived>) {
            return nullptr;
        } else {
            return std::make_unique<Derived>(static_cast<Derived&&>(*this));
        }
    };
#if 0
    bool assign(const Base* source) override {
        if constexpr (std::is_abstract_v<Derived> ||
                      !std::is_copy_assignable_v<Derived>) {
            return false;
        } else {
            auto src = dynamic_cast<const Derived*>(source);
            if (!src) {
                return false;
            }
            auto target = static_cast<Derived*>(this);
            *target = *src;
            return true;
        }
    };
    bool move_assign(Base* source) override {
        if constexpr (std::is_abstract_v<Derived> ||
                      !std::is_move_assignable_v<Derived>) {
            return false;
        } else {
            auto src = dynamic_cast<const Derived*>(source);
            if (!src) {
                return false;
            }
            auto target = static_cast<Derived*>(this);
            *target = std::move(*src);
            return true;
        }
    };
#endif

};  //  struct NodeClone

struct Node {
    Node() = default;
    Node(const Node&) = default;
    Node(Node&&) = default;
    Node& operator=(const Node&) = default;
    Node& operator=(Node&&) = default;
    virtual ~Node() = default;

    virtual std::unique_ptr<Node> clone() const {
        return nullptr;
    };

    virtual std::unique_ptr<Node> move_clone() {
        return nullptr;
    };

    virtual bool assign(const Node*) {
        return false;
    };

    virtual bool move_assign(Node*) {
        return false;
    };

};  // struct Node

}  // namespace beacon
