#include <condition_variable>
#include <deque>
#include <mutex>
#include <shared_mutex>

namespace beacon {

template <typename DataType>
class ProducerConsumer {
  public:
    using Data = std::decay_t<DataType>;

  public:
    Data pop_front() {
        std::unique_lock lk{m_mtx};

        m_cv.wait(lk, [this] {
            return !m_data.empty();
        });

        auto res = std::move(m_data.front());
        m_data.pop_front();

        return res;
    }

    void push_back(Data d) {
        std::unique_lock lk{m_mtx};
        m_data.push_back(std::move(d));

        m_cv.notify_one();
    }

    auto size() const {
        std::shared_lock lk{m_mtx};

        return m_data.size();
    }

  protected:
    mutable std::mutex m_mtx{};
    std::condition_variable m_cv{};
    std::deque<DataType> m_data{};

};  // class ProducerConsumer

};  // namespace beacon
