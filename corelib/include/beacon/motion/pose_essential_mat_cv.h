#pragma once

#include "../sensor/pinhole.h"
#include "pose_essential_mat.h"

namespace beacon {

class PoseFromMatchEssentialMatCV : public PoseFromMatchEssentialMat {
  protected:
    void findEssentialMat() override;

    void recoverPose() override;

  protected:
    // intermediate data

    cv::Mat m_K{Pinhole::getCVIdentityK()};

    std::vector<uchar> m_status{};

    std::vector<cv::Point2f> m_kpxy0{};
    std::vector<cv::Point2f> m_kpxy1{};
    cv::Mat m_essential_mat;

};  // class PoseFromMatchEssentialMatCV

}  // namespace beacon
