#pragma once

#include <optional>

#include "../core/transform.h"
#include "beacon/datatype/visual_match_data.h"

namespace beacon {

class PoseFromMatchPnP {
  public:
    using ResultType = std::optional<Transform>;

  public:
    virtual ~PoseFromMatchPnP() = default;

  public:
    // match pdata0 should be valid and provide obj points
    BEACON_NODISCARD virtual ResultType process(
        const VisualFeatureMatchDataPtr& match) = 0;

  public:
    // getter for data member

    ResultType getLastResult() const {
        return m_res;
    }

  protected:
    ResultType m_res{std::nullopt};

};  // class PoseFromMatchPnP

}  // namespace beacon
