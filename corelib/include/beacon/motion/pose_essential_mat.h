#pragma once

#include <optional>

#include "../core/transform.h"
#include "beacon/datatype/visual_match_data.h"

namespace beacon {

class PoseFromMatchEssentialMat {
  public:
    using ResultType = std::optional<Transform>;

  public:
    virtual ~PoseFromMatchEssentialMat() = default;

    BEACON_NODISCARD ResultType
    process(const VisualFeatureMatchDataPtr& match) {
        m_res = std::nullopt;

        m_input_data = match;

        preprocess();

        findEssentialMat();

        postprocessFindEssentialMat();

        recoverPose();

        postprocessRecoverPose();

        return m_res;
    };

  protected:
    virtual void preprocess() {};

    virtual void findEssentialMat() = 0;

    virtual void postprocessFindEssentialMat() {};

    virtual void recoverPose() = 0;

    virtual void postprocessRecoverPose() {};

  public:
    // getter for data member

    ResultType getLastResult() const {
        return m_res;
    }

  protected:
    // intermediate data

    VisualFeatureMatchDataPtr m_input_data{};

    ResultType m_res{std::nullopt};

};  // class PoseFromMatchEssentialMat

}  // namespace beacon
