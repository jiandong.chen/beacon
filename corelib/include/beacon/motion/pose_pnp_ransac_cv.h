#pragma once

#include "../sensor/pinhole.h"
#include "pose_pnp.h"

namespace beacon {

class PoseFromMatchPnPRansacCV : public PoseFromMatchPnP {
  public:
    BEACON_NODISCARD ResultType
    process(const VisualFeatureMatchDataPtr& match) override;

  protected:
    ResultType calculate();

  protected:
    // intermediate data
    std::vector<int> m_status{};

    cv::Mat m_K{Pinhole::getCVIdentityK()};

    std::vector<cv::Point3f> m_objpt;
    std::vector<cv::Point2f> m_imgpt;

};  // class PoseFromMatchPnPRansacCV

}  // namespace beacon
