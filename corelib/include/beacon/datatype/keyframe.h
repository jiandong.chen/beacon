#pragma once

#include "beacon/core/transform.h"
#include "beacon/datatype/visual_feature_data.h"
#include "beacon/sensor/camera.h"

namespace beacon {

struct ImageData;

class KeyframeBase {
  public:
    // constructor
    KeyframeBase(Transform t = {}) : m_T_wf{std::move(t)} {
    }

  public:
    Transform getTransform() const {
        return m_T_wf;
    }

    KeyframeBase& setTransform(Transform t) {
        m_T_wf = std::move(t);
        return *this;
    }

  protected:
    // Transform between world and frame
    // similar to T_wc of ImageData
    Transform m_T_wf{};

};  // class KeyframeBase

class Keyframe : public KeyframeBase {
  public:
    using Ptr = std::shared_ptr<Keyframe>;

  public:
    // constructor
    Keyframe(const ImageData& pdata);

    static Ptr create(const ImageData& data) {
        return std::make_shared<Keyframe>(data);
    }

  public:
    // getter for general data members

    // access to 2d features
    VisualFeatureDataPtr getFeature() const {
        return m_feature;
    }

    // access to camera
    CameraPtr getCamera() const {
        return m_camera;
    }

  protected:
    VisualFeatureDataPtr m_feature{};  // feature data
    CameraPtr m_camera{};              // camera by which the image take

};  // class Keyframe

using KeyframePtr = Keyframe::Ptr;

}  // namespace beacon
