#pragma once

#include <gtsam/geometry/Point2.h>

#include <Eigen/Eigen>
#include <opencv4/opencv2/opencv.hpp>

namespace beacon {

struct PointUVXY {
    using PointType = Eigen::Vector2d;

    PointType uv{};  // distorted pixel value
    PointType xy{};  // undistorted normalized value

  public:
    // Helper Function for OpenCV

    // convert uv of points to cv point
    template <typename DataType = float>
    cv::Point_<DataType> uv2CV() const;

    template <typename DataType = float,
              template <typename> typename PointType = cv::Point_,
              template <typename> typename ContType = std::vector>
    static ContType<PointType<DataType>> uv2CV(
        typename ContType<PointUVXY>::const_iterator cbeg,
        typename ContType<PointUVXY>::const_iterator cend);

    // convert xy of points to cv point
    template <typename DataType = float>
    cv::Point_<DataType> xy2CV() const;

    template <typename DataType = float,
              template <typename> typename PointType = cv::Point_,
              template <typename> typename ContType = std::vector>
    static ContType<PointType<DataType>> xy2CV(
        typename ContType<PointUVXY>::const_iterator cbeg,
        typename ContType<PointUVXY>::const_iterator cend);

  public:
    // Helper Function for GTSAM
    gtsam::Point2 uv2GTSAM() const;

    gtsam::Point2 xy2GTSAM() const;

};  // struct PointUVXY

/* ##################### PointUVXY ########################################## */
/* ########################################################################## */
template <typename DataType>
cv::Point_<DataType> PointUVXY::uv2CV() const {
    return cv::Point_<DataType>{static_cast<DataType>(uv.x()),
                                static_cast<DataType>(uv.y())};
}

/* ########################################################################## */
template <typename DataType,
          template <typename>
          typename PointType,
          template <typename>
          typename ContType>
ContType<PointType<DataType>> PointUVXY::uv2CV(
    typename ContType<PointUVXY>::const_iterator cbeg,
    typename ContType<PointUVXY>::const_iterator cend) {
    const auto sz = std::distance(cbeg, cend);
    ContType<PointType<DataType>> res(sz);
    std::transform(cbeg, cend, res.begin(), [](const auto& pt) {
        return static_cast<PointUVXY>(pt).uv2CV<DataType>();
    });
    return res;
}

/* ########################################################################## */
template <typename DataType>
cv::Point_<DataType> PointUVXY::xy2CV() const {
    return cv::Point_<DataType>{static_cast<DataType>(xy.x()),
                                static_cast<DataType>(xy.y())};
}

/* ########################################################################## */
template <typename DataType,
          template <typename>
          typename PointType,
          template <typename>
          typename ContType>
ContType<PointType<DataType>> PointUVXY::xy2CV(
    typename ContType<PointUVXY>::const_iterator cbeg,
    typename ContType<PointUVXY>::const_iterator cend) {
    const auto sz = std::distance(cbeg, cend);
    ContType<PointType<DataType>> res(sz);
    std::transform(cbeg, cend, res.begin(), [](const auto& pt) {
        return static_cast<PointUVXY>(pt).xy2CV<DataType>();
    });
    return res;
}

/* ########################################################################## */
inline gtsam::Point2 PointUVXY::uv2GTSAM() const {
    if constexpr (std::is_same_v<PointType, gtsam::Point2>) {
        return uv;

    } else {
        return gtsam::Point2{uv.x(), uv.y()};
    }
}

/* ########################################################################## */
inline gtsam::Point2 PointUVXY::xy2GTSAM() const {
    if constexpr (std::is_same_v<PointType, gtsam::Point2>) {
        return xy;

    } else {
        return gtsam::Point2{xy.x(), xy.y()};
    }
}

}  // namespace beacon
