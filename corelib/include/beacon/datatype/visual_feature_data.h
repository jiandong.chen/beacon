#pragma once

#include <Eigen/Eigen>
#include <any>
#include <memory>
#include <opencv4/opencv2/opencv.hpp>
#include <vector>

#include "../core/def.h"
#include "landmark.h"
#include "point_uvxy.h"

namespace beacon {

struct VisualFeaturePoint : public PointUVXY {
    BEACON_ENUM_UNDERLYING label{};      // used to classify the Point
    std::weak_ptr<Landmark> landmark{};  // related lanmarks
};  // struct VisualFeaturePoint

struct VisualFeatureData {
  public:
    // shorthand
    using Ptr = std::shared_ptr<VisualFeatureData>;
    using ConstPtr = std::shared_ptr<const VisualFeatureData>;

  public:
    // Vec of feature points
    std::vector<VisualFeaturePoint> points;

    // descriptor related to feature points [num_kp, dim_desc]
    cv::Mat descriptors{};

    // GPU memory (data) for both points and descriptor
    // exact type depends on algorithm implementation
    std::any device_data{};

  public:
    // constructor
    BEACON_NODISCARD static Ptr create();
    BEACON_NODISCARD static Ptr create(const std::vector<cv::KeyPoint>& cvkp);

  public:
    // helper function for OpenCV
    // convert uv of points to cv point
    template <typename DataType>
    std::vector<cv::Point_<DataType>> uv2CV() const;

    // convert xy of points to cv point
    template <typename DataType>
    std::vector<cv::Point_<DataType>> xy2CV() const;

};  // struct VisualFeatureData

using VisualFeatureDataPtr = VisualFeatureData::Ptr;

using VisualFeatureDataPtrConst = VisualFeatureData::ConstPtr;

/* ##################### VisualFeatureData ################################## */
/* ########################################################################## */

template <typename DataType>
std::vector<cv::Point_<DataType>> VisualFeatureData::uv2CV() const {
    std::vector<cv::Point_<DataType>> res(points.size());
    std::transform(points.cbegin(),
                   points.cend(),
                   res.begin(),
                   [](const VisualFeaturePoint& pt) {
                       return pt.uv2CV<DataType>();
                   });
    return res;
}

/* ########################################################################## */
template <typename DataType>
std::vector<cv::Point_<DataType>> VisualFeatureData::xy2CV() const {
    std::vector<cv::Point_<DataType>> res(points.size());
    std::transform(points.cbegin(),
                   points.cend(),
                   res.begin(),
                   [](const VisualFeaturePoint& pt) {
                       return pt.xy2CV<DataType>();
                   });
    return res;
}

}  // namespace beacon
