#pragma once

#include <memory>
#include <opencv4/opencv2/opencv.hpp>

#include "../core/timestamp.h"
#include "../sensor/camera.h"
#include "visual_feature_data.h"

namespace beacon {

// Main image dataframe for beacon
struct ImageData {
  public:
    // shorthand for pointers
    using Ptr = std::shared_ptr<ImageData>;
    using ConstPtr = std::shared_ptr<const ImageData>;
    using CameraPtr = std::shared_ptr<Camera>;
    using FeaturePtr = std::shared_ptr<VisualFeatureData>;

    Timestamp timestamp{};           // image take timestamp
    cv::Mat image{};                 // image data
    cv::Mat mask{};                  // mask of image for image processing
    FeaturePtr feature{};            // feature data
    CameraPtr camera{};              // camera by which the image is taken
    Transform T_wc{};                // Transform between world and camera
    BEACON_ENUM_UNDERLYING label{};  // labels for the frame, isKeyframe etc.

  public:
    // constructor
    ImageData();
    ImageData(const cv::Mat& img,
              Timestamp timestamp,
              const CameraPtr& cam = nullptr,
              Transform T_wc = Transform{});

    static Ptr create();
    static Ptr create(cv::Mat img,
                      Timestamp timestamp,
                      const CameraPtr& cam = nullptr,
                      Transform T_wc = Transform{});

  public:
    // get the grey image
    // if use_buffer set, return image_gray_buffer if not empty
    // if use_buffer set, return gray image and save to image_gray_buffer if
    // empty
    BEACON_NODISCARD cv::Mat getGrayImage(
        const bool use_buffer = use_gray_image_buffer) const;
    BEACON_NODISCARD static cv::Mat getGrayImage(
        const ImageData& data,
        const bool use_buffer = use_gray_image_buffer);

  public:
    // field for helper function
    inline static bool use_gray_image_buffer{false};

  protected:
    mutable cv::Mat image_gray_buffer;

};  // struct ImageData

using ImagePtr = ImageData::Ptr;

using ImageConstPtr = ImageData::ConstPtr;

}  // namespace beacon
