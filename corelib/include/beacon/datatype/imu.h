#pragma once
#include <Eigen/Eigen>

namespace beacon {

struct IMUData {
    Eigen::Vector3d angular_vel{};  // [rad/s]
    Eigen::Vector3d linear_vel{};   // [m/s]
    Eigen::Quaterniond orientation{};

};  // struct IMUData

struct IMUDataExtended : public IMUData {
    Eigen::Matrix3d angular_vel_covariance{};
    Eigen::Matrix3d linear_vel_covariance{};
    Eigen::Matrix3d orientation_covariance{};
};

}  // namespace beacon
