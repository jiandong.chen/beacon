#pragma once

#include <memory>
#include <opencv4/opencv2/opencv.hpp>
#include <vector>

#include "image_data.h"
#include "point_uvxy.h"

namespace beacon {

template <typename DataType,
          template <typename> typename PointType = cv::Point_>
struct VisualMatchPointVec {
    // two vec of matched points
    // point0 and point1 should have same size
    std::vector<PointType<DataType>> point0{};
    std::vector<PointType<DataType>> point1{};
};  // struct VisualMatchPointVecCV

template <>
struct VisualMatchPointVec<PointUVXY> {
    // two vec of matched points with uv and xy data
    // point0 and point1 should have same size
    std::vector<PointUVXY> point0{};
    std::vector<PointUVXY> point1{};

};  // struct VisualMatchPointVec<PointUVXY>

struct VisualFeatureMatch {
    // Idx of features in ImageData.feture.points
    int idx0{};
    int idx1{};

    // score (or distance) of this match
    double score{-1.0};

    BEACON_ENUM_UNDERLYING label{};  // used to classify the match

};  // struct VisualFeatureMatch

struct VisualFeatureMatchData : public VisualMatchPointVec<PointUVXY> {
  public:
    // shorthand
    using Ptr = std::shared_ptr<VisualFeatureMatchData>;
    using ConstPtr = std::shared_ptr<const VisualFeatureMatchData>;

  public:
    // ImageData relates to this match
    std::weak_ptr<ImageData> pdata0{};
    std::weak_ptr<ImageData> pdata1{};

    // Vec of Matches
    // Size of Vec is same as point0 and point1
    std::vector<VisualFeatureMatch> matches;

  public:
    // constructor
    static Ptr create();

  public:
    // fill point0 given pdata0 and vec of match.idx0
    // fill point1 given pdata1 and vec of match.idx1
    void fillPoints();

    // fill point0 given vec of match.idx0 and data0
    // fill point1 given vec of match.idx1 and data1
    void fillPoints(const ImageData& data0, const ImageData& data1);

    // evaluate *Iter
    // if *Iter is true, the corresponding point and match stay
    // otherwise remove
    // distance(beg, end) should equal to size of matches
    template <typename IterType>
    BEACON_NODISCARD static VisualFeatureMatchData refineDataByMask(
        const VisualFeatureMatchData& data,
        IterType mask_beg,
        IterType mask_end);

};  // struct VisualMatchData

/* ##################### VisualFeatureMatchData ############################# */
/* ########################################################################## */
template <typename IterType>
VisualFeatureMatchData VisualFeatureMatchData::refineDataByMask(
    const VisualFeatureMatchData& data,
    IterType mask_beg,
    IterType mask_end) {
    VisualFeatureMatchData res;
    res.pdata0 = data.pdata0;
    res.pdata1 = data.pdata1;

    std::size_t count = 0;
    while (mask_beg != mask_end) {
        if (*mask_beg) {
            res.point0.push_back(data.point0[count]);
            res.point1.push_back(data.point1[count]);
            res.matches.push_back(data.matches[count]);
        }
        ++mask_beg;
        ++count;
    }
    return res;
}

using VisualFeatureMatchDataPtr = VisualFeatureMatchData::Ptr;

using VisualFeatureMatchDataConstPtr = VisualFeatureMatchData::ConstPtr;

}  // namespace beacon
