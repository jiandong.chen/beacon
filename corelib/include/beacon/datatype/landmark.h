#pragma once
#include <Eigen/Eigen>
#include <memory>

#include "../core/def.h"
#include "../core/transform.h"

namespace beacon {

struct ImageData;

struct LandmarkLink {
    std::weak_ptr<ImageData> image{};
    std::size_t keypoint_idx{};

};  // struct LandmarkLink

// Landmark Point ysing inverse depth parameterization
class LandmarkInverseDepth {
  public:
    // shorthand
    using Ptr = std::shared_ptr<LandmarkInverseDepth>;

  public:
    LandmarkInverseDepth(Eigen::Vector3d ref_xyz,
                         const double theta,
                         const double phi,
                         const double inv_depth)
        : m_rho{inv_depth},
          m_theta_phi{theta, phi},
          m_ref_xyz{std::move(ref_xyz)} {
    }

    LandmarkInverseDepth(Eigen::Vector3d pt_in_base,
                         const Transform& T_wb,  // world and base frame
                         const Transform& T_bc   // base and refence cam frame
    ) {
        // build from 3d point
        // and reference base pose T_wb

        const auto pt_in_cam = T_bc.getInverse() * pt_in_base;
        m_rho = 1.0 / pt_in_cam.norm();

        const auto norm_pt_in_cam = pt_in_cam.normalized();

        const auto cam_in_world = T_wb * T_bc;

        const auto norm_pt_in_world = T_wb * T_bc * norm_pt_in_cam;
        const auto cam_trans_in_world = cam_in_world.getTranslation();
        const auto ray = norm_pt_in_world - cam_trans_in_world;

        m_ref_xyz = cam_in_world.getTranslation();
        m_theta_phi = {
            std::atan2(ray.y(), ray.x()),
            std::atan2(ray.z(),
                       std::sqrt(ray.x() * ray.x() + ray.y() * ray.y()))};
    }

  public:
    // getter for data members
    BEACON_NODISCARD Eigen::Vector3d getRefXYZ() const {
        return m_ref_xyz;
    }

    BEACON_NODISCARD Eigen::Vector2d getThetaPhi() const {
        return m_theta_phi;
    }

    BEACON_NODISCARD double getInvDepth() const {
        return m_rho;
    };

    BEACON_NODISCARD auto getRefFrameAndFeatureIdx() const {
        return m_ref_frame_and_feature_idx;
    };

    BEACON_NODISCARD Eigen::Vector3d get3DXYZ() const {
        gtsam::Point3 m(std::cos(m_theta_phi[0]) * std::cos(m_theta_phi[1]),
                        std::sin(m_theta_phi[0]) * std::cos(m_theta_phi[1]),
                        std::sin(m_theta_phi[1]));
        return m_ref_xyz + m / m_rho;
    }

  public:
    // setter

    LandmarkInverseDepth& setRefXYZ(Eigen::Vector3d xyz) {
        m_ref_xyz = std::move(xyz);
        return *this;
    }

    LandmarkInverseDepth& setThetaPhi(Eigen::Vector2d theta_phi) {
        m_theta_phi = std::move(theta_phi);
        return *this;
    }

    LandmarkInverseDepth& setInvZ(double rho) {
        m_rho = rho;
        return *this;
    }

    LandmarkInverseDepth& setRefFrame(const std::shared_ptr<ImageData>& p_frame,
                                      const std::size_t feature_idx);

  public:
    // constructor
    BEACON_NODISCARD static Ptr create(Eigen::Vector3d xyz,
                                       const double theta,
                                       const double phi,
                                       const double inv_depth) {
        return std::make_shared<LandmarkInverseDepth>(xyz,
                                                      theta,
                                                      phi,
                                                      inv_depth);
    }

    BEACON_NODISCARD static Ptr create(Eigen::Vector3d pt_in_base,
                                       const Transform& T_wb,
                                       const Transform& T_bc) {
        return std::make_shared<LandmarkInverseDepth>(pt_in_base, T_wb, T_bc);
    }

  protected:
    // inverse depth
    double m_rho{};

    // theta & phi
    Eigen::Vector2d m_theta_phi{};

    // xy values on image normalized plane
    Eigen::Vector3d m_ref_xyz{};

    // reference frame for the LandmarkInverseDepth
    std::pair<std::shared_ptr<ImageData>, int> m_ref_frame_and_feature_idx{};

};  // class LandmarkInverseDepth

// currently we only use inverse depth representation
using Landmark = LandmarkInverseDepth;

// shorthand
using LandmarkPtr = Landmark::Ptr;

}  // namespace beacon
