#pragma once

#ifdef BEACON_ENABLE_TENSORRT
#include "visual_tracker_base.h"

namespace beacon {

struct LightGlueTRTParameter
    : public VisualTrackerParameterClone<LightGlueTRTParameter> {
    BEACON_NODISCARD TrackerType type() const noexcept override {
        return TrackerType::LightGlueTRT;
    }
    ADD_VISUAL_FEATURE_PARAM(match_threshold, double, 0.2, MatchThreshold)

    std::string m_engine_path{};

    auto setModelPath(const std::string& path) {
        m_engine_path = path;
    }

};  // struct LightGlueTRTParameter

class LightGlueTRT : public VisualTrackerClone<LightGlueTRT> {
  public:
    explicit LightGlueTRT(const LightGlueTRTParameter& params = {});

  public:
    BEACON_NODISCARD ResultType processImpl(ImageData& data0,
                                            ImageData& data1) override;

    void setParameter(const VisualTrackerParameter& params) override;
    void setParameter(const LightGlueTRTParameter& params);

    VISUAL_TRACKER_DEFAULT_REGISTER("LightGlueTRT",
                                    LightGlueTRT,
                                    LightGlueTRTParameter)
  protected:
    struct Impl;
    std::unique_ptr<Impl> m_impl;

};  // class LightGlueTRT

}  // namespace beacon

#endif
