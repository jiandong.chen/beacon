#pragma once
#ifdef BEACON_ENABLE_TENSORRT
#include "visual_tracker_base.h"

namespace beacon {

struct SuperGlueTRTParameter
    : public VisualTrackerParameterClone<SuperGlueTRTParameter> {
    BEACON_NODISCARD TrackerType type() const noexcept override {
        return TrackerType::SuperGlueTRT;
    }
    ADD_VISUAL_FEATURE_PARAM(match_threshold, double, 0.2, MatchThreshold)
    ADD_VISUAL_FEATURE_PARAM(sinkhorn_iterations, int, 15, SinkhornIteration)

    std::string m_engine_path{};

    auto setModelPath(const std::string& path) {
        m_engine_path = path;
    }

};  // struct SuperGlueTRTParameter

class SuperGlueTRT : public VisualTrackerClone<SuperGlueTRT> {
  public:
    explicit SuperGlueTRT(const SuperGlueTRTParameter& params = {});

  public:
    BEACON_NODISCARD ResultType processImpl(ImageData& data0,
                                            ImageData& data1) override;

    void setParameter(const VisualTrackerParameter& params) override;
    void setParameter(const SuperGlueTRTParameter& params);

    VISUAL_TRACKER_DEFAULT_REGISTER("SuperGlueTRT",
                                    SuperGlueTRT,
                                    SuperGlueTRTParameter)
  protected:
    struct Impl;
    std::unique_ptr<Impl> m_impl;

};  // class SuperGlueTRT

}  // namespace beacon
#endif
