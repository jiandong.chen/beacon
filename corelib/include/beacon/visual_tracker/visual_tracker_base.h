#pragma once

#include <memory>
#include <opencv4/opencv2/opencv.hpp>

#include "../core/def.h"
#include "../core/factory.h"
#include "../core/node.h"
#include "../datatype/image_data.h"
#include "../datatype/visual_match_data.h"
#include "../visual_feature/visual_feature_base.h"

#define ADD_VISUAL_TRACKER_PARAM(var_name, type, init_value, set_func_name) \
    ADD_VISUAL_FEATURE_PARAM(var_name, type, init_value, set_func_name)

#define ADD_VISUAL_TRACKER_PARAM_REF_INPUT(var_name,      \
                                           type,          \
                                           init_value,    \
                                           set_func_name) \
    ADD_VISUAL_FEATURE_PARAM_REF_INPUT(var_name,          \
                                       type,              \
                                       init_value,        \
                                       set_func_name)

#define VISUAL_TRACKER_DEFAULT_FACTORY(TYPE, PARAM_TYPE)    \
    BEACON_DEFAULT_FACTORY_FUNCTION(VisualTracker,          \
                                    VisualTrackerParameter, \
                                    TYPE,                   \
                                    PARAM_TYPE)

#define VISUAL_TRACKER_DEFAULT_REGISTER(NAME, TYPE, PARAM_TYPE) \
    BEACON_DEFAULT_REGISTER_HELPER(                             \
        std::string,                                            \
        NAME,                                                   \
        VISUAL_TRACKER_DEFAULT_FACTORY(TYPE, PARAM_TYPE))

namespace beacon {

// Base Class for Visual Feature Tracker Parameter Class
// The Parameter data are used for create the specific algo
// and (re-)set the parameter
struct VisualTrackerParameter : public NodeBase<VisualTrackerParameter> {
    // add custom Feature Tracker type to def.h
    // then override type()
    BEACON_NODISCARD virtual TrackerType type() const noexcept = 0;
};  // struct VisualTrackerParameter

// shorthand
// for implement clone() for VisualTrackerParameter
template <typename Derived>
using VisualTrackerParameterClone = NodeClone<Derived, VisualTrackerParameter>;

// Base Class for Visual Feature Tracker Class
class VisualTracker
    : public NodeBase<VisualTracker>,
      public FactoryBase<std::string,
                         std::function<std::unique_ptr<VisualTracker>(
                             const VisualTrackerParameter&)>> {
  public:
    using Ptr = std::unique_ptr<VisualTracker>;
    using ResultType = VisualFeatureMatchDataPtr;

  public:
    // process the ImageData
    // pre-process + processImpl + post-process
    BEACON_NODISCARD ResultType process(ImageData& data0, ImageData& data1);

    // process shptr of ImageData
    // process ImageData + fill corresponding weakptr filed
    BEACON_NODISCARD ResultType process(ImagePtr& data0, ImagePtr& data1);

    // set parameter for specific algorithm
    // derived class should create Parameter Type by deriving
    // VisualTrackerParameter override by derived algorithm
    virtual void setParameter(const VisualTrackerParameter& params) = 0;

  protected:
    // actual tracker process
    // override by derived algorithm
    BEACON_NODISCARD virtual ResultType processImpl(ImageData& data0,
                                                    ImageData& data1) = 0;

  public:
    // factory
    static Ptr create(const std::string& idx,
                      const VisualTrackerParameter& param);

};  // class VisualTracker

using VisualTrackerPtr = VisualTracker::Ptr;

template <typename Derived>
using VisualTrackerClone = NodeClone<Derived, VisualTracker>;

}  // namespace beacon
