#pragma once
#include <opencv4/opencv2/opencv.hpp>

#include "visual_tracker_base.h"

namespace beacon {

struct LucasKanadeTrackerParameter
    : public VisualTrackerParameterClone<LucasKanadeTrackerParameter> {
    BEACON_NODISCARD TrackerType type() const noexcept override {
        return TrackerType::LucasKanade;
    }

    ADD_VISUAL_TRACKER_PARAM(max_pyramid_level, int, 3, MaxPyramidLevel)
    ADD_VISUAL_TRACKER_PARAM(flag, int, 0, Flag)
    ADD_VISUAL_TRACKER_PARAM(min_eigen_threshold,
                             double,
                             0.0004,
                             MinEigenThreshold)
    ADD_VISUAL_TRACKER_PARAM(win_size, int, 21, WinSize)
    ADD_VISUAL_TRACKER_PARAM(max_iter, int, 30, MaxIterNum)
    ADD_VISUAL_TRACKER_PARAM(iter_stop_eps, double, 0.01, IterStopEPS)

};  // struct LucasKanadeTrackerParameter

class LucasKanadeTracker : public VisualTrackerClone<LucasKanadeTracker> {
  public:
    explicit LucasKanadeTracker(const LucasKanadeTrackerParameter& params = {});

  public:
    BEACON_NODISCARD ResultType processImpl(ImageData& data0,
                                            ImageData& data1) override;

    void setParameter(const VisualTrackerParameter& params) override;
    void setParameter(const LucasKanadeTrackerParameter& params);

  protected:
    int m_max_pyramid_level{3};
    int m_flag{0};
    double m_min_eigen_threshold{0.0004};
    cv::Size m_win_size{cv::Size(21, 21)};
    cv::TermCriteria m_criteria{
        cv::TermCriteria(cv::TermCriteria::COUNT + cv::TermCriteria::EPS,
                         30,
                         0.01)};

    VISUAL_TRACKER_DEFAULT_REGISTER("LucasKanade",
                                    LucasKanadeTracker,
                                    LucasKanadeTrackerParameter)

};  // class LucasKanadeTracker

}  // namespace beacon
