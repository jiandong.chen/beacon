#pragma once

#include "../datatype/visual_match_data.h"

namespace beacon {

class TriangulateMatch {
  public:
    using PointType = Eigen::Vector3d;

  public:
    // triangulate 2d point in match data
    // require point0 point1 pdata0.camera pdata1.camera available
    // result points represents in `base` frame
    BEACON_NODISCARD virtual std::vector<PointType> triangulate(
        const VisualFeatureMatchData& data,
        const Transform& base = {}) = 0;

};  // class TriangulateMatch

}  // namespace beacon
