#pragma once

#include "triangulate_match.h"

namespace beacon {

class TriangulateMatchGTSAMDLT : public TriangulateMatch {
  public:
    // triangulate 2d point in match data using gtsam DLT algorithm
    // require point0 point1 pdata0.camera pdata1.camera available
    // result points represents in `base` frame
    BEACON_NODISCARD std::vector<PointType> triangulate(
        const VisualFeatureMatchData& data,
        const Transform& base = {}) override;

};  // class TriangulateMatchGTSAMDLT

}  // namespace beacon
