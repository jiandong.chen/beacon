#pragma once
#include <Eigen/Eigen>
#include <cmath>
#include <opencv4/opencv2/core/eigen.hpp>
#include <opencv4/opencv2/opencv.hpp>

#include "../core/transform.h"

namespace beacon {

/* ##################### cv_helper ########################################## */
/* ########################################################################## */
inline cv::Mat buildCVMat34FromTransform(const Transform& trans) {
    auto rot_mat = trans.getRotationMatrix();
    auto translate = trans.getTranslation();
    cv::Mat res(3, 4, CV_64F);
    for (int i = 0; i < 3; ++i) {
        res.at<double>(i, 0) = rot_mat.coeff(i, 0);
        res.at<double>(i, 1) = rot_mat.coeff(i, 1);
        res.at<double>(i, 2) = rot_mat.coeff(i, 2);
        res.at<double>(i, 3) = translate.coeff(i);
    }

    return res;
}

/* ########################################################################## */
template <typename DataType = double>
inline cv::Mat buildCVTranslationFromEigen(
    const Eigen::Matrix<DataType, 3, 1>& t) {
    cv::Mat res(3, 1, CV_64F);
    for (int i = 0; i < 3; ++i) {
        res.at<double>(i, 0) = t.coeff(i);
    }
    return res;
}

/* ########################################################################## */
template <typename DataType = double>
inline cv::Mat buildCVRotationFromEigen(
    const Eigen::Matrix<DataType, 3, 3>& R) {
    cv::Mat res(3, 3, CV_64F);
    for (int i = 0; i < 3; ++i) {
        res.at<double>(i, 0) = R.coeff(i, 0);
        res.at<double>(i, 1) = R.coeff(i, 1);
        res.at<double>(i, 2) = R.coeff(i, 2);
    }
    return res;
}

/* ########################################################################## */
template <typename DataType = double>
inline Eigen::Quaternion<DataType> buildRotationQuatFromCVMat(
    const cv::Mat& mat) {
    Eigen::Matrix3d tmp;
    cv::cv2eigen(mat, tmp);

    Eigen::Quaternion<DataType> quat{tmp};
    return quat.normalized();
}

/* ########################################################################## */
template <typename DataType = double>
inline Eigen::Matrix<DataType, 3, 1> buildTranslationFromCVMat(
    const cv::Mat& mat) {
    Eigen::Matrix<DataType, 3, 1> res;
    cv::cv2eigen(mat, res);
    return res;
}

/* ########################################################################## */
inline double norm(const cv::KeyPoint& pt1, const cv::KeyPoint& pt2) {
    const double diff_x = pt1.pt.x - pt2.pt.x;
    const double diff_y = pt1.pt.y - pt2.pt.y;
    return std::sqrt(diff_x * diff_x + diff_y * diff_y);
}

/* ########################################################################## */
template <typename StatusDataType, typename DataType>
std::vector<DataType> cvFilterByStatus(
    const std::vector<DataType>& ori_data,
    const std::vector<StatusDataType>& status) {
    std::vector<DataType> res;
    for (size_t i = 0; i < status.size(); ++i) {
        if (status[i] == 1) {
            res.push_back(ori_data.at(i));
        }
    }
    return res;
}

}  // namespace beacon
