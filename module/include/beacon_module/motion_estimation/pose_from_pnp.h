#pragma once

#include "../logger/logger.h"
#include "beacon/motion/pose_pnp_ransac_cv.h"

namespace beacon {

class PoseFromPnP : public PoseFromMatchPnPRansacCV {
  public:
    BEACON_NODISCARD ResultType process(
        const VisualFeatureMatchDataPtr& match) override;

  protected:
    BeaconLoggerPtr m_logger{BeaconLogger::get()};

    std::vector<int> m_status_idx{};

};  // class PoseFromMatch

}  // namespace beacon
