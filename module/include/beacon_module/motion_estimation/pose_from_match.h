#pragma once

#include "../logger/logger.h"
#include "beacon/motion/pose_essential_mat_cv.h"

namespace beacon {

class PoseFromMatch : public PoseFromMatchEssentialMatCV {
  public:
    void preprocess() override;

    void postprocessFindEssentialMat() override;

    void postprocessRecoverPose() override;

  protected:
    BeaconLoggerPtr m_logger{BeaconLogger::get()};

};  // class PoseFromMatch

}  // namespace beacon
