#pragma once

#include <memory>
#include <opencv4/opencv2/opencv.hpp>
#include <string>
#include <vector>

#include "../logger/logger.h"
#include "beacon/sensor/sensor.h"
#include "beacon/visual_feature/visual_feature.h"
#include "beacon/visual_tracker/visual_tracker.h"

#define BEACON_LOAD_PARAMETER(NAME, PARAM_NAME)           \
  public:                                                 \
    std::unique_ptr<PARAM_NAME> get##PARAM_NAME() const { \
        return std::make_unique<PARAM_NAME>(m_##NAME);    \
    }                                                     \
                                                          \
  protected:                                              \
    PARAM_NAME m_##NAME{};                                \
    bool load##PARAM_NAME(const cv::FileStorage& fs);

namespace beacon {

class ParameterHandler {
  public:
    ParameterHandler();
    ParameterHandler(const std::string& path);

    bool loadBeaconParameter(const std::string& path);
    bool loadCameraParameter(const std::string& path);

  public:
    std::unique_ptr<VisualFeature> getVisualFeature() const;
    std::unique_ptr<VisualTracker> getVisualTracker() const;
    const std::vector<CameraPtr>& getCameras() const;

  protected:
    bool loadBeaconParameterImpl(const cv::FileStorage& fs);
    bool loadCameraParameterImpl(const cv::FileStorage& fs);

  protected:
    BeaconLogger::Ptr m_logger{BeaconLogger::get()};

    // camera parameter
    std::vector<CameraPtr> m_cameras;

    // visual feature and tracker type
    std::string m_visual_feature{};
    std::string m_visual_tracker{};

    // beacon parameter
    BEACON_LOAD_PARAMETER(gftt, GFTTParameter)
    BEACON_LOAD_PARAMETER(lucas_kanade, LucasKanadeTrackerParameter)

#ifdef BEACON_ENABLE_TORCH
    BEACON_LOAD_PARAMETER(superpoint, SuperPointParameter)
#endif

#ifdef BEACON_ENABLE_TENSORRT
    BEACON_LOAD_PARAMETER(superpoint_trt, SuperPointTRTParameter)
    BEACON_LOAD_PARAMETER(superglue_trt, SuperGlueTRTParameter)
    BEACON_LOAD_PARAMETER(lightglue_trt, LightGlueTRTParameter)
#endif

};  // class ParameterHandler

}  // namespace beacon
