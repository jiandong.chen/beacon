#pragma once
#include <memory>

#include "../feature_management/feature_manager.h"
#include "../labeler/odometry_label.h"
#include "../logger/logger.h"
#include "../motion_estimation/pose_from_match.h"
#include "../motion_estimation/pose_from_pnp.h"
#include "../parameter_handler/parameter_handler.h"
#include "../ticktock/ticktock.h"
#include "beacon/datatype/image_data.h"
#include "beacon/geometry/triangulate_match_cv.h"

namespace beacon {

class StereoOdometry {
  public:
    using Ptr = std::unique_ptr<StereoOdometry>;

  public:
    // constructor
    StereoOdometry();

    static Ptr create();

    // setup from config file
    void loadParameter(const ParameterHandler& param);

    // check everything odometry required
    // set valid if pass the test
    // should be called before process
    bool init();

  public:
    // main process function for images
    void processImage(ImagePtr left_image_data, ImagePtr right_image_data);

  protected:
    void processImageImpl(ImagePtr left_image_data, ImagePtr right_image_data);

  public:
    // setter for modules

    void setVisualFeature(VisualFeaturePtr p) {
        m_logger->info("StereoOdometry new Visual Feature Extractor set!");
        m_extractor = std::move(p);
    }

    void setVisualTracker(VisualTrackerPtr p) {
        m_logger->info("StereoOdometry new Visual Feature Tracker set!");
        m_tracker = std::move(p);
    }

    void setBasePose(const Transform& T_wb) {
        m_T_wb = T_wb;

        m_logger->warn(
            "StereoOdometry setBasePose() Base Pose is updated to {}!",
            m_T_wb);
    }

  public:
    // getter for processed data

    BEACON_NODISCARD ImageConstPtr getLastLeftKeyframe() const {
        return m_last_left_keyframe;
    }

    BEACON_NODISCARD ImageConstPtr getLastRightKeyframe() const {
        return m_last_right_keyframe;
    }

    BEACON_NODISCARD ImageConstPtr getLastLeftImage() const {
        return m_last_left_image;
    }

    BEACON_NODISCARD ImageConstPtr getLastRightImage() const {
        return m_last_right_image;
    }

    BEACON_NODISCARD VisualFeatureMatchDataConstPtr getLastTrack() const {
        return m_last_track;
    }

    BEACON_NODISCARD VisualFeatureMatchDataConstPtr getLastStereoTrack() const {
        return m_last_stereo_track;
    }

    BEACON_NODISCARD Transform getTransformWB() const {
        return m_T_wb;
    }

  protected:
    VisualFeatureMatchDataPtr processMonocular(ImagePtr last, ImagePtr curr);
    VisualFeatureMatchDataPtr processStereo(ImagePtr left, ImagePtr right);

  protected:
    // feature extractor for left image
    VisualFeaturePtr m_extractor{};

    // feature tracker monocular style
    VisualTrackerPtr m_tracker{};

    VisualTrackerPtr m_stereo_tracker{};

    // manager to handle landmarks
    FeatureManagerPtr m_feature_manager{};

    // triangulate algorithm
    TriangulateMatchCV m_triangulate{};

    // motion estimation algorithm
    PoseFromMatch m_pose_from_ess_mat{};
    PoseFromPnP m_pose_from_pnp{};

  protected:
    // intermediate data

    ImagePtr m_last_left_keyframe{};           // last processed left keyframe
    ImagePtr m_last_right_keyframe{};          // last processed right keyframe
    ImagePtr m_last_left_image{};              // last processed left frame
    ImagePtr m_last_right_image{};             // last processed right frame
    VisualFeatureMatchDataPtr m_last_track{};  // last monocular track res
    VisualFeatureMatchDataPtr m_last_stereo_track{};  // last stereo track res

    using StereoLabel = StereoOdometryLabel::LabelEnum;
    StereoOdometryLabel m_label{};  // handler represent current state of odom

    Transform m_T_wb{};  // Transform between world and base

  protected:
    // Logger for Debug, Info and Warning
    BeaconLoggerPtr m_logger{};

    // Timer for recording processing time
    TickTock m_timer{};

};  // class StereoOdometry

using StereoOdometryPtr = StereoOdometry::Ptr;

}  // namespace beacon
