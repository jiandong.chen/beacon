#pragma once
#include "beacon/core/enum_label.h"

namespace beacon {

enum class StereoOdometryLabelEnum : BEACON_ENUM_UNDERLYING {
    Valid = 1 << 0,
};

using StereoOdometryLabel = LabelHandler<StereoOdometryLabelEnum>;

}  // namespace beacon
