#pragma once
#include "beacon/core/enum_label.h"

namespace beacon {

enum class StereoPipelineLabelEnum : BEACON_ENUM_UNDERLYING {
    Valid = 1 << 0,
};

using StereoPipelineLabel = LabelHandler<StereoPipelineLabelEnum>;

}  // namespace beacon
