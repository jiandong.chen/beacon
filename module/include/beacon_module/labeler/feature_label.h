#pragma once

#include "beacon/core/enum_label.h"
#include "beacon/datatype/visual_feature_data.h"

namespace beacon {

enum class FeatureLabelEnum : BEACON_ENUM_UNDERLYING {
    MonocularMatched = 1 << 0,      // match with last (left) frame
    StereoMatched = 1 << 1,         // match with left / right image
    LandmarkMatched = 1 << 2,       // match with existed Landmark
    StereoLowParallax = 1 << 3,     // match with right image but LowParallax
    MonocularLowParallax = 1 << 4,  // match with last image but LowParallax

};  // enum class FeatureLabel

struct FeatureLabelGet {
    BEACON_NODISCARD BEACON_ENUM_UNDERLYING& operator()(
        VisualFeaturePoint& data) const {
        return data.label;
    }

    BEACON_NODISCARD const BEACON_ENUM_UNDERLYING& operator()(
        const VisualFeaturePoint& data) const {
        return data.label;
    }
};  // struct FeatureLabelGet

using FeatureLabel = FeatureLabelEnum;

using FeatureLabelAdaptor =
    LabelAdaptor<FeatureLabelEnum, VisualFeaturePoint, FeatureLabelGet>;

using ConstFeatureLabelAdaptor =
    LabelAdaptor<FeatureLabelEnum, const VisualFeaturePoint, FeatureLabelGet>;

}  // namespace beacon
