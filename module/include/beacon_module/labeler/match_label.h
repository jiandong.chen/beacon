#pragma once

#include "beacon/core/enum_label.h"
#include "beacon/datatype/visual_match_data.h"

namespace beacon {

enum class MatchLabelEnum : BEACON_ENUM_UNDERLYING {
    Inlier = 1 << 0,
    InlierEssentialMat = 1 << 1,
    InlierRecoverPose = 1 << 2,
    LowParallax = 1 << 3,

};  // enum class MatchLabel

struct MatchLabelGet {
    BEACON_NODISCARD BEACON_ENUM_UNDERLYING& operator()(
        VisualFeatureMatch& data) const {
        return data.label;
    }

    BEACON_NODISCARD const BEACON_ENUM_UNDERLYING& operator()(
        const VisualFeatureMatch& data) const {
        return data.label;
    }
};  // struct MatchLabelGet

using MatchLabel = MatchLabelEnum;

using MatchLabelAdaptor =
    LabelAdaptor<MatchLabelEnum, VisualFeatureMatch, MatchLabelGet>;

using ConstMatchLabelAdaptor =
    LabelAdaptor<MatchLabelEnum, const VisualFeatureMatch, MatchLabelGet>;

}  // namespace beacon
