#pragma once

#include "beacon/core/enum_label.h"
#include "beacon/datatype/image_data.h"

namespace beacon {
enum class ImageDataLabelEnum : BEACON_ENUM_UNDERLYING {
    Keyframe = 1 << 0,  // Is this ImageData a Keyframe

};  // enum class ImageDataLabelEnum

struct ImageDataLabelGet {
    BEACON_NODISCARD BEACON_ENUM_UNDERLYING& operator()(ImageData& data) const {
        return data.label;
    }

    BEACON_NODISCARD const BEACON_ENUM_UNDERLYING& operator()(
        const ImageData& data) const {
        return data.label;
    }
};  // struct ImageDataLabelGet

using ImageDataLabel = ImageDataLabelEnum;

using ImageDataLabelAdaptor =
    LabelAdaptor<ImageDataLabelEnum, ImageData, ImageDataLabelGet>;

using ImageDataatureLabelAdaptor =
    LabelAdaptor<ImageDataLabelEnum, const ImageData, ImageDataLabelGet>;

}  // namespace beacon
