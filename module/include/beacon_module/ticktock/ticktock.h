#pragma once
#include <chrono>
#include <mutex>
#include <string>
#include <unordered_map>
#include <vector>

#include "../logger/logger.h"

namespace beacon {

class TickTock {
  public:
    TickTock();

    void tick(const std::string& name);
    long long tock(const std::string& name);

    const std::vector<long long>& getData(const std::string& name);

  protected:
    void registerTimer(const std::string& name);

  protected:
    BeaconLogger::Ptr m_logger;
    std::mutex m_mtx;

    using TimePoint = std::chrono::steady_clock::time_point;
    std::unordered_map<std::string, TimePoint> m_begin_stamp{};

    // map name to list of time duration [microsecond]
    std::unordered_map<std::string, std::vector<long long>> m_data{};

  public:
    inline static bool enable_record{false};

};  // class TickTock

}  // namespace beacon
