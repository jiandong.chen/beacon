#pragma once

#include <functional>
#include <opencv4/opencv2/opencv.hpp>
#include <vector>

#include "../logger/logger.h"
#include "beacon/datatype/image_data.h"
#include "beacon/datatype/visual_feature_data.h"
#include "beacon/datatype/visual_match_data.h"

#define BEACON_ADD_IMAGE_DRAWER_PARAM(var_name,      \
                                      type,          \
                                      init_value,    \
                                      set_func_name) \
  public:                                            \
    auto set##set_func_name(type val) {              \
        m_##var_name = val;                          \
        return *this;                                \
    }                                                \
                                                     \
  protected:                                         \
    type m_##var_name{init_value};

namespace beacon {

// Input ImageData and draw the feature point on it
// Input Match and draw the match line and (optionally) feature points
// return drew image (cv::Mat)
class ImageDrawer {
  public:
    // main feature drawing function
    BEACON_NODISCARD cv::Mat drawKeypoint(const ImageData& image);

  public:
    // add color to represent feature
    using FeaturePred = std::function<bool(const VisualFeaturePoint&)>;
    using FeaturePredColorPair = std::pair<FeaturePred, cv::Scalar>;

    void registerColorWithFeaturePred(cv::Scalar color_bgr,
                                      const FeaturePred& pred);

  protected:
    // store all color and pred pair
    std::vector<FeaturePredColorPair> m_feature_pred;

  public:
    // directly draw match with non-nullptr pdata
    BEACON_NODISCARD cv::Mat drawMatch(const VisualFeatureMatchData& match);

    BEACON_NODISCARD cv::Mat drawMatchSingle(
        const VisualFeatureMatchData& match);

    // draw match with given imagedata and match
    // no weakptr needed
    BEACON_NODISCARD cv::Mat drawMatch(const ImageData& image0,
                                       const ImageData& image1,
                                       const VisualFeatureMatchData& match);

    BEACON_NODISCARD cv::Mat drawMatchSingle(
        const ImageData& image0,
        const ImageData& image1,
        const VisualFeatureMatchData& match);

  protected:
    // main match drawing function
    template <typename PointDataType>
    BEACON_NODISCARD cv::Mat drawMatchImpl(
        const cv::Mat& img0,
        const std::vector<cv::Point_<PointDataType>>& vec_pt0,
        const cv::Mat& img1,
        const std::vector<cv::Point_<PointDataType>>& vec_pt1);

    template <typename PointDataType>
    BEACON_NODISCARD cv::Mat drawMatchSingleImpl(
        const cv::Mat& img,
        const std::vector<cv::Point_<PointDataType>>& vec_pt0,
        const std::vector<cv::Point_<PointDataType>>& vec_pt1);

  public:
    // parameters for behavior of drawer
    BEACON_ADD_IMAGE_DRAWER_PARAM(draw_match_with_all_point,
                                  bool,
                                  true,
                                  MatchWithAllPoint)
    BEACON_ADD_IMAGE_DRAWER_PARAM(point_size, int, 5, PointSize)
    BEACON_ADD_IMAGE_DRAWER_PARAM(point_color, cv::Scalar, yellow, PointColor)
    BEACON_ADD_IMAGE_DRAWER_PARAM(line_thick, int, 1, LinkThickness)
    BEACON_ADD_IMAGE_DRAWER_PARAM(line_color, cv::Scalar, green, LineColor)

  public:
    // color preset
    inline static cv::Scalar blue{255, 0, 0};       // unused
    inline static cv::Scalar cyan{255, 255, 0};     // unused
    inline static cv::Scalar red{0, 0, 255};        // unused
    inline static cv::Scalar violet{255, 0, 139};   // unused
    inline static cv::Scalar white{255, 255, 255};  // unused
    inline static cv::Scalar green{0, 255, 0};      // default match line
    inline static cv::Scalar yellow{0, 255, 255};   // default feature point

  public:
    // logger for debug
    BeaconLoggerPtr m_logger{BeaconLogger::get()};

};  // class ImageDrawer

/* ##################### ImageDrawer ######################################## */
/* ########################################################################## */
template <typename PointDataType>
cv::Mat ImageDrawer::drawMatchImpl(
    const cv::Mat& img0,
    const std::vector<cv::Point_<PointDataType>>& vec_pt0,
    const cv::Mat& img1,
    const std::vector<cv::Point_<PointDataType>>& vec_pt1) {
    if (img0.empty() || img1.empty() || vec_pt0.size() != vec_pt1.size()) {
        return {};
    }

    cv::Mat res;
    cv::hconcat(img0, img1, res);

    for (std::size_t i = 0; i < vec_pt0.size(); ++i) {
        const auto& kp0 = vec_pt0[i];
        auto kp1 = vec_pt1[i];
        kp1.x += img0.cols;
        cv::line(res,
                 kp0,
                 kp1,
                 m_line_color,
                 m_line_thick,
                 cv::LineTypes::LINE_4);
    }

    return res;
}

/* ########################################################################## */
template <typename PointDataType>
cv::Mat ImageDrawer::drawMatchSingleImpl(
    const cv::Mat& img1,
    const std::vector<cv::Point_<PointDataType>>& vec_pt0,
    const std::vector<cv::Point_<PointDataType>>& vec_pt1) {
    if (img1.empty() || vec_pt0.size() != vec_pt1.size()) {
        return {};
    }

    cv::Mat res = img1.clone();

    for (std::size_t i = 0; i < vec_pt0.size(); ++i) {
        cv::line(res,
                 vec_pt0[i],
                 vec_pt1[i],
                 m_line_color,
                 m_line_thick,
                 cv::LineTypes::LINE_4);
    }

    return res;
}

}  // namespace beacon
