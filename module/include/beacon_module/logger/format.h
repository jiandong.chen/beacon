#pragma once
#include <spdlog/spdlog.h>

#include <opencv2/core/types.hpp>

#include "beacon/core/timestamp.h"
#include "beacon/core/transform.h"

// enable spdlog (fmt) for user-define class

/* ##################### Timestamp ########################################## */
template <>
struct fmt::formatter<beacon::Timestamp> : fmt::formatter<std::string> {
    auto format(const beacon::Timestamp& timestamp,
                format_context& ctx) const -> decltype(ctx.out()) {
        const auto sec = timestamp.getSec();

        return format_to(ctx.out(), "{}", sec);
    }
};

/* ##################### Transform ########################################## */
template <>
struct fmt::formatter<beacon::Transform> : fmt::formatter<std::string> {
    auto format(const beacon::Transform& transform,
                format_context& ctx) const -> decltype(ctx.out()) {
        return format_to(ctx.out(),
                         "xyz: [{}, {}, {}] q_wxyz: [{}, {}, {}, {}]",
                         transform.getTranslation().x(),
                         transform.getTranslation().y(),
                         transform.getTranslation().z(),
                         transform.getRotation().w(),
                         transform.getRotation().x(),
                         transform.getRotation().y(),
                         transform.getRotation().z());
    }
};

/* ##################### cv::Scalar ######################################### */
template <>
struct fmt::formatter<cv::Scalar> : fmt::formatter<std::string> {
    auto format(const cv::Scalar& val,
                format_context& ctx) const -> decltype(ctx.out()) {
        return format_to(ctx.out(),
                         "BGR: [{} {} {}]",
                         val.val[0],
                         val.val[1],
                         val.val[2]);
    }
};

namespace beacon {};  // namespace beacon
