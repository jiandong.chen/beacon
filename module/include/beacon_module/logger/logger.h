#pragma once

#include <spdlog/spdlog.h>

#include "format.h"

namespace beacon {

class BeaconLogger {
  public:
    using Ptr = std::shared_ptr<spdlog::logger>;

  public:
    // member func to get and setup the logger
    static Ptr get();
    static Ptr enableLogFile(const std::string& path = default_logger_file_path,
                             const bool append = default_logger_file_append);
    static Ptr enableDebug();
    static Ptr enableInfo();
    static void disableLogger();

  protected:
    static Ptr initLogger();
    static void clearLogger();  // TODO NO USE

  public:
    BeaconLogger() = delete;
    BeaconLogger(const BeaconLogger&) = delete;
    BeaconLogger& operator=(const BeaconLogger&) = delete;

  protected:
    inline static Ptr instance{nullptr};

  public:
    // logger default setup
    inline static auto default_logger_name{"Beacon"};
    inline static auto default_logger_file_path{"/workspace/log/beaconlog.txt"};
    inline static auto default_logger_file_append{false};
    inline static auto default_logger_level{spdlog::level::info};

};  // class BeaconLogger

using BeaconLoggerPtr = BeaconLogger::Ptr;

}  // namespace beacon
