#pragma once

#include <opencv4/opencv2/opencv.hpp>

#include "../image_drawer/image_drawer.h"
#include "../labeler/pipeline_label.h"
#include "../stereo_odometry/stereo_odometry.h"
#include "display_image.h"

namespace beacon {

class StereoPipeline {
  public:
    // constructor
    StereoPipeline();

    // setup from config file
    void loadParameter(const ParameterHandler& param);

    // check everything pipeline required
    // set valid if pass the test
    // should be called before process
    bool init();

  public:
    // main process func
    // create ImageData, pass to odom, visulize the results
    void process(cv::Mat left_img,
                 const Timestamp& left_timestamp,
                 cv::Mat right_img,
                 const Timestamp& right_timestamp);

  protected:
    void processImpl(cv::Mat left_img,
                     const Timestamp& left_timestamp,
                     cv::Mat right_img,
                     const Timestamp& right_timestamp);

  public:
    // setter for data member

    void setCameraPair(CameraPtr left_cam, CameraPtr right_cam) {
        m_left_cam = left_cam;
        m_right_cam = right_cam;
    }

    void setDrawer(ImageDrawer drawer) {
        m_left_drawer = std::move(drawer);
    }

  public:
    // getter for processed data

    DisplayImage getDisplayImage() const {
        return m_display_image;
    }

    ImageConstPtr getLastLeftKeyframe() const {
        return m_odom->getLastLeftKeyframe();
    }

    ImageConstPtr getLastRightKeyframe() const {
        return m_odom->getLastRightKeyframe();
    }

  protected:
    // intermediate data

    // images for displaying
    DisplayImage m_display_image{};

    // handler represent current state of pipeline
    using PipelineLabel = StereoPipelineLabel::LabelEnum;
    StereoPipelineLabel m_label{};

  protected:
    // main odometry
    StereoOdometryPtr m_odom{};

    // left and right camera model for ImageData
    CameraPtr m_left_cam{};
    CameraPtr m_right_cam{};

    // Logger for Debug, Info and Warning
    BeaconLoggerPtr m_logger{};

    // drawer for displaying processing image with feature
    ImageDrawer m_left_drawer{};
    ImageDrawer m_right_drawer{};

};  // class StereoPipeline

}  // namespace beacon
