#pragma once
#include <opencv4/opencv2/opencv.hpp>

namespace beacon {

struct DisplayImage {
    cv::Mat left_image{};       // display left image with feature
    cv::Mat right_image{};      // display rightimage with feature
    cv::Mat monocular_match{};  // display match with last & curr image
    cv::Mat stereo_match{};     // display match with left & right image

};  // struct DisplayImage

}  // namespace beacon
