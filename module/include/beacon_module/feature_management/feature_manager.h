#pragma once

#include <memory>
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>

#include "../logger/logger.h"
#include "beacon/datatype/image_data.h"
#include "beacon/datatype/landmark.h"

namespace beacon {

class FeatureManager {
  public:
    using Ptr = std::shared_ptr<FeatureManager>;

    using FrameSet = std::unordered_set<ImagePtr>;
    using LandmarkLinkVec = std::vector<LandmarkLink>;
    using LandmarkMap = std::unordered_map<LandmarkPtr, LandmarkLinkVec>;

  public:
    // constructor
    FeatureManager(std::string_view name = default_name) : m_name(name) {
    }

    static Ptr create() {
        return std::make_shared<FeatureManager>();
    }

  public:
    FrameSet::iterator addFrame(ImagePtr pf);
    LandmarkMap::iterator addLandmark(LandmarkPtr pl);

    void removeFrame(ImagePtr pf);
    void removeLandmark(LandmarkPtr pl);

    FrameSet getFrames() const {
        return m_frames;
    }

    LandmarkMap getLandmarks() const {
        return m_landmarks;
    }

    void linkLandmarkToFrame(ImagePtr pf);

  protected:
    std::string m_name{};
    BeaconLogger::Ptr m_logger{BeaconLogger::get()};

    FrameSet m_frames{};
    LandmarkMap m_landmarks{};

    inline static std::string default_name = "defaultFeatureManager";

};  // class FeatureManager

using FeatureManagerPtr = FeatureManager::Ptr;

};  // namespace beacon
