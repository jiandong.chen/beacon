#include "../include/beacon_module/image_drawer/image_drawer.h"

namespace beacon {

/* ##################### ImageDrawer ######################################## */
/* ########################################################################## */
void ImageDrawer::registerColorWithFeaturePred(cv::Scalar color_bgr,
                                               const FeaturePred& pred) {
    m_logger->debug("ImageDrawer register feature predicate with color: {}",
                    color_bgr);

    m_feature_pred.emplace_back(std::make_pair(pred, color_bgr));
}

/* ########################################################################## */
BEACON_NODISCARD cv::Mat ImageDrawer::drawKeypoint(const ImageData& image) {
    cv::Mat res = image.image.clone();
    if (image.feature == nullptr || image.feature->points.empty()) {
        return res;
    }

    for (const auto& pt : image.feature->points) {
        auto point_color = m_point_color;
        for (const auto& [pred, color] : m_feature_pred) {
            if (pred(pt)) {
                point_color = color;
            }
        }
        cv::circle(res, pt.uv2CV<float>(), 0, point_color, m_point_size);
    }
    return res;
}

/* ########################################################################## */
BEACON_NODISCARD cv::Mat ImageDrawer::drawMatch(
    const VisualFeatureMatchData& match) {
    if (auto p0 = match.pdata0.lock(), p1 = match.pdata1.lock();
        !p0 || !p1 || match.point0.size() != match.point1.size()) {
        // some data missing
        // cannot draw
        throw std::runtime_error("drawMatch drawMatch required data miss!!");

    } else {
        return drawMatch(*p0, *p1, match);
    }

    return {};
}

/* ########################################################################## */
BEACON_NODISCARD cv::Mat ImageDrawer::drawMatchSingle(
    const VisualFeatureMatchData& match) {
    if (auto p0 = match.pdata0.lock(), p1 = match.pdata1.lock();
        !p0 || !p1 || match.point0.size() != match.point1.size()) {
        // some data missing
        // cannot draw
        throw std::runtime_error(
            "drawMatch drawMatchSingle required data miss!!");

    } else {
        return drawMatchSingle(*p0, *p1, match);
    }

    return {};
}

/* ########################################################################## */
cv::Mat ImageDrawer::drawMatch(const ImageData& image0,
                               const ImageData& image1,
                               const VisualFeatureMatchData& match) {
    if (!image0.feature || !image1.feature) {
        return {};
    }

    return drawMatchImpl(
        m_draw_match_with_all_point ? drawKeypoint(image0) : image0.image,
        PointUVXY::uv2CV<float>(match.point0.cbegin(), match.point0.cend()),
        m_draw_match_with_all_point ? drawKeypoint(image1) : image1.image,
        PointUVXY::uv2CV<float>(match.point1.cbegin(), match.point1.cend()));
}

/* ########################################################################## */
cv::Mat ImageDrawer::drawMatchSingle(const ImageData& image0,
                                     const ImageData& image1,
                                     const VisualFeatureMatchData& match) {
    if (!image0.feature || !image1.feature) {
        return {};
    }

    return drawMatchSingleImpl(
        drawKeypoint(image1),
        PointUVXY::uv2CV<float>(match.point0.cbegin(), match.point0.cend()),
        PointUVXY::uv2CV<float>(match.point1.cbegin(), match.point1.cend()));
}

}  // namespace beacon
