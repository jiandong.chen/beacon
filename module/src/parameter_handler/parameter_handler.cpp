#include "../../include/beacon_module/parameter_handler/parameter_handler.h"

#include <filesystem>

#define BEACON_READ_BOOL(NAME, VAR)                             \
    if (node[NAME].isInt()) {                                   \
        VAR = static_cast<int>(node[NAME]) != 0 ? true : false; \
    }
#define BEACON_READ_INT(NAME, VAR)          \
    if (node[NAME].isInt()) {               \
        VAR = static_cast<int>(node[NAME]); \
    }
#define BEACON_READ_REAL(NAME, VAR) \
    if (node[NAME].isReal()) {      \
        VAR = node[NAME].real();    \
    }
#define BEACON_READ_STRING(NAME, VAR) \
    if (node[NAME].isString()) {      \
        VAR = node[NAME].string();    \
    }
#define BEACON_VISUAL_FEATURE_DISPATCH(REG_ID, PARAM)                    \
    if (m_visual_feature == REG_ID) {                                    \
        m_logger->info("ParameterHandler request feature extractor: {}", \
                       m_visual_feature);                                \
        return VisualFeature::create(REG_ID, PARAM);                     \
    }
#define BEACON_VISUAL_TRACKER_DISPATCH(REG_ID, PARAM)                  \
    if (m_visual_tracker == REG_ID) {                                  \
        m_logger->info("ParameterHandler request feature tracker: {}", \
                       m_visual_tracker);                              \
        return VisualTracker::create(REG_ID, PARAM);                   \
    }

namespace beacon {

/* ##################### ParameterHandler Helper ############################ */
/* ########################################################################## */
static bool isValidPath(const std::string& s) {
    std::filesystem::path path{s};

    return std::filesystem::exists(path);
}

/* ##################### ParameterHandler ################################### */
/* ########################################################################## */
ParameterHandler::ParameterHandler() = default;

/* ########################################################################## */
ParameterHandler::ParameterHandler(const std::string& path) {
    loadBeaconParameter(path);
}

/* ########################################################################## */
bool ParameterHandler::loadBeaconParameter(const std::string& path) {
    m_logger->info("ParameterHandler loading Beacon Config File: {}", path);

    if (!isValidPath(path)) {
        m_logger->warn("ParameterHandler find invalid path: {}", path);
        return false;
    }

    cv::FileStorage fs(path, cv::FileStorage::READ);
    if (!fs.isOpened()) {
        m_logger->warn("ParameterHandler cannot open file: {}", path);
        return false;
    }

    // process general config
    bool status = loadBeaconParameterImpl(fs);

    fs.release();
    return status;
}

/* ########################################################################## */
bool ParameterHandler::loadBeaconParameterImpl(const cv::FileStorage& fs) {
    if (fs["beacon"].empty()) {
        m_logger->warn(
            "ParameterHandler loadBeaconParameter FAILED!! Beacon Config is "
            "empty!");
        return false;
    }

    bool status = false;
    status |= loadGFTTParameter(fs);
    status |= loadLucasKanadeTrackerParameter(fs);

#ifdef BEACON_ENABLE_TORCH
    status |= loadSuperPointParameter(fs);
#endif

#ifdef BEACON_ENABLE_TENSORRT
    status |= loadSuperPointTRTParameter(fs);
    status |= loadSuperGlueTRTParameter(fs);
    status |= loadLightGlueTRTParameter(fs);
#endif

    // other
    const auto node = fs["beacon"];
    BEACON_READ_STRING("detector", m_visual_feature);
    BEACON_READ_STRING("tracker", m_visual_tracker);

    // if camera_config_path provided, load camera config
    std::string camera_path;
    BEACON_READ_STRING("camera_config_path", camera_path);
    if (!camera_path.empty()) {
        loadCameraParameter(camera_path);
    }

    return status;
}

/* ########################################################################## */
bool ParameterHandler::loadCameraParameter(const std::string& path) {
    m_logger->info("ParameterHandler loading Camera Config File: {}", path);

    if (!isValidPath(path)) {
        m_logger->info("ParameterHandler find invalid path: {}", path);
        return false;
    }

    cv::FileStorage fs(path, cv::FileStorage::READ);
    if (!fs.isOpened()) {
        m_logger->info("ParameterHandler cannot open file: {}", path);
        return false;
    }

    // process general config
    bool status = loadCameraParameterImpl(fs);

    fs.release();
    return status;
}

/* ########################################################################## */
bool ParameterHandler::loadCameraParameterImpl(const cv::FileStorage& fs) {
    const auto cam_node = fs["camera"];
    if (cam_node.empty()) {
        m_logger->warn(
            "ParameterHandler loadCameraParameter FAILED!! Camera Config is "
            "empty!");
        return false;
    }

    bool is_resize = false;
    auto resize_node = fs["resize_image"];
    if (!resize_node.empty()) {
        is_resize = resize_node["enable"].isInt() &&
                            static_cast<int>(resize_node["enable"]) != 0
                        ? true
                        : false;
    }

    auto cam_it = cam_node.begin();
    for (; cam_it != cam_node.end(); ++cam_it) {
        // process each camera

        auto node = *cam_it;

        int img_height{}, img_width{};
        BEACON_READ_INT("height", img_height);
        BEACON_READ_INT("width", img_width);

        double fx{}, fy{}, cx{}, cy{};
        BEACON_READ_REAL("fx", fx);
        BEACON_READ_REAL("fy", fy);
        BEACON_READ_REAL("cx", cx);
        BEACON_READ_REAL("cy", cy);
        CameraPtr p_cam = nullptr;
        if (is_resize) {
            int new_width = resize_node["width"];
            int new_height = resize_node["height"];
            p_cam = PinholeResize::create(img_width,
                                          img_height,
                                          new_width,
                                          new_height,
                                          fx,
                                          fy,
                                          cx,
                                          cy);
            m_logger->info(
                "ParameterHandler Camera Image Resize Enable! [w, h] = [{}, "
                "{}]",
                new_width,
                new_height);
        } else {
            p_cam = Pinhole::create(fx, fy, cx, cy);
            p_cam->setImageSize(img_width, img_height);
        }
        m_logger->info(
            "ParameterHandler Camera get Camera Intrinsic! [fx, fy] = [{}, {}] "
            "[cx, cy] = [{}, {}]",
            fx,
            fy,
            cx,
            cy);

        double qw{}, qx{}, qy{}, qz{};
        BEACON_READ_REAL("qw", qw);
        BEACON_READ_REAL("qx", qx);
        BEACON_READ_REAL("qy", qy);
        BEACON_READ_REAL("qz", qz);

        double tx{}, ty{}, tz{};
        BEACON_READ_REAL("tx", tx);
        BEACON_READ_REAL("ty", ty);
        BEACON_READ_REAL("tz", tz);
        Transform tf{tx, ty, tz, qw, qx, qy, qz};
        p_cam->setTransformBC(tf);

        std::vector<double> distortion;
        auto dist_node = node["distortion"];
        auto dist_it = dist_node.begin();
        for (; dist_it != dist_node.end(); ++dist_it) {
            distortion.push_back((*dist_it).real());
        }
        p_cam->setDistortion(distortion.begin(), distortion.end());
        m_cameras.push_back(p_cam);
    }

    return true;
}

/* ########################################################################## */
std::unique_ptr<VisualFeature> ParameterHandler::getVisualFeature() const {
    BEACON_VISUAL_FEATURE_DISPATCH(GFTT::register_idx, m_gftt);

#ifdef BEACON_ENABLE_TORCH
    BEACON_VISUAL_FEATURE_DISPATCH(SuperPoint::register_idx, m_superpoint);
#endif

#ifdef BEACON_ENABLE_TENSORRT
    BEACON_VISUAL_FEATURE_DISPATCH(SuperPointTRT::register_idx,
                                   m_superpoint_trt);
#endif

    return nullptr;
}

/* ########################################################################## */
std::unique_ptr<VisualTracker> ParameterHandler::getVisualTracker() const {
    BEACON_VISUAL_TRACKER_DISPATCH(LucasKanadeTracker::register_idx,
                                   m_lucas_kanade);

#ifdef BEACON_ENABLE_TENSORRT
    BEACON_VISUAL_TRACKER_DISPATCH(SuperGlueTRT::register_idx, m_superglue_trt);
    BEACON_VISUAL_TRACKER_DISPATCH(LightGlueTRT::register_idx, m_lightglue_trt);
#endif

    return nullptr;
}

/* ########################################################################## */
const std::vector<CameraPtr>& ParameterHandler::getCameras() const {
    return m_cameras;
}

/* ########################################################################## */
bool ParameterHandler::loadGFTTParameter(const cv::FileStorage& fs) {
    auto node = fs["beacon"]["GFTT"];
    if (node.empty()) {
        return false;
    }

    BEACON_READ_INT("max_keypoint", m_gftt.m_max_keypoint);
    BEACON_READ_INT("block_size", m_gftt.m_block_size);
    BEACON_READ_REAL("quality_level", m_gftt.m_quality_level);
    BEACON_READ_REAL("min_keypoint_distance", m_gftt.m_min_keypoint_distance);

    return true;
}

/* ########################################################################## */
bool ParameterHandler::loadLucasKanadeTrackerParameter(
    const cv::FileStorage& fs) {
    auto node = fs["beacon"]["LucasKanade"];
    if (node.empty()) {
        return false;
    }

    BEACON_READ_INT("max_iter", m_lucas_kanade.m_max_iter);
    BEACON_READ_INT("win_size", m_lucas_kanade.m_win_size);
    BEACON_READ_INT("max_pyramid_level", m_lucas_kanade.m_max_pyramid_level);
    BEACON_READ_INT("flag", m_lucas_kanade.m_flag);
    BEACON_READ_REAL("min_eigen_threshold",
                     m_lucas_kanade.m_min_eigen_threshold);
    BEACON_READ_REAL("iter_stop_eps", m_lucas_kanade.m_iter_stop_eps);

    return true;
}

/* ########################################################################## */
#ifdef BEACON_ENABLE_TORCH
bool ParameterHandler::loadSuperPointParameter(const cv::FileStorage& fs) {
    auto node = fs["beacon"]["SuperPoint"];
    if (node.empty()) {
        return false;
    }

    BEACON_READ_BOOL("use_cuda", m_superpoint.m_use_cuda);
    BEACON_READ_INT("max_keypoint", m_superpoint.m_max_keypoint);
    BEACON_READ_INT("remove_borders", m_superpoint.m_remove_borders);
    BEACON_READ_INT("nms_radius", m_superpoint.m_nms_radius);
    BEACON_READ_REAL("keypoint_threshold", m_superpoint.m_keypoint_threshold);
    BEACON_READ_STRING("model_path", m_superpoint.m_model_path);

    return true;
}
#endif

/* ########################################################################## */
#ifdef BEACON_ENABLE_TENSORRT
bool ParameterHandler::loadSuperPointTRTParameter(const cv::FileStorage& fs) {
    auto node = fs["beacon"]["SuperPointTRT"];
    if (node.empty()) {
        return false;
    }

    BEACON_READ_INT("max_keypoint", m_superpoint_trt.m_max_keypoint);
    BEACON_READ_INT("remove_borders", m_superpoint_trt.m_remove_borders);
    BEACON_READ_INT("nms_radius", m_superpoint_trt.m_nms_radius);
    BEACON_READ_REAL("keypoint_threshold",
                     m_superpoint_trt.m_keypoint_threshold);
    BEACON_READ_STRING("engine_path", m_superpoint_trt.m_engine_path);

    return true;
}

/* ########################################################################## */
bool ParameterHandler::loadSuperGlueTRTParameter(const cv::FileStorage& fs) {
    auto node = fs["beacon"]["SuperGlueTRT"];
    if (node.empty()) {
        return false;
    }

    BEACON_READ_INT("match_threshold", m_superglue_trt.m_match_threshold);
    BEACON_READ_REAL("sinkhorn_iterations",
                     m_superpoint_trt.m_keypoint_threshold);
    BEACON_READ_STRING("engine_path", m_superglue_trt.m_engine_path);

    return true;
}

/* ########################################################################## */
bool ParameterHandler::loadLightGlueTRTParameter(const cv::FileStorage& fs) {
    auto node = fs["beacon"]["LightGlueTRT"];
    if (node.empty()) {
        return false;
    }

    BEACON_READ_INT("match_threshold", m_lightglue_trt.m_match_threshold);
    BEACON_READ_STRING("engine_path", m_lightglue_trt.m_engine_path);

    return true;
}
#endif

}  // namespace beacon
