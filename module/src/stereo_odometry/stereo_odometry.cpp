#include "../../include/beacon_module/stereo_odometry/stereo_odometry.h"

#include <gtsam/geometry/Pose3.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/NonlinearEquality.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/slam/ProjectionFactor.h>
#include <gtsam_unstable/slam/InvDepthFactor3.h>

#include "../../include/beacon_module/labeler/feature_label.h"
#include "../../include/beacon_module/labeler/image_label.h"
#include "../../include/beacon_module/labeler/match_label.h"

using gtsam::symbol_shorthand::C;  // pose
// using gtsam::symbol_shorthand::I;  // inv depth
using gtsam::symbol_shorthand::L;  // landmark

namespace beacon {
/* ##################### StereoOdometry Helper ############################## */
/* ########################################################################## */
static auto markLowParallax(VisualFeatureMatchData& match_data,
                            const double min_parallax,
                            const BeaconLoggerPtr& logger) {
    // helper function for marking low parallax
    int count = 0;
    for (std::size_t i = 0; i < match_data.point0.size(); ++i) {
        // manhattan distance
        if ((match_data.point0[i].uv - match_data.point1[i].uv).lpNorm<1>() <
            min_parallax) {
            MatchLabelAdaptor(match_data.matches[i])
                .set(MatchLabel::LowParallax);
            // FeatureLabelAdaptor(data1.points[i])
            //     .set(FeatureLabel::StereoLowParallax);

            ++count;
        }
    }
    logger->info(
        "StereoOdometry mark {}/{} matches ({}%) with LowParallax "
        "(threshold: {} pixels)!",
        count,
        match_data.point0.size(),
        100.0 * count / match_data.point0.size(),
        min_parallax);

    return count;
};

/* ########################################################################## */
static void refineTriangulationMatch(
    VisualFeatureMatchData& tri_feature,
    std::vector<Eigen::Vector3d>& tri_pt,  // in base frame
    const int max_depth,
    const BeaconLoggerPtr& logger) {
    // assume using the stereo setup and the T_lr is known
    // assume all 2d points are normalized
    // refine match triangulation resule using gtsam

    assert(tri_feature.matches.size() == tri_pt.size());
    assert(tri_feature.point0.size() == tri_pt.size());
    assert(tri_feature.point1.size() == tri_pt.size());

    // check cam pose
    std::optional<Transform> T_bl{std::nullopt}, T_br{std::nullopt};
    if (auto left_img = tri_feature.pdata0.lock();
        left_img && left_img->camera) {
        T_bl = left_img->camera->getTransformBC();
    }
    if (auto right_img = tri_feature.pdata1.lock();
        right_img && right_img->camera) {
        T_br = right_img->camera->getTransformBC();
    }
    if (!T_bl.has_value() || !T_br.has_value()) {
        logger->warn(
            "StereoOdometry refineTriangulationMatch Get invalid left and "
            "right cameras! Refinement Skip!");
        return;
    }

    // build optimization graph
    gtsam::NonlinearFactorGraph graph;

    // we use normalized measure point
    // and fix both cam
    graph.emplace_shared<gtsam::NonlinearEquality<gtsam::Pose3>>(
        C(0),
        T_bl->getPose3gtsam());
    graph.emplace_shared<gtsam::NonlinearEquality<gtsam::Pose3>>(
        C(1),
        T_br->getPose3gtsam());
    const gtsam::Cal3_S2::shared_ptr K(new gtsam::Cal3_S2(1, 1, 0, 0, 0));

    // init values
    gtsam::Values init_val;
    init_val.insert(C(0), T_bl->getPose3gtsam());
    init_val.insert(C(1), T_br->getPose3gtsam());

    // add observation
    const auto model = gtsam::noiseModel::Diagonal::Sigmas(
        (gtsam::Vector(2) << 0.001, 0.001).finished());
    for (int i = 0; i < static_cast<int>(tri_pt.size()); ++i) {
        const auto pt0 = tri_feature.point0[i].xy2GTSAM();
        graph.emplace_shared<gtsam::GenericProjectionFactor<gtsam::Pose3>>(
            gtsam::GenericProjectionFactor(pt0, model, C(0), L(i), K));

        const auto pt1 = tri_feature.point1[i].xy2GTSAM();
        graph.emplace_shared<gtsam::GenericProjectionFactor<gtsam::Pose3>>(
            gtsam::GenericProjectionFactor(pt1, model, C(1), L(i), K));

        init_val.insert(L(i), tri_pt[i]);
    }

    gtsam::LevenbergMarquardtOptimizer optimizer(graph, init_val);
    gtsam::Values result = optimizer.optimize();

    logger->info("DONE");

    // remove invalid & undesired pt before optimization
    int count_neg_depth_point = 0;
    int count_far_point = 0;
    std::vector<bool> valid(tri_feature.matches.size(), true);

    for (std::size_t i = 0; i < tri_feature.matches.size(); ++i) {
        // Transform to cam frame and check
        // assume z axis point toward front

        const auto& pt = result.at<gtsam::Point3>(L(i));
        const auto& left_pt = T_bl->getInverse() * pt;
        const auto& right_pt = T_br->getInverse() * pt;
        if (left_pt.z() < 0 || right_pt.z() < 0) {
            count_neg_depth_point++;
            valid[i] = false;
        }
        if (left_pt.z() > max_depth || right_pt.z() > max_depth) {
            count_far_point++;
            valid[i] = false;
        }
    }
    logger->info(
        "StereoOdometry stereo triangulate Refinement get {} resulting point "
        "with "
        "negative depth! These points are excluded!",
        count_neg_depth_point);
    logger->info(
        "StereoOdometry stereo triangulate Refinement get {} far point! "
        "(Threshold {}m) "
        " These points are excluded!",
        count_far_point,
        max_depth);

    tri_feature = VisualFeatureMatchData::refineDataByMask(tri_feature,
                                                           valid.begin(),
                                                           valid.end());
    std::vector<Eigen::Vector3d> new_tri_pt;
    for (std::size_t i = 0; i < tri_pt.size(); ++i) {
        if (valid[i]) {
            new_tri_pt.push_back(tri_pt[i]);
        }
    }
    tri_pt = std::move(new_tri_pt);
}

/* ##################### StereoOdometry ##################################### */
/* ########################################################################## */
StereoOdometry::StereoOdometry() : m_logger{BeaconLogger::get()} {
    m_feature_manager = FeatureManager::create();

    m_logger->info("StereoOdometry Created!");
}

/* ########################################################################## */
void StereoOdometry::processImageImpl(ImagePtr left_image_data,
                                      ImagePtr right_image_data) {
    const auto imageIsValid = [this](const ImageData& left,
                                     const ImageData& right) {
        m_logger->debug(
            "StereoOdometry get two images with timestamp Left: {} Right: {}",
            left.timestamp,
            left.timestamp);

        if (left.timestamp != right.timestamp) {
            m_logger->warn(
                "StereoOdometry Got Timestamp not synchronized! Left: {} "
                "Right: {}",
                left.timestamp,
                right.timestamp);
            // different timestamps might be ok
            // no return false
        }

        if (left.image.empty() || right.image.empty()) {
            m_logger->error("StereoOdometry Get ImageData without image!");

            return false;
        }

        if (!left.camera || !right.camera) {
            m_logger->error("StereoOdometry Get ImageData without camera!");

            return false;
        }

        return true;
    };

    if (!imageIsValid(*left_image_data, *right_image_data)) {
        m_logger->warn(
            "StereoOdometry Get invalid Image pair! Abort the process()!");
        return;
    }

    const auto preprocessImage = [this](ImageData& img) {
        if (!img.feature || img.feature->points.size() == 0) {
            m_extractor->process(img);

            m_logger->info("StereoOdometry extract {} features.",
                           img.feature->points.size());

        } else {
            m_logger->info("StereoOdometry feature extraction SKIP.");
        }
    };

    preprocessImage(*left_image_data);
    preprocessImage(*right_image_data);

    // process last and curr left image
    // and check if current frame is keyframe
    const auto monocular_track =
        processMonocular(m_last_left_keyframe, left_image_data);

    // estimate left cam pose using PnP
    const auto T_lw = m_pose_from_pnp.process(monocular_track);
    if (T_lw.has_value()) {
        m_logger->info("StereoOdometry PnP curr left cam pose T_lw: {}", *T_lw);
        const auto T_wb =
            T_lw->getInverse() *
            left_image_data->camera->getTransformBC().getInverse();
        m_logger->info("StereoOdometry PnP curr base pose T_wb: {}", T_wb);

    } else {
        m_logger->warn("StereoOdometry failed to calculate T_prev_curr");
    }

    // estimate using essential mat
    auto T_prev_curr_ess = m_pose_from_ess_mat.process(monocular_track);
    if (T_prev_curr_ess.has_value()) {
        m_logger->info("StereoOdometry T_prev_curr_ess: {}", *T_prev_curr_ess);

    } else {
        m_logger->warn("StereoOdometry failed to calculate T_prev_curr_ess");
    }

    // check if keyframe
    const auto checkIsKeyframe = [this](
                                     const VisualFeatureMatchData& matchdata,
                                     const double parallax_threshold) -> bool {
        // check by parallax
        int low_parallax_count = 0;
        for (const auto& m : matchdata.matches) {
            if (ConstMatchLabelAdaptor(m).is(MatchLabel::LowParallax)) {
                ++low_parallax_count;
            }
        }
        bool res = (static_cast<double>(low_parallax_count) /
                    matchdata.matches.size()) < parallax_threshold;
        if (!res) {
            m_logger->info(
                "StereoOdometry Monocular Track find too many features "
                "marked "
                "as LowParallax! ({}/{}) (Parallax Threshold: {}%) Current "
                "frame is NOT Keyframe!",
                low_parallax_count,
                matchdata.matches.size(),
                parallax_threshold * 100.0);
        }

        return res;
    };

    // TODO: parallax parameter need to be config
    if (monocular_track && checkIsKeyframe(*monocular_track, 0.6)) {
        ImageDataLabelAdaptor(*left_image_data).set(ImageDataLabel::Keyframe);
    }
    m_logger->info(
        "StereoOdometry Is current frame a Keyframe: {}",
        ImageDataLabelAdaptor(*left_image_data).is(ImageDataLabel::Keyframe));

    // process curr left and right image
    const auto stereo_track = processStereo(left_image_data, right_image_data);

    // update intermediate data
    if (ImageDataLabelAdaptor(*left_image_data).is(ImageDataLabel::Keyframe)) {
        m_logger->info("Get New Keyframe! Update Intermediate Data!");

        m_last_left_keyframe = left_image_data;
        m_last_right_keyframe = right_image_data;
    }
    m_last_left_image = left_image_data;
    m_last_right_image = right_image_data;
    m_last_track = monocular_track;
    m_last_stereo_track = stereo_track;
}

/* ########################################################################## */
VisualFeatureMatchDataPtr StereoOdometry::processMonocular(ImagePtr last,
                                                           ImagePtr curr) {
    if (!last) {
        // skip first frame
        m_logger->info("StereoOdometry monocular get FIRST frame!");

        ImageDataLabelAdaptor(*curr).set(ImageDataLabel::Keyframe);

        return {};
    }

    // track last and curr ImageData
    auto monocular_track = m_tracker->process(last, curr);
    m_logger->info("StereoOdometry monocular track {} matches.",
                   monocular_track->matches.size());

    // check LowParallax
    markLowParallax(*monocular_track, 10, m_logger);

    // check match label
    int landmark_match_count = 0;
    for (const auto& m : monocular_track->matches) {
        auto& pt0 = last->feature->points[m.idx0];
        auto& pt1 = curr->feature->points[m.idx1];

        // new feature point found a match with point of last frame
        FeatureLabelAdaptor(pt1).set(FeatureLabel::MonocularMatched);

        // mark LandmarkMatched
        if (auto lm0 = pt0.landmark.lock()) {
            // point of last frame relates to a existed landmark
            // so do new feature point
            // but no update on landmark here
            FeatureLabelAdaptor(pt1).set(FeatureLabel::LandmarkMatched);
            pt1.landmark = lm0;

            ++landmark_match_count;
        }
    }
    m_logger->info(
        "StereoOdometry mark {}/{} curr features ({}%) with "
        "LandmarkMatched!",
        landmark_match_count,
        curr->feature->points.size(),
        100.0 * landmark_match_count / curr->feature->points.size());

    return monocular_track;
}

/* ########################################################################## */
VisualFeatureMatchDataPtr StereoOdometry::processStereo(ImagePtr left,
                                                        ImagePtr right) {
    // track curr left and right ImageData
    auto stereo_track = m_stereo_tracker->process(left, right);

    m_logger->info("StereoOdometry stereo track {} matches.",
                   stereo_track->matches.size());

    // check LowParallax
    markLowParallax(*stereo_track, 20, m_logger);

    // check match label
    std::vector<bool> triangulate_mask(stereo_track->matches.size(), true);
    for (std::size_t i = 0; i < stereo_track->matches.size(); ++i) {
        const auto& m = stereo_track->matches[i];

        auto& left_pt = left->feature->points[m.idx0];
        auto& right_pt = right->feature->points[m.idx1];
        if (ConstMatchLabelAdaptor(m).is(MatchLabel::LowParallax)) {
            // add LowParallax Label and build LowParallax mask

            FeatureLabelAdaptor(left_pt).set(FeatureLabel::StereoLowParallax);
            FeatureLabelAdaptor(right_pt).set(FeatureLabel::StereoLowParallax);

            triangulate_mask[i] = false;
        }

        FeatureLabelAdaptor(left_pt).set(FeatureLabel::StereoMatched);
        FeatureLabelAdaptor(right_pt).set(FeatureLabel::StereoMatched);

        if (auto lm = left_pt.landmark.lock()) {
            // if left feature relates to a existed landmark
            // so do the right feature
            FeatureLabelAdaptor(right_pt).set(FeatureLabel::LandmarkMatched);
            right_pt.landmark = lm;

            // we dont triangulate the feature with existed landmark
            triangulate_mask[i] = false;
        }
    }

    if (!ImageDataLabelAdaptor(*left).is(ImageDataLabel::Keyframe)) {
        m_logger->info(
            "StereoOdometry stereo track current frame is not Keyframe! "
            "Skip Triangulation and Landmark building!");
        return stereo_track;
    }

    // Triangulate stereo match to base coordinate
    // using features without LowParallax
    auto tri_feature =
        VisualFeatureMatchData::refineDataByMask(*stereo_track,
                                                 triangulate_mask.begin(),
                                                 triangulate_mask.end());
    auto triangulate_pt = m_triangulate.triangulate(tri_feature);
    m_logger->info("StereoOdometry stereo triangulate {} features",
                   triangulate_pt.size());

    constexpr int triangulate_far_threshold = 32;
    refineTriangulationMatch(tri_feature,
                             triangulate_pt,
                             triangulate_far_threshold,
                             m_logger);

    // build landmark from triangulation result
    for (std::size_t i = 0; i < tri_feature.matches.size(); ++i) {
        const auto& pt = triangulate_pt[i];

        const auto left_idx = tri_feature.matches[i].idx0;
        const auto right_idx = tri_feature.matches[i].idx1;

        auto lm = Landmark::create(pt, m_T_wb, left->camera->getTransformBC());
        lm->setRefFrame(left, left_idx);
        m_feature_manager->addLandmark(lm);

        left->feature->points[left_idx].landmark = lm;
        right->feature->points[right_idx].landmark = lm;
    }

    m_feature_manager->addFrame(left);
    m_feature_manager->addFrame(right);
    m_feature_manager->linkLandmarkToFrame(left);
    m_feature_manager->linkLandmarkToFrame(right);

    return stereo_track;
}

/* ########################################################################## */
bool StereoOdometry::init() {
    m_logger->info("StereoOdometry Initializing!");

    bool res = true;

    // check the feature extractor
    if (!m_extractor) {
        m_logger->warn("StereoOdometry require a Feature Extractor!");

        res &= false;
    }

    // check the feature tracker
    if (!m_tracker) {
        m_logger->warn("StereoOdometry require a Feature Tracker");

        res &= false;
    }

    if (!m_stereo_tracker) {
        m_logger->warn("StereoOdometry require a Stereo Tracker");

        res &= false;
    }

    if (!m_feature_manager) {
        m_logger->warn("StereoOdometry require a Feature Manager");

        res &= false;
    }

    // init done
    if (res == true) {
        m_label.set(StereoLabel::Valid);
        m_logger->info("StereoOdometry Initialize Pass!");

    } else {
        m_label.reset();
        m_logger->warn("StereoOdometry Initialize Failed!");
    }

    return res;
}

/* ########################################################################## */
void StereoOdometry::processImage(ImagePtr left_image_data,
                                  ImagePtr right_image_data) {
    if (!m_label.is(StereoLabel::Valid)) {
        throw std::runtime_error(
            "StereoOdometry have not been initialized! Call init() first!");
    }

    processImageImpl(left_image_data, right_image_data);
}

/* ########################################################################## */
void StereoOdometry::loadParameter(const ParameterHandler& param) {
    m_logger->info("StereoOdometry Loading Parameters!");

    m_extractor = param.getVisualFeature();
    m_tracker = param.getVisualTracker();

    m_stereo_tracker = param.getVisualTracker();

    // m_stereo_tracker =
    //     VisualTracker::create(LucasKanadeTracker::register_idx,
    //                           *param.getLucasKanadeTrackerParameter());
}

/* ########################################################################## */
StereoOdometry::Ptr StereoOdometry::create() {
    return std::make_unique<StereoOdometry>();
}

}  // namespace beacon
