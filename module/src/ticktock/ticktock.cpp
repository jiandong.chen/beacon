#include "../../include/beacon_module/ticktock/ticktock.h"

namespace beacon {

/* ########################### TickTock ##################################### */
/* ########################################################################## */
TickTock::TickTock() : m_logger{BeaconLogger::get()} {
}

/* ########################################################################## */
void TickTock::registerTimer(const std::string& name) {
    m_begin_stamp.emplace(name, std::chrono::steady_clock::time_point());
    m_data.emplace(name, std::vector<long long>());
}

/* ########################################################################## */
void TickTock::tick(const std::string& name) {
    auto t = std::chrono::steady_clock::now();

    std::unique_lock lock(m_mtx);

    auto it = m_begin_stamp.find(name);

    if (it == m_begin_stamp.end()) {
        m_logger->info("Register and Tick new Timer {}", name);
        registerTimer(name);
        it = m_begin_stamp.find(name);
    }

    it->second = t;
}

/* ########################################################################## */
long long TickTock::tock(const std::string& name) {
    auto t = std::chrono::steady_clock::now();

    std::unique_lock lock(m_mtx);

    if (auto beg_it = m_begin_stamp.find(name); beg_it != m_begin_stamp.end()) {
        auto dt = std::chrono::duration_cast<std::chrono::microseconds>(
                      t - beg_it->second)
                      .count();

        if (enable_record) {
            m_data[name].push_back(dt);
        }
        beg_it->second = {};

        m_logger->info("Tock {} duration {} microseconds ({} milliseconds)",
                       name,
                       dt,
                       dt / 1000.0);

        return dt;
    } else {
        m_logger->warn("Tock for invalid name {} !!", name);
        return 0.0;
    }
}

/* ########################################################################## */
const std::vector<long long>& TickTock::getData(const std::string& name) {
    if (auto it = m_data.find(name); it != m_data.end()) {
        return it->second;

    } else {
        m_logger->warn(
            "TickTock getData for invalid name {} !! New name resigering!",
            name);
        registerTimer(name);
        return m_data[name];
    }
}

}  // namespace beacon
