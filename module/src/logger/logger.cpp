#include "../../include/beacon_module/logger/logger.h"

#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace beacon {

/* ##################### BeaconLogger ####################################### */
/* ########################################################################## */
BeaconLogger::Ptr BeaconLogger::get() {
    if (instance == nullptr) {
        instance = initLogger();
    }

    return instance;
}

/* ########################################################################## */
BeaconLogger::Ptr BeaconLogger::enableLogFile(const std::string& path,
                                              const bool append) {
    auto logger = get();

    static bool logfile_enable = false;
    if (logfile_enable) {
        // current we only write to one file
        logger->warn("LogFile is already enabled! SKIP!");
        return logger;
    }

    logger->sinks().push_back(
        std::make_shared<spdlog::sinks::basic_file_sink_mt>(path, !append));
    logfile_enable = true;

    logger->info("LogFile is saved to: {}", path);

    return logger;
}

/* ########################################################################## */
BeaconLogger::Ptr BeaconLogger::initLogger() {
    std::vector<spdlog::sink_ptr> vsink;
    vsink.push_back(std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
    auto logger =
        std::make_shared<spdlog::logger>(BeaconLogger::default_logger_name,
                                         vsink.begin(),
                                         vsink.end());

    spdlog::register_logger(logger);

    spdlog::set_level(default_logger_level);

    return logger;
}

/* ########################################################################## */
BeaconLogger::Ptr BeaconLogger::enableInfo() {
    get()->set_level(spdlog::level::info);

    return get();
}

/* ########################################################################## */
BeaconLogger::Ptr BeaconLogger::enableDebug() {
    get()->set_level(spdlog::level::debug);

    return get();
}

/* ########################################################################## */
void BeaconLogger::disableLogger() {
    get()->set_level(spdlog::level::off);
}

}  // namespace beacon
