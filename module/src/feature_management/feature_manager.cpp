#include "../../include/beacon_module/feature_management/feature_manager.h"

namespace beacon {

/* ##################### FeatureManager ##################################### */
/* ########################################################################## */
FeatureManager::FrameSet::iterator FeatureManager::addFrame(ImagePtr pf) {
    const auto [frame_it, success] = m_frames.insert(pf);

    m_logger->info("{} add a frame with transform: {}", m_name, pf->T_wc);

    return frame_it;
}

/* ########################################################################## */
FeatureManager::LandmarkMap::iterator FeatureManager::addLandmark(
    LandmarkPtr pl) {
    const auto [landmark_it, success] = m_landmarks.try_emplace(pl);
    if (!success) {
        throw std::runtime_error(
            "Try to add a existed landmark into database: " + m_name);
    }
    return landmark_it;
}

/* ########################################################################## */
void FeatureManager::linkLandmarkToFrame(ImagePtr pf) {
    if (m_frames.count(pf) == 0) {
        m_logger->error(
            "{} linkLandmarkToFrame cannot find Keyframe in the database. "
            "SKIP!",
            m_name);
        return;
    }

    auto frame_feature = pf->feature;
    if (!frame_feature) {
        return;
    }

    // iter over all feature points
    // check if there are landmarks need to be connected
    // update all related landmarks pointing to added frame
    int total_landmark_count = 0;
    int new_landmark_count = 0;
    for (std::size_t idx = 0; idx < frame_feature->points.size(); ++idx) {
        const auto& pt = frame_feature->points[idx];

        if (auto p = pt.landmark.lock()) {
            // the point has related landmark
            total_landmark_count++;

            auto landmark_it = m_landmarks.find(p);
            if (landmark_it == m_landmarks.end()) {
                // not found landmark in the database
                // need to add new landmark
                landmark_it = m_landmarks.try_emplace(p).first;
                new_landmark_count++;
            }

            // connect the landmark
            landmark_it->second.push_back({pf, idx});
        }
    }

    m_logger->debug(
        "{} process total {} landmarks, add {} new landmarks, connect {} "
        "existed landmarks",
        m_name,
        total_landmark_count,
        new_landmark_count,
        total_landmark_count - new_landmark_count);
    m_logger->debug("{} currently has {} landmarks and {} frames",
                    m_name,
                    m_landmarks.size(),
                    m_frames.size());
}

/* ########################################################################## */
void FeatureManager::removeFrame(ImagePtr pf) {
    if (auto it = m_frames.find(pf); it != m_frames.end()) {
        m_frames.erase(pf);

        m_logger->debug("{} remove a keyframe", m_name);
    }
}

/* ########################################################################## */
void FeatureManager::removeLandmark(LandmarkPtr pl) {
    if (auto it = m_landmarks.find(pl); it != m_landmarks.end()) {
        auto link_count = it->second.size();

        m_landmarks.erase(pl);

        m_logger->debug("{} remove a landmark with {} links",
                        m_name,
                        link_count);
    }
}

};  // namespace beacon
