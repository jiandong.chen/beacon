#include "../../include/beacon_module/motion_estimation/pose_from_pnp.h"

#include "../../include/beacon_module/labeler/match_label.h"

namespace beacon {
/* ############################ PoseFromMatch ############################### */
/* ########################################################################## */
PoseFromPnP::ResultType PoseFromPnP::process(
    const VisualFeatureMatchDataPtr& match) {
    if (!match) {
        return std::nullopt;
    }

    m_status_idx.clear();

    m_logger->info("PoseFromPnP Begin!");

    // extract all pair with obj and ima points if not exist yet
    if (auto p0 = match->pdata0.lock(); p0 && !match->point1.empty()) {
        for (std::size_t i = 0; i < match->matches.size(); ++i) {
            const auto& m = match->matches[i];
            const auto& pt0 = p0->feature->points[m.idx0];
            if (auto objpt0 = pt0.landmark.lock()) {
                m_status_idx.push_back(i);
            }
        }
        m_logger->info("PoseFromPnP get valid {} pairs!", m_status_idx.size());

    } else {
        m_logger->warn("PoseFromPnP dont have data! Get p0: {} Get Point1: {}",
                       p0 != nullptr,
                       !match->point1.empty());
    }

    auto res = PoseFromMatchPnPRansacCV::process(match);

    const auto count = m_status.size();
    for (const auto& s : m_status) {
        const auto& ori_idx = m_status_idx[s];
        MatchLabelAdaptor(match->matches[ori_idx])
            .set(MatchLabel::InlierRecoverPose);
    }

    m_logger->info("PoseFromPnP Get {}/{} inliers ({}%)!",
                   count,
                   m_objpt.size(),
                   100.0 * count / m_objpt.size());

    return res;
}

}  // namespace beacon
