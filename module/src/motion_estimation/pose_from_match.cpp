#include "../../include/beacon_module/motion_estimation/pose_from_match.h"

#include "../../include/beacon_module/labeler/match_label.h"

namespace beacon {

/* ############################ PoseFromMatch ############################### */
/* ########################################################################## */
void PoseFromMatch::preprocess() {
    m_logger->info("PoseFromMatch Estimate Pose From Match Begin!");

    if (!m_input_data) {
        m_logger->warn("PoseFromMatch Get NULL input data! ABORT!");
        return;
    }

    m_logger->info("PoseFromMatch Get {} of keypoints0 and {} of keypoints1",
                   m_input_data->point0.size(),
                   m_input_data->point1.size());
}

/* ########################################################################## */
void PoseFromMatch::postprocessFindEssentialMat() {
    if (m_essential_mat.empty() || m_status.empty()) {
        m_logger->warn("PoseFromMatch failed to find Essential Mat!");
        return;
    }
    m_logger->info("PoseFromMatch Found Essential Mat!");

    // mark inliers of stage findEssentialMat
    std::size_t count = 0;
    for (std::size_t idx = 0; idx < m_status.size(); ++idx) {
        if (m_status[idx]) {
            MatchLabelAdaptor(m_input_data->matches[idx])
                .set(MatchLabel::InlierEssentialMat);
            ++count;
        }
    }
    m_logger->info("PoseFromMatch Found Essential Mat Get {}/{} inliers ({}%)!",
                   count,
                   m_input_data->matches.size(),
                   100.0 * count / m_input_data->matches.size());
}

/* ########################################################################## */
void PoseFromMatch::postprocessRecoverPose() {
    // mark inliers of stage recoverPose
    if (!m_res.has_value()) {
        m_logger->warn("PoseFromMatch cannot calculate the pose!");
    }

    std::size_t count = 0;
    for (std::size_t idx = 0; idx < m_status.size(); ++idx) {
        if (m_status[idx]) {
            MatchLabelAdaptor(m_input_data->matches[idx])
                .set(MatchLabel::InlierRecoverPose);
            ++count;
        }
    }
    if (count != 0) {
        m_logger->info("PoseFromMatch RecoverPose Get {}/{} inliers ({}%)!",
                       count,
                       m_input_data->matches.size(),
                       100.0 * count / m_input_data->matches.size());
    }
}

}  // namespace beacon
