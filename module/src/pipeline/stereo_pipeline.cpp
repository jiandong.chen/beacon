#include "../../include/beacon_module/pipeline/stereo_pipeline.h"

#include "../../include/beacon_module/labeler/feature_label.h"

namespace beacon {

/* ##################### StereoPipeline ##################################### */
/* ########################################################################## */
StereoPipeline::StereoPipeline()
    : m_odom{StereoOdometry::create()}, m_logger{BeaconLogger::get()} {
    // draw blue on right image if stereo match is low parallax
    m_right_drawer.registerColorWithFeaturePred(
        ImageDrawer::blue,
        [](const VisualFeaturePoint& pt) -> bool {
            return ConstFeatureLabelAdaptor(pt).is(
                FeatureLabelEnum::StereoLowParallax);
        });
    m_logger->info("StereoPipeline Created!");
}

/* ########################################################################## */
void StereoPipeline::process(cv::Mat left_img,
                             const Timestamp& left_timestamp,
                             cv::Mat right_img,
                             const Timestamp& right_timestamp) {
    m_logger->info("----------------------- New Frame -----------------------");

    if (!m_label.is(PipelineLabel::Valid)) {
        throw std::runtime_error(
            "StereoPipeline have not been initialized! Call init() first!");
    }

    auto preprocessImage = [this](cv::Mat img,
                                  const Timestamp& time,
                                  const CameraPtr& cam) {
        m_logger->info("StereoPipeline get odometry base transform T_wb: {}",
                       m_odom->getTransformWB());

        // get T_wc
        const auto cam_pose = m_odom->getTransformWB() * cam->getTransformBC();
        auto data = ImageData::create(img, time, cam, cam_pose);

        cam->undistortImage(*data);

        return data;
    };

    // feed images to odom
    m_odom->processImage(
        preprocessImage(left_img, left_timestamp, m_left_cam),
        preprocessImage(right_img, right_timestamp, m_right_cam));

    // visualization
    if (m_odom->getLastLeftImage()) {
        m_display_image.left_image =
            m_left_drawer.drawKeypoint(*m_odom->getLastLeftImage());
    }
    if (m_odom->getLastRightImage()) {
        m_display_image.right_image =
            m_right_drawer.drawKeypoint(*m_odom->getLastRightImage());
    }
    if (m_odom->getLastTrack()) {
        m_display_image.monocular_match =
            m_left_drawer.drawMatchSingle(*m_odom->getLastTrack());
    }
    if (m_odom->getLastStereoTrack()) {
        m_display_image.stereo_match =
            m_left_drawer.drawMatchSingle(*m_odom->getLastStereoTrack());
    }

#if 0
    // TOOD replace by feature display + classify
    display_imgs.monocular_match = m_drawer.drawMatch(*m_odom->getLastTrack());
    display_imgs.stereo_match =m_drawer.drawMatch(*m_odom->getLastStereoTrack());
#endif
}

/* ########################################################################## */
bool StereoPipeline::init() {
    m_logger->info("StereoPipeline Initializing!");

    bool res = true;

    // camera init
    if (!m_left_cam) {
        m_logger->warn("StereoPipeline require a left camera!");

        res &= false;
    }
    if (!m_right_cam) {
        m_logger->warn("StereoPipeline require a right camera!");

        res &= false;
    }

    // odometry init
    if (!m_odom) {
        m_logger->warn("StereoPipeline require a StereoOdometry!");

        res &= false;
    }
    if (!m_odom->init()) {
        m_logger->warn("StereoPipeline cannot init the StereoOdometry!");

        res &= false;
    }

    if (res) {
        m_label.set(PipelineLabel::Valid);
        m_logger->info("StereoPipeline Initialize Pass!");
    } else {
        m_label.reset();
        m_logger->info("StereoPipeline Initialize Failed!");
    }

    return res;
}

/* ########################################################################## */
void StereoPipeline::loadParameter(const ParameterHandler& param) {
    m_logger->info("StereoPipeline loading parameters!");

    m_odom->loadParameter(param);

    m_left_cam = param.getCameras().at(0);
    {
        std::stringstream ss;
        m_left_cam->print(ss);
        m_logger->info("StereoPipeline load Left Cam:");
        m_logger->info("{}", ss.str());
    }

    m_right_cam = param.getCameras().at(1);
    {
        std::stringstream ss;
        m_right_cam->print(ss);
        m_logger->info("StereoPipeline load Right Cam:");
        m_logger->info("{}", ss.str());
    }
}

}  // namespace beacon
