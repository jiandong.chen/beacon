#include "../test/util/load_image.h"
#include "beacon_module/pipeline/stereo_pipeline.h"

using namespace beacon;
using namespace beacon::test;

int main() {
    ParameterHandler ph{"/workspace/beacon/config/base.yaml"};

    StereoPipeline p;
    p.loadParameter(ph);
    p.init();

    test::CanalTestImagePair data;
    p.process(data.m_img_front->image, {}, data.m_img_back->image, {});

    return 0;
}
