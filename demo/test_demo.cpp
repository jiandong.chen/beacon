#include "../test/util/load_image.h"
#include "beacon/sensor/stereo_pinhole.h"
#include "beacon_module/parameter_handler/parameter_handler.h"

using namespace beacon;
using namespace beacon::test;

int main() {
    auto logger = BeaconLogger::get();
    BeaconLogger::enableLogFile();
    ParameterHandler ph{"/workspace/beacon/config/base.yaml"};

    // auto sp = VisualFeature::create(
    //     SuperPointTRT::register_idx,
    //     ph.getSuperPointTRTParameter()->setMaxKeypoint(100));
    // auto lg = VisualTracker::create(LightGlueTRT::register_idx,
    //                                 *ph.getLightGlueTRTParameter());

    test::CanalTestImagePair data;
    auto left_img = data.m_img_left->image.clone();
    auto right_img = data.m_img_right->image.clone();
    data.m_img_left->camera->undistortImage(*data.m_img_left);
    data.m_img_right->camera->undistortImage(*data.m_img_right);

    cv::Mat resize_res;
    cv::hconcat(data.m_img_left->image, data.m_img_right->image, resize_res);
    std::cout << resize_res.size << std::endl;

    // data.m_img_left->image = left_img;
    // data.m_img_right->image = right_img;
    auto stereo = StereoPinhole(data.m_img_left->camera,
                                data.m_img_right->camera,
                                640,
                                360);
    stereo.undistortRectifyImagePair(*data.m_img_left, *data.m_img_right);


    cv::Mat rectify_res;
    cv::hconcat(data.m_img_left->image, data.m_img_right->image, rectify_res);

    cv::Mat res;
    cv::vconcat(resize_res, rectify_res, res);

    cv::imshow("test", res);
    cv::waitKey();

    // data.m_img_front->camera->undistortImage(*data.m_img_front);
    // data.m_img_back->camera->undistortImage(*data.m_img_back);

    return 0;
}
