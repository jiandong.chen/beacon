#include "../test/util/load_image.h"
#include "beacon/visual_feature/gftt.h"
#include "beacon/visual_tracker/lucas_kanade.h"
#include "beacon_module/image_drawer/image_drawer.h"
#include "beacon_module/parameter_handler/parameter_handler.h"

using namespace beacon;
using namespace beacon::test;

int main() {
    auto logger = BeaconLogger::get();
    spdlog::set_level(spdlog::level::debug);

    // test param reader
    ParameterHandler ph{"/workspace/beacon/config/base.yaml"};

    // get feature extractor
    auto gftt_param = ph.getGFTTParameter();
    gftt_param->setMaxKeypoint(200);
    auto gftt = VisualFeature::create(GFTT::register_idx, *gftt_param);

    // get feature tracker
    auto lk_param = std::make_unique<LucasKanadeTrackerParameter>();
    auto lk =
        VisualTracker::create(LucasKanadeTracker::register_idx, *lk_param);

    // get and resize the test image
    test::CanalTestImagePair data;
    // data.m_img_front->camera =
    //     std::make_shared<PinholeResize>(*data.m_img_front->camera, 640, 480);
    // data.m_img_back->camera =
    //     std::make_shared<PinholeResize>(*data.m_img_back->camera, 640, 480);

    data.m_img_front->camera->undistortImage(*data.m_img_front);
    data.m_img_back->camera->undistortImage(*data.m_img_back);

    // calc the feature and match
    gftt->process(*data.m_img_front);
    auto track_res = lk->process(data.m_img_front, data.m_img_back);

    // add pred for feature display
    ImageDrawer drawer{};
    drawer.registerColorWithFeaturePred(
        ImageDrawer::white,
        [](const VisualFeaturePoint& pt) -> bool {
            return pt.uv.x() > 200;
        });
    auto test_img = drawer.drawMatch(*track_res);

    cv::imshow("test", test_img);
    cv::waitKey();

    return 0;
}
