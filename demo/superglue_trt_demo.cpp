#include "../test/util/load_image.h"
#include "beacon/motion/pose_pnp_ransac_cv.h"
#include "beacon/visual_feature/superpoint_trt.h"
#include "beacon/visual_tracker/superglue_trt.h"
#include "beacon_module/image_drawer/image_drawer.h"
#include "beacon_module/logger/logger.h"
#include "beacon_module/parameter_handler/parameter_handler.h"
#include "beacon_module/ticktock/ticktock.h"

using namespace beacon;
using namespace beacon::test;

int main() {
    auto logger = BeaconLogger::get();
    BeaconLogger::enableLogFile();

    // test param reader
    ParameterHandler ph{"/workspace/beacon/config/base.yaml"};

    logger->info("SuperGlue Engine Path: {}",
                 ph.getSuperGlueTRTParameter()->m_engine_path);

    auto sp = VisualFeature::create(
        SuperPointTRT::register_idx,
        ph.getSuperPointTRTParameter()->setMaxKeypoint(500));
    auto sg = VisualTracker::create(SuperGlueTRT::register_idx,
                                    *ph.getSuperGlueTRTParameter());

    TickTock timer;
    test::CanalTestImagePair data;
    data.m_img_front->camera =
        std::make_shared<PinholeResize>(*data.m_img_front->camera, 640, 480);
    data.m_img_back->camera =
        std::make_shared<PinholeResize>(*data.m_img_back->camera, 640, 480);

    data.m_img_front->camera->undistortImage(*data.m_img_front);
    data.m_img_back->camera->undistortImage(*data.m_img_back);

    timer.tick("SuperPoint Process");
    sp->process(*data.m_img_front);
    timer.tock("SuperPoint Process");

    timer.tick("SuperPoint Process");
    sp->process(*data.m_img_back);
    timer.tock("SuperPoint Process");

    timer.tick("SuperGLue Process");
    auto res = sg->process(data.m_img_front, data.m_img_back);
    timer.tock("SuperGLue Process");

    timer.tick("SuperGLue Process");
    res = sg->process(data.m_img_front, data.m_img_back);
    timer.tock("SuperGLue Process");

    logger->info("Get Total Match: {}", res->matches.size());

    ImageDrawer drawer{};
    // auto test_img = drawer.drawMatch(*data.m_img_front, *data.m_img_back,
    // *res);
    auto test_img = drawer.drawMatch(*res);
    cv::imshow("test", test_img);
    cv::waitKey();

    return 0;
}
