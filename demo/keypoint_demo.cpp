#include <beacon/cuda/cuda_buffer.h>

#include "../corelib/src/visual_feature/superpoint_trt/superpoint_trt_util.h"

int main() {
    constexpr int h = 1080;
    constexpr int w = 2048;
    // constexpr int h = 32;
    // constexpr int w = 32;
    constexpr int image_sz = h * w;
    constexpr double threshold = 0.5;
    constexpr int win_sz = 5;
    constexpr int max_kp = 5000;
    constexpr int remove_border = 5;

    beacon::cuda::NormalBuffer<float> image;
    image.resize(image_sz);

    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < w; ++j) {
            image.getHostData()[i * w + j] =
                static_cast<float>(rand()) / RAND_MAX;
            // std::cout << buf.getHostData()[i * w + j] << " ";
        }
        // std::cout << "\n";
    }
    image.moveToDevice();

    beacon::cuda::DeviceBuffer<float3> res_buf_1;
    beacon::sp_trt_util::getKeypointFromScore(image.getDeviceData(),
                                              h,
                                              w,
                                              threshold,
                                              max_kp,
                                              win_sz,
                                              remove_border,
                                              res_buf_1);

    auto data = std::make_unique<float[]>(res_buf_1.getSize() * 3);
    BEACON_CUDA_CHECK(cudaMemcpy(data.get(),
                                 res_buf_1.getData(),
                                 res_buf_1.getSize() * sizeof(float3),
                                 cudaMemcpyDeviceToHost));

    constexpr int h8 = h / 8;
    constexpr int w8 = w / 8;
    constexpr int ch = 3;
    constexpr int desc_sz = h8 * w8 * ch;

    beacon::cuda::NormalBuffer<float> desc;
    desc.resize(desc_sz);

    for (int i = 0; i < h8; ++i) {
        for (int j = 0; j < w8; ++j) {
            // std::cout << "desc: " << i << " " << j << "\n";
            for (int c = 0; c < ch; ++c) {
                desc.getHostData()[i * w8 * c + j * c + c] =
                    static_cast<float>(rand()) / RAND_MAX + 1;
                // std::cout << desc.getHostData()[i * w8 * c + j * c + c] << "
                // ";
            }
            // std::cout << "\n";
        }
    }
    desc.moveToDevice();

    beacon::cuda::DeviceBuffer<float> desc_out;
    beacon::sp_trt_util::getDescriptorFromKeypoint(desc.getDeviceData(),
                                                   h8,
                                                   w8,
                                                   ch,
                                                   res_buf_1,
                                                   desc_out);

    auto desc_data = std::make_unique<float[]>(desc_out.getSize());
    BEACON_CUDA_CHECK(cudaMemcpy(desc_data.get(),
                                 desc_out.getData(),
                                 desc_out.getSize() * sizeof(float),
                                 cudaMemcpyDeviceToHost));

    return 0;

    for (std::size_t i = 0; i < res_buf_1.getSize(); ++i) {
        std::cout << "pt " << i << ": ";
        std::cout << data[3 * i] << " ";
        std::cout << data[3 * i + 1] << " ";
        std::cout << data[3 * i + 2] << " ";
        std::cout << "\n";
        for (int j = 0; j < ch; ++j) {
            std::cout << desc_data[i * ch + j] << " ";
        }
        std::cout << "\n";
    }

    for (std::size_t i = 0; i < res_buf_1.getSize(); ++i) {
        std::cout << data[3 * i] << " ";
        std::cout << data[3 * i + 1] << " ";
        std::cout << data[3 * i + 2] << " ";
        std::cout << "\n";
    }
    std::cout << res_buf_1.getSize() << std::endl;

    int count = 0;
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < w; ++j) {
            bool valid = true;

            for (int ii = i - win_sz; ii <= i + win_sz; ++ii) {
                for (int jj = j - win_sz; jj <= j + win_sz; ++jj) {
                    if (ii < 0 || jj < 0 || ii >= h || jj >= w) {
                        continue;
                    }
                    if (image.getHostData()[i * w + j] <
                        image.getHostData()[ii * w + jj]) {
                        valid = false;
                    }
                }
            }
            if (valid) {
                // std::cout << i << " " << j << "\n";
                count++;
            }
        }
    }
    std::cout << count << std::endl;
}
