#include "../test/util/load_image.h"
#include "beacon/motion/recover_pose.h"
#include "beacon/visual_feature/superpoint.h"
#include "beacon_module/image_drawer/image_drawer.h"
#include "beacon_module/parameter_handler/parameter_handler.h"

using namespace beacon;
using namespace beacon::test;

int main() {
    // test param reader
    ParameterHandler ph{"/workspace/beacon/config/base.yaml"};

    std::cout << ph.getSuperPointParameter()->m_model_path << std::endl;

    auto sp = VisualFeature::create(SuperPoint::register_idx,
                                    *ph.getSuperPointParameter());

    ImageDrawer drawer{};
    test::CanalTestImagePair data;

    constexpr double resize_factor = 0.4;
    cv::resize(data.m_img_front->image,
               data.m_img_front->image,
               cv::Size(),
               resize_factor,
               resize_factor);

    sp->process(*data.m_img_front);

    auto test_img = drawer.drawKeypoint(*data.m_img_front);

    cv::imshow("test", test_img);
    cv::waitKey();

    return 0;
}
