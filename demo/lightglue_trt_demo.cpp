#include "../test/util/load_image.h"
#include "beacon/visual_feature/superpoint_trt.h"
#include "beacon/visual_tracker/lightglue_trt.h"
#include "beacon_module/image_drawer/image_drawer.h"
#include "beacon_module/logger/logger.h"
#include "beacon_module/parameter_handler/parameter_handler.h"
#include "beacon_module/ticktock/ticktock.h"

using namespace beacon;
using namespace beacon::test;

int main() {
    auto logger = BeaconLogger::get();
    BeaconLogger::enableLogFile();

    // test param reader
    ParameterHandler ph{"/workspace/beacon/config/base.yaml"};

    logger->info("LightGlue Engine Path: {}",
                 ph.getLightGlueTRTParameter()->m_engine_path);

    auto lg = ph.getVisualTracker();
    auto sp = ph.getVisualFeature();

    test::CanalTestImagePair data;
    data.m_img_front->image = cv::imread("/workspace/tmpimg/match00.png");
    data.m_img_back->image = cv::imread("/workspace/tmpimg/match10.png");

    // data.m_img_front->camera->undistortImage(*data.m_img_front);
    // data.m_img_back->camera->undistortImage(*data.m_img_back);

    sp->process(*data.m_img_front);
    sp->process(*data.m_img_back);

    TickTock timer;
    timer.tick("LightGLue Process");
    auto res = lg->process(data.m_img_front, data.m_img_back);
    timer.tock("LightGLue Process");

    logger->info("Get Total Match: {}", res->matches.size());

    ImageDrawer drawer{};
    // auto test_img = drawer.drawMatch(*data.m_img_front, *data.m_img_back,
    // *res);
    auto test_img = drawer.drawMatch(*res);
    cv::imshow("test", test_img);
    cv::waitKey();

    return 0;
}
