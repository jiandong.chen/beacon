#include "../test/util/load_image.h"
#include "beacon/visual_feature/superpoint_trt.h"
#include "beacon_module/image_drawer/image_drawer.h"
#include "beacon_module/parameter_handler/parameter_handler.h"

using namespace beacon;
using namespace beacon::test;

int main() {
    // test param reader
    ParameterHandler ph{"/workspace/beacon/config/base.yaml"};

    std::cout << ph.getSuperPointTRTParameter()->m_engine_path << std::endl;

    auto sp = VisualFeature::create(SuperPointTRT::register_idx,
                                    *ph.getSuperPointTRTParameter());

    // ori img size: 1080 2048
    // resize to 480 640
    test::CanalTestImagePair data;
    data.m_img_front->camera =
        std::make_shared<PinholeResize>(*data.m_img_front->camera, 640, 480);
    data.m_img_front->camera->undistortImage(*data.m_img_front);

    sp->process(*data.m_img_front);

    // for (int i = 0; i < 10; ++i) {
    //     std::cout << data.m_img_front->feature->points.at(i).uv << "\n";
    //     std::cout << data.m_img_front->feature->points.at(i).xy << "\n";
    //     std::cout << "\n";
    // }

    ImageDrawer drawer{};
    auto test_img = drawer.drawKeypoint(*data.m_img_front);
    cv::imshow("test", test_img);
    cv::waitKey();

    return 0;
}
