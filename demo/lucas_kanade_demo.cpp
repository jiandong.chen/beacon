#include "../test/util/load_image.h"
#include "beacon/motion/motion.h"
#include "beacon/visual_feature/gftt.h"
#include "beacon/visual_tracker/lucas_kanade.h"
#include "beacon_module/image_drawer/image_drawer.h"
#include "beacon_module/parameter_handler/parameter_handler.h"

using namespace beacon;
using namespace beacon::test;

int main() {
    // test param reader
    ParameterHandler ph{"/workspace/beacon/config/base.yaml"};

    auto gftt_param = ph.getGFTTParameter();
    std::cout << " Read from config Max Kp Num: " << gftt_param->m_max_keypoint
              << std::endl;
    // test virtual clone
    gftt_param->setMaxKeypoint(100);
    std::unique_ptr<VisualFeatureParameter> gftt_param_base =
        std::move(gftt_param);
    auto gftt_copy = gftt_param_base->clone();
    auto gftt = VisualFeature::create(GFTT::register_idx, *gftt_copy);

    // test virtual clone
    auto lk_param = std::make_unique<LucasKanadeTrackerParameter>();
    std::unique_ptr<VisualTrackerParameter> lk_param_base = std::move(lk_param);
    auto lk_copy = lk_param_base->clone();
    auto lk = VisualTracker::create(LucasKanadeTracker::register_idx, *lk_copy);

    ImageDrawer drawer{};
    test::CanalTestImagePair data;

    const double resize_factor = 0.4;
    cv::resize(data.m_img_front->image,
               data.m_img_front->image,
               cv::Size(),
               resize_factor,
               resize_factor);
    cv::resize(data.m_img_back->image,
               data.m_img_back->image,
               cv::Size(),
               resize_factor,
               resize_factor);

    gftt->process(*data.m_img_front);
    auto track_res = lk->process(data.m_img_front, data.m_img_back);

    // test recover_pose
    // T_12
    [[maybe_unused]] auto T = PoseFromMatchEssentialMatCV().process(track_res);
    std::cout << T->getRotationMatrix() << std::endl;
    std::cout << T->getTranslation() << std::endl;

    auto test_img =
        drawer.drawMatch(*data.m_img_front, *data.m_img_back, *track_res);

    cv::imshow("test", test_img);
    cv::waitKey();

    return 0;
}
