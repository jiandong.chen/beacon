import torch
import torch.nn as nn
from beacon_script_config import config


class KeypointEncoder(nn.Module):
    def __init__(self):
        super().__init__()
        self.layer1 = nn.Conv1d(3, 32, 1)
        self.bn1 = nn.BatchNorm1d(32)
        self.layer2 = nn.Conv1d(32, 64, 1)
        self.bn2 = nn.BatchNorm1d(64)
        self.layer3 = nn.Conv1d(64, 128, 1)
        self.bn3 = nn.BatchNorm1d(128)
        self.layer4 = nn.Conv1d(128, 256, 1)
        self.bn4 = nn.BatchNorm1d(256)
        self.layer5 = nn.Conv1d(256, 256, 1)

    def forward(self, x):
        x = torch.relu(self.bn1(self.layer1(x)))
        x = torch.relu(self.bn2(self.layer2(x)))
        x = torch.relu(self.bn3(self.layer3(x)))
        x = torch.relu(self.bn4(self.layer4(x)))
        x = self.layer5(x)
        return x


class MultiHeadedAttention(nn.Module):
    def __init__(self):
        super().__init__()

        self.merge = nn.Conv1d(256, 256, 1)

        self.query_proj = nn.Conv1d(256, 256, 1)
        self.key_proj = nn.Conv1d(256, 256, 1)
        self.value_proj = nn.Conv1d(256, 256, 1)

    def forward(self, query, key, value):
        batch_dim = query.size(0)

        query = self.query_proj(query).view(batch_dim, 256 // 4, 4, -1)
        key = self.key_proj(key).view(batch_dim, 256 // 4, 4, -1)
        value = self.value_proj(value).view(batch_dim, 256 // 4, 4, -1)

        # attention
        dim = query.shape[1]
        scores = torch.einsum("bdhn,bdhm->bhnm", query, key) / dim**0.5
        prob = torch.nn.functional.softmax(scores, dim=-1)
        res = torch.einsum("bhnm,bdhm->bdhn", prob, value)

        return self.merge(res.contiguous().view(batch_dim, 256, -1))


class AttentionalPropagation(nn.Module):
    def __init__(self):
        super().__init__()

        self.attn = MultiHeadedAttention()
        self.layer1 = nn.Conv1d(512, 512, 1)
        self.bn1 = nn.BatchNorm1d(512)
        self.layer2 = nn.Conv1d(512, 256, 1)

    def forward(self, x, src):
        msg = self.attn(x, src, src)
        msg = torch.cat([x, msg], dim=1)
        msg = torch.relu(self.bn1(self.layer1(msg)))

        x = self.layer2(msg)
        return x


class AttentionalGNN(nn.Module):
    def __init__(self):
        super().__init__()

        self.self_0 = AttentionalPropagation()
        self.cross_0 = AttentionalPropagation()

        self.self_1 = AttentionalPropagation()
        self.cross_1 = AttentionalPropagation()

        self.self_2 = AttentionalPropagation()
        self.cross_2 = AttentionalPropagation()

        self.self_3 = AttentionalPropagation()
        self.cross_3 = AttentionalPropagation()

        self.self_4 = AttentionalPropagation()
        self.cross_4 = AttentionalPropagation()

        self.self_5 = AttentionalPropagation()
        self.cross_5 = AttentionalPropagation()

        self.self_6 = AttentionalPropagation()
        self.cross_6 = AttentionalPropagation()

        self.self_7 = AttentionalPropagation()
        self.cross_7 = AttentionalPropagation()

        self.self_8 = AttentionalPropagation()
        self.cross_8 = AttentionalPropagation()

    def forward(self, desc0, desc1):

        delta_0 = self.self_0(desc0, desc0)
        delta_1 = self.self_0(desc1, desc1)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1
        delta_0 = self.cross_0(desc0, desc1)
        delta_1 = self.cross_0(desc1, desc0)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1

        delta_0 = self.self_1(desc0, desc0)
        delta_1 = self.self_1(desc1, desc1)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1
        delta_0 = self.cross_1(desc0, desc1)
        delta_1 = self.cross_1(desc1, desc0)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1

        delta_0 = self.self_2(desc0, desc0)
        delta_1 = self.self_2(desc1, desc1)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1
        delta_0 = self.cross_2(desc0, desc1)
        delta_1 = self.cross_2(desc1, desc0)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1

        delta_0 = self.self_3(desc0, desc0)
        delta_1 = self.self_3(desc1, desc1)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1
        delta_0 = self.cross_3(desc0, desc1)
        delta_1 = self.cross_3(desc1, desc0)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1

        delta_0 = self.self_4(desc0, desc0)
        delta_1 = self.self_4(desc1, desc1)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1
        delta_0 = self.cross_4(desc0, desc1)
        delta_1 = self.cross_4(desc1, desc0)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1

        delta_0 = self.self_5(desc0, desc0)
        delta_1 = self.self_5(desc1, desc1)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1
        delta_0 = self.cross_5(desc0, desc1)
        delta_1 = self.cross_5(desc1, desc0)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1

        delta_0 = self.self_6(desc0, desc0)
        delta_1 = self.self_6(desc1, desc1)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1
        delta_0 = self.cross_6(desc0, desc1)
        delta_1 = self.cross_6(desc1, desc0)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1

        delta_0 = self.self_7(desc0, desc0)
        delta_1 = self.self_7(desc1, desc1)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1
        delta_0 = self.cross_7(desc0, desc1)
        delta_1 = self.cross_7(desc1, desc0)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1

        delta_0 = self.self_8(desc0, desc0)
        delta_1 = self.self_8(desc1, desc1)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1
        delta_0 = self.cross_8(desc0, desc1)
        delta_1 = self.cross_8(desc1, desc0)
        desc0 = desc0 + delta_0
        desc1 = desc1 + delta_1

        return desc0, desc1


class SuperGlueNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.keypoint_encoder = KeypointEncoder()
        self.attentional_gnn = AttentionalGNN()
        self.final_proj = nn.Conv1d(256, 256, 1)

        self.bin_score = nn.Parameter(torch.tensor(1.0))
        self.register_parameter("bin_score", self.bin_score)

    def forward(self, kpsc0, kpsc1, desc0, desc1):
        desc0 = desc0 + self.keypoint_encoder(kpsc0)
        desc1 = desc1 + self.keypoint_encoder(kpsc1)

        desc0, desc1 = self.attentional_gnn(desc0, desc1)

        mdesc0 = self.final_proj(desc0)
        mdesc1 = self.final_proj(desc1)
        return mdesc0, mdesc1


def getModel(verbose=False):
    weights = torch.load(config["SuperGlueWeightPath"])
    net = SuperGlueNet().eval()
    net_dict = net.state_dict()

    count = 0
    for key in weights:
        count += 1

    if verbose:
        print(f"Need to move {count} weights")

    count = 0
    # keypoint encoder
    suffix = ["weight", "bias"]
    suffix_bn = ["weight", "bias", "running_mean", "running_var", "num_batches_tracked"]
    for i, j in enumerate(range(0, 10, 3)):
        for suff in suffix:
            # conv
            old_name = f"kenc.encoder.{j}." + suff
            new_name = f"keypoint_encoder.layer{i + 1}." + suff
            net_dict[new_name] = weights[old_name]
            if verbose:
                print(old_name, " --->>> ", new_name)
            count += 1

        # bn
        for suff in suffix_bn:
            old_name = f"kenc.encoder.{j+1}." + suff
            new_name = f"keypoint_encoder.bn{i + 1}." + suff
            net_dict[new_name] = weights[old_name]
            if verbose:
                print(old_name, " --->>> ", new_name)
            count += 1

    for suff in suffix:
        old_name = f"kenc.encoder.{12}." + suff
        new_name = f"keypoint_encoder.layer{5}." + suff
        net_dict[new_name] = weights[old_name]
        if verbose:
            print(old_name, " --->>> ", new_name)
        count += 1

    # gnn
    qkv = ["query_proj", "key_proj", "value_proj"]
    self_cross = ["self", "cross"]
    for i in range(0, 18, 2):
        for layer_offset, sc in enumerate(self_cross):
            for suff in suffix:
                # merge
                old_name = f"gnn.layers.{i+layer_offset}.attn.merge." + suff
                new_name = f"attentional_gnn." + sc + f"_{i//2}.attn.merge." + suff
                net_dict[new_name] = weights[old_name]
                if verbose:
                    print(old_name, " --->>> ", new_name)
                count += 1

                # qkv
                for j, s in enumerate(qkv):
                    old_name = f"gnn.layers.{i+layer_offset}.attn.proj.{j}." + suff
                    new_name = (
                        f"attentional_gnn." + sc + f"_{i//2}.attn." + s + "." + suff
                    )
                    net_dict[new_name] = weights[old_name]
                    if verbose:
                        print(old_name, " --->>> ", new_name)
                    count += 1

                # mlp
                old_name = f"gnn.layers.{i+layer_offset}.mlp.0." + suff
                new_name = f"attentional_gnn." + sc + f"_{i//2}.layer1." + suff
                net_dict[new_name] = weights[old_name]
                if verbose:
                    print(old_name, " --->>> ", new_name)
                count += 1

                old_name = f"gnn.layers.{i+layer_offset}.mlp.3." + suff
                new_name = f"attentional_gnn." + sc + f"_{i//2}.layer2." + suff
                net_dict[new_name] = weights[old_name]
                if verbose:
                    print(old_name, " --->>> ", new_name)
                count += 1

            for suff in suffix_bn:
                old_name = f"gnn.layers.{i+layer_offset}.mlp.1." + suff
                new_name = f"attentional_gnn." + sc + f"_{i//2}.bn1." + suff
                net_dict[new_name] = weights[old_name]
                if verbose:
                    print(old_name, " --->>> ", new_name)
                count += 1

    # final proj
    for suff in suffix:
        name = "final_proj." + suff
        net_dict[name] = weights[name]
        net_dict[name] = weights[name]
        if verbose:
            print(name, " --->>> ", name)
        count += 1

    net_dict["bin_score"] = weights["bin_score"]
    count += 1

    net.load_state_dict(net_dict)

    return net

    kp0 = torch.rand((1, 2, 10))
    kp1 = torch.rand((1, 2, 10))
    sc0 = torch.rand((1, 1, 10))
    sc1 = torch.rand((1, 1, 10))
    desc0 = torch.rand((1, 256, 10))
    desc1 = torch.rand((1, 256, 10))

    kpsc0 = torch.cat((kp0, sc0), dim=1)
    kpsc1 = torch.cat((kp1, sc1), dim=1)

    trace_model = torch.jit.trace(net, (kpsc0, kpsc1, desc0, desc1))
    trace_model.save("superglueNet.pt")

    #  torch.save(net_dict, "superglueNet.pt")
    if verbose:
        print(f"Moved {count} weights")

    return net

