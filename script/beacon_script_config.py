# general
config = {
    "BaseDir": "/workspace/beacon",
}

# LightGlue
config = {
    **config,
    "LightGlueWeightPath": config["BaseDir"]
    + "/thirdparty/LightGluePretrainedNetwork/models/weights/lightglue_outdoor.pth",
    "LightGlueOnnxPath": config["BaseDir"] + "/model/lightglue.onnx",
    "LightGlueOptimizeOnnxPath": config["BaseDir"] + "/model/lightglue_optimize.onnx",
    "LightGlueEnginePath": config["BaseDir"] + "/model/lightglue.engine",
    "LightGlueKeypointScoreName0": "kpts0",
    "LightGlueKeypointScoreName1": "kpts1",
    "LightGlueDescriptorName0": "desc0",
    "LightGlueDescriptorName1": "desc1",
    "LightGlueKeypointNumName0": "num_kp0",
    "LightGlueKeypointNumName1": "num_kp1",
    "LightGlueMinKeypointNum": 10,
    "LightGlueOptKeypointNum": 1000,
    "LightGlueMaxKeypointNum": 2000,
    # "LightGlueOutputName": "Score",
    "LightGlueOutputName0": "Indice0",
    "LightGlueOutputName1": "Score0",
    "LightGlueOutputName2": "Indice1",
    "LightGlueOutputName3": "Score1",
    "LightGlueBatchSizeName": "BatchSize",
    "LightGlueMinBatchSize": 1,
    "LightGlueOptBatchSize": 1,
    "LightGlueMaxBatchSize": 1,
    "LightGlueEnableOptimize": True,
    "LightGlueAttentionHeadNum": 4,
    "LightGlueAttentionHiddenSize": 256,
}
config = {
    **config,
    "LightGlueInputNameOnnx": [
        config["LightGlueKeypointScoreName0"],
        config["LightGlueKeypointScoreName1"],
        config["LightGlueDescriptorName0"],
        config["LightGlueDescriptorName1"],
    ],
    "LightGlueOutputNameOnnx": [
        config["LightGlueOutputName0"],
        config["LightGlueOutputName1"],
        config["LightGlueOutputName2"],
        config["LightGlueOutputName3"],
    ],
    "LightGlueDynamicShapeOnnx": {
        config["LightGlueKeypointScoreName0"]: {
            0: config["LightGlueBatchSizeName"],
            1: config["LightGlueKeypointNumName0"],
        },
        config["LightGlueKeypointScoreName1"]: {
            0: config["LightGlueBatchSizeName"],
            1: config["LightGlueKeypointNumName1"],
        },
        config["LightGlueDescriptorName0"]: {
            0: config["LightGlueBatchSizeName"],
            1: config["LightGlueKeypointNumName0"],
        },
        config["LightGlueDescriptorName1"]: {
            0: config["LightGlueBatchSizeName"],
            1: config["LightGlueKeypointNumName1"],
        },
        config["LightGlueOutputName0"]: {
            0: config["LightGlueBatchSizeName"],
            1: config["LightGlueKeypointNumName0"],
        },
        config["LightGlueOutputName1"]: {
            0: config["LightGlueBatchSizeName"],
            1: config["LightGlueKeypointNumName0"],
        },
        config["LightGlueOutputName2"]: {
            0: config["LightGlueBatchSizeName"],
            1: config["LightGlueKeypointNumName1"],
        },
        config["LightGlueOutputName3"]: {
            0: config["LightGlueBatchSizeName"],
            1: config["LightGlueKeypointNumName1"],
        },
        # config["LightGlueOutputName"]: {
        #     0: config["LightGlueBatchSizeName"],
        #     1: config["LightGlueKeypointNumName0"],
        #     2: config["LightGlueKeypointNumName1"],
        # },
    },
    "LightGlueKeypointScoreMinShape": (
        config["LightGlueMinBatchSize"],
        config["LightGlueMinKeypointNum"],
        2,
    ),
    "LightGlueKeypointScoreOptShape": (
        config["LightGlueOptBatchSize"],
        config["LightGlueOptKeypointNum"],
        2,
    ),
    "LightGlueKeypointScoreMaxShape": (
        config["LightGlueMaxBatchSize"],
        config["LightGlueMaxKeypointNum"],
        2,
    ),
    "LightGlueDescriptorMinShape": (
        config["LightGlueMinBatchSize"],
        config["LightGlueMinKeypointNum"],
        256,
    ),
    "LightGlueDescriptorOptShape": (
        config["LightGlueOptBatchSize"],
        config["LightGlueOptKeypointNum"],
        256,
    ),
    "LightGlueDescriptorMaxShape": (
        config["LightGlueMaxBatchSize"],
        config["LightGlueMaxKeypointNum"],
        256,
    ),
}

# SuperGlue
config = {
    **config,
    "SuperGlueWeightPath": config["BaseDir"]
    + "/thirdparty/SuperGluePretrainedNetwork/models/weights/superglue_outdoor.pth",
    "SuperGlueOnnxPath": config["BaseDir"] + "/model/superglue.onnx",
    "SuperGlueEnginePath": config["BaseDir"] + "/model/superglue.engine",
    "SuperGlueKeypointScoreName0": "kpsc0",
    "SuperGlueKeypointScoreName1": "kpsc1",
    "SuperGlueDescriptorName0": "desc0",
    "SuperGlueDescriptorName1": "desc1",
    "SuperGlueKeypointNumName0": "num_kp0",
    "SuperGlueKeypointNumName1": "num_kp1",
    "SuperGlueMinKeypointNum": 10,
    "SuperGlueOptKeypointNum": 1000,
    "SuperGlueMaxKeypointNum": 2000,
    "SuperGlueOutputName": "Score",
    "SuperGlueBatchSizeName": "BatchSize",
    "SuperGlueMinBatchSize": 1,
    "SuperGlueOptBatchSize": 2,
    "SuperGlueMaxBatchSize": 2,
}
config = {
    **config,
    "SuperGlueInputNameOnnx": [
        config["SuperGlueKeypointScoreName0"],
        config["SuperGlueKeypointScoreName1"],
        config["SuperGlueDescriptorName0"],
        config["SuperGlueDescriptorName1"],
    ],
    "SuperGlueOutputNameOnnx": [
        config["SuperGlueOutputName"],
    ],
    "SuperGlueDynamicShapeOnnx": {
        config["SuperGlueKeypointScoreName0"]: {
            0: config["SuperGlueBatchSizeName"],
            1: config["SuperGlueKeypointNumName0"],
        },
        config["SuperGlueKeypointScoreName1"]: {
            0: config["SuperGlueBatchSizeName"],
            1: config["SuperGlueKeypointNumName1"],
        },
        config["SuperGlueDescriptorName0"]: {
            0: config["SuperGlueBatchSizeName"],
            1: config["SuperGlueKeypointNumName0"],
        },
        config["SuperGlueDescriptorName1"]: {
            0: config["SuperGlueBatchSizeName"],
            1: config["SuperGlueKeypointNumName1"],
        },
        config["SuperGlueOutputName"]: {
            0: config["SuperGlueBatchSizeName"],
            1: config["SuperGlueKeypointNumName0"],
            2: config["SuperGlueKeypointNumName1"],
        },
    },
    "SuperGlueKeypointScoreMinShape": (
        config["SuperGlueMinBatchSize"],
        config["SuperGlueMinKeypointNum"],
        3,
    ),
    "SuperGlueKeypointScoreOptShape": (
        config["SuperGlueOptBatchSize"],
        config["SuperGlueOptKeypointNum"],
        3,
    ),
    "SuperGlueKeypointScoreMaxShape": (
        config["SuperGlueMaxBatchSize"],
        config["SuperGlueMaxKeypointNum"],
        3,
    ),
    "SuperGlueDescriptorMinShape": (
        config["SuperGlueMinBatchSize"],
        config["SuperGlueMinKeypointNum"],
        256,
    ),
    "SuperGlueDescriptorOptShape": (
        config["SuperGlueOptBatchSize"],
        config["SuperGlueOptKeypointNum"],
        256,
    ),
    "SuperGlueDescriptorMaxShape": (
        config["SuperGlueMaxBatchSize"],
        config["SuperGlueMaxKeypointNum"],
        256,
    ),
}

# SuperPoint
config = {
    **config,
    "SuperPointWeightPath": config["BaseDir"]
    + "/thirdparty/SuperPointPretrainedNetwork/superpoint_v1.pth",
    "SuperPointOnnxPath": config["BaseDir"] + "/model/superpoint.onnx",
    "SuperPointEnginePath": config["BaseDir"] + "/model/superpoint.engine",
    "SuperPointInputName": "Image",
    "SuperPointInputHeightName": "Height",
    "SuperPointInputMinHeight": 120,
    "SuperPointInputOptHeight": 360,
    "SuperPointInputMaxHeight": 1080,
    "SuperPointInputWidthName": "Width",
    "SuperPointInputMinWidth": 160,
    "SuperPointInputOptWidth": 640,
    "SuperPointInputMaxWidth": 2048,
    "SuperPointBatchSizeName": "BatchSize",
    "SuperPointMinBatchSize": 1,
    "SuperPointOptBatchSize": 2,
    "SuperPointMaxBatchSize": 2,
    "SuperPointKeypointOutputName": "Score",
    "SuperPointDescriptorOutputName": "Desc",
    "SuperPointDescriptorHeightName": "H8",
    "SuperPointDescriptorWidthName": "W8",
}
config = {
    **config,
    "SuperPointInputNameOnnx": [config["SuperPointInputName"]],
    "SuperPointOutputNameOnnx": [
        config["SuperPointKeypointOutputName"],
        config["SuperPointDescriptorOutputName"],
    ],
    "SuperPointDynamicShapeOnnx": {
        config["SuperPointInputName"]: {
            0: config["SuperPointBatchSizeName"],
            2: config["SuperPointInputHeightName"],
            3: config["SuperPointInputWidthName"],
        },
        config["SuperPointKeypointOutputName"]: {
            0: config["SuperPointBatchSizeName"],
            1: config["SuperPointInputHeightName"],
            2: config["SuperPointInputWidthName"],
        },
        config["SuperPointDescriptorOutputName"]: {
            0: config["SuperPointBatchSizeName"],
            1: config["SuperPointDescriptorHeightName"],
            2: config["SuperPointDescriptorWidthName"],
        },
    },
    "SuperPointInputMinShape": (
        config["SuperPointMinBatchSize"],
        1,
        config["SuperPointInputMinHeight"],
        config["SuperPointInputMinWidth"],
    ),
    "SuperPointInputOptShape": (
        config["SuperPointOptBatchSize"],
        1,
        config["SuperPointInputOptHeight"],
        config["SuperPointInputOptWidth"],
    ),
    "SuperPointInputMaxShape": (
        config["SuperPointMaxBatchSize"],
        1,
        config["SuperPointInputMaxHeight"],
        config["SuperPointInputMaxWidth"],
    ),
}
