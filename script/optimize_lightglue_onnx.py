from typing import Optional

from beacon_script_config import config

from onnx import load_model, save_model, ModelProto
from onnxruntime.transformers.onnx_model_bert import BertOnnxModel
from onnxruntime.transformers.fusion_options import FusionOptions


class LightGlueOnnx(BertOnnxModel):
    def __init__(self, model: ModelProto, num_heads: int = 0, hidden_size: int = 0):
        super().__init__(model, num_heads, hidden_size)

    def optimize(
        self, options: Optional[FusionOptions] = None, add_dynamic_axes: bool = False
    ):
        if (options is not None) and not options.enable_shape_inference:
            self.disable_shape_inference()

        self.utils.remove_identity_nodes()

        self.utils.remove_useless_cast_nodes()

        if (options is None) or options.enable_layer_norm:
            self.fuse_layer_norm()
        if (options is None) or options.enable_gelu:
            self.fuse_gelu()

        self.remove_useless_div()
        self.fuse_reshape()

        
        # TODO: LightGlue attention

        self.fuse_shape()

        # Remove reshape nodes that having same shape of input and output based on symbolic shape inference.
        self.utils.remove_useless_reshape_nodes()

        self.prune_graph()
        self.remove_unused_constant()


    def fuse_lightglue_attention(self, options: Optional[FusionOptions] = None):
        enable_packed_qkv = (options is None) or options.enable_packed_qkv


def optimize_lightglue_onnx():

    model = load_model(config["LightGlueOnnxPath"])
    onnx_model = LightGlueOnnx(
        model,
        config["LightGlueAttentionHeadNum"],
        config["LightGlueAttentionHiddenSize"],
    )

    print(f"Loaded LightGlue Onnx Model: {config['LightGlueOnnxPath']}")


if __name__ == "__main__":

    optimize_lightglue_onnx()
