from types import SimpleNamespace
from typing import Optional, Tuple
from pathlib import Path

import numpy as np

import torch
import torch.nn.functional as F
from torch import nn


def filter_matches(scores: torch.Tensor):

    return scores.max(2), scores.max(1)


class LearnableFourierPositionalEncoding(nn.Module):
    def __init__(self, M: int, head_dim: int, gamma: float = 1.0) -> None:
        super().__init__()

        self.head_dim = head_dim
        self.gamma = gamma

        self.Wr = nn.Linear(M, self.head_dim // 2, bias=False)
        nn.init.normal_(self.Wr.weight.data, mean=0, std=self.gamma**-2)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """encode position vector"""

        b, n, sz = x.shape

        projected = self.Wr(x)
        cosines, sines = torch.cos(projected), torch.sin(projected)
        emb = torch.stack([cosines, sines], 0).unsqueeze(-1)
        # emb.shape == (2, b, n, 32, 1)
        emb = torch.cat((emb, emb), dim=-1)
        # emb.shape == (2, b, n, 32, 2)
        emb = emb.reshape(2, b, 1, -1, self.head_dim)
        # emb.shape == (2, b, 1, n, 64)

        return emb


def sigmoid_log_double_softmax(
    sim: torch.Tensor, z0: torch.Tensor, z1: torch.Tensor
) -> torch.Tensor:
    """create the log assignment matrix from logits and similarity"""

    b, m, n = sim.shape
    certainties = F.logsigmoid(z0) + F.logsigmoid(z1).transpose(1, 2)
    scores0 = F.log_softmax(sim, 2)
    scores1 = F.log_softmax(sim, 1)

    scores = scores0 + scores1 + certainties

    return scores

    scores = sim.new_full((b, m + 1, n + 1), 0)
    scores[:, :m, :n] = scores0 + scores1 + certainties
    scores[:, :-1, -1] = F.logsigmoid(-z0.squeeze(-1))
    scores[:, -1, :-1] = F.logsigmoid(-z1.squeeze(-1))

    return scores


class MatchAssignment(nn.Module):
    def __init__(self, dim: int) -> None:
        super().__init__()
        self.dim = dim
        self.scale = dim**0.25
        self.matchability = nn.Linear(dim, 1, bias=True)
        self.final_proj = nn.Linear(dim, dim, bias=True)

    def forward(self, desc0: torch.Tensor, desc1: torch.Tensor):
        """build assignment matrix from descriptors"""
        mdesc0, mdesc1 = self.final_proj(desc0), self.final_proj(desc1)
        mdesc0, mdesc1 = map(lambda t: t / self.scale, (mdesc0, mdesc1))

        sim = mdesc0 @ mdesc1.transpose(1, 2)
        # sim.shape == [batch, num_kp0, num_kp1]

        z0 = self.matchability(desc0)
        z1 = self.matchability(desc1)
        scores = sigmoid_log_double_softmax(sim, z0, z1)

        return scores
        # return scores, sim

    def get_matchability(self, desc: torch.Tensor):
        return torch.sigmoid(self.matchability(desc)).squeeze(-1)


class TokenConfidence(nn.Module):
    def __init__(self, dim: int) -> None:
        super().__init__()
        self.token = nn.Sequential(nn.Linear(dim, 1), nn.Sigmoid())

    def forward(self, desc0: torch.Tensor, desc1: torch.Tensor):
        """get confidence tokens"""
        return (
            self.token(desc0.detach()).squeeze(-1),
            self.token(desc1.detach()).squeeze(-1),
        )


class Attention(nn.Module):
    def __init__(self) -> None:
        super().__init__()

    def forward(self, q, k, v) -> torch.Tensor:
        return F.scaled_dot_product_attention(q, k, v)


class SelfBlock(nn.Module):
    def __init__(self, embed_dim: int, num_heads: int, bias: bool = True) -> None:
        super().__init__()

        self.embed_dim = embed_dim  # desc dim
        self.num_heads = num_heads
        self.head_dim = self.embed_dim // self.num_heads

        self.Wqkv = nn.Linear(embed_dim, 3 * embed_dim, bias=bias)
        self.inner_attn = Attention()
        self.out_proj = nn.Linear(embed_dim, embed_dim, bias=bias)
        self.ffn = nn.Sequential(
            nn.Linear(2 * embed_dim, 2 * embed_dim),
            nn.LayerNorm(2 * embed_dim, elementwise_affine=True),
            nn.GELU(),
            nn.Linear(2 * embed_dim, embed_dim),
        )

    def forward(self, desc: torch.Tensor, encoding: torch.Tensor) -> torch.Tensor:

        batch, num_kp, _ = desc.shape
        qkv = self.Wqkv(desc)
        # qkv.shape = [batch, num_kp, 3 * desc_dim]
        qkv = qkv.reshape(batch, -1, self.num_heads, self.head_dim, 3)
        # qkv.shape = [batch, num_kp, haed_num, head_dim, 3]
        qkv = qkv.transpose(1, 2)
        # qkv.shape = [batch, haed_num, num_kp, head_dim, 3]

        q, k, v = qkv[..., 0], qkv[..., 1], qkv[..., 2]

        # encoding.shape == (2, batch, 1, num_kp, desc_dim // head_num)
        q = self.apply_cached_rotary_emb(encoding, q)
        k = self.apply_cached_rotary_emb(encoding, k)

        context = self.inner_attn(q, k, v)
        # context.shape == [batch, 4, num_kp, desc_dim // haed_num]

        context = context.transpose(1, 2)
        context = context.reshape(batch, -1, self.embed_dim)
        message = self.out_proj(context)

        return desc + self.ffn(torch.cat([desc, message], -1))

    @staticmethod
    def rotate_half(t: torch.Tensor) -> torch.Tensor:

        batch, head_num, num_kp, head_dim = t.shape

        t = t.reshape(batch, head_num, num_kp, head_dim // 2, 2)
        t = torch.stack((-t[..., 1], t[..., 0]), dim=-1)
        t = t.reshape(batch, head_num, num_kp, head_dim)

        return t

    @staticmethod
    def apply_cached_rotary_emb(freqs: torch.Tensor, t: torch.Tensor) -> torch.Tensor:

        # t.shape = [batch, haed_num, num_kp, head_dim]
        # freqs.shape == [2, batch, 1, num_kp, desc_dim // head_num]

        return (t * freqs[0]) + (SelfBlock.rotate_half(t) * freqs[1])


class CrossBlock(nn.Module):
    def __init__(self, embed_dim: int, num_heads: int, bias: bool = True) -> None:

        super().__init__()

        # embed_dim == desc_dim

        self.embed_dim = embed_dim
        self.num_heads = num_heads
        self.head_dim = self.embed_dim // self.num_heads

        self.to_qk = nn.Linear(self.embed_dim, self.embed_dim, bias=bias)
        self.to_v = nn.Linear(self.embed_dim, self.embed_dim, bias=bias)
        self.to_out = nn.Linear(self.embed_dim, self.embed_dim, bias=bias)
        self.atten = Attention()

        self.ffn = nn.Sequential(
            nn.Linear(2 * self.embed_dim, 2 * self.embed_dim),
            nn.LayerNorm(2 * self.embed_dim, elementwise_affine=True),
            nn.GELU(),
            nn.Linear(2 * self.embed_dim, self.embed_dim),
        )

    def forward(
        self, desc0: torch.Tensor, desc1: torch.Tensor
    ) -> Tuple[torch.Tensor, torch.Tensor]:

        batch_sz, num_kp0, desc_dim = desc0.shape
        # desc.shape == [batch, num_kp, desc_dim]

        qk0, qk1 = map(self.to_qk, (desc0, desc1))
        v0, v1 = map(self.to_v, (desc0, desc1))
        # v.shape == qk.shape == [batch, num_kp, desc_dim]

        qk0, qk1, v0, v1 = map(
            lambda t: t.reshape(batch_sz, -1, self.num_heads, self.head_dim).transpose(
                1, 2
            ),
            (qk0, qk1, v0, v1),
        )
        # v.shape == qk.shape == [batch, head_num, num_kp, head_dim]

        m0 = self.atten(qk0, qk1, v1)
        m1 = self.atten(qk1, qk0, v0)
        # m.shape  == [batch, head_num, num_kp, head_dim]

        m0, m1 = map(
            lambda t: t.transpose(1, 2).reshape(batch_sz, -1, self.embed_dim), (m0, m1)
        )
        m0, m1 = map(self.to_out, (m0, m1))
        # m.shape  == [batch, num_kp, desc_dim]

        desc0 = desc0 + self.ffn(torch.cat([desc0, m0], -1))
        desc1 = desc1 + self.ffn(torch.cat([desc1, m1], -1))

        return desc0, desc1


class TransformerLayer(nn.Module):
    def __init__(self, embed_dim: int, num_heads: int):
        super().__init__()
        self.self_attn = SelfBlock(embed_dim, num_heads)
        self.cross_attn = CrossBlock(embed_dim, num_heads)

    def forward(
        self,
        desc0: torch.Tensor,
        desc1: torch.Tensor,
        encoding0: torch.Tensor,
        encoding1: torch.Tensor,
    ) -> Tuple[torch.Tensor]:
        desc0 = self.self_attn(desc0, encoding0)
        desc1 = self.self_attn(desc1, encoding1)
        return self.cross_attn(desc0, desc1)


class LightGlue(nn.Module):
    default_conf = {
        "name": "lightglue",  # just for interfacing
        "input_dim": 256,  # input descriptor dimension (autoselected from weights)
        "descriptor_dim": 256,
        "add_scale_ori": False,
        "n_layers": 9,
        "num_heads": 4,
        "depth_confidence": 0.95,  # early stopping, disable with -1
        "width_confidence": 0.99,  # point pruning, disable with -1
        "filter_threshold": 0.1,  # match threshold
        "weights": None,
    }

    required_data_keys = ["image0", "image1"]

    version = "v0.1_arxiv"
    url = "https://github.com/cvg/LightGlue/releases/download/{}/{}_lightglue.pth"

    features = {
        "superpoint": ("superpoint_lightglue", 256),
    }

    def __init__(self, features="superpoint", **conf) -> None:
        super().__init__()

        self.conf = {**self.default_conf, **conf}

        if features is not None:
            assert features in self.features
            self.conf["weights"], self.conf["input_dim"] = self.features[features]

        self.conf = conf = SimpleNamespace(**self.conf)

        if self.conf.input_dim != self.conf.descriptor_dim:
            self.input_proj = nn.Linear(conf.input_dim, conf.descriptor_dim, bias=True)
        else:
            self.input_proj = nn.Identity()

        head_dim = conf.descriptor_dim // conf.num_heads
        self.posenc = LearnableFourierPositionalEncoding(
            2 + 2 * self.conf.add_scale_ori, head_dim, head_dim
        )

        h, n, d = conf.num_heads, conf.n_layers, conf.descriptor_dim

        self.transformers = nn.ModuleList([TransformerLayer(d, h) for _ in range(n)])

        self.log_assignment = nn.ModuleList([MatchAssignment(d) for _ in range(n)])
        self.token_confidence = nn.ModuleList(
            [TokenConfidence(d) for _ in range(n - 1)]
        )
        self.register_buffer(
            "confidence_thresholds",
            torch.Tensor(
                [self.confidence_threshold(i) for i in range(self.conf.n_layers)]
            ),
        )

        state_dict = None
        if features is not None:
            fname = f"{conf.weights}_{self.version.replace('.', '-')}.pth"
            state_dict = torch.hub.load_state_dict_from_url(
                self.url.format(self.version, features), file_name=fname
            )
            self.load_state_dict(state_dict, strict=False)
        elif conf.weights is not None:
            path = Path(__file__).parent
            path = path / "weights/{}.pth".format(self.conf.weights)
            state_dict = torch.load(str(path), map_location="cpu")

        if state_dict is not None:
            # rename old state dict entries
            for i in range(self.conf.n_layers):
                pattern = f"self_attn.{i}", f"transformers.{i}.self_attn"
                state_dict = {k.replace(*pattern): v for k, v in state_dict.items()}
                pattern = f"cross_attn.{i}", f"transformers.{i}.cross_attn"
                state_dict = {k.replace(*pattern): v for k, v in state_dict.items()}
            self.load_state_dict(state_dict, strict=False)

        # static lengths LightGlue is compiled for (only used with torch.compile)
        # self.static_lengths = None

        print(f"LightGlue Model Loaded!")

    def forward(
        self,
        kpts0: torch.Tensor,
        kpts1: torch.Tensor,
        desc0: torch.Tensor,
        desc1: torch.Tensor,
    ):
        """
        Match keypoints and descriptors between two images

        Input:
                kpts0: [B x M x 2] normalized
                kpts1: [B x M x 2] normalized
                desc0: [B x M x D]
                desc1: [B x M x D]
        Output:
            scores: [B x M x N]
        """

        b, m, _ = kpts0.shape
        b, n, _ = kpts1.shape

        desc0 = self.input_proj(desc0)
        desc1 = self.input_proj(desc1)

        encoding0 = self.posenc(kpts0)
        encoding1 = self.posenc(kpts1)

        i = 0
        # we dont use early stoping and pruning
        for i in range(self.conf.n_layers):
            # if desc0.shape[1] == 0 or desc1.shape[1] == 0:  # no keypoints
            #     break
            desc0, desc1 = self.transformers[i](desc0, desc1, encoding0, encoding1)

        assert i == (self.conf.n_layers - 1)
        scores = self.log_assignment[-1](desc0, desc1)

        # matches, mscores = filter_matches(scores, self.conf.filter_threshold)
        max0, max1 = filter_matches(scores)

        return max0.indices, max0.values, max1.indices, max1.values

    def confidence_threshold(self, layer_index: int) -> float:
        """scaled confidence threshold"""
        threshold = 0.8 + 0.1 * np.exp(-4.0 * layer_index / self.conf.n_layers)
        return np.clip(threshold, 0, 1)
