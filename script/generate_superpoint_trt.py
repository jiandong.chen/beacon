import sys
import torch
import torch.nn as nn
import tensorrt as trt
from beacon_script_config import config


sys.path.append(config["BaseDir"])

from thirdparty.SuperPointPretrainedNetwork.demo_superpoint import SuperPointNet


class SPNonnx(nn.Module):
    def __init__(self):
        super().__init__()

        self.net = SuperPointNet()
        self.net.load_state_dict(torch.load(config["SuperPointWeightPath"]))
        self.net = self.net.eval().cuda()

    def forward(self, x):
        semi, desc = self.net(x)
        # semi part
        # net output -----> score map(heatmap)

        scores = torch.nn.functional.softmax(semi, 1)[:, :-1]
        b, _, h, w = scores.shape
        scores = scores.permute(0, 2, 3, 1).reshape(b, h, w, 8, 8)
        scores = scores.permute(0, 1, 3, 2, 4).reshape(b, h * 8, w * 8)

        # desc [1, 256, h/8, w/8] ---> [1, h/8, w/8, 256]
        desc = torch.permute(desc, (0, 2, 3, 1))

        return scores, desc


def write_onnx_model():
    model = SPNonnx().cuda().eval()
    input_names = config["SuperPointInputNameOnnx"]
    output_names = config["SuperPointOutputNameOnnx"]
    dynamic_shape = config["SuperPointDynamicShapeOnnx"]

    img_height = 360
    img_width = 640
    x = torch.rand((1, 1, img_height, img_width), dtype=torch.float32, device="cuda")

    torch.onnx.export(
        model,
        x,
        config["SuperPointOnnxPath"],
        opset_version=17,
        verbose=False,
        input_names=input_names,
        output_names=output_names,
        dynamic_axes=dynamic_shape,
    )


def write_trt_model():

    write_onnx_model()

    logger = trt.Logger(trt.Logger.INFO)
    builder = trt.Builder(logger)
    network = builder.create_network(
        1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH)
    )

    parser = trt.OnnxParser(network, logger)
    success = parser.parse_from_file(config["SuperPointOnnxPath"])

    builder_config = builder.create_builder_config()
    builder_config.profiling_verbosity = trt.ProfilingVerbosity.DETAILED
    builder_config.builder_optimization_level = 5
    builder_config.set_memory_pool_limit(trt.MemoryPoolType.WORKSPACE, 1 << 32)  # 4 GiB
    # config.set_flag(trt.BuilderFlag.GPU_FALLBACK)
    builder_config.set_flag(trt.BuilderFlag.FP16)
    #  config.set_flag(trt.BuilderFlag.INT8)

    profile = builder.create_optimization_profile()
    profile.set_shape(
        config["SuperPointInputName"],
        config["SuperPointInputMinShape"],
        config["SuperPointInputOptShape"],
        config["SuperPointInputMaxShape"],
    )
    builder_config.add_optimization_profile(profile)

    serialized_engine = builder.build_serialized_network(network, builder_config)

    with open(config["SuperPointEnginePath"], "wb") as f:
        f.write(serialized_engine)


if __name__ == "__main__":

    write_trt_model()
