import torch
import torch.nn as nn
import tensorrt as trt

from lightglue_net import LightGlue
from beacon_script_config import config
from optimize_lightglue_onnx import optimize_lightglue_onnx


def write_onnx_model():
    model = LightGlue().cuda().eval()

    kpsc0 = torch.rand((1, 1000, 2), device="cuda")
    kpsc1 = torch.rand((1, 1000, 2), device="cuda")
    desc0 = torch.rand((1, 1000, 256), device="cuda")
    desc1 = torch.rand((1, 1000, 256), device="cuda")
    x = (kpsc0, kpsc1, desc0, desc1)

    input_names = config["LightGlueInputNameOnnx"]
    output_names = config["LightGlueOutputNameOnnx"]
    dynamic_shape = config["LightGlueDynamicShapeOnnx"]

    torch.onnx.export(
        model,
        x,
        config["LightGlueOnnxPath"],
        opset_version=17,
        verbose=False,
        input_names=input_names,
        output_names=output_names,
        dynamic_axes=dynamic_shape,
    )


def write_trt_model():

    write_onnx_model()

    logger = trt.Logger(trt.Logger.INFO)
    builder = trt.Builder(logger)
    network = builder.create_network(
        1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH)
    )

    parser = trt.OnnxParser(network, logger)
    success = parser.parse_from_file(config["LightGlueOnnxPath"])

    builder_config = builder.create_builder_config()
    builder_config.profiling_verbosity = trt.ProfilingVerbosity.DETAILED
    builder_config.builder_optimization_level = 5
    builder_config.set_memory_pool_limit(trt.MemoryPoolType.WORKSPACE, 1 << 33)  # 8 GiB
    # config.set_flag(trt.BuilderFlag.GPU_FALLBACK)
    builder_config.set_flag(trt.BuilderFlag.FP16)
    #  config.set_flag(trt.BuilderFlag.INT8)

    profile = builder.create_optimization_profile()
    profile.set_shape(
        config["LightGlueKeypointScoreName0"],
        config["LightGlueKeypointScoreMinShape"],
        config["LightGlueKeypointScoreOptShape"],
        config["LightGlueKeypointScoreMaxShape"],
    )
    profile.set_shape(
        config["LightGlueKeypointScoreName1"],
        config["LightGlueKeypointScoreMinShape"],
        config["LightGlueKeypointScoreOptShape"],
        config["LightGlueKeypointScoreMaxShape"],
    )
    profile.set_shape(
        config["LightGlueDescriptorName0"],
        config["LightGlueDescriptorMinShape"],
        config["LightGlueDescriptorOptShape"],
        config["LightGlueDescriptorMaxShape"],
    )
    profile.set_shape(
        config["LightGlueDescriptorName1"],
        config["LightGlueDescriptorMinShape"],
        config["LightGlueDescriptorOptShape"],
        config["LightGlueDescriptorMaxShape"],
    )
    builder_config.add_optimization_profile(profile)

    serialized_engine = builder.build_serialized_network(network, builder_config)

    with open(config["LightGlueEnginePath"], "wb") as f:
        f.write(serialized_engine)


if __name__ == "__main__":

    write_trt_model()
