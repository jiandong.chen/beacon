import sys
import torch
import torch.nn as nn
import tensorrt as trt
from beacon_script_config import config
from superglue_net import getModel


class SGonnx(nn.Module):

    def __init__(self):
        super().__init__()
        self.net = getModel()

    def forward(self, kpsc0, kpsc1, descriptors0, descriptors1):

        # kpsc [1, num_kp, 3] --- > [1, 3, num_kp]
        kpsc0 = torch.permute(kpsc0, (0, 2, 1))
        kpsc1 = torch.permute(kpsc1, (0, 2, 1))

        # desc [1,num_kp, 256] ---> [1, 256, num_kp]
        descriptors0 = torch.permute(descriptors0, (0, 2, 1))
        descriptors1 = torch.permute(descriptors1, (0, 2, 1))

        mdesc0, mdesc1 = self.net(kpsc0, kpsc1, descriptors0, descriptors1)

        # 256 == desc_dim
        scores = torch.einsum("bdn,bdm->bnm", mdesc0, mdesc1) / (256**0.5)

        return scores

    def log_optimal_transport(self, scores, num_iter):
        b, m, n = scores.shape
        alpha = float(self.net.bin_score)

        bins0 = torch.full((b, m, 1), alpha, dtype=torch.float, device="cuda")
        bins1 = torch.full((b, 1, n), alpha, dtype=torch.float, device="cuda")
        alpha = torch.full((b, 1, 1), alpha, dtype=torch.float, device="cuda")

        couplings = torch.cat(
            [torch.cat([scores, bins0], -1), torch.cat([bins1, alpha], -1)], 1
        )

        norm = torch.tensor(-(m + n), dtype=torch.float, device="cuda").log()
        norm_m = norm.expand(m)
        norm_n = norm.expand(n)
        ms_log_norm = (
            torch.tensor(m, dtype=torch.float, device="cuda").log()[None] + norm
        )
        ns_log_norm = (
            torch.tensor(n, dtype=torch.float, device="cuda").log()[None] + norm
        )

        log_mu = torch.cat([norm_m, ns_log_norm]).expand(b, -1)
        log_nu = torch.cat([norm_n, ms_log_norm]).expand(b, -1)

        # log_sinkhorn_iterations
        u = torch.zeros_like(log_mu, dtype=torch.float, device="cuda")
        v = torch.zeros_like(log_nu, dtype=torch.float, device="cuda")
        for _ in range(num_iter):
            u = log_mu - torch.logsumexp(couplings + v.unsqueeze(1), dim=2)
            v = log_nu - torch.logsumexp(couplings + u.unsqueeze(2), dim=1)

        return couplings + u.unsqueeze(2) + v.unsqueeze(1)


def write_onnx_model():
    model = SGonnx().cuda().eval()

    kpsc0 = torch.rand((1, 1000, 3), device="cuda")
    kpsc1 = torch.rand((1, 1000, 3), device="cuda")
    desc0 = torch.rand((1, 1000, 256), device="cuda")
    desc1 = torch.rand((1, 1000, 256), device="cuda")
    x = (kpsc0, kpsc1, desc0, desc1)

    input_names = config["SuperGlueInputNameOnnx"]
    output_names = config["SuperGlueOutputNameOnnx"]
    dynamic_shape = config["SuperGlueDynamicShapeOnnx"]

    torch.onnx.export(
        model,
        x,
        config["SuperGlueOnnxPath"],
        opset_version=17,
        verbose=False,
        input_names=input_names,
        output_names=output_names,
        dynamic_axes=dynamic_shape,
    )


def write_trt_model():

    write_onnx_model()

    logger = trt.Logger(trt.Logger.INFO)
    builder = trt.Builder(logger)
    network = builder.create_network(
        1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH)
    )

    parser = trt.OnnxParser(network, logger)
    success = parser.parse_from_file(config["SuperGlueOnnxPath"])

    builder_config = builder.create_builder_config()
    builder_config.profiling_verbosity = trt.ProfilingVerbosity.DETAILED
    builder_config.builder_optimization_level = 5
    builder_config.set_memory_pool_limit(trt.MemoryPoolType.WORKSPACE, 1 << 33)  # 8 GiB
    # config.set_flag(trt.BuilderFlag.GPU_FALLBACK)
    builder_config.set_flag(trt.BuilderFlag.FP16)
    #  config.set_flag(trt.BuilderFlag.INT8)

    profile = builder.create_optimization_profile()
    profile.set_shape(
        config["SuperGlueKeypointScoreName0"],
        config["SuperGlueKeypointScoreMinShape"],
        config["SuperGlueKeypointScoreOptShape"],
        config["SuperGlueKeypointScoreMaxShape"],
    )
    profile.set_shape(
        config["SuperGlueKeypointScoreName1"],
        config["SuperGlueKeypointScoreMinShape"],
        config["SuperGlueKeypointScoreOptShape"],
        config["SuperGlueKeypointScoreMaxShape"],
    )
    profile.set_shape(
        config["SuperGlueDescriptorName0"],
        config["SuperGlueDescriptorMinShape"],
        config["SuperGlueDescriptorOptShape"],
        config["SuperGlueDescriptorMaxShape"],
    )
    profile.set_shape(
        config["SuperGlueDescriptorName1"],
        config["SuperGlueDescriptorMinShape"],
        config["SuperGlueDescriptorOptShape"],
        config["SuperGlueDescriptorMaxShape"],
    )
    builder_config.add_optimization_profile(profile)

    serialized_engine = builder.build_serialized_network(network, builder_config)

    with open(config["SuperGlueEnginePath"], "wb") as f:
        f.write(serialized_engine)


if __name__ == "__main__":

    write_trt_model()
