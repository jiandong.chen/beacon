#include "beacon/core/duration.h"

#include "gtest/gtest.h"

TEST(DurationTest, BasicFunctionality) {
    using namespace beacon;

    double t1 = 0.0, t2 = 3.14, t3 = 314.314;
    auto ts1 = Timestamp{t1}, ts2 = Timestamp{t2}, ts3 = Timestamp{t3};

    // check correct conversion and interfaces
    auto run_test = [](auto t1, auto t2) {
        auto d1 = Duration(Timestamp{t1}, Timestamp{t2});
        double dt = t2 > t1 ? t2 - t1 : t1 - t2;

        // EXPECT_EQ(d1.getData().first, Timestamp{dt}.getData().first);
        // EXPECT_EQ(d1.getData().second, Timestamp{dt}.getData().second);
        EXPECT_FLOAT_EQ(d1.getSec(), dt);
    };

    run_test(t1, t2);
    run_test(t2, t1);
    run_test(t2, t2);
    run_test(t3, t2);

    // check comparison
    auto d1 = ts1 - ts2, d2 = ts3 - ts2;
    EXPECT_TRUE(d1 == d1);
    EXPECT_TRUE(d1 != d2);
    EXPECT_TRUE(d2 > d1);
    EXPECT_TRUE(d2 >= d2);
    EXPECT_TRUE(d1 < d2);
    EXPECT_TRUE(d1 <= d2);
}
