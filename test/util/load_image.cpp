#include "load_image.h"

#include <filesystem>

#include "beacon_module/parameter_handler/parameter_handler.h"

namespace beacon::test {

TestImage ::TestImage(std::string_view image_name) {
    auto path =
        std::filesystem::path(__FILE__).parent_path().parent_path().append(
            "asset");
    path = path.append(image_name);

    if (!std::filesystem::exists(path)) {
        std::cerr << path << " NOT EXIST!!!!!!!\n";
        return;
    }
    image = cv::imread(path);
}

CanalTestImagePair::CanalTestImagePair() {
    auto path = std::filesystem::path(__FILE__)
                    .parent_path()  // util
                    .parent_path()  // test
                    .parent_path()  // beacon
                    .append("config");
    path = path.append("base.yaml");
    ParameterHandler ph{path.string()};
    m_left_camera = ph.getCameras().at(0);
    m_right_camera = ph.getCameras().at(1);

    m_T_bl = m_left_camera->getTransformBC();
    m_T_br = m_right_camera->getTransformBC();

    // load image

    TestImage img1{"pohang_canal_test_1_left.png"};
    m_img_left = ImageData::create();
    m_img_left->image = img1.image;
    m_img_left->camera = m_left_camera;

    TestImage img2{"pohang_canal_test_1_right.png"};
    m_img_right = ImageData::create();
    m_img_right->image = img2.image;
    m_img_right->camera = m_right_camera;

    TestImage img3{"pohang_canal_test_2_front.png"};
    m_img_front = ImageData::create();
    m_img_front->image = img3.image;
    m_img_front->camera = m_left_camera;

    TestImage img4{"pohang_canal_test_2_back.png"};
    m_img_back = ImageData::create();
    m_img_back->image = img4.image;
    m_img_back->camera = m_left_camera;
}

}  // namespace beacon::test
