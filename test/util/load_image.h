#include <memory>
#include <opencv4/opencv2/opencv.hpp>
#include <string_view>

#include "../include/beacon/datatype/image_data.h"
#include "../include/beacon/sensor/camera.h"

namespace beacon::test {

struct TestImage {
    TestImage(std::string_view image_name);

    cv::Mat image;
};  // struct TestImage

struct CanalTestImagePair {
    CanalTestImagePair();

    ImageData::Ptr m_img_left{};
    ImageData::Ptr m_img_right{};
    ImageData::Ptr m_img_front{};
    ImageData::Ptr m_img_back{};
    std::shared_ptr<Camera> m_left_camera{};
    std::shared_ptr<Camera> m_right_camera{};
    Transform m_T_bl;
    Transform m_T_br;

};  // class CanalTestImagePair

}  // namespace beacon::test
