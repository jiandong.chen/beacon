#include "beacon/core/transform.h"

#include "gtest/gtest.h"

TEST(TransformTest, BasicFunctionality) {
    using namespace beacon;

    // Test Constructor

    Transform transform;
    auto t = transform.getTranslation();
    auto r = transform.getRotation();

    EXPECT_FLOAT_EQ(t.x(), 0.0);
    EXPECT_FLOAT_EQ(t.y(), 0.0);
    EXPECT_FLOAT_EQ(t.z(), 0.0);

    EXPECT_FLOAT_EQ(r.w(), 1.0);
    EXPECT_FLOAT_EQ(r.x(), 0.0);
    EXPECT_FLOAT_EQ(r.y(), 0.0);
    EXPECT_FLOAT_EQ(r.z(), 0.0);

    Eigen::Vector3d v{1.0, 2.0, 3.0};
    Eigen::Quaternion q{1.0, 2.0, 3.0, 4.0};
    auto norm_q = q.normalized();
    transform = Transform(v, q);
    t = transform.getTranslation();
    r = transform.getRotation();

    EXPECT_FLOAT_EQ(t.x(), 1.0);
    EXPECT_FLOAT_EQ(t.y(), 2.0);
    EXPECT_FLOAT_EQ(t.z(), 3.0);

    EXPECT_FLOAT_EQ(r.w(), norm_q.w());
    EXPECT_FLOAT_EQ(r.x(), norm_q.x());
    EXPECT_FLOAT_EQ(r.y(), norm_q.y());
    EXPECT_FLOAT_EQ(r.z(), norm_q.z());

    transform = Transform(v);
    t = transform.getTranslation();
    r = transform.getRotation();

    EXPECT_FLOAT_EQ(t.x(), 1.0);
    EXPECT_FLOAT_EQ(t.y(), 2.0);
    EXPECT_FLOAT_EQ(t.z(), 3.0);

    EXPECT_FLOAT_EQ(r.w(), 1.0);
    EXPECT_FLOAT_EQ(r.x(), 0.0);
    EXPECT_FLOAT_EQ(r.y(), 0.0);
    EXPECT_FLOAT_EQ(r.z(), 0.0);

    transform = Transform(q);
    t = transform.getTranslation();
    r = transform.getRotation();

    EXPECT_FLOAT_EQ(t.x(), 0.0);
    EXPECT_FLOAT_EQ(t.y(), 0.0);
    EXPECT_FLOAT_EQ(t.z(), 0.0);

    EXPECT_FLOAT_EQ(r.w(), norm_q.w());
    EXPECT_FLOAT_EQ(r.x(), norm_q.x());
    EXPECT_FLOAT_EQ(r.y(), norm_q.y());
    EXPECT_FLOAT_EQ(r.z(), norm_q.z());

    transform = Transform{1.0, 2.0, 3.0};
    t = transform.getTranslation();
    r = transform.getRotation();

    EXPECT_FLOAT_EQ(t.x(), 1.0);
    EXPECT_FLOAT_EQ(t.y(), 2.0);
    EXPECT_FLOAT_EQ(t.z(), 3.0);

    EXPECT_FLOAT_EQ(r.w(), 1.0);
    EXPECT_FLOAT_EQ(r.x(), 0.0);
    EXPECT_FLOAT_EQ(r.y(), 0.0);
    EXPECT_FLOAT_EQ(r.z(), 0.0);

    transform = Transform{1.0, 2.0, 3.0, 4.0};
    t = transform.getTranslation();
    r = transform.getRotation();

    EXPECT_FLOAT_EQ(t.x(), 0.0);
    EXPECT_FLOAT_EQ(t.y(), 0.0);
    EXPECT_FLOAT_EQ(t.z(), 0.0);

    EXPECT_FLOAT_EQ(r.w(), norm_q.w());
    EXPECT_FLOAT_EQ(r.x(), norm_q.x());
    EXPECT_FLOAT_EQ(r.y(), norm_q.y());
    EXPECT_FLOAT_EQ(r.z(), norm_q.z());

    transform = Transform{1.0, 2.0, 3.0, 1.0, 2.0, 3.0, 4.0};
    t = transform.getTranslation();
    r = transform.getRotation();

    EXPECT_FLOAT_EQ(t.x(), 1.0);
    EXPECT_FLOAT_EQ(t.y(), 2.0);
    EXPECT_FLOAT_EQ(t.z(), 3.0);

    EXPECT_FLOAT_EQ(r.w(), norm_q.w());
    EXPECT_FLOAT_EQ(r.x(), norm_q.x());
    EXPECT_FLOAT_EQ(r.y(), norm_q.y());
    EXPECT_FLOAT_EQ(r.z(), norm_q.z());

    // Test Constructor End

    transform.clear();
    t = transform.getTranslation();
    r = transform.getRotation();
    EXPECT_FLOAT_EQ(t.x(), 0.0);
    EXPECT_FLOAT_EQ(t.y(), 0.0);
    EXPECT_FLOAT_EQ(t.z(), 0.0);

    EXPECT_FLOAT_EQ(r.w(), 1.0);
    EXPECT_FLOAT_EQ(r.x(), 0.0);
    EXPECT_FLOAT_EQ(r.y(), 0.0);
    EXPECT_FLOAT_EQ(r.z(), 0.0);

    // Test transform setter
    transform.clear();
    transform.setRotation(q);
    t = transform.getTranslation();
    r = transform.getRotation();
    EXPECT_FLOAT_EQ(t.x(), 0.0);
    EXPECT_FLOAT_EQ(t.y(), 0.0);
    EXPECT_FLOAT_EQ(t.z(), 0.0);

    EXPECT_FLOAT_EQ(r.w(), norm_q.w());
    EXPECT_FLOAT_EQ(r.x(), norm_q.x());
    EXPECT_FLOAT_EQ(r.y(), norm_q.y());
    EXPECT_FLOAT_EQ(r.z(), norm_q.z());

    transform.setTranslation(v);
    t = transform.getTranslation();
    r = transform.getRotation();
    EXPECT_FLOAT_EQ(t.x(), 1.0);
    EXPECT_FLOAT_EQ(t.y(), 2.0);
    EXPECT_FLOAT_EQ(t.z(), 3.0);

    EXPECT_FLOAT_EQ(r.w(), norm_q.w());
    EXPECT_FLOAT_EQ(r.x(), norm_q.x());
    EXPECT_FLOAT_EQ(r.y(), norm_q.y());
    EXPECT_FLOAT_EQ(r.z(), norm_q.z());

    // Test transform setter End

    auto inv_q = q.inverse().normalized();
    auto inv_v =
        Eigen::Vector3d(-q.normalized().toRotationMatrix().transpose() * v);
    transform = Transform{v, q};
    transform = transform.getInverse();

    t = transform.getTranslation();
    r = transform.getRotation();
    EXPECT_FLOAT_EQ(t.x(), inv_v.x());
    EXPECT_FLOAT_EQ(t.y(), inv_v.y());
    EXPECT_FLOAT_EQ(t.z(), inv_v.z());

    EXPECT_FLOAT_EQ(r.w(), inv_q.w());
    EXPECT_FLOAT_EQ(r.x(), inv_q.x());
    EXPECT_FLOAT_EQ(r.y(), inv_q.y());
    EXPECT_FLOAT_EQ(r.z(), inv_q.z());
}
