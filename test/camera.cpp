#include <Eigen/Eigen>
#include <opencv4/opencv2/core/eigen.hpp>

#include "./util/load_image.h"
#include "gtest/gtest.h"

using namespace beacon;
using namespace beacon::test;

class CameraTest : public testing::Test, public CanalTestImagePair {};

TEST_F(CameraTest, MonocularCameraTest) {
    auto p_lb_cv = m_left_camera->getBaseProjectMat();
    Eigen::Matrix<double, 3, 4> p_lb;
    cv::cv2eigen(p_lb_cv, p_lb);
    Eigen::Quaterniond q_bl{0.4994824732081517, 0.50451874772890937,
                            0.49271166335600353, 0.50320304941532457};
    Eigen::Matrix3d K_l;
    K_l << 1814.5061420730431, 0, 1026.1977130691339, 0, 1814.5061420730431,
        524.7984412358129, 0, 0, 1;
    Eigen::Matrix3d R_bl = q_bl.normalized().toRotationMatrix();
    Eigen::Matrix3d R_lb = R_bl.transpose();
    Eigen::Vector3d t_bl{5.0362549728344614, -0.44101021275849878,
                         -1.771000242852925};
    Eigen::Vector3d t_lb = -R_lb * t_bl;
    double accept_eps = 0.0001;
    auto R_diff = ((K_l * R_lb) - (p_lb.block<3, 3>(0, 0))).norm();
    auto t_diff = ((K_l * t_lb) - (p_lb.block<3, 1>(0, 3))).norm();
    EXPECT_NEAR(R_diff, 0.0, accept_eps);
    EXPECT_NEAR(t_diff, 0.0, accept_eps);
}
