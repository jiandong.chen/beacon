#include "beacon/core/timestamp.h"

#include "gtest/gtest.h"

TEST(TimestampTest, BasicFunctionality) {
    using beacon::Timestamp;
    Timestamp t;
    EXPECT_EQ(t.getData().first, 0);
    EXPECT_EQ(t.getData().second, 0);
    EXPECT_FLOAT_EQ(t.getSec(), 0.0);

    double test_time = 314.314;
    t = Timestamp{test_time};
    EXPECT_EQ(t.getData().first, 314);
    EXPECT_EQ(t.getData().second, 314'000'000);
    EXPECT_FLOAT_EQ(t.getSec(), test_time);

    uint64_t i_test_time = 3'321'123'321;
    t = Timestamp(i_test_time, Timestamp::nanosec);
    EXPECT_EQ(t.getData().first, 3);
    EXPECT_EQ(t.getData().second, 321'123'321);
    EXPECT_FLOAT_EQ(t.getSec(), i_test_time / 1e9);

    // check comparison
    Timestamp t1{1.3}, t2{233.123};
    EXPECT_TRUE(t1 == t1);
    EXPECT_TRUE(t1 != t2);
    EXPECT_TRUE(t2 > t1);
    EXPECT_TRUE(t2 >= t1);
    EXPECT_TRUE(t1 < t2);
    EXPECT_TRUE(t1 <= t2);
}

TEST(TimestampTest, ThrowLargeSecAndNegative) {
    using beacon::Timestamp;
    auto t = Timestamp{};

    EXPECT_THROW(
        {
            try {
                t = Timestamp{std::numeric_limits<double>::max()};
            } catch (std::runtime_error& e) {
                EXPECT_STREQ("Cannot store too large value as Timestamp!",
                             e.what());
                throw;
            }
        },
        std::runtime_error);

    EXPECT_THROW(
        {
            try {
                t = Timestamp{-123.321};
            } catch (std::runtime_error& e) {
                EXPECT_STREQ("Cannot set negative timestamp!", e.what());
                throw;
            }
        },
        std::runtime_error);
}
